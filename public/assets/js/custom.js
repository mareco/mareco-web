$(document).ready(function(){

    $('#modalDelete').on('shown.bs.modal', function(e) {
        e.stopPropagation();
        var id = $(e.relatedTarget).data('id');
        var parentTr = $(e.relatedTarget).closest('tr');
        var title = $(e.relatedTarget).data('title');
        var message = $(e.relatedTarget).data('message');
        var action = $(e.relatedTarget).data('url');
        var table_name = $(e.relatedTarget).data('table-name');
        var method = $(e.relatedTarget).data('method') == undefined ? 'DELETE' : $(e.relatedTarget).data('method');
        $('#modal_id_delete').val(id);
        $('.modal-header h4').text(title);
        $('.modal-body p').text(message);
        $('#modalDelete form').attr('action',action).on("submit", function(e){
            e.preventDefault();
            e.stopPropagation();
            var that = this;
            $.ajax({
                url: action,
                type: method,
                data: $(that).serialize() ,
                success: function(result) {
                    $("#modalDelete").modal('hide')
                    if (typeof table_name === "string") {
                        $(table_name).DataTable().ajax.reload();
                    }
                    $.gritter.add( 
                    {  
                        title: '<i class="entypo-check"></i> Process Success',  
                        // text: '<i class="entypo-check"></i> Request successfully process',  
                        sticky: false,  
                        time: ""
                    } 
                    ); 
                }
            });
        });
    }).on('hidden.bs.modal', function (e) {
        e.stopPropagation();
        $('#modal_id_delete').val('');
        $('.modal-header h4').text('');
        $('.modal-body p').text('');
        $('#modalDelete form').attr('action','/').off('submit');
    });
    $('#modalCancel').on('shown.bs.modal', function(e) {
        e.stopPropagation();
        var id = $(e.relatedTarget).data('id');
        var parentTr = $(e.relatedTarget).closest('tr');
        var title = $(e.relatedTarget).data('title');
        var message = $(e.relatedTarget).data('message');
        var action = $(e.relatedTarget).data('url');
        var table_name = $(e.relatedTarget).data('table-name');
        $('#modal_id_delete').val(id);
        $('.modal-header h4').text(title);
        $('.modal-body p').text(message);
    });

});


var deleteRow = function(parentTr, tableName, obj, callback) {
    var url = $(obj).data('url');
    var title = $(obj).data('title');
    var message = $(obj).data('message');
    $('#modal .modal-header h4').text(title);
    $('#modal .modal-body p').text(message);
    $('#modal form').on("submit", function(e){
        e.preventDefault();
        var that = this;
        $.ajax({
            url: url,
            type: 'DELETE',
            data: $(that).serialize() ,
            success: function(result) {
                $("#modal").modal('hide');
                if ('undefined' !== typeof callback || 'function' === typeof callback ) {
                    callback(obj);
                }
                $.gritter.add(
                { 
                    title: "Delete Success", 
                    text: "Request deleted successfully", 
                    sticky: false, 
                    time: "" 
                }
                );
                location.reload();
            }
        });
    });
    $("#modal").modal('show');
};

var rearrange = function(tableName) {

    $('#'+tableName+' tbody tr').each(function(index){
        $(this).find('input[name*='+tableName+'], select[name*='+tableName+']').each(function(){
            var regex = /(\[[\w\d]*\])(?=\[[\w\d]*\])/g;
            this.name = this.name.replace(regex,"["+index+"]"); 
        });
    });
};

