<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeOrdersTableField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function ($t) {
            $t->dropForeign('orders_shipping_method_id_foreign');
            $t->dropColumn('shipping_method_id');
            $t->dropColumn('photo');
            $t->dropColumn('account_name');
            $t->dropColumn('account_number');
            $t->dropColumn('total_shipment_fee');
            $t->integer('payment_type');
            $t->string('shipment_service', 50);
            $t->string('shipment_type', 50);
            $t->string('remark', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function ($t) {
            $t->dropColumn('payment_type');
            $t->dropColumn('shipment_service');
            $t->dropColumn('shipment_type');
            $t->dropColumn('remark');
        });
    }
}
