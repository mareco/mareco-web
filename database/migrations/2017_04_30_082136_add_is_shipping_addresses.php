<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsShippingAddresses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('addresses', function ($t) {
            $t->boolean('is_shipping_address')->nullable();
            $t->boolean('is_billing_address')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('addresses', function ($t) {
            $t->dropColumn('is_billing_address');
            $t->dropColumn('is_shipping_address');
        });
    }
}
