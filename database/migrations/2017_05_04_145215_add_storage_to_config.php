<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStorageToConfig extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('configs', function ($table) {
            $table->string('storage_address')->nullable();
            $table->string('storage_lat')->nullable();
            $table->string('storage_lng')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('configs', function (Blueprint $table) {
            $table->dropColumn('storage_address');
            $table->dropColumn('storage_lat');
            $table->dropColumn('storage_lng');
        });
    }
}
