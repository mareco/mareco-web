<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('created_by')->unsigned();
            $table->integer('sub_category_id')->unsigned();
            $table->string('photo', 150)->default('')->nullable();
            $table->string('product_image', 150)->default('')->nullable();
            $table->string('product_image_second', 150)->default('')->nullable();
            $table->string('product_image_third', 150)->default('')->nullable();
            $table->string('product_image_fourth', 150)->default('')->nullable();
            $table->string('product_image_fifth', 150)->default('')->nullable();
            $table->string('name', 75);
            $table->string('slug_url', 75);
            $table->string('code', 15)->nullable();
            $table->integer('sold')->nullable();
            $table->string('description', 350)->nullable();
            $table->nullableTimestamps();

            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('sub_category_id')->references('id')->on('sub_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
