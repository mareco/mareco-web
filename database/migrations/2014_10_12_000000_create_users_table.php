<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('address_id')->nullable();
            $table->string('role', 15)->default('user');
            $table->string('first_name');
            $table->string('last_name')->default('');
            $table->string('address')->default('');
            $table->string('email', 100)->default('');
            $table->string('password', 100);
            // $table->string('facebook_id', 250)->nullable();
            // $table->string('google_id', 250)->nullable();
            $table->string('phone', 25)->nullable();
            $table->string('photo')->default('');
            // $table->datetime('token_expire')->nullable();
            // $table->string('reset_code', 4)->nullable();
            $table->rememberToken()->nullable();
            $table->nullableTimestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
