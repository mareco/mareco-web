<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SelectColorPromotion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('promotions', function ($t){
            $t->string('font_color', 50)->nullable();
            $t->string('bg_color', 50)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('promotions', function ($t) {
            $t->dropColumn('font_color');
            $t->dropColumn('bg_color');
        });
    }
}
