<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCodAddressToOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function ($t) {
            $t->string('cod_address', 100)->nullable();
            $t->double('cod_lat')->nullable();
            $t->double('cod_lng')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function ($t) {
            $t->dropColumn('cod_address');
            $t->dropColumn('cod_lat');
            $t->dropColumn('cod_lng');
        });
    }
}
