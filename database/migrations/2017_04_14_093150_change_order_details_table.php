<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_details', function ($t) {
            $t->string('size', 50);
            $t->string('sku', 50);
            $t->string('color', 25);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_details', function ($t) {
            $t->dropColumn('size');
            $t->dropColumn('sku');
            $t->dropColumn('color');
        });
    }
}
