<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function ($t) {
            $t->dropForeign('orders_created_by_foreign');
            $t->dropColumn('created_by');
            $t->integer('user_id')->unsigned()->nullable();
            $t->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $t->double('shipment_fee')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function ($t) {
            $t->dropForeign('orders_user_id_foreign');
            $t->dropColumn('user_id');
            $t->dropColumn('shipment_fee');
        });
    }
}
