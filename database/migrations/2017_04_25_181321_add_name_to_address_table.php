<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNameToAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('addresses', function ($t) {
            $t->string('city_name', 100)->nullable();
            $t->string('province_name', 100)->nullable();
            $t->string('district_name', 100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('addresses', function ($t) {
            $t->dropColumn('province_name');
            $t->dropColumn('city_name');
            $t->dropColumn('district_name');
        });
    }
}
