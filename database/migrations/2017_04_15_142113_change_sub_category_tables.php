<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeSubCategoryTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function ($t) {
            $t->dropForeign('products_sub_category_id_foreign');
            $t->dropColumn('sub_category_id');
            $t->integer('category_id')->unsigned();
            $t->softDeletes();

            $t->foreign('category_id')->references('id')->on('categories');
        });
        Schema::dropIfExists('sub_categories');
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
