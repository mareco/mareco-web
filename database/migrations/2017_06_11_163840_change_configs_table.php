<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('about_us_lang', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('config_id');
            $table->foreign('config_id')->references('id')->on('configs');
            $table->string('lang');
            $table->string('title');
            $table->text('about_us_content');
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('about_us_lang');
    }
}
