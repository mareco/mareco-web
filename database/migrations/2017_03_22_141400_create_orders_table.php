<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shipping_method_id')->unsigned();
            $table->integer('address_id')->unsigned();
            $table->integer('created_by')->unsigned();
            $table->string('photo', 255)->default('');
            $table->string('order_number', 45)->nullable();
            $table->string('account_name', 100)->nullable();
            $table->integer('account_number')->length(30)->nullable();
            $table->integer('status');
            $table->double('total_shipment_fee');
            $table->double('grand_total')->default(0);
            $table->datetime('expire_date')->nullable();
            $table->nullableTimestamps();

            $table->foreign('shipping_method_id')->references('id')->on('shipping_methods')->onDelete('cascade');
            // $table->foreign('address_id')->references('id')->on('addresses')->onDelete('cascade');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}
