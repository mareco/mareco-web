<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRotateProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function ($t){
            $t->integer('rotate')->nullable()->default(0);
            $t->integer('secondRotate')->nullable()->default(0);
            $t->integer('thirdRotate')->nullable()->default(0);
            $t->integer('fourRotate')->nullable()->default(0);
            $t->integer('fiveRotate')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function ($t) {
            $t->dropColumn('rotate');
            $t->dropColumn('secondRotate');
            $t->dropColumn('thirdRotate');
            $t->dropColumn('fourRotate');
            $t->dropColumn('fiveRotate');
        });
    }
}
