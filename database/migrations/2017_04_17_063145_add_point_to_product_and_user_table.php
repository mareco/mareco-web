<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPointToProductAndUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function ($t) {
            $t->integer('point')->nullable()->default(0);
        });
        Schema::table('products', function ($t) {
            $t->integer('point')->nullable()->default(0);
        });
        Schema::table('variants', function ($t) {
            $t->double('discount_price')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::table('users', function ($t) {
        //     $t->dropColumn('point');
        // });
        // Schema::table('products', function ($t) {
        //     $t->dropColumn('point');
        // });
        // Schema::table('variants', function ($t) {
        //     $t->dropColumn('discount_price');
        // });
    }
}
