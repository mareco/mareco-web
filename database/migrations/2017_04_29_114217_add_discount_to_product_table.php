<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDiscountToProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('photo');
            $table->integer('status')->default(0);
            $table->integer('sale_type')->default(0);
            $table->double('sale_amount')->default(0);
            $table->double('grand_amount')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('status');
            $table->dropColumn('sale_type');
            $table->dropColumn('sale_amount');
            $table->dropColumn('grand_amount');
        });
    }
}
