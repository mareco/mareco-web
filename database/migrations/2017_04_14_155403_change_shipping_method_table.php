<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeShippingMethodTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shipping_methods', function ($t) {
            $t->dropForeign('shipping_methods_created_by_foreign');
            $t->dropColumn('created_by');
            $t->dropColumn('name');
            $t->dropColumn('enable');
            $t->integer('jne')->nullable();
            $t->integer('pos')->nullable();
            $t->integer('tiki')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shipping_methods', function ($t) {
            $t->dropColumn('jne');
            $t->dropColumn('pos');
            $t->dropColumn('tiki');
        });
    }
}
