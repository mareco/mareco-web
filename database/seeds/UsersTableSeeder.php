<?php

use App\Helpers\Enums\UserRole;
use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$user = new User();
    	$user->first_name = 'Admin';
    	$user->last_name = 'Charlie';
    	$user->email = 'admin@admin.com';
    	$user->password = bcrypt('admin');
    	$user->role = UserRole::SUPER_ADMIN;
    	$user->phone = 655566342;
    	$user->save();
    }
}
