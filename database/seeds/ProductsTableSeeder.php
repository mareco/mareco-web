<?php

use App\Models\BankAccount;
use App\Models\Category;
use App\Models\SubCategory;
use App\Models\Unit;
use App\Models\ShippingMethod;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$bank = new BankAccount();
    	$bank->user_id = 1;
    	$bank->bank = 'BCA';
        $bank->account_number = '0390533833';
    	$bank->account_name = 'Dylan Jonathan';
    	$bank->logo = '';
    	$bank->is_active = 1;
    	$bank->save();

    	$bank = new BankAccount();
    	$bank->user_id = 1;
    	$bank->bank = 'MANDIRI';
    	$bank->account_number = '140003335701';
        $bank->account_name = 'Dylan Jonathan';
    	$bank->logo = '';
    	$bank->is_active = 1;
    	$bank->save();

    	$category = new Category();
    	$category->name = 'Sport Equipment';
    	$category->photo = '';
    	$category->slug_url = 'sport-equipment';
    	$category->description = 'Adidas, Nike, Fila, Converse';
    	$category->is_active = 1;
    	$category->save();

    	$category = new Category();
    	$category->name = 'Shirt';
    	$category->photo = '';
    	$category->slug_url = 'shirt';
    	$category->description = 'Long Sleeve, Pyjamas';
    	$category->is_active = 1;
    	$category->save();

    	$unit = new Unit();
    	$unit->name = 'pcs';
    	$unit->description = 'pieces';
    	$unit->save();

    	$unit = new Unit();
    	$unit->name = 'pack';
    	$unit->description = 'packages';
    	$unit->save();

        DB::table('shipping_methods')->delete();
        $shippingMethod = new ShippingMethod();
        $shippingMethod->jne = 1;
        $shippingMethod->tiki = 1;
        $shippingMethod->pos = 1;
        $shippingMethod->save();
    }
}
