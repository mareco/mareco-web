<?php

namespace App\Providers;

use App\Models\BankAccount;
use App\Models\Category;
use App\Models\City;
use App\Models\Config;
use App\Models\Courier;
use App\Models\District;
use App\Models\Order;
use App\Models\Product;
use App\Models\Promotion;
use App\Models\Province;
use App\Models\Review;
use App\Models\ShippingMethod;
use App\Models\SubCategory;
use App\Models\Term;
use App\Models\Unit;
use App\Models\User;
use App\Models\Variant;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        Route::model('user', User::class);
        Route::model('bank', BankAccount::class);
        Route::model('province', Province::class);
        Route::model('city', City::class);
        Route::model('district', District::class);
        Route::model('courier', Courier::class);
        Route::model('shippingMethod', ShippingMethod::class);
        Route::model('category', Category::class);
        Route::model('subCategory', SubCategory::class);
        Route::model('unit', Unit::class);
        Route::model('product', Product::class);
        Route::model('config', Config::class);
        Route::model('variant', Variant::class);
        Route::model('promotion', Promotion::class);
        Route::model('review', Review::class);
        Route::model('term', Term::class);
        Route::model('order', Order::class);
        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }
}
