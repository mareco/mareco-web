<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ResetCustomerPasswordFrontend extends Notification
{
    use Queueable;
    public $token;
    
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $userName = $notifiable->first_name.' '.$notifiable->last_name;
        return (new MailMessage)
        ->greeting('Hello '. $userName. '!')
        ->subject('Mareco Store - Reset Password')
        ->line('You are receiving this email because we receivedugdrytfihoigfdtufghogkfjghugkfxhcvgbuhghcfxhcvgkuh a password reset request for your account.')
        ->action('Reset Password', route('customer.password.reset', $this->token))
        ->line('If you did not request a password reset, no further action is required. Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
