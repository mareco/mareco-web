<?php

namespace App\Models;

use App\Models\Category;
use App\Models\Picture;
use App\Models\ProductLang;
use App\Models\Review;
use App\Models\SubCategory;
use App\Models\Term;
use App\Models\Unit;
use App\Models\User;
use App\Models\Variant;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	protected $table = 'products';
	protected $fillable = ['name','code','description','slug_url','product_image','product_image_second','product_image_third','product_image_fourth','product_image_fifth','point'];
	protected $guarded = ['category_id', 'created_by'];

	public static function addRules() {
        return [
        'code' => 'required|max:10|unique:products',
        'name' => 'required|max:50',
        'category_id' => 'required',
        'point' => 'min:0',
        'product_image' => 'required'
        ];
    }

    public static function editRules($id) {
        return [
        'code' => 'required|max:10|unique:products,id,$id',
        'name' => 'required|max:50',
        'category_id' => 'required',
        'point' => 'min:0',
        'product_image' => 'required'
        ];
    }

	public function category()
	{
		return $this->belongsTo(Category::class);
	}

	public function unit()
	{
		return $this->belongsTo(Unit::class);
	}

	public function user()
	{
		return $this->belongsTo(User::class,'id');
	}

	public function totalReview()
	{
		return $this->reviews->count();
	}

	public function totalRate()
	{
		if($this->totalReview() == 0) return 0;
		return round($this->reviews->sum('rate')/$this->totalReview(),2);
	}

	public function productVariants()
	{
		return $this->hasMany(Variant::class);
	}

	public function reviews()
	{
		return $this->hasMany(Review::class);
	}

	public function productTerms()
	{
		return $this->hasMany(Term::class);
	}

	public function getProductImageAttribute($value){
		if(!$value) return '';
		return 'http://'.$_SERVER['SERVER_NAME'].'/'.$value; 
	}

	public function getProductImageSecondAttribute($value){
		if(!$value) return '';
		return 'http://'.$_SERVER['SERVER_NAME'].'/'.$value; 
	}

	public function getProductImageThirdAttribute($value){
		if(!$value) return '';
		return 'http://'.$_SERVER['SERVER_NAME'].'/'.$value; 
	}

	public function getProductImageFourthAttribute($value){
		if(!$value) return '';
		return 'http://'.$_SERVER['SERVER_NAME'].'/'.$value; 
	}

	public function getProductImageFifthAttribute($value){
		if(!$value) return '';
		return 'http://'.$_SERVER['SERVER_NAME'].'/'.$value; 
	}

	public function productLang($lang) {
        return $this->hasOne(ProductLang::class)->where('lang', $lang)->first();
    }

    public function productLanguages(){
    	return $this->hasMany(ProductLang::class);
    }
}
