<?php

namespace App\Models;

use App\Models\Order;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class CashOnDelivery extends Model
{
	protected $table = 'cash_on_deliveries';
	protected $fillable = ['user_address', 'rate','user_id','order_id'];

	public static function addRules() {
        return [
            'order_id' => 'required',
        ];
    }

    public static function editRules($id) {
        return [
            'order_id' => 'required',
        ];
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

}	
