<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class UserSocialAccount extends Model
{
    protected $fillable = [
        'user_id', 'provider', 'provider_user_id',
    ];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}
