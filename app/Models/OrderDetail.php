<?php

namespace App\Models;

use App\Models\Order;
use App\Models\Product;
use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
	protected $table = 'order_details';
	protected $fillable = ['qty', 'price','amount','weight'];
	protected $guarded = ['order_id', 'product_id'];

	public function order() {
		return $this->belongsTo(Order::class);
	}

	public function product() {
		return $this->belongsTo(Product::class);
	}

	public function checkOrderDetail($variant) {
		$this->product_id = $variant->product->id;
		$this->price = $variant->price;
		$this->amount = $this->qty*$this->price;
		$this->weight = $variant->weight;
		$this->size = $variant->size;
		$this->sku = $variant->sku;
		$this->color = $variant->color;
		return $this;
	}

	// protected static function boot()
	// {
	// 	parent::boot();

	// 	static::creating(function($orderDetail) {
	// 		$product = $orderDetail->product;
	// 		if($product) {
	// 			$product->qty -= $orderDetail->qty;
	// 			$product->save();
	// 		}
	// 	});

	// 	// static::updating(function($purchaseDetail) {
	// 	// 	$original = $purchaseDetail->getOriginal();
	// 	// 	$product = Product::find($original['product_id']);
	// 	// 	if($product) {
	// 	// 		$product->qty -= $original['qty'];
	// 	// 		$product->save();
	// 	// 	}
	// 	// 	$product = $purchaseDetail->product;
	// 	// 	if($product) {
	// 	// 		$product->qty += $purchaseDetail->qty;
	// 	// 		$product->save();
	// 	// 	}
	// 	// });

	// 	// static::deleting(function($purchaseDetail) {
	// 	// 	$product = $purchaseDetail->product;
	// 	// 	if($product) {
	// 	// 		$product->qty -= $purchaseDetail->qty;
	// 	// 		$product->save();
	// 	// 	}
	// 	// });

	// }
}
