<?php

namespace App\Models;

use App\Models\Product;
use App\Models\Category;
use Illuminate\Database\Eloquent\Model;

class Picture extends Model
{
	protected $table = 'pictures';
	protected $fillable = ['url','category_id','is_first'];

	public function getUrlAttribute($value){
		if(!$value) return $value;
		return 'http://'.$_SERVER['SERVER_NAME'].'/'.$value;
	}

	public function category()
	{
		return $this->belongsTo(Category::class);
	}
}
