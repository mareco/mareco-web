<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BankAccount extends Model
{
    use SoftDeletes;

	protected $table = 'bank_accounts';
    protected $dates = ['deleted_at'];
	protected $fillable = ['bank','account_number','account_name','logo','is_active'];
	protected $guarded = ['user_id'];

	public static function addRules() {
        return [
            'account_number' => 'required|min:5',
            'account_name' => 'required|max:25',
            'bank' => 'required'
        ];
    }

    public static function editRules($id) {
        return [
            'account_name' => 'required|max:25',
            'account_number' => 'required|min:5',
            'bank' => 'required'
        ];
    }


	public function user()
	{
		return $this->belongsTo(User::class);
	}

	public function getLogoAttribute($value){
		if(!$value) return '';
		return 'http://'.$_SERVER['SERVER_NAME'].'/'.$value;
	}
}
