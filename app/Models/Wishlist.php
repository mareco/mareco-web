<?php

namespace App\Models;

use App\Models\Variant;
use Illuminate\Database\Eloquent\Model;

class Wishlist extends Model
{
	protected $table = 'wishlists';
    protected $guarded = ['user_id','variant_id'];

    public static function addRules() {
        return [
        'variant_id' => 'required|exists:variants,id',
        ];
    }

    public function variant() {
        return $this->belongsTo(Variant::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }
}	
