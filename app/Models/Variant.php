<?php

namespace App\Models;

use App\Models\Product;
use App\Models\Unit;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Variant extends Model
{
	protected $table = 'variants';
	protected $fillable = ['sku','qty','size','color','price','unit_id','weight','discount_price','is_discount'];
	protected $guarded = ['product_id'];

	public static function addRules() {
        return [
        'sku' => 'required|max:15|unique:variants',
        'product_id' => 'required',
        ];
    }

    public static function editRules($id) {
        return [
        'sku' => 'required|max:15|unique:variants,id,$id',
        'product_id' => 'required',
        ];
    }

	public function product()
	{
		return $this->belongsTo(Product::class);
	}

    public function unit() {
        return $this->belongsTo(Unit::class);
    }

}
