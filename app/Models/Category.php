<?php

namespace App\Models;

use App\Models\CategoryLang;
use App\Models\Picture;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	protected $table = 'categories';
	protected $fillable = ['name', 'description','photo','is_active','slug_url'];


    public static function editRules($id) {
        return [
            'name' => 'required',
        ];
    }

    public function picture()
    {
        return $this->hasMany(Category::class);
    }

    public function categoryLangs() {
        return $this->hasMany(CategoryLang::class);
    }


    public function categoryLang($lang) {
        return $this->hasOne(CategoryLang::class)->where('lang', $lang)->first();
    }

	public function getPhotoAttribute($value){
		if(!$value) return '';
		return 'http://'.$_SERVER['SERVER_NAME'].'/'.$value;
	}

}	
