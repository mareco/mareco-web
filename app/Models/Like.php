<?php

namespace App\Models;

use App\Models\Category;
use App\Models\Forum;
use App\Models\Product;
use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
	protected $table = 'likes';

	public function forum()
	{
		return $this->belongsTo(Forum::class);
	}
	
}
