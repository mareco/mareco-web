<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Activity_log extends Model
{

    protected $fillable = ['type','action','description'];
    protected $guarded = ['id','user_id'];
    protected $dates = ['created_at','updated_at'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
