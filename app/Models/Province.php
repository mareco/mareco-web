<?php

namespace App\Models;

use App\Models\City;
use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
	protected $table = 'provinces';
	protected $fillable = ['name'];

	public static function addRules() {
        return [
            'name' => 'required'
        ];
    }

    public static function editRules($id) {
        return [
            'name' => 'required'
        ];
    }

	public function city()
	{
		return $this->hasMany(City::class);
	}
}
