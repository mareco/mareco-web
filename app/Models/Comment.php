<?php

namespace App\Models;

use App\Models\Product;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
	protected $table = 'comments';
	protected $fillable = ['description','rate'];
	protected $guarded = ['user_id','product_id'];

	public static function addRules() {
		return [
		'user_id' => 'required',
		'product_id' => 'required'
		];
	}

	public static function editRules($id) {
		return [
		'user_id' => 'required',
		'product_id' => 'required'
		];
	}
	public function product()
	{
		return $this->belongsTo(Product::class);
	}

	public function user() {
		return $this->belongsTo(User::class);
	}
	
}
