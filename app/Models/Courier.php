<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Courier extends Model
{
	protected $table = 'couriers';
  	protected $fillable = ['name','logo'];

	public static function addRules() {
        return [
            'name' => 'required',
        ];
    }

    public static function editRules($id) {
        return [
            'name' => 'required',
        ];
    }

	public function getLogoAttribute($value){
		if(!$value) return '';
		return 'http://'.$_SERVER['SERVER_NAME'].'/'.$value;
	}
}
