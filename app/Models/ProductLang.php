<?php

namespace App\Models;

use App\Models\Product;
use Illuminate\Database\Eloquent\Model;

class ProductLang extends Model
{
	protected $table = 'product_langs';
	protected $fillable = ['name', 'description','lang'];
    protected $guarded = ['product_id'];

	public static function addRules() {
        return [
            'name' => 'required',
        ];
    }

    public static function editRules($id) {
        return [
            'name' => 'required',
        ];
    }

    public function product() {
        return $this->belongsTo(Product::class);
    }
}	
