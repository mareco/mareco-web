<?php

namespace App\Models;

use App\Models\User;
use App\Models\Order;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
	protected $table = 'addresses';
	protected $fillable = ['city','province','district','address','postal_code','state','city_name','province_name','district_name','is_shipping_address','is_billing_address'];
	protected $guarded = ['user_id'];

	public static function addRules() {
		return [
		'city' => 'required',
		'province' => 'required',
		'district' => 'required',
		'address' => 'required'
		];
	}

	public static function updateAddres($id) {
		return [
		'city' => 'required',
		'province' => 'required',
		'district' => 'required',
		'address' => 'required'
		];
	}

	public function user() 
	{ 
		return $this->belongsTo(User::class); 
	} 

	public function order() 
	{
		return $this->hasMany(Order::class);
	}
}
