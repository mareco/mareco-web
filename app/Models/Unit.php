<?php

namespace App\Models;

use App\Models\Product;
use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
	protected $table = 'units';
	protected $fillable = ['name','description'];

	public static function addRules() {
        return [
            'name' => 'required',
        ];
    }

    public static function editRules($id) {
        return [
            'name' => 'required'
        ];
    }

	public function products()
	{
		return $this->hasMany(Product::class);
	}
	
}
