<?php

namespace App\Models;

use App\Models\City;
use App\Models\Courier;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class ShippingMethod extends Model
{
	protected $table = 'shipping_methods';
	protected $fillable = ['name', 'type','fee'];
	protected $guarded = ['created_by', 'courier_id','city_id'];

	public static function addRules() {
        return [
            'name' => 'required',
            'type' => 'required',
            'fee' => 'required',
            'courier_id' => 'required',
            'city_id' => 'required',
        ];
    }

    public static function editRules($id) {
        return [
            'name' => 'required',
            'type' => 'required',
            'fee' => 'required',
            'courier_id' => 'required',
            'city_id' => 'required',
        ];
    }
    
	public function user() {
		return $this->belongsTo(User::class,'created_by');
	}

	public function city() {
		return $this->belongsTo(City::class);
	}

	public function courier() {
		return $this->belongsTo(Courier::class);
	}
}
