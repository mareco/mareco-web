<?php

namespace App\Models;

use App\Models\Order;
use App\Models\PurchaseDetail;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
	protected $table = 'payments';
	protected $fillable = ['total','date','status','remark'];
	protected $guarded = ['bank_account_id','orders_id'];

	public function order()
	{
		return $this->belongsTo(Order::class, 'orders_id');
	}
}
