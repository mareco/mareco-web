<?php

namespace App\Models;

use App\Helpers\Enums\UserRole;
use App\Models\Address;
use App\Models\Order;
use App\Models\Wishlist;
use App\Notifications\MyOwnResetPassword as ResetPasswordNotification;
use App\Notifications\ResetCustomerPasswordFrontend as ResetCustomerPassword;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;
    protected $table = "users";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
    'first_name','last_name', 'email', 'password','role','photo','point'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function addRules() {
        return [
        'email' => 'required|email|unique:users,email',
        'first_name' => 'required',
        'password' => 'required|min:5'
        ];
    }

    public static function editRules($id) {
        return [
        'email' => 'required|email|unique:users,email,'.$id
        ];
    }

    public static function updateRules($id) {
        return [
        'first_name' => 'required',
        'last_name' => 'required',
        'phone' => 'required'
        ];
    }

    public static function updateProfileRules($id) {
        return [
            'email' => 'email|unique:users,email,'.$id,
            'new_password' => 'min:5',
            'picture' => 'image',
        ];
    }

    public function isAdmin()
    {
        return $this->role == UserRole::ADMIN || $this->role == UserRole::SUPER_ADMIN;
    }
    
    public function addresses() {
        return $this->hasMany(Address::class);
    }

    public function order() {
        return $this->hasMany(Order::class);
    }

    public function wishlists() {
        return $this->hasMany(Wishlist::class);
    }
    
    public function getPhotoAttribute($value){
        if(!$value) return '';
        if(strpos($value, 'http') !== false) return $value;
        return 'http://'.$_SERVER['SERVER_NAME'].'/'.$value;
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    public function sendPasswordResetFrontend($token)
    {
        $this->notify(new ResetCustomerPassword($token));
    }

    
}
