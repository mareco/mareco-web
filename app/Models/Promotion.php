<?php

namespace App\Models;

use App\Models\Product;
use App\Models\ProductPromotion;
use Illuminate\Database\Eloquent\Model;

class Promotion extends Model
{
	protected $table = 'promotions';
	protected $fillable = ['content','title','photo','size','start_date','end_date','discount_type','discount_value','font_color','bg_color'];

	public static function addRules() {
        return [
        'title' => 'max:50',
        'size' => 'required',
        'product_id' => 'required',
        'start_date' => 'required',
        'end_date' => 'required',
        'discount_type' => 'required',
        'discount_value' => 'required',
        'font_color' => 'required',
        'bg_color' => 'required'
        ];
    }

    public static function editRules($id) {
        return [
        'title' => 'max:50',
        'size' => 'required',
        'product_id' => 'required',
        'start_date' => 'required',
        'end_date' => 'required',
        'discount_type' => 'required',
        'discount_value' => 'required',
        'font_color' => 'required',
        'bg_color' => 'required'
        ];
    }
    public function product() {
        return $this->belongsTo(Product::class);
    }

    public function productPromotions() {
        return $this->hasMany(ProductPromotion::class);
    }

	public function getPhotoAttribute($value){
		if(!$value) return '';
		return 'http://'.$_SERVER['SERVER_NAME'].'/'.$value;
	}

}
