<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaypalPayment extends Model
{
	protected $table = 'paypal_payments';
	protected $fillable = ['payment_id','payment_status','payment_amount'];

	public static function addRules() {
		return [
		'payment_id' => 'required|unique:paypal_payments',
		];
	}
	
}
