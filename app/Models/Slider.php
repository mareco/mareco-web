<?php

namespace App\Models;

use App\Models\Picture;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
	protected $table = 'sliders';
	protected $guarded = ['user_id','picture_id','title'];

	public function user() {
		return $this->belongsTo(User::class);
	}

	public function picture() {
		return $this->belongsTo(Picture::class);
	}

}
