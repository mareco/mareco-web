<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserService extends Model
{
    protected $table = "user_services";

    protected $fillable = [
    'name', 'email', 'message'
    ];
    protected $timestamp =false;
}
