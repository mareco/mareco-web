<?php

namespace App\Models;

use App\Models\District;
use App\Models\Province;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
	protected $table = 'cities';
	protected $fillable = ['name'];
	protected $guarded = ['province_id'];

	public static function addRules() {
        return [
            'name' => 'required',
            'province_id' => 'required'
        ];
    }

    public static function editRules($id) {
        return [
            'name' => 'required',
            'province_id' => 'required'
        ];
    }

	public function province()
	{
		return $this->belongsTo(Province::class);
	}

	public function districts()
	{
		return $this->hasMany(District::class);
	}
}
