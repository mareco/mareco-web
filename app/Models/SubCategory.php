<?php

namespace App\Models;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
	protected $table = 'sub_categories';
	protected $fillable = ['name', 'slug_url','photo','is_active'];
    protected $guarded = ['category_id'];

	public static function addRules() {
        return [
            'name' => 'required',
            'category_id' => 'required',
        ];
    }

    public static function editRules($id) {
        return [
            'name' => 'required',
            'category_id' => 'required',
        ];
    }

	public function getPhotoAttribute($value){
		if(!$value) return '';
		return 'http://'.$_SERVER['SERVER_NAME'].'/'.$value;
	}

    public function category() {
        return $this->belongsTo(Category::class);
    }

    public function products() {
        return $this->hasMany(Product::class);
    }
}	
