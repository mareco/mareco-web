<?php

namespace App\Models;

use App\Models\Product;
use Illuminate\Database\Eloquent\Model;

class Term extends Model
{
	protected $table = 'terms';
	protected $fillable = ['message'];

}
