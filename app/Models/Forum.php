<?php

namespace App\Models;

use App\Models\Category;
use App\Models\Comment;
use App\Models\Like;
use App\Models\Product;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Forum extends Model
{
	protected $table = 'forums';
	protected $fillable = ['description', 'total_likes','attachment','attachment_type'];
	protected $guarded = ['product_id','user_id'];

	public function likes()
	{
		return $this->hasMany(Like::class);
	}

	public function product()
	{
		return $this->belongsTo(Product::class, 'product_id');
	}

	public function user() {
		return $this->belongsTo(User::class);
	}
	
 	public function comments() {
 		return $this->hasMany(Comment::class);
 	}

 	public function totalLike()
	{
		return $this->likes->count();
	}

	public function isLike($id)
	{
		$this->likes();
		for ($i=0; $i < count($this->likes); $i++) { 
			if($this->likes[$i]->user_id == $id) {
				return true;
			}
		}
		return false;
	}

	public function getAttachmentAttribute($value){
		if(!$value) return '';
		return 'http://'.$_SERVER['SERVER_NAME'].'/'.$value;
	}
}