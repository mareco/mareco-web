<?php

namespace App\Models;

use App\Models\Address;
use App\Models\BankAccount;
use App\Models\OrderDetail;
use App\Models\Payment;
use App\Models\Shipment;
use App\Models\ShippingMethod;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
	protected $table = 'orders';
	protected $fillable = ['expire_date', 'status','grand_total','order_number','payment_type','shipment_service','shipment_type','remark','account_name','account_number','receipt_url','date_payment','reference_id','bank_account_id','point','amount_payment','additional_note'];
	protected $guarded = ['address_id','user_id'];

	public function shippingMethod() {
		return $this->belongsTo(ShippingMethod::class);
	}

	public function address() {
		return $this->belongsTo(Address::class);
	}

	public function bankAccount() {
		return $this->belongsTo(BankAccount::class);
	}

	public function orderDetails() {
		return $this->hasMany(OrderDetail::class);
	}

	public function shipment() {
		return $this->hasOne(Shipment::class);
	}

	public function user() {
		return $this->belongsTo(User::class);
	}

	public static function addRules() {
		return [
		'address_id' => 'required',
		'shipment_type' => 'required',
		'payment_type' => 'required',
		];
	}

	public function getGrandTotal() {
		$this->grand_total = $this->orderDetails->sum('amount')+$this->shipment_fee;
		$this->save();
		return true;
	}

	public function getReceiptUrlAttribute($value){
		if(!$value) return '';
		return 'http://'.$_SERVER['SERVER_NAME'].'/'.$value;
	}
}
