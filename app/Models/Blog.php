<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
	protected $table = 'blogs';
	protected $fillable = ['title', 'description','post_date', 'photo'];
	protected $guarded = ['created_by'];

	public function user()
	{
		return $this->belongsTo(User::class, 'created_by');
	}

	public function getPhotoAttribute($value){
		if(!$value) return '';
		return 'http://'.$_SERVER['SERVER_NAME'].'/'.$value;
	}
}
