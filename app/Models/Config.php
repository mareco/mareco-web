<?php

namespace App\Models;

use App\Models\AboutUsLang;
use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
	protected $table = 'configs';
	protected $fillable = ['about_us_content','about_us_title','about_us_email','cod_amount','about_us_phone','about_us_mobile','facebook_account','twitter_account','instagram_account','storage_address','storage_lat','storage_lng','admin_email'];

	public function aboutUsLang($lang) {
        return $this->hasOne(AboutUsLang::class)->where('lang', $lang)->first();
    }
}
