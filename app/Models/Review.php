<?php

namespace App\Models;

use App\Models\Product;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
	protected $table = 'reviews';
	protected $fillable = ['rate','remarks'];
	protected $guarded = ['user_id','product_id'];

	public static function addRules() {
        return [
            'remark' => 'required'
        ];
    }

    public static function editRules($id) {
        return [
            'remark' => 'required'
        ];
    }

	public function user() {
		return $this->belongsTo(User::class);
	}

	public function product() {
		return $this->belongsTo(Product::class);
	}
}
