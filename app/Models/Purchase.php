<?php

namespace App\Models;

use App\Models\PurchaseDetail;
use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
	protected $table = 'purchases';
	protected $fillable = ['date'];
	protected $guarded = ['user_id'];

	public function purchaseDetails() {
		return $this->hasMany(PurchaseDetail::class);
	}

	protected static function boot()
	{
		parent::boot();

		static::deleting(function($purchase) {
			foreach ($purchase->purchaseDetails as $value) {
				$product = $value->product;
				if($product) {
					$product->qty -= $value->qty;
					$product->save();
				}
			}
			$purchase->purchaseDetails()->delete();
		});

		
	}
}
