<?php

namespace App\Models;

use App\Models\Config;
use Illuminate\Database\Eloquent\Model;

class AboutUsLang extends Model
{
	protected $table = 'about_us_lang';
	protected $fillable = ['title', 'about_us_content','lang'];
    protected $guarded = ['config_id'];

    public function config() {
        return $this->belongsTo(Config::class);
    }
}	
