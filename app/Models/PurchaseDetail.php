<?php

namespace App\Models;

use App\Models\Product;
use App\Models\Purchase;
use Illuminate\Database\Eloquent\Model;

class PurchaseDetail extends Model
{
	protected $table = 'purchase_details';
	protected $fillable = ['qty'];
	protected $guarded = ['product_id'];

	public function purchase() {
		return $this->belongsTo(Purchase::class);
	}

	public function product() {
		return $this->belongsTo(Product::class);
	}

	protected static function boot()
	{
		parent::boot();

		static::creating(function($purchaseDetail) {
			$product = $purchaseDetail->product;
			if($product) {
				$product->qty += $purchaseDetail->qty;
				$product->save();
			}
		});

		static::updating(function($purchaseDetail) {
			$original = $purchaseDetail->getOriginal();
			$product = Product::find($original['product_id']);
			if($product) {
				$product->qty -= $original['qty'];
				$product->save();
			}
			$product = $purchaseDetail->product;
			if($product) {
				$product->qty += $purchaseDetail->qty;
				$product->save();
			}
		});
		
		static::deleting(function($purchaseDetail) {
			$product = $purchaseDetail->product;
			if($product) {
				$product->qty -= $purchaseDetail->qty;
				$product->save();
			}
		});

	}

}
