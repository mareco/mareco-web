<?php

namespace App\Models;

use App\Models\Product;
use App\Models\Promotion;
use Illuminate\Database\Eloquent\Model;

class ProductPromotion extends Model
{
	protected $table = 'product_promotions';
	protected $guarded = ['promotion_id','product_id'];

	public static function addRules() {
		return [
		'promotion_id' => 'required',
		'product_id' => 'required',
		];
	}

	public static function editRules($id) {
		return [
		'promotion_id' => 'required',
		'product_id' => 'required',
		];
	}

	public function product() {
		return $this->belongsTo(Product::class);
	}
	public function promotion() {
		return $this->belongsTo(Promotion::class);
	}
}
