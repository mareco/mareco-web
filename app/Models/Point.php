<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Point extends Model
{
	protected $table = 'points';
	protected $fillable = ['description','amount'];
	protected $guarded = ['user_id'];

	public static function addRules() {
		return [
		'user_id' => 'required',
		'amount' => 'required'
		];
	}

	public static function editRules($id) {
		return [
		'user_id' => 'required',
		'amount' => 'required'
		];
	}

	public function user() {
		return $this->belongsTo(User::class);
	}
	
}
