<?php

namespace App\Models;

use App\Models\City;
use App\Models\State;
use Illuminate\Database\Eloquent\Model;

class District extends Model
{
	protected $table = 'districts';
	protected $fillable = ['name'];
	protected $guarded = ['city_id'];

	public static function addRules() {
        return [
            'name' => 'required',
            'city_id' => 'required'
        ];
    }

    public static function editRules($id) {
        return [
            'name' => 'required',
            'city_id' => 'required'
        ];
    }

	public function city()
	{
		return $this->belongsTo(City::class);
	}
}
