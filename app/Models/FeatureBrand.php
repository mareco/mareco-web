<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FeatureBrand extends Model
{
	protected $table = 'feature_brands';
	protected $fillable = ['feature_url'];

	public function getFeatureUrlAttribute($value){
		if(!$value) return $value;
		return 'http://'.$_SERVER['SERVER_NAME'].'/'.$value;
	}

}
