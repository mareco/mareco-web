<?php

namespace App\Models;

use App\Models\Category;
use Illuminate\Database\Eloquent\Model;

class CategoryLang extends Model
{
	protected $table = 'category_langs';
	protected $fillable = ['name', 'description','lang'];
    protected $guarded = ['category_id'];

	public static function addRules() {
        return [
            'name' => 'required',
        ];
    }

    public static function editRules($id) {
        return [
            'name' => 'required',
        ];
    }

    public function category() {
        return $this->belongsTo(Category::class);
    }
}	
