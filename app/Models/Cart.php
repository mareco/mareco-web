<?php

namespace App\Models;

use App\Models\User;
use App\Models\Variant;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
	protected $table = 'carts';
	protected $fillable = ['qty'];
	protected $guarded = ['variant_id','user_id'];

	public static function addRules() {
		return [
		'variant_id' => 'required|exists:variants,id',
		'qty' => 'min:1|required|numeric'
		];
	}

	public function variant() 
	{
		return $this->belongsTo(Variant::class);
	}

	public function user() 
	{
		return $this->belongsTo(User::class);
	}
}
