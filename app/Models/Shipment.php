<?php

namespace App\Models;

use App\Models\Order;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Shipment extends Model
{
	protected $table = 'shipments';
	protected $fillable = ['shipping_date','received','received_date','tracking_number'];
	protected $guarded = ['order_id','created_by'];

	public function order() {
		return $this->belongsTo(Order::class);
	}

	public function user() {
		return $this->belongsTo(User::class);
	}
}
