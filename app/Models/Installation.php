<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Installation extends Model
{
	protected $fillable = [
        'user_id', 'session_token', 'device_type', 'device_token', 
    ];

    public $timestamps = false;

    public static function editRules()
    {
        return [
            'device_type' => 'required|in:ios,android',
            'device_token' => 'required',
        ];
    }

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}
