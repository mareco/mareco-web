<?php

namespace App\Helpers;

final class ReportType {

	const EXPORT_CUSTOMER = 0;
	// const POINT_USAGE = 1;
	const TOTAL_ORDERS = 2;
	const STOCK = 3;
	const PAYMENT = 4;

	public static function getList() {
		return [
			ReportType::EXPORT_CUSTOMER,
			// ReportType::POINT_USAGE,
			ReportType::TOTAL_ORDERS,
			ReportType::STOCK,
			ReportType::PAYMENT
		];
	}

	public static function getArray() {
		$result = [];
		foreach (self::getList() as $arr) {
			$result[$arr] = self::getString($arr);
		}
		return $result;
	}

	public static function getString($val) {
		switch ($val) {
			case 0:
				return "Customer Data";	
			// case 1:
			// 	return "Point Usage";
			case 2:
				return "Total Orders Data";
			case 3:
				return "Stock";
			case 4:
				return "Payment";
		}
	}

}

?>
