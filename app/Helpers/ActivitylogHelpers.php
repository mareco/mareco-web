<?php

namespace App\Helpers;

use Auth;
use App\Models\Activity_log;

final class ActivitylogHelpers
{
    public static function generate($type,$action,$description)
    {
    	$activity_log = new Activity_log();
    	$activity_log->type = $type;
    	$activity_log->description = $description;
    	$activity_log->action = $action;
    	$activity_log->user_id = Auth::User()->id;
    	$activity_log->save();
    }
}

?>
