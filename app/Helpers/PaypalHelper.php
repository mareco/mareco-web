<?php

namespace App\Helpers;

use Paypal;

final class PaypalHelper {

    private $_apiContext;

    public function __construct()
    {
        $this->_apiContext = Paypal::ApiContext(
            config('services.paypal.client_id'),
            config('services.paypal.secret'));

        $this->_apiContext->setConfig(array(
            'mode' => 'sandbox',
            'service.EndPoint' => 'https://api.sandbox.paypal.com',
            'http.ConnectionTimeOut' => 30,
            'log.LogEnabled' => true,
            'log.FileName' => storage_path('logs/paypal.log'),
            'log.LogLevel' => 'FINE'
            ));
    }

    public function checkPaymentId($payment_id) {
        return Paypal::getById($payment_id, $this->_apiContext);
    }
}

?>
