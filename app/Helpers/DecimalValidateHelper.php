<?php

namespace App\Helpers;

final class DecimalValidateHelper {

    public static function validate($number) {
        if(preg_match('/^[0-9.]+$/', $number)) { 
            return true; 
        }
        else{ 
            return false;
        }
    }

}