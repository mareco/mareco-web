<?php

namespace App\Helpers;

use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

trait ApiHelper {

	public function validateJson(Request $request, array $rules, array $messages = [], array $customAttributes = []) {
        $validator = validator()->make($request->all(), $rules, $messages, $customAttributes);
		if ($validator->fails()) {
    		$error = $validator->errors()->first();
    		throw new ValidationException($validator, response()->json(['message' => $error], 400));
    	}
	}
}

?>
