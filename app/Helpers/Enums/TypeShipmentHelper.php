<?php

namespace App\Helpers\Enums;

final class TypeShipmentHelper {

	const JNE = 0;
	const POS_INDONESIA = 1;
	const TIKI = 2;
	const BATAM = 3;

	public static function getList() {
		return [
			TypeShipmentHelper::JNE,
			TypeShipmentHelper::POS_INDONESIA,
			TypeShipmentHelper::TIKI,
			TypeShipmentHelper::BATAM
		];
	}

	public static function getArray() {
		$result = [];
		foreach (self::getList() as $arr) {
			$result[$arr] = self::getString($arr);
		}
		return $result;
	}

	public static function getString($val) {
		switch ($val) {
			case 0:
				return "jne";
			case 1:
				return "pos";
			case 2:
				return "tiki";	
			case 3:
				return "batam";	
		}
	}

}

?>
