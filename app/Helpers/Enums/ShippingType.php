<?php

namespace App\Helpers\Enums;

final class ShippingType {

	const PAKET_REGULAR = 'REG';
	const PAKET_YES = 'YES';

	public static function getList() {
		return [
			ShippingType::PAKET_REGULAR,
			ShippingType::PAKET_YES
		];
	}

	public static function getArray() {
		$result = [];
		foreach (self::getList() as $arr) {
			$result[$arr] = self::getString($arr);
		}
		return $result;
	}

	public static function getString($val) {
		switch ($val) {
			case 'REG':
				return "Paket REGULAR";
			case 'YES':
				return "Paket YES";
		}
	}

}

?>
