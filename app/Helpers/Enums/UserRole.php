<?php

namespace App\Helpers\Enums;

final class UserRole {

	const ADMIN = 'admin';
	const USER = 'user';
	const SUPER_ADMIN = 'super admin';

	public static function getList() {
		return [
			UserRole::ADMIN,
			UserRole::USER,
			UserRole::SUPER_ADMIN
		];
	}

	public static function getArray() {
		$result = [];
		foreach (self::getList() as $arr) {
			$result[$arr] = self::getString($arr);
		}
		return $result;
	}

	public static function getString($val) {
		switch ($val) {
			case 'admin':
				return "Admin";
			case 'user':
				return "User";
			case 'super admin':
				return "Super Admin";
		}
	}

}

?>
