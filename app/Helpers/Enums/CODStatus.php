<?php

namespace App\Helpers\Enums;

final class CODStatus {

	const ON_GOING = 0;
	const COMPLETED = 1;
	const CANCEL = 2;

	public static function getList() {
		return [
			CODStatus::ON_GOING,
			CODStatus::COMPLETED,
			CODStatus::CANCEL,
		];
	}

	public static function getArray() {
		$result = [];
		foreach (self::getList() as $arr) {
			$result[$arr] = self::getString($arr);
		}
		return $result;
	}

	public static function getString($val) {
		switch ($val) {
			case 0:
				return "On Going";
			case 1:
				return "Completed";
			case 2:
				return "Cancel";
		}
	}

}

?>
