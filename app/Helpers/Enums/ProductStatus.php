<?php

namespace App\Helpers\Enums;

final class ProductStatus {

	const IN_STOCK = 0;
	const NEW_ARRIVAL = 1;
	const NOW_SALE = 2;
	const BEST_SELLING = 3;
	const OUT_OF_STOCK = 4;
	const FEATURED = 5;

	public static function getList() {
		return [
			ProductStatus::IN_STOCK,
			ProductStatus::NEW_ARRIVAL,
			ProductStatus::NOW_SALE,
			ProductStatus::BEST_SELLING,
			ProductStatus::OUT_OF_STOCK,
			ProductStatus::FEATURED,
		];
	}

	public static function getArray() {
		$result = [];
		foreach (self::getList() as $arr) {
			$result[$arr] = self::getString($arr);
		}
		return $result;
	}

	public static function getString($val) {
		switch ($val) {
			case 0:
				return "In Stock";
			case 1:
				return "New Arrival";
			case 2:
				return "Now Sale";
			case 3:
				return "Best Selling";	
			case 4:
				return "Sold Out";
			case 5:
				return "Featured";
		}
	}

}

?>
