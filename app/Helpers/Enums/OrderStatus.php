<?php

namespace App\Helpers\Enums;

final class OrderStatus {

	const WAITING_FOR_PAYMENT = 0;
	const PACKING = 1;
	const ON_SHIPPING = 2;
	const DELIVERED = 3;
	const REFUND = 4;
	const CANCEL = 5;
	const ON_VERIFICATION = 6;
	const COMPLETED = 7;

	public static function getList() {
		return [
			OrderStatus::WAITING_FOR_PAYMENT,
			OrderStatus::PACKING,
			OrderStatus::ON_SHIPPING,
			OrderStatus::DELIVERED,
			OrderStatus::REFUND,
			OrderStatus::CANCEL,
			OrderStatus::ON_VERIFICATION,
			OrderStatus::COMPLETED
		];
	}

	public static function getArray() {
		$result = [];
		foreach (self::getList() as $arr) {
			$result[$arr] = self::getString($arr);
		}
		return $result;
	}

	public static function getString($val) {
		switch ($val) {
			case 0:
				return "Waiting For Payment";
			case 1:
				return "Packing";
			case 2:
				return "On Shipping";
			case 3:
				return "Delivered";	
			case 4:
				return "Refund";
			case 5:
				return "Cancel";
			case 6:
				return "On Verification";
			case 7:
				return "Completed";
		}
	}

}

?>
