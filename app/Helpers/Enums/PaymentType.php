<?php

namespace App\Helpers\Enums;

final class PaymentType {

	const PAYPAL = 0;
	const BANK_TRANSFER = 1;
	const CASH_ON_DELIVERY = 2;

	public static function getList() {
		return [
			PaymentType::PAYPAL,
			PaymentType::BANK_TRANSFER,
			PaymentType::CASH_ON_DELIVERY,
		];
	}

	public static function getArray() {
		$result = [];
		foreach (self::getList() as $arr) {
			$result[$arr] = self::getString($arr);
		}
		return $result;
	}

	public static function getString($val) {
		switch ($val) {
			case 0:
				return "Paypal";
			case 1:
				return "Bank Transfer";
			case 2:
				return "Cash On Delivery";
		}
	}

}

?>
