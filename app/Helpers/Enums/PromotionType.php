<?php

namespace App\Helpers\Enums;

final class PromotionType {

	const PERCENTAGE = 0;
	const AMOUNT = 1;

	public static function getList() {
		return [
			PromotionType::PERCENTAGE,
			PromotionType::AMOUNT,
		];
	}

	public static function getArray() {
		$result = [];
		foreach (self::getList() as $arr) {
			$result[$arr] = self::getString($arr);
		}
		return $result;
	}

	public static function getString($val) {
		switch ($val) {
			case 0:
				return "Percentage %";
			case 1:
				return "Amount";
		}
	}

}

?>
