<?php

namespace App\Helpers\Enums;

final class OrderStatusLang {

	const WAITING_FOR_PAYMENT = 0;
	const PACKING = 1;
	const ON_SHIPPING = 2;
	const DELIVERED = 3;
	const REFUND = 4;
	const CANCEL = 5;
	const ON_VERIFICATION = 6;
	const COMPLETED = 7;

	public static function getList() {
		return [
			OrderStatusLang::WAITING_FOR_PAYMENT,
			OrderStatusLang::PACKING,
			OrderStatusLang::ON_SHIPPING,
			OrderStatusLang::DELIVERED,
			OrderStatusLang::REFUND,
			OrderStatusLang::CANCEL,
			OrderStatusLang::ON_VERIFICATION,
			OrderStatusLang::COMPLETED
		];
	}

	public static function getArray() {
		$result = [];
		foreach (self::getList() as $arr) {
			$result[$arr] = self::getString($arr);
		}
		return $result;
	}

	public static function getString($val) {
		switch ($val) {
			case 0:
				return "Menunggu Pembayaran";
			case 1:
				return "Bungkus";
			case 2:
				return "Dalam Pengiriman";
			case 3:
				return "Terkirim";	
			case 4:
				return "Refund";
			case 5:
				return "Batal";
			case 6:
				return "Verifikasi";
			case 7:
				return "Selesai";
		}
	}

}

?>
