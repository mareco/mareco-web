<?php

namespace App\Helpers;

use App\Helpers\Enums\UserRole;
use App\Models\Address;
use App\Models\Category;
use App\Models\City;
use App\Models\Courier;
use App\Models\District;
use App\Models\Order;
use App\Models\Product;
use App\Models\Province;
use App\Models\ShippingMethod;
use App\Models\SubCategory;
use App\Models\Unit;
use App\Models\User;
use App\Models\BankAccount;
use DB;
use Log;

trait BackEndHelper {

	public function getUserAdminList() {
		return User::select('first_name','last_name','id', DB::raw('CONCAT (users.first_name, " ", users.last_name) AS userName'))
		->where('role','=', UserRole::ADMIN)
		->pluck('userName','id');
	}

	public function getProvinceList() {
		return Province::all()->pluck('name','id');
	}

	public function getCityList() {
		return City::all()->pluck('name','id');
	}

	public function getBankList() {
		return BankAccount::all()->pluck('bank','id');
	}

	public function getCourierList() {
		return Courier::all()->pluck('name','id');
	}

	public function getCategoryList() {
		return Category::where('is_active','=',1)->pluck('name','id');
	}

	public function getSubCategoryList() {
		return SubCategory::where('is_active','=',1)->pluck('name','id');
	}
	
	public function getUnitList() {
		return Unit::all()->pluck('name','id');
	}
	// public function getShippingMethodList() {
	// 	return ShippingMethod::join('couriers','shipping_methods.courier_id','=','couriers.id')
	// 	->join('cities','shipping_methods.city_id','=','cities.id')
	// 	->select('couriers.name','shipping_methods.id','cities.name', DB::raw('CONCAT (couriers.name, " - ", shipping_methods.type," - ", cities.name) AS courierName'))
	// 	->lists('courierName','shipping_methods.id');
	// }


	// public function getAddressList() {
	// 	return Address::join('users','addresses.user_id','=','users.id')
	// 	->join('cities','addresses.city_id','=','cities.id')
	// 	->select('addresses.id','users.first_name','users.last_name','addresses.address','cities.name', DB::raw('CONCAT (users.first_name, " ", users.last_name, " - ",addresses.address, " - ", cities.name) AS customerAddress'))
	// 	->lists('customerAddress','id');
	// }

	// public function getUserCustomerList() {
	// 	return User::select('first_name','last_name','id', DB::raw('CONCAT (users.first_name, " ", users.last_name) AS userName'))
	// 	->where('role_id','=',2)
	// 	->lists('userName','id');
	// }

	// public function getProductList() {
	// 	return Product::select('name','id', DB::raw('CONCAT (products.name, " - ", products.code) AS productName'))
	// 	->lists('productName','id');
	// }

	// public function getOrderList() {
	// 	return Order::join('addresses','orders.address_id','=','addresses.id') 
	// 	->select('orders.id','addresses.address','orders.order_number','orders.status', DB::raw('CONCAT(orders.order_number, " - ", addresses.address) AS orderNumber')) 
	// 	->where(function($query) {
	// 		return $query->where('orders.status','=',0)
	// 		->orWhere('orders.status','=',1)
	// 		->orWhere('orders.status','=',3);
	// 	})
	// 	->lists('orderNumber','id');
	// }

	// public function getDistrictList() {
	// 	return District::all()->lists('name','id');
	// }

	// public function getUnitList() {
	// 	return Unit::all()->lists('name','id');
	// }

}

?>
