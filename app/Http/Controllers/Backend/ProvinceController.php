<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\FileUploadHelper;
use App\Helpers\SessionHelper;
use App\Http\Controllers\Controller;
use App\Models\Province;
use App\Models\User;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Session;
use Validator;

class ProvinceController extends Controller
{
	protected $view = 'backend.province';
	protected $route = 'admin.provinces';

	public function index() {
		$province = Province::all();
		return view($this->view.'.index')
		->with('provinces', $province)
		->with('title', 'Province')
		->with('small_title', 'province information')
		->with('manageShipping', 'active root-level opened')
		->with('provinceList', 'active')
		->with('breadcrumbs', array('Province'));
	}
	
	public function newForm() {
		$province = new Province();
		return view($this->view.'.form')
		->with('method','POST')
		->with('title', 'Province')
		->with('small_title', 'province information')
		->with('manageShipping', 'active root-level opened')
		->with('provinceList', 'active')
		->with('listOfUser', $this->getUserAdminList() )
		->with('breadcrumbs', array('Province', 'Register'));

	}

	public function edit($province) {
		return view($this->view.'.form')
		->with('province',$province)
		->with('method', 'PUT')
		->with('title', 'Province')
		->with('small_title', 'province information')
		->with('manageShipping', 'active root-level opened')
		->with('listOfUser', $this->getUserAdminList() )
		->with('provinceList', 'active')
		->with('breadcrumbs', array('Province', 'Update'));
	}
	

	public function save(Request $request) {

		$this->validate($request, Province::addRules());

		try{

			$province = new Province();
			$province->name = $request->input('name');
			if(!$province->save()){
				return back()->withInput()->withErrors($province->errors());
			}
			SessionHelper::setMessage("Form succesfully created");
			return redirect()->route($this->route.'.index');

		} catch (\Exception $e) {
			DB::rollBack();
			\Log::error($e);
			return back()->withInput()->withErrors('Something wrong');
		}
	}


	public function update(Request $request, $province) {
		$this->validate($request, Province::editRules($province->id));

		try{

			$province->name = $request->input('name');
			if(!$province->save()){
				return back()->withInput()->withErrors($province->errors());
			}
			SessionHelper::setMessage("Form succesfully updated");
			return redirect()->route($this->route.'.index');
			
		} catch (\Exception $e) {
			DB::rollBack();
			\Log::error($e);
			return back()->withInput()->withErrors('Something wrong');
		}
	}

	public function destroy($province, Request $request) {
		$province->delete();
		if ($request->ajax() || $request->wantsJson()) {
			return response('ok.', 200);
		}
		SessionHelper::setMessage("Record Has Been Deleted!");
		return redirect()->route($this->route.'.index');
	}

}