<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\Enums\ShippingType;
use App\Helpers\FileUploadHelper;
use App\Helpers\SessionHelper;
use App\Http\Controllers\Controller;
use App\Models\ShippingMethod;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Session;
use Validator;

class ShippingMethodController extends Controller
{
	protected $view = 'backend.shipping-method';
	protected $route = 'admin.shipping.methods';

	public function index() {
		$shippingMethod = ShippingMethod::all();
		return view($this->view.'.index')
		->with('shippingMethods', $shippingMethod)
		->with('title', 'Shipping Method')
		->with('small_title', 'shipping method information')
		->with('manageShipping', 'active root-level opened')
		->with('shippingMethodList', 'active')
		->with('breadcrumbs', array('Shipping Method'));
	}
	
	public function newForm() {
		$shippingMethod = new ShippingMethod();
		return view($this->view.'.form')
		->with('shippingType', ShippingType::getArray())
		->with('method','POST')
		->with('title', 'Shipping Method')
		->with('small_title', 'shipping method information')
		->with('listOfCities', $this->getCityList() )
		->with('listOfCouriers', $this->getCourierList() )
		->with('manageShipping', 'active root-level opened')
		->with('shippingMethodList', 'active')
		->with('breadcrumbs', array('ShippingMethod', 'Register'));

	}

	public function edit($shippingMethod) {
		return view($this->view.'.form')
		->with('shippingType', ShippingType::getArray())
		->with('shippingMethod',$shippingMethod)
		->with('method', 'PUT')
		->with('title', 'Shipping Method')
		->with('listOfCouriers', $this->getCourierList() )
		->with('listOfCities', $this->getCityList() )
		->with('small_title', 'shipping method information')
		->with('manageShipping', 'active root-level opened')
		->with('shippingMethodList', 'active')
		->with('breadcrumbs', array('Shipping Method', 'Update'));
	}
	

	public function save(Request $request) {

		$this->validate($request, ShippingMethod::addRules());

		try{

			$shippingMethod = new ShippingMethod();
			$shippingMethod->courier_id = $request->input('courier_id');
			$shippingMethod->city_id = $request->input('city_id');
			$shippingMethod->name = $request->input('name');
			if($request->type == 'REG') {
				$shippingMethod->type = ShippingType::PAKET_REGULAR;
			} else {
				$shippingMethod->type = ShippingType::PAKET_YES;
			}
			$shippingMethod->fee = $request->input('fee');
			$shippingMethod->created_by = Auth::user()->id;
			if(!$shippingMethod->save()){
				return back()->withInput()->withErrors($shippingMethod->errors());
			}
			SessionHelper::setMessage("Form succesfully created");
			return redirect()->route($this->route.'.index');

		} catch (\Exception $e) {
			DB::rollBack();
			\Log::error($e);
			return back()->withInput()->withErrors('Something wrong');
		}
	}


	public function update(Request $request, $shippingMethod) {
		$this->validate($request, ShippingMethod::editRules($shippingMethod->id));

		try{

			$shippingMethod->courier_id = $request->input('courier_id');
			$shippingMethod->city_id = $request->input('city_id');
			$shippingMethod->name = $request->input('name');
			if($request->type == 'REG') {
				$shippingMethod->type = ShippingType::PAKET_REGULAR;
			} else {
				$shippingMethod->type = ShippingType::PAKET_YES;
			}
			$shippingMethod->fee = $request->input('fee');
			$shippingMethod->created_by = Auth::user()->id;
			if(!$shippingMethod->save()){
				return back()->withInput()->withErrors($shippingMethod->errors());
			}
			SessionHelper::setMessage("Form succesfully updated");
			return redirect()->route($this->route.'.index');
			
		} catch (\Exception $e) {
			DB::rollBack();
			\Log::error($e);
			return back()->withInput()->withErrors('Something wrong');
		}
	}

	public function destroy($shippingMethod, Request $request) {
		$shippingMethod->delete();
		if ($request->ajax() || $request->wantsJson()) {
			return response('ok.', 200);
		}
		SessionHelper::setMessage("Record Has Been Deleted!");
		return redirect()->route($this->route.'.index');
	}

}