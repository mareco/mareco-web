<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\Enums\UserRole;
use App\Helpers\FileUploadHelper;
use App\Helpers\SessionHelper;
use App\Http\Controllers\Controller;
use App\Models\User;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Session;
use Validator;
use Yajra\Datatables\Datatables;
use App\Helpers\ActivitylogHelpers;

class AdminController extends Controller
{
	protected $view = 'backend.admin';
	protected $route = 'admin.admins';

	public function index() {
		$user = User::where('role','=','admin')->get();
		return view($this->view.'.index')
		->with('users', $user)
		->with('title', 'Admin')
		->with('small_title', 'admin information')
		->with('manageAccount', 'active root-level opened')
		->with('adminList', 'active')
		->with('breadcrumbs', array('Admin'));
	}
	
	public function newForm() {
		$user = new User();
		return view($this->view.'.form')
		->with('method','POST')
		->with('title', 'Admin')
		->with('small_title', 'admin information')
		->with('manageAccount', 'active root-level opened')
		->with('adminList', 'active')
		->with('breadcrumbs', array('Admin', 'Register'))
		->with('disabled', '');

	}

	public function edit($user) {
		return view($this->view.'.form')
		->with('user',$user)
		->with('method', 'PUT')
		->with('title', 'Admin')
		->with('small_title', 'user information')
		->with('manageAccount', 'active root-level opened')
		->with('adminList', 'active')
		->with('breadcrumbs', array('Admin', 'Update'))
		->with('disabled', 'disabled');
	}
	

	public function save(Request $request) {
		$this->validate($request, User::addRules());

		try{

			$user = new User();
			$user->first_name = $request->input('first_name');
			$user->last_name = $request->input('last_name');
			$user->email = $request->input('email');
			$user->role = UserRole::ADMIN;
			$user->password = Hash::make($request->input('password'));
			$user->phone = $request->input('phone');
			if ($request->file('photo')) {
				$upload = FileUploadHelper::upload($request, 'photo', 'jpeg,bmp,png,jpg');
				$user->photo = $upload;
			}
			if(!$user->save()){
				return back()->withInput()->withErrors($user->errors());
			}

			$type = "Admin";
	        $action = "Create";
	        $description = Auth::User()->name . " has succesful create a user admin " . $user->first_name;
	        ActivitylogHelpers::generate($type,$action,$description);

			SessionHelper::setMessage("Form succesfully created");
			return redirect()->route($this->route.'.index');

		} catch (\Exception $e) {
			DB::rollBack();
			\Log::error($e);
			return back()->withInput()->withErrors('Something wrong');
		}
	}


	public function update(Request $request, $user) {
		$this->validate($request, User::editRules($user->id));

		try{

			$user->first_name = $request->input('first_name');
			$user->last_name = $request->input('last_name');
			$user->role = UserRole::ADMIN;
			$user->email = $request->input('email');
			$user->phone = $request->input('phone');
			if ($request->file('photo')) {
				$upload = FileUploadHelper::upload($request, 'photo', 'jpeg,bmp,png,jpg');
				$user->photo = $upload;
			}
			if(!$user->save()){
				return back()->withInput()->withErrors($user->errors());
			}

			$type = "Admin";
	        $action = "Update";
	        $description = Auth::User()->name . " has succesful update a user admin " . $user->first_name;
	        ActivitylogHelpers::generate($type,$action,$description);

			SessionHelper::setMessage("Form succesfully updated");
			return redirect()->route($this->route.'.index');
			
		} catch (\Exception $e) {
			DB::rollBack();
			\Log::error($e);
			return back()->withInput()->withErrors('Something wrong');
		}
	}

	public function destroy($user, Request $request) {
		$user->delete();
		if ($request->ajax() || $request->wantsJson()) {
			return response('ok.', 200);
		}
		SessionHelper::setMessage("Record Has Been Deleted!");
		return redirect()->route($this->route.'.index');
	}

	public function listAllAdmin() {
		$userList = User::where('role','=', UserRole::ADMIN)->where('id','<>',Auth::user()->id);
		return Datatables::of($userList)
		->addColumn('action', function ($model) {
			return '
			<a href="'.route('admin.admins.edit', ['user' => $model->id]).'"
				class="btn btn-info btn-outline btn-sm">
				<span class="fa fa-edit"></span> Edit
			</a>
			<a data-url="'.route('admin.admins.destroy', ['user' => $model->id]).'"
				data-toggle="modal" data-target=" #modalDelete" data-title="Confirmation" data-table-name="#admin-list" data-message="Would you like to delete this admin?" 
				class="btn btn-danger btn-outline btn-sm" >
				<span class="fa fa-trash-o"></span> Delete
			</a>';
		})
		->make(true);
	}

}