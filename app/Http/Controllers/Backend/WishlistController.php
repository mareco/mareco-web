<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\FileUploadHelper;
use App\Helpers\SessionHelper;
use App\Http\Controllers\Controller;
use App\Models\Wishlist;
use App\Models\User;
use App\Models\Variant;
use App\Models\Product;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Session;
use Validator;
use Yajra\Datatables\Datatables;

class WishlistController extends Controller
{
	protected $view = 'backend.wishlist';
	protected $route = 'admin.wishlists';

	public function index() {
		$wishlist = Wishlist::all();
		return view($this->view.'.index')
		->with('wishlists', $wishlist)
		->with('title', 'Wishlist')
		->with('small_title', 'wishlist information')
		->with('manageProduct', 'active root-level opened')
		->with('wishlistList', 'active')
		->with('breadcrumbs', array('Wishlist'));
	}

	public function getVariant(Request $request) {
		$user = User::where('email','=',$request->email)->first();
		$wishlist = Wishlist::where('user_id','=',$user->id)->get();
		$data = array();
		$dataProduct = array();
		for ($i = 0; $i < count($wishlist); $i++) {
			$variant = Variant::where('id','=',$wishlist[$i]->variant_id)->first();
			$product = Product::where('id','=',$variant->product_id)->first();
			array_push($data, $variant);
			array_push($dataProduct, $product);
		}
		return response()->json(['data' => $data, 'product' => $dataProduct]);
	}
	
	public function destroy($wishlist, Request $request) {
		$wishlist->delete();
		if ($request->ajax() || $request->wantsJson()) {
			return response('ok.', 200);
		}
		SessionHelper::setMessage("Form succesfully deleted");
		return redirect()->route($this->route.'.index');
	}

	public function listAllWishlist() {
		$wishlistList = Wishlist::query();
		// $wishlistList = DB::table('users')->select('id')->join('wishlists','user.id','=','id')->get();
		return Datatables::of($wishlistList)
		->addColumn('action', function ($model) {
			return '
			<a href=""
				class="btn btn-primary btn-outline btn-sm">
				<span class="fa fa-edit"></span> View
			</a>
			';
		})
		->editColumn('user_id', function ($model) { 
			return $model->user->first_name.' '.$model->user->last_name;
		})
		->editColumn('variant_id', function ($model) { 
			return $model->variant->product->name."\n".$model->variant->sku;
		})
		->make(true);
	}
}