<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\FcmHelper;
use App\Helpers\FileUploadHelper;
use App\Helpers\SessionHelper;
use App\Http\Controllers\Controller;
use App\Models\Installation;
use App\Models\Order;
use App\Models\Point;
use App\Models\User;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Session;
use Validator;

class PointController extends Controller
{
	protected $view = 'backend.point';
	protected $route = 'admin.points';

	public function index($user) {
		$orderHistory = Order::where('user_id','=', $user->id)->orderBy('created_at','desc')->paginate(10);
		$lifeTimeOrderByUser = DB::table('orders')->where('user_id','=', $user->id)->sum('grand_total');
		$pointHistory = Point::where('user_id','=', $user->id)->orderBy('created_at','desc')->paginate(10);
		$totalPoint = DB::table('points')->where('user_id','=', $user->id)->sum('amount');

		return view($this->view.'.index')
		->with('title', 'User')
		->with('orderHistory', $orderHistory)
		->with('totalPoint', $totalPoint)
		->with('user', $user)
		->with('pointHistory', $pointHistory)
		->with('lifeTimeOrderByUser', $lifeTimeOrderByUser)
		->with('small_title', 'user information')
		->with('manageAccount', 'active root-level opened')
		->with('userList', 'active')
		->with('breadcrumbs', array('User'));

	}

	public function newForm($user) {
		return view($this->view.'.form')
		->with('user', $user)
		->with('title', 'User')
		->with('small_title', 'user information')
		->with('manageAccount', 'active root-level opened')
		->with('userList', 'active')
		->with('breadcrumbs', array('User'));
	}


	public function save(Request $request, $user) {

		try{

			$point = new Point();
			$point->description = $request->input('description');
			$point->user_id = $user->id;
			if($request->addition != null) {
				$point->amount = $request->addition;
			}
			if($request->deduction != null) {
				$point->amount = -$request->deduction;
			}
			if(!$point->save()){
				return back()->withInput()->withErrors($point->errors());
			}

			$user = User::where('id','=',$user->id)->first();
			if($request->addition != null) {
				$user->point = $user->point+$request->addition;
			}
			if($request->deduction != null) {
				$user->point = $user->point-$request->deduction;
			}
			$user->save();

			$tokens = Installation::where('user_id','=', $user->id)
			->whereNotNull('device_token')
			->where('device_token','<>', '')
			->pluck('device_token')->toArray();

			$setting = [
			'title' => 'Mareco Store',
			'body' => 'Hi!'.$point->description.' bonus point untuk anda, sebesar '.$point->amount.' point',
			'sound' => 'default', 
			'data' => ['user' => $user->id],
			];

			$response = FcmHelper::pushNotification($tokens, $setting);
			SessionHelper::setMessage("Form succesfully created");
			return redirect()->route('admin.users.point.index',$user->id);

		} catch (\Exception $e) {
			DB::rollBack();
			\Log::error($e);
			return back()->withInput()->withErrors('Something wrong');
		}
	}

}