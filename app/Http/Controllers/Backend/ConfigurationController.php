<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\Enums\ProductStatus;
use App\Helpers\FileUploadHelper;
use App\Helpers\SessionHelper;
use App\Http\Controllers\Controller;
use App\Models\AboutUsLang;
use App\Models\Config;
use App\Models\Product;
use App\Models\Promotion;
use App\Models\Term;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Session;
use Validator;
use App\Helpers\ActivitylogHelpers;

class ConfigurationController extends Controller
{
	protected $view = 'backend.config';
	protected $route = 'admin.configs';

	public function newForm() {
		$configs = Config::all();
		if(count($configs) == 0 ) {
			$config = '';
		} else {
			$config = Config::first();
		}
		$promotion = Promotion::all();
		$promoProduct = Product::where('status','=', ProductStatus::NOW_SALE)->pluck('name','id');
		$enTerm = Term::where('lang','=','en')->first();
		$idTerm = Term::where('lang','=','id')->first();
		
		return view($this->view.'.form')
		->with('method','POST')
		->with('title', 'Config')
		->with('config' ,$config)
		->with('promotions', $promotion)
		->with('enTerm', $enTerm)
		->with('idTerm', $idTerm)
		->with('promoProduct', $promoProduct)
		->with('small_title', 'config information')
		->with('manageSystem', 'active root-level opened')
		->with('configList', 'active')
		->with('breadcrumbs', array('Config', 'Register'));

	}

	public function save(Request $request) {
		try{

			$config = new Config();
			// $config->admin_email = $request->input('admin_email');
			$config->about_us_content = $request->input('about_us_content');
			$config->about_us_email = $request->input('about_us_email');
			$config->about_us_title = $request->input('about_us_title');
			$config->about_us_phone = $request->input('about_us_phone');
			$config->about_us_mobile = $request->input('about_us_mobile');
			$config->facebook_account = $request->input('facebook_account');
			$config->instagram_account = $request->input('instagram_account');
			$config->twitter_account = $request->input('twitter_account');
			$config->storage_address = $request->input('storage_address');
			$config->storage_lat = $request->input('storage_lat');
			$config->storage_lng = $request->input('storage_lng');
			$config->cod_amount = $request->input('cod_amount');
			if(!$config->save()){
				return back()->withInput()->withErrors($config->errors());
			}

			$configLang = new AboutUsLang();
			$configLang->title = $request->input('about_us_title');
			$configLang->about_us_content = $request->input('about_us_content');
			$configLang->lang = 'en';
			$configLang->config_id = $config->id;
			$configLang->save();

			if ($request->title_lang) {
				$configLang = new AboutUsLang();
				$configLang->title = $request->input('title_lang');
				$configLang->about_us_content = $request->input('about_us_lang');
				$configLang->lang = 'id';
				$configLang->config_id = $config->id;
				$configLang->save();
			}

			$type = "Configuration";
	        $action = "Update";
	        $description = Auth::User()->name . " has succesful update a configuration";
	        ActivitylogHelpers::generate($type,$action,$description);

			SessionHelper::setMessage("Config Update!");
			return redirect()->route($this->route.'.new');

		} catch (\Exception $e) {
			DB::rollBack();
			\Log::error($e);
			return back()->withInput()->withErrors('Something Wrong');
		}
	}


	public function update(Request $request, $config) {
		$enConfig = AboutUsLang::where('lang','=','en')->first();
		$idConfig = AboutUsLang::where('lang','=','id')->first();

		try{
			// $config->admin_email = $request->input('admin_email');
			$config->about_us_content = $request->input('about_us_content');
			$config->about_us_email = $request->input('about_us_email');
			$config->about_us_title = $request->input('about_us_title');
			$config->about_us_phone = $request->input('about_us_phone');
			$config->about_us_mobile = $request->input('about_us_mobile');
			$config->facebook_account = $request->input('facebook_account');
			$config->instagram_account = $request->input('instagram_account');
			$config->twitter_account = $request->input('twitter_account');
			$config->storage_address = $request->input('storage_address');
			$config->storage_lat = $request->input('storage_lat');
			$config->storage_lng = $request->input('storage_lng');
			$config->cod_amount = $request->input('cod_amount');
			if(!$config->save()){
				return back()->withInput()->withErrors($config->errors());
			}

			if(!$enConfig) {
				$configLang = new AboutUsLang();
			} else {
				$configLang = $config->aboutUsLang('en');
			}
			$configLang->title = $request->input('about_us_title');
			$configLang->about_us_content = $request->input('about_us_content');
			$configLang->lang = 'en';
			$configLang->config_id = $config->id;
			$configLang->save();

			if(!$idConfig) {
				$configLang = new AboutUsLang();
			} else {
				$configLang = $config->aboutUsLang('id');
			}
			$configLang->title = $request->input('title_lang');
			$configLang->about_us_content = $request->input('about_us_lang');
			$configLang->lang = 'id';
			$configLang->config_id = $config->id;
			$configLang->save();

			// if($request->title_lang) {
			// 	if($config->aboutUsLang('id')){
			// 		$configLang = $config->configLang('id');
			// 		$configLang->title = $request->input('title_lang');
			// 		$configLang->about_us_content = $request->input('about_us_lang');
			// 	} else {
			// 		$configLang = new AboutUsLang();
			// 		$configLang->title = $request->input('title_lang');
			// 		$configLang->about_us_content = $request->input('about_us_lang');
			// 	}
			// 	$configLang->lang = 'id';
			// 	$configLang->config_id = $config->id;
			// 	$configLang->save();
			// }

			$type = "Configuration";
	        $action = "Update";
	        $description = Auth::User()->name . " has succesful update a configuration";
	        ActivitylogHelpers::generate($type,$action,$description);

			SessionHelper::setMessage("Config Update!");
			return redirect()->route($this->route.'.new');
			
		} catch (\Exception $e) {
			DB::rollBack();
			\Log::error($e);
			return back()->withInput()->withErrors('Something wrong');
		}
	}

	public function storePromotion(Request $request) {
		try {
			$allPromotion = Promotion::all();
			$total = 0;
			foreach ($allPromotion as $key) {
				if ($key->size == 1) {
					$total = $total + 1;
				} else if ($key->size == 0) {
					$total = $total + 4;
				}
			}
			if ($total ==  8) {
				SessionHelper::errorMessage("You cannot add another promotion, please delete another promotion to create a new one");
				return redirect('admin/configs');
			} else {
				if ($total > 4 && $request->size == 0) {
					SessionHelper::errorMessage("The size is not suitable with your data, please choose a small size");
					return redirect('admin/configs');
				}
			}

			$promotion = new Promotion();
			$promotion->title = $request->input('title');
			$promotion->product_id = $request->input('product_id');
			$promotion->content = $request->input('content');
			if ($request->file('photo')) {
				$upload = FileUploadHelper::upload($request, 'photo', 'jpeg,bmp,png,jpg');
				$promotion->photo = $upload;
			}
			$promotion->size = $request->input('size');
			if(!$promotion->save()){
				return back()->withInput()->withErrors($promotion->errors());
			}
			SessionHelper::setMessage("Promotion Update!");
			return redirect()->route($this->route.'.new');
		} catch (\Exception $e) {
			DB::rollBack();
			\Log::error($e);
			return back()->withInput()->withErrors('Something wrong');
		}
	}

	public function destroyPromotion($promotion, Request $request) {
		$promotion->delete();
		if ($request->ajax() || $request->wantsJson()) {
			return response('ok.', 200);
		}
		SessionHelper::setMessage("Promotion deleted!",'warning');
		return redirect()->route($this->route.'.new');
	}

	public function storeTermCondition(Request $request) {
		$enTerm = Term::where('lang','=', 'en')->first();
		try {
			if(!$enTerm) {
				$enTerm = new Term;
			}
			$enTerm->lang = 'en';
			$enTerm->message = $request->message;
			$enTerm->save();

			$type = "Term and Conditions";
	        $action = "Update";
	        $description = Auth::User()->name . " has succesful update a Term and Conditions";
	        ActivitylogHelpers::generate($type,$action,$description);

			SessionHelper::setMessage("Terms & Conditions Updated!");
			return redirect('admin/configs');
		} catch (\Exception $e) {
			DB::rollBack();
			\Log::error($e);
			return back()->withInput()->withErrors('This action has been terminated, Please contact system administrator!');
		}
	}

	public function storeTermConditionId(Request $request) {
		$idTerm = Term::where('lang','=', 'id')->first();
		try {
			if(!$idTerm) {
				$idTerm = new Term;
			}
			$idTerm->lang = 'id';
			$idTerm->message = $request->alternate_message;
			$idTerm->save();
			SessionHelper::setMessage("Terms & Conditions Updated!");
			return redirect('admin/configs');
		} catch (\Exception $e) {
			DB::rollBack();
			\Log::error($e);
			return back()->withInput()->withErrors('This action has been terminated, Please contact system administrator!');
		}
	}

	public function termDestroy($term, Request $request) {
		$term->delete();
		if ($request->ajax() || $request->wantsJson()) {
			return response('ok.', 200);
		}
		SessionHelper::setMessage("Terms & Conditions deleted!",'warning');
		return redirect()->route($this->route.'.new');
	}

}