<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\FileUploadHelper;
use App\Helpers\SessionHelper;
use App\Http\Controllers\Controller;
use App\Models\FeatureBrand;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Session;
use Validator;
use Yajra\Datatables\Datatables;
use App\Helpers\ActivitylogHelpers;

class FeatureBrandController extends Controller
{
	protected $view = 'backend.brand';
	protected $route = 'admin.brands';

	public function index() {
		$pictures = FeatureBrand::all();
		return view($this->view.'.index')
		->with('pictures', $pictures)
		->with('title', 'Feature Brand')
		->with('manageSystem', 'active root-level opened')
		->with('small_title', 'slider information')
		->with('brandList', 'active root-level opened')
		->with('breadcrumbs', array('Brand'));
	}

	public function save() {
		$input = Input::all();
		$file = Input::file('file');
		$filename = md5(Carbon::now().rand(1111111,9999999)).'-'.str_replace(" ", "-", $file->getClientOriginalName() );
		$picture = new FeatureBrand();
		$folder = 'uploads';
		$directory = public_path().'/'. $folder;
		$picture->feature_url = $folder.'/'.$filename;
		$file->move($directory, $filename);
		$picture->save();

		$type = "Brand";
        $action = "Create";
        $description = Auth::User()->name . " has succesful create a brand";
        ActivitylogHelpers::generate($type,$action,$description);

		SessionHelper::setMessage("FeatureBrand(s) added");
	}

	public function delete(Request $request){
		$type = "Brand";
        $action = "Delete";
        $description = Auth::User()->name . " has succesful delete a brand";
        ActivitylogHelpers::generate($type,$action,$description);

		$picture = FeatureBrand::findOrFail($request->id);
		$picture->delete();
	}

}
