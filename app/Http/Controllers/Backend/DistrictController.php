<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\FileUploadHelper;
use App\Helpers\SessionHelper;
use App\Http\Controllers\Controller;
use App\Models\District;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Session;
use Validator;

class DistrictController extends Controller
{
	protected $view = 'backend.district';
	protected $route = 'admin.districts';

	public function index() {
		$district = District::all();
		return view($this->view.'.index')
		->with('districts', $district)
		->with('title', 'Sub district')
		->with('small_title', 'district information')
		->with('manageShipping', 'active root-level opened')
		->with('districtList', 'active')
		->with('breadcrumbs', array('District'));
	}
	
	public function newForm() {
		$district = new District();
		return view($this->view.'.form')
		->with('method','POST')
		->with('title', 'Sub district')
		->with('small_title', 'district information')
		->with('listOfCities', $this->getCityList() )
		->with('manageShipping', 'active root-level opened')
		->with('districtList', 'active')
		->with('breadcrumbs', array('District', 'Register'));

	}

	public function edit($district) {
		return view($this->view.'.form')
		->with('district',$district)
		->with('method', 'PUT')
		->with('title', 'Sub district')
		->with('listOfCities', $this->getCityList() )
		->with('small_title', 'district information')
		->with('manageShipping', 'active root-level opened')
		->with('districtList', 'active')
		->with('breadcrumbs', array('District', 'Update'));
	}
	

	public function save(Request $request) {

		$this->validate($request, District::addRules());

		try{

			$district = new District();
			$district->name = $request->input('name');
			$district->city_id = $request->input('city_id');
			if(!$district->save()){
				return back()->withInput()->withErrors($district->errors());
			}
			SessionHelper::setMessage("Form succesfully created");
			return redirect()->route($this->route.'.index');

		} catch (\Exception $e) {
			DB::rollBack();
			\Log::error($e);
			return back()->withInput()->withErrors('Something wrong');
		}
	}


	public function update(Request $request, $district) {
		$this->validate($request, District::editRules($district->id));

		try{

			$district->name = $request->input('name');
			$district->city_id = $request->input('city_id');
			if(!$district->save()){
				return back()->withInput()->withErrors($district->errors());
			}
			SessionHelper::setMessage("Form succesfully updated");
			return redirect()->route($this->route.'.index');
			
		} catch (\Exception $e) {
			DB::rollBack();
			\Log::error($e);
			return back()->withInput()->withErrors('Something wrong');
		}
	}

	public function destroy($district, Request $request) {
		$district->delete();
		if ($request->ajax() || $request->wantsJson()) {
			return response('ok.', 200);
		}
		SessionHelper::setMessage("Record Has Been Deleted!");
		return redirect()->route($this->route.'.index');
	}

}