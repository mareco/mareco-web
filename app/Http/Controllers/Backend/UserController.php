<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\Enums\UserRole;
use App\Helpers\FileUploadHelper;
use App\Helpers\SessionHelper;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserService;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Session;
use Validator;
use Yajra\Datatables\Datatables;
use App\Helpers\ActivitylogHelpers;

class UserController extends Controller
{
	protected $view = 'backend.user';
	protected $route = 'admin.users';

	public function index() {
		$user = User::where('role','=','user')->get();
	
		return view($this->view.'.index')
		->with('users', $user)
		->with('title', 'User')
		->with('small_title', 'user information')
		->with('manageAccount', 'active root-level opened')
		->with('userList', 'active')
		->with('breadcrumbs', array('User'));
	}
	
	public function newForm() {
		$user = new User();
		return view($this->view.'.form')
		->with('method','POST')
		->with('title', 'User')
		->with('small_title', 'user information')
		->with('manageAccount', 'active root-level opened')
		->with('userList', 'active')
		->with('breadcrumbs', array('User', 'Register'));

	}

	public function edit($user) {
		return view($this->view.'.form')
		->with('user',$user)
		->with('method', 'PUT')
		->with('title', 'User')
		->with('small_title', 'user information')
		->with('manageAccount', 'active root-level opened')
		->with('userList', 'active')
		->with('breadcrumbs', array('User', 'Update'));
	}
	

	public function save(Request $request) {
		$this->validate($request, User::addRules());
		try{

			$user = new User();
			$user->first_name = $request->input('first_name');
			$user->last_name = $request->input('last_name');
			$user->email = $request->input('email');
			$user->role = UserRole::USER;
			$user->password = Hash::make($request->input('password'));
			$user->phone = $request->input('phone');
			// $user->point = $request->input('point');
			if ($request->file('photo')) {
				$upload = FileUploadHelper::upload($request, 'photo', 'jpeg,bmp,png,jpg');
				$user->photo = $upload;
			}
			if(!$user->save()){
				return back()->withInput()->withErrors($user->errors());
			}

			$type = "User";
	        $action = "Create";
	        $description = Auth::User()->name . " has succesful create a user " . $user->first_name . ' ' . $user->last_name;
	        ActivitylogHelpers::generate($type,$action,$description);

			SessionHelper::setMessage("Form succesfully created");
			return redirect()->route($this->route.'.index');

		} catch (\Exception $e) {
			DB::rollBack();
			\Log::error($e);
			return back()->withInput()->withErrors('Something wrong');
		}
	}


	public function update(Request $request, $user) {
		$this->validate($request, User::editRules($user->id));

		try{

			$user->first_name = $request->input('first_name');
			$user->last_name = $request->input('last_name');
			$user->role = UserRole::USER;
			$user->email = $request->input('email');
			$user->phone = $request->input('phone');
			// $user->point = $request->input('point');
			if ($request->file('photo')) {
				$upload = FileUploadHelper::upload($request, 'photo', 'jpeg,bmp,png,jpg');
				$user->photo = $upload;
			}
			if(!$user->save()){
				return back()->withInput()->withErrors($user->errors());
			}

			$type = "User";
	        $action = "Update";
	        $description = Auth::User()->name . " has succesful update a user " . $user->first_name . ' ' . $user->last_name;
	        ActivitylogHelpers::generate($type,$action,$description);

			SessionHelper::setMessage("Form succesfully updated");
			return redirect()->route($this->route.'.index');
			
		} catch (\Exception $e) {
			DB::rollBack();
			\Log::error($e);
			return back()->withInput()->withErrors('Something wrong');
		}
	}

	public function destroy($user, Request $request) {
		$type = "User";
        $action = "Delete";
        $description = Auth::User()->name . " has succesful delete a user " . $user->first_name . ' ' . $user->last_name;
        ActivitylogHelpers::generate($type,$action,$description);
        
		$user->delete();
		if ($request->ajax() || $request->wantsJson()) {
			return response('ok.', 200);
		}

		SessionHelper::setMessage("Record Has Been Deleted!");
		return redirect()->route($this->route.'.index');
	}

	public function listAllUser() {
		$userList = User::where('role','=', UserRole::USER)->get();
		return Datatables::of($userList)
		->addColumn('action', function ($model) {
			$value = $model->wishlists->count();
				if($value != 0) {
					return '
					<a href="'.route('admin.users.point.index', ['user' => $model->id]).'"
						class="btn btn-green btn-outline btn-sm">
						<span class="fa fa-edit"></span> Edit
					</a>
					<a class="btn btn-primary btn-outline btn-sm" onclick="getDataVariant(event)">
						<span class="fa fa-edit"></span> Wish List
					</a>
					<a href="'.route('admin.users.edit', ['user' => $model->id]).'"
						class="btn btn-info btn-outline btn-sm">
						<span class="fa fa-edit"></span> View
					</a>
					<a data-url="'.route('admin.users.destroy', ['user' => $model->id]).'"
						data-toggle="modal" data-target=" #modalDelete" data-title="Confirmation" data-table-name="#user-list" data-message="Would you like to delete this user?" 
						class="btn btn-danger btn-outline btn-sm" >
						<span class="fa fa-trash-o"></span> Delete
					</a>';
				} else {
					return '
					<a href="'.route('admin.users.point.index', ['user' => $model->id]).'"
						class="btn btn-green btn-outline btn-sm">
						<span class="fa fa-edit"></span> Edit
					</a>
					<a class="btn btn-primary btn-outline btn-sm" onclick="getDataVariant(event)">
						<span class="fa fa-edit"></span> Wish List
					</a>
					<a href="'.route('admin.users.edit', ['user' => $model->id]).'"
						class="btn btn-info btn-outline btn-sm">
						<span class="fa fa-edit"></span> View
					</a>
					<a data-url="'.route('admin.users.destroy', ['user' => $model->id]).'"
						data-toggle="modal" data-target=" #modalDelete" data-title="Confirmation" data-table-name="#user-list" data-message="Would you like to delete this user?" 
						class="btn btn-danger btn-outline btn-sm" >
						<span class="fa fa-trash-o"></span> Delete
					</a>';
				}
		})
		->make(true);
	}

	public function getInTouch() {
		$ct = UserService::all();
		return view('backend.user.feedback')
		->with('users', $ct)
		->with('title', 'User')
		->with('small_title', 'user feedback')
		->with('manageAccount', 'active root-level opened')
		->with('userService', 'active')
		->with('breadcrumbs', array('User'));
	}

}