<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\FileUploadHelper;
use App\Helpers\SessionHelper;
use App\Http\Controllers\Controller;
use App\Models\BankAccount;
use App\Models\User;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Session;
use Validator;
use Yajra\Datatables\Datatables;
use App\Helpers\ActivitylogHelpers;

class BankAccountController extends Controller
{
	protected $view = 'backend.bank_account';
	protected $route = 'admin.banks';

	public function index() {
		$bank = BankAccount::all();
		return view($this->view.'.index')
		->with('banks', $bank)
		->with('title', 'Bank Account')
		->with('small_title', 'bank account information')
		->with('manageSystem', 'active root-level opened')
		->with('bankList', 'active')
		->with('breadcrumbs', array('Bank Account'));
	}
	
	public function newForm() {
		$bank = new User();
		return view($this->view.'.form')
		->with('method','POST')
		->with('title', 'Bank Account')
		->with('small_title', 'bank account information')
		->with('manageAccount', 'active root-level opened')
		->with('bankList', 'active')
		->with('listOfUser', $this->getUserAdminList() )
		->with('breadcrumbs', array('Bank Account', 'Register'));

	}

	public function edit($bank) {
		if(count($bank) > 1) {
			foreach ($bank as $value){
				if($value->is_active == 1) {
					$result = 'checked';
				} else {
					$result = '';
				}
			}
		} else if($bank->is_active == 1) {
			$result = 'checked';
		} else {
			$result = '';
		}
		return view($this->view.'.form')
		->with('bank',$bank)
		->with('result', $result)
		->with('method', 'PUT')
		->with('title', 'Bank Account')
		->with('small_title', 'bank information')
		->with('manageAccount', 'active root-level opened')
		->with('listOfUser', $this->getUserAdminList() )
		->with('bankList', 'active')
		->with('breadcrumbs', array('Bank Account', 'Update'));
	}
	

	public function save(Request $request) {

		$this->validate($request, BankAccount::addRules());

		try{

			$bank = new BankAccount();
			$bank->user_id = Auth::user()->id;
			$bank->bank = $request->input('bank');
			$bank->is_active = 1;
			$bank->account_number = $request->input('account_number');
			$bank->account_name = $request->input('account_name');
			if ($request->file('logo')) {
				$upload = FileUploadHelper::upload($request, 'logo', 'jpeg,bmp,png,jpg');
				$bank->logo = $upload;
			}
			if(!$bank->save()){
				return back()->withInput()->withErrors($bank->errors());
			}

			$type = "Bank";
	        $action = "Create";
	        $description = Auth::User()->name . " has succesful create a bank " . $bank->bank;
	        ActivitylogHelpers::generate($type,$action,$description);

			SessionHelper::setMessage("Form succesfully created");
			return redirect()->route($this->route.'.index');

		} catch (\Exception $e) {
			DB::rollBack();
			\Log::error($e);
			return back()->withInput()->withErrors('Something wrong');
		}
	}


	public function update(Request $request, $bank) {
		$this->validate($request, BankAccount::editRules($bank->id));

		try{

			$bank->user_id = Auth::user()->id;
			$bank->bank = $request->input('bank');
			$bank->is_active = (isset($request->is_active) ? 1 : 0);
			$bank->account_number = $request->input('account_number');
			$bank->account_name = $request->input('account_name');
			if ($request->file('logo')) {
				$upload = FileUploadHelper::upload($request, 'logo', 'jpeg,bmp,png,jpg');
				$bank->logo = $upload;
			}
			if(!$bank->save()){
				return back()->withInput()->withErrors($bank->errors());
			}

			$type = "Bank";
	        $action = "Update";
	        $description = Auth::User()->name . " has succesful Update a bank " . $bank->bank;
	        ActivitylogHelpers::generate($type,$action,$description);

			SessionHelper::setMessage("Form succesfully updated");
			return redirect()->route($this->route.'.index');
			
		} catch (\Exception $e) {
			DB::rollBack();
			\Log::error($e);
			return back()->withInput()->withErrors('Something wrong');
		}
	}

	public function destroy($bank, Request $request) {
		$type = "Bank";
        $action = "Delete";
        $description = Auth::User()->name . " has succesful delete a bank " . $bank->bank;
        ActivitylogHelpers::generate($type,$action,$description);

		$bank->delete();
		if ($request->ajax() || $request->wantsJson()) {
			return response('ok.', 200);
		}
		SessionHelper::setMessage("Record Has Been Deleted!");
		return redirect()->route($this->route.'.index');
	}

	public function listAllBank() {
		$bankList = BankAccount::query();
		return Datatables::of($bankList)
		->addColumn('action', function ($model) {
			return '
			<a href="'.route('admin.banks.edit', ['bank' => $model->id]).'"
				class="btn btn-info btn-outline btn-sm">
				<span class="fa fa-edit"></span> Edit
			</a>
			<a data-url="'.route('admin.banks.destroy', ['bank' => $model->id]).'"
				data-toggle="modal" data-target=" #modalDelete" data-title="Confirmation" data-table-name="#bank-list" data-message="Would you like to delete this bank?" 
				class="btn btn-danger btn-outline btn-sm" >
				<span class="fa fa-trash-o"></span> Delete
			</a>';
		})
		->make(true);
	}

}