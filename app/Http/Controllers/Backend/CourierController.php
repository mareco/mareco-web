<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\FileUploadHelper;
use App\Helpers\SessionHelper;
use App\Http\Controllers\Controller;
use App\Models\Courier;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Session;
use Validator;

class CourierController extends Controller
{
	protected $view = 'backend.courier';
	protected $route = 'admin.couriers';

	public function index() {
		$courier = Courier::all();
		return view($this->view.'.index')
		->with('couriers', $courier)
		->with('title', 'Courier')
		->with('small_title', 'courier information')
		->with('manageShipping', 'active root-level opened')
		->with('courierList', 'active')
		->with('breadcrumbs', array('Courier'));
	}
	
	public function newForm() {
		$courier = new Courier();
		return view($this->view.'.form')
		->with('method','POST')
		->with('title', 'Courier')
		->with('small_title', 'courier information')
		->with('manageShipping', 'active root-level opened')
		->with('courierList', 'active')
		->with('breadcrumbs', array('Courier', 'Register'));

	}

	public function edit($courier) {
		return view($this->view.'.form')
		->with('courier',$courier)
		->with('method', 'PUT')
		->with('title', 'Courier')
		->with('small_title', 'courier information')
		->with('manageShipping', 'active root-level opened')
		->with('courierList', 'active')
		->with('breadcrumbs', array('Courier', 'Update'));
	}
	

	public function save(Request $request) {

		$this->validate($request, Courier::addRules());

		try{

			$courier = new Courier();
			$courier->name = $request->input('name');
			if ($request->file('logo')) {
				$upload = FileUploadHelper::upload($request, 'logo', 'jpeg,bmp,png,jpg');
				$courier->logo = $upload;
			}
			if(!$courier->save()){
				return back()->withInput()->withErrors($courier->errors());
			}
			SessionHelper::setMessage("Form succesfully created");
			return redirect()->route($this->route.'.index');

		} catch (\Exception $e) {
			DB::rollBack();
			\Log::error($e);
			return back()->withInput()->withErrors('Something wrong');
		}
	}


	public function update(Request $request, $courier) {
		$this->validate($request, Courier::editRules($courier->id));

		try{

			$courier->name = $request->input('name');
			if ($request->file('logo')) {
				$upload = FileUploadHelper::upload($request, 'logo', 'jpeg,bmp,png,jpg');
				$courier->logo = $upload;
			}
			if(!$courier->save()){
				return back()->withInput()->withErrors($courier->errors());
			}
			SessionHelper::setMessage("Form succesfully updated");
			return redirect()->route($this->route.'.index');
			
		} catch (\Exception $e) {
			DB::rollBack();
			\Log::error($e);
			return back()->withInput()->withErrors('Something wrong');
		}
	}

	public function destroy($courier, Request $request) {
		$courier->delete();
		if ($request->ajax() || $request->wantsJson()) {
			return response('ok.', 200);
		}
		SessionHelper::setMessage("Record Has Been Deleted!");
		return redirect()->route($this->route.'.index');
	}

}