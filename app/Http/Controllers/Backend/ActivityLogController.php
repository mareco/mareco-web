<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\FileUploadHelper;
use App\Helpers\SessionHelper;
use App\Http\Controllers\Controller;
use App\Models\Activity_log;
use App\Models\User;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Session;
use Validator;
use Yajra\Datatables\Datatables;
use App\Helpers\ActivitylogHelpers;

class ActivityLogController extends Controller
{
	protected $view = 'backend.activity_log';

	public function index() {
		$activity_logs = Activity_log::all();
		return view($this->view.'.index')
		->with('activity_logs', $activity_logs)
		->with('title', 'Activity Log')
		->with('small_title', 'Activity Log information')
		->with('manageSystem', 'active root-level opened');
	}

	public function listAllLog() {
		$activity_log = Activity_log::query();
		return Datatables::of($activity_log)
		->editColumn('user_id', function ($model) { 
			return $model->user->first_name.' '.$model->user->last_name;
		})
		->make(true);
	}

}