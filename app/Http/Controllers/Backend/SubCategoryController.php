<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\FileUploadHelper;
use App\Helpers\SessionHelper;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\SubCategory;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Session;
use Validator;
use Yajra\Datatables\Datatables;

class SubCategoryController extends Controller
{
	protected $view = 'backend.sub-category';
	protected $route = 'admin.sub-categories';

	public function index() {
		$categoryList = Category::all()->pluck('name','id');
		$subCategory = SubCategory::all();
		return view($this->view.'.index')
		->with('subCategories', $subCategory)
		->with('title', 'Sub Category')
		->with('listOfCategories', $this->getCategoryList() )
		->with('small_title', 'sub category information')
		->with('manageProduct', 'active root-level opened')
        ->with('categoryList', $categoryList)
		->with('subCategoryList', 'active')
		->with('breadcrumbs', array('Sub Category'));
	}
	
	public function newForm() {
		$subCategory = new SubCategory();
		return view($this->view.'.form')
		->with('method','POST')
		->with('title', 'Sub Category')
		->with('listOfCategories', $this->getCategoryList() )
		->with('small_title', 'sub category information')
		->with('manageProduct', 'active root-level opened')
		->with('subCategoryList', 'active')
		->with('breadcrumbs', array('Sub Category', 'Register'));

	}

	public function edit($subCategory) {
		if(count($subCategory) > 1) {
			foreach ($subCategory as $value){
				if($value->is_active == 1) {
					$result = 'checked';
				} else {
					$result = '';
				}
			}
		} else if($subCategory->is_active == 1) {
			$result = 'checked';
		} else {
			$result = '';
		}
		return view($this->view.'.form')
		->with('result', $result)
		->with('subCategory',$subCategory)
		->with('method', 'PUT')
		->with('listOfCategories', $this->getCategoryList() )
		->with('title', 'Sub Category')
		->with('small_title', 'sub category information')
		->with('manageProduct', 'active root-level opened')
		->with('subCategoryList', 'active')
		->with('breadcrumbs', array('Sub Category', 'Update'));
	}
	

	public function save(Request $request) {

		$this->validate($request, SubCategory::addRules());

		try{

			$subCategory = new SubCategory();
			$subCategory->category_id = $request->input('category_id');
			$subCategory->name = $request->input('name');
			$subCategory->is_active = 1;
			$subCategory->slug_url = str_slug($subCategory->name, '-');
			if ($request->file('photo')) {
				$upload = FileUploadHelper::upload($request, 'photo', 'jpeg,bmp,png,jpg');
				$subCategory->photo = $upload;
			}
			if(!$subCategory->save()){
				return back()->withInput()->withErrors($subCategory->errors());
			}
			SessionHelper::setMessage("Form succesfully created");
			return redirect()->route($this->route.'.index');

		} catch (\Exception $e) {
			DB::rollBack();
			\Log::error($e);
			return back()->withInput()->withErrors('Something wrong');
		}
	}


	public function update(Request $request, $subCategory) {
		$this->validate($request, SubCategory::editRules($subCategory->id));

		try{

			$subCategory->category_id = $request->input('category_id');
			$subCategory->is_active = (isset($request->is_active) ? 1 : 0);
			$subCategory->name = $request->input('name');
			$subCategory->slug_url = str_slug($subCategory->name, '-');
			if ($request->file('photo')) {
				$upload = FileUploadHelper::upload($request, 'photo', 'jpeg,bmp,png,jpg');
				$subCategory->photo = $upload;
			}
			if(!$subCategory->save()){
				return back()->withInput()->withErrors($subCategory->errors());
			}
			SessionHelper::setMessage("Form succesfully updated");
			return redirect()->route($this->route.'.index');
			
		} catch (\Exception $e) {
			DB::rollBack();
			\Log::error($e);
			return back()->withInput()->withErrors('Something wrong');
		}
	}

	public function destroy($subCategory, Request $request) {
		$subCategory->delete();
		if ($request->ajax() || $request->wantsJson()) {
			return response('ok.', 200);
		}
		SessionHelper::setMessage("Record Has Been Deleted!");
		return redirect()->route($this->route.'.index');
	}

	public function listAllSubCategory() {
		$subCategoryList = SubCategory::query();
		return Datatables::of($subCategoryList)
		->addColumn('action', function ($model) {
			return '
			<a href="'.route('admin.sub-categories.edit', ['subCategory' => $model->id]).'"
				class="btn btn-info btn-outline btn-sm">
				<span class="fa fa-edit"></span> Edit
			</a>
			<a data-url="'.route('admin.sub-categories.destroy', ['subCategory' => $model->id]).'"
				data-toggle="modal" data-target=" #modalDelete" data-title="Confirmation" data-table-name="#subCategory-list" data-message="Would you like to delete this sub category?" 
				class="btn btn-danger btn-outline btn-sm" >
				<span class="fa fa-trash-o"></span> Delete
			</a>';
		})->editColumn('category_id', function ($model) { 
			return $model->category->name;
		})
		->make(true);
	}
}