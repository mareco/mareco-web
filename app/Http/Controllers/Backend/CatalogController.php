<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\Enums\ProductStatus;
use App\Helpers\Enums\SaleType;
use App\Helpers\FcmHelper;
use App\Helpers\FileUploadHelper;
use App\Helpers\SessionHelper;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Installation;
use App\Models\Product;
use App\Models\User;
use App\Models\Variant;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Session;
use Validator;
use Yajra\Datatables\Facades\Datatables;

class CatalogController extends Controller
{
	protected $view = 'backend.catalog';
	protected $route = 'admin.catalogs';

	public function index() {
		$categoryList = Category::all()->pluck('name','id');
		$product = Product::where('status','<>',ProductStatus::OUT_OF_STOCK)->get();
		return view($this->view.'.index')
		->with('products', $product)
		->with('productStatus', ProductStatus::getArray())
		->with('saleType', SaleType::getArray())
		->with('categoryList', $categoryList)
		->with('title', 'Catalog')
		->with('small_title', 'catalog information')
		->with('manageSystem', 'active root-level opened')
		->with('catalogList', 'active')
		->with('breadcrumbs', array('Catalog'));
	}

	public function settingCatalog(Request $request) {
		$productStatus = ProductStatus::getArray();
		$saleType = SaleType::getArray();
		$product = Product::where('code', '=', $request->code)->first();
		$variant = Variant::where('id','=',$product->id)->first();

		return response()->json(['productStatus' => $productStatus, 'saleType' => $saleType, 'variant' => $variant, 'product' => $product]);
	}

	public function update(Request $request) {
		$data = Product::find($request->id);
		$variant = Variant::where('product_id','=',$request->id)->get();
		$data->status = $request->statusSetting;
		if ($request->saleAmount == 0) {
			$data->sale_amount = 0;
			$data->sale_type = 0;
		} else {
			$data->sale_type = $request->saleType;
			$data->sale_amount = $request->saleAmount;
		}
		foreach ($variant as $value) {
			if ($request->saleAmount == null) {
				$value->discount_price = 0;
				$value->is_discount = 0;
			} else {
				if ($request->saleType == 0) {
					$value->discount_price = $value->price - ($value->price * $request->saleAmount / 100);
				} else {
					$value->discount_price = $value->price - $request->saleAmount;
				}	
				$value->is_discount = 1;

				$users = User::where('promotion','=',1)->get();
				if($users) {
					foreach($users as $user) {
						$tokens = Installation::where('user_id','=', $user->id)
						->whereNotNull('device_token')
						->where('device_token','<>', '')
						->pluck('device_token')->toArray();

						$setting = [
						'title' => 'Mareco Store',
						'body' => 'Promotion !',
						'sound' => 'default', 
						];

						$response = FcmHelper::pushNotification($tokens, $setting);
					}
				}

			}
			$value->save();
		}
		$data->save();
		
		

		return response()->json(['success' => 'ok']);
	}
	
	public function listAllCatalog() {
		$ProductList = Product::with('productVariants')->get();
		return Datatables::of($ProductList)
		->addColumn('action', function ($model) {
			return '
			<a
			class="btn btn-danger btn-outline btn-sm" onclick="settingCatalog(event)">
			Setting
		</a>
		';
	})
		->addColumn('category_name', function($model) {
			return $model->category->name;
		})
		->addColumn('status', function($model) {
			return ProductStatus::getString($model->status);
		})
		->addColumn('variant_price', function($model) {
			foreach ($model->productVariants as $variant) {
				$getFirstData = $variant->first();
				$data = $getFirstData->price;
				\Log::info($getFirstData);
			}
			// \Log::info(number_format($data,2,'.',','));
			return $data;
		})
		->make(true);
	}



}