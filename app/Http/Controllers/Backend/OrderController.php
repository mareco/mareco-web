<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\Enums\OrderStatus;
use App\Helpers\Enums\PaymentType;
use App\Helpers\Enums\TypeShipmentHelper;
use App\Helpers\FileUploadHelper;
use App\Helpers\SessionHelper;
use App\Http\Controllers\Controller;
use App\Models\Address;
use App\Models\BankAccount;
use App\Models\CashOnDelivery;
use App\Models\Config;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Payment;
use App\Models\Point;
use App\Models\Product;
use App\Models\Shipment;
use App\Models\ShippingMethod;
use App\Models\User;
use App\Models\Variant;
use Auth;
use Carbon\Carbon;
use Cart;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Mail;
use PDF;
use Session;
use Steevenz\Rajaongkir;
use Validator;
use Yajra\Datatables\Datatables;
use App\Helpers\ActivitylogHelpers;

class OrderController extends Controller
{
	protected $view = 'backend.order';
	protected $route = 'admin.orders';

	public function index() {
		$orders = Order::all();
		return view($this->view.'.index')
		->with('orders', $orders)
		->with('title', 'Order')
		->with('status', OrderStatus::getArray())
		->with('shipmentType', TypeShipmentHelper::getArray())
		->with('paymentType', PaymentType::getArray())
		->with('small_title', 'order information')
		->with('manageOrder', 'active root-level opened')
		->with('breadcrumbs', array('Order'));
	}

	public function newForm() {
		$config['api_key'] = '56950ca5aa637a0c5efa1b266e28f160';
		$config['account_type'] = 'pro';
		$rajaongkir = new Rajaongkir($config);
		$province = $rajaongkir->get_provinces();

		$order = new Order();
		$shippingMethod = ShippingMethod::first();
		$user = User::where('role','=','user')->get();
		$bank = BankAccount::all();
		$product = Product::all();
		$cart = Cart::content();
		$totalWeight = 0;
		$totalPrice = 0;
		foreach(Cart::content() as $row) {
			$totalWeight = $totalWeight + $row->options->weight * $row->qty;
			$totalPrice = $totalPrice + $row->price * $row->qty;
		}

		return view($this->view.'.form')
		->with('order', $order)
		->with('shippingMethod', $shippingMethod)
		->with('user', $user)
		->with('bank', $bank)
		->with('product', $product)
		->with('cart', $cart)
		->with('province', $province)
		->with('totalWeight', $totalWeight)
		->with('totalPrice', $totalPrice)
		->with('method','POST')
		->with('title', 'Order')
		->with('small_title', 'order information')
		->with('listOfUnits', $this->getUnitList() )
		->with('manageOrder', 'active root-level opened')
		->with('orderList', 'active')
		->with('breadcrumbs', array('Order', 'Register'));
	}

	public function getAddress(Request $request) {
		$address = Address::where('user_id','=',$request->user_id)->get();
		return response()->json(['address' => $address, 'count' => count($address)]);
	}

	public function getSize(Request $request) {
		$variant_product = Variant::where('product_id','=',$request->product_id)->get();
		$arrSize = [];
		for ($i = 0; $i < count($variant_product); $i++) {
			if (count($arrSize) == 0) {
				array_push($arrSize, $variant_product[$i]->size);
			} else {
				for ($n = 0; $n < count($arrSize); $n++) {
					if ($variant_product[$i]->size == $arrSize[$n]) {
						$push = false;
					} else {
						$push = true;
					}
				}
				if ($push == true) {
					array_push($arrSize, $variant_product[$i]->size);
				}
			}
		}
		return response()->json(['variant_product' => $arrSize, 'count' => count($variant_product)]);
	}

	public function getColor(Request $request) {
		$variant_product = Variant::where('product_id','=',$request->product_id)->where('size','=',$request->size_id)->get();
		return response()->json(['variant_product' => $variant_product, 'count' => count($variant_product)]);
	}

	public function getDataProduct(Request $request) {
		$variant_product = Variant::where('product_id','=',$request->product_id)->where('size','=',$request->size_id)->first();
		return response()->json(['variant_product' => $variant_product]);
	}

	public function addCart(Request $request) {
		$product = Product::where('id','=',$request->product_id)->first();
		$variant = Variant::where('product_id','=',$request->product_id)->where('size','=',$request->size_id)->first();
		$message = '';
		if ($variant->qty < $request->qty) {
			$message = '<div class="alert alert-warning">Your quantity is not enough</div>';
		} else {
			Cart::add(array('id' => $request->product_id,'name' => $product->name,'qty' => $request->qty,'price' => $request->price,'options' => ['size' => $request->size_id,'variant' => $variant->size,'weight' => $request->weight,'sku' => $variant->sku,'color' => $request->color]));
		}
		$cart = Cart::content();
		return response()->json(['cart' => $cart, 'message' => $message]);
	}

	public function deleteCart($rowId) {
		Cart::remove($rowId);
		return redirect('/admin/orders/new');
	}

	public static function getProduct($id) {
		$product = Product::where('id','=',$id)->first();
		return $product;
	}

	public function getCity(Request $request) {
		$config['api_key'] = '56950ca5aa637a0c5efa1b266e28f160';
		$config['account_type'] = 'pro';
		$rajaongkir = new Rajaongkir($config);

		if ($request->type == "International") {
			$city = $rajaongkir->get_international_origins($request->province_id);
		}
		else {
			$city = $rajaongkir->get_cities($request->province_id);
		}
		return response()->json(['city' => $city,'count' => count($city)]);
	}

	public function getDistrict(Request $request) {
		$config['api_key'] = '56950ca5aa637a0c5efa1b266e28f160';
		$config['account_type'] = 'pro';
		$rajaongkir = new Rajaongkir($config);
		$city = $rajaongkir->get_city($request->city_id);
		$postal_code = $city->postal_code;
		$district = $rajaongkir->get_subdistricts($request->city_id);
		return response()->json(['district' => $district,'count' => count($district),'postal_code' => $postal_code]);
	}

	public function getCost(Request $request) {
		$config['api_key'] = '56950ca5aa637a0c5efa1b266e28f160';
		$config['account_type'] = 'pro';
		$rajaongkir = new Rajaongkir($config);
		if ($request->weight == null) {
			$dataCart = array();
			$totalWeight = 0;
			if ($request->type = 'admin') {
				$dataCart = array();
				$totalWeight = 0;
				foreach(Cart::content() as $row) {
					array_push($dataCart,$row);
					$totalWeight = $totalWeight + $row->options->weight * $row->qty;
				}	
			}
			foreach(Cart::content() as $row) {
				array_push($dataCart,$row);
				$totalWeight = $totalWeight + $row->options->weight * $row->qty;
			}
		} else {
			$totalWeight = $request->weight;
		}
		if ($request->district_id == null) {
			$address = Address::where('id','=',$request->address_id)->first();
			$request->city_id = $address->city;
			$request->district_id = $address->district;
		}
		$costJNE = $rajaongkir->get_cost(['city' => 48], ['subdistrict' => $request->district_id], $totalWeight, 'jne');
		$costPOS = $rajaongkir->get_cost(['city' => 48], ['subdistrict' => $request->district_id], $totalWeight, 'pos');
		$costTIKI = $rajaongkir->get_cost(['city' => 48], ['subdistrict' => $request->district_id], $totalWeight, 'tiki');

		return response()->json(['costJNE' => $costJNE,'costPOS' => $costPOS,'costTIKI' => $costTIKI,'countJNE' => count($costJNE[0]->costs),'countPOS' => count($costPOS[0]->costs),'countTIKI' => count(count($costTIKI[0]->costs))]);
	}

	public function save(Request $request) {
		if ($request->user_id == 'none') {
			SessionHelper::errorMessage("Please select the user");
			return redirect('/admin/orders/new');
		}
		if ($request->cod_address == '') {
			if ($request->address_id == 'Please Select' && $request->serviceType = '') {
				SessionHelper::errorMessage("Please select the shipment method");
				return redirect('/admin/orders/new');
			}
			if ($request->paymentRadio == null) {
				SessionHelper::errorMessage("Please select the payment method");
				return redirect('/admin/orders/new');
			}
		}
		$cart = Cart::content();
		$allOrder = Order::all();
		$grand_total = 0;
		$address_id = null;
		if (count($allOrder) == 0) {
			$randomNumber = str_pad(1, 5, '0', STR_PAD_LEFT);
		} else {
			$number = Order::orderBy('order_number','desc')->value('order_number') + 1;
			$randomNumber = str_pad($number, 5, '0', STR_PAD_LEFT);;
		}

		if ($request->cod_address != '') {
			$cod = new CashOnDelivery();
			$cod->user_address = $request->cod_address;
			$cod->rate = $request->rate;
			$cod->user_id = Auth::user()->id;
			$cod->save();
		} else {
			if ($request->address_id == "none") {
				$address = new Address();
				$address->state = "Indonesia";
				$address->city = $request->city;
				$address->province = $request->province;
				$address->district = $request->district;
				$address->user_id = $request->user_id;
				$address->address = $request->street;
				$address->postal_code = $request->postal_code;
				$address->save();
				$address_id = $address->id;
			} else {
				$address_id = $request->address_id;
			}
		}
		$user = User::findOrFail($request->user_id);
		try {
			DB::beginTransaction();

			if ($request->paymentRadio == 'bank') {
				$payment_type = 1;
			} else {
				$payment_type = 0;   
			}

			foreach($cart as $row){
				$grand_total = $row->price + $grand_total;
			}

			if ($request->cod_address != '') {
				$order = new Order();
				$order->order_number = $randomNumber;
				$order->shipment_type = TypeShipmentHelper::getString($request->shippingRadio);
				$order->shipment_fee = $request->rate;
				$order->grand_total = $grand_total;
				$order->status = 0;
				$order->payment_type = 2;
				$order->cod_address = $request->cod_address;
				$order->cod_lat = $request->lat;
				$order->cod_lng = $request->lng;
				$order->user_id = $request->user_id;
			} else {
				$order = new Order();
				$order->order_number = $randomNumber;
				$order->shipment_type = TypeShipmentHelper::getString($request->shippingRadio);
				$order->shipment_service = $request->serviceType;
				$order->shipment_fee = $request->serviceFee;
				$order->payment_type = $payment_type;
				if ($address_id != null) {
					$order->address_id = $address_id;
				} else {
					$order->address_id = $request->address_id;
				}
				$order->grand_total = $grand_total;
				$order->status = 0;
				$order->user_id = $request->user_id;
			}

			if (!$order->save()) {
				throw new ValidationException($order->errors());
			}
			
			// $this->saveOrderDetail($order, $request);
			
			if (count($cart) != 0) {
				foreach($cart as $row) {
					$detail = new OrderDetail();
					$detail->product_id = $row->id;
					$detail->order_id = $order->id;
					$detail->size = $row->options->variant;
					$detail->qty = $row->qty;
					$detail->price = $row->price;
					$detail->amount = $row->price * $row->qty;
					$detail->weight = $row->options->weight;
					$detail->sku = $row->options->sku;
					$detail->color = $row->options->color;

					$variant = Variant::where('product_id','=',$row->id)->first();
					$variant->qty = $variant->qty - $row->qty;
					$variant->save();
					if (!$detail->save()) {
						throw new ValidationException($detail->errors());
					}
				}
			}
			Cart::destroy();

			DB::commit();
			SessionHelper::setMessage("Form succesfully created");
			return redirect('/admin/orders');

		} catch (ValidationException $e) {
			DB::rollBack();
			return $this->sendErrorWhenFailed($e->errors);

		} catch (Exception $e) {
			DB::rollBack();
		}
	}

	// public function edit($order) {
	// 	return view($this->view.'.form')
	// 	->with('order',$order)
	// 	->with('method', 'PUT')
	// 	->with('title', 'Order')
	// 	->with('small_title', 'order information')
	// 	->with('listOfSubCategories', $this->getSubCategoryList() )
	// 	->with('listOfUnits', $this->getUnitList() )
	// 	->with('manageOrder', 'active root-level opened')
	// 	->with('orderList', 'active')
	// 	->with('breadcrumbs', array('Order', 'Update'));
	// }


	// public function save(Request $request) {

	// 	$this->validate($request, Order::addRules());

	// 	try{
	// 		$order = new Order();
	// 		$order->created_by = Auth::user()->id;
	// 		$order->sub_category_id = $request->input('sub_category_id');

	// 		$order->name = $request->input('name');
	// 		$order->slug_url = str_slug($order->name, '-');
	// 		$order->code = $request->input('code');
	// 		$order->description = $request->input('description');
	// 		if(!$order->save()){
	// 			return back()->withInput()->withErrors($order->errors());
	// 		}

	// 		$this->saveOrderVariant($order, $request);
	// 		SessionHelper::setMessage("Form succesfully created");
	// 		return redirect()->route($this->route.'.index');
	// 	} catch (\Exception $e) {
	// 		DB::rollBack();
	// 		\Log::error($e);
	// 		return back()->withInput()->withErrors('Fail to submit order, Please try again later.');
	// 	}
	// }

	// public function saveOrderVariant(Order $order, Request $request) {
	// 	foreach($request->orderVariant as $orderVariant){
	// 		$variant = new Variant($orderVariant);
	// 		$variant->sku = $orderVariant['sku'];
	// 		$variant->qty = $orderVariant['qty'];
	// 		$variant->size = $orderVariant['size'];
	// 		$variant->unit_id = $orderVariant['unit_id'];
	// 		$variant->color = $orderVariant['color'];
	// 		$variant->price = $orderVariant['price'];
	// 		if (!$order->orderVariants()->save($variant)) {
	// 			throw new ValidationException($variant->errors());
	// 		}
	// 	}
	// 	return redirect()->route($this->route.'.index');
	// }


	// public function update(Request $request, $order) {
	// 	$this->validate($request, Order::editRules($order->id));

	// 	try{

	// 		$order->sub_category_id = $request->input('sub_category_id');

	// 		$order->name = $request->input('name');
	// 		$order->slug_url = str_slug($order->name, '-');
	// 		$order->code = $request->input('code');
	// 		$order->description = $request->input('description');
	// 		if(!$order->save()){
	// 			return back()->withInput()->withErrors($order->errors());
	// 		}

	// 		foreach ($request->orderVariant as $value) {
	// 			if (!isset($value['id'])) {
	// 				$variant = new Variant();
	// 				$variant->sku = $value['sku'];
	// 				$variant->qty = $value['qty'];
	// 				$variant->size = $value['size'];
	// 				$variant->unit_id = $value['unit_id'];
	// 				$variant->color = $value['color'];
	// 				$variant->price = $value['price'];
	// 			} else {
	// 				$variant = Variant::find($value['id']);
	// 			}
	// 			// $this->updatePayrollDetail($payrollDetails, $value, $payroll);
	// 			$this->updateOrderVariant($variant, $value, $order);
	// 		}
	// 		SessionHelper::setMessage("Form succesfully updated");
	// 		return redirect()->route($this->route.'.index');

	// 	} catch (\Exception $e) {
	// 		DB::rollBack();
	// 		\Log::error($e);
	// 		return back()->withInput()->withErrors('Something wrong');
	// 	}
	// }

	// public function updateOrderVariant(Variant $variant, $value, $order) {
 //        if (isset($value['order_id'])) {
 //            $variant->order_id = $value['order_id'];
 //        }
 //        $variant->fill($value);
 //        if (!$order->orderVariants()->save($variant)) {
 //            throw new ValidationException($variant->errors());
 //        }
 //    }

	public function destroy($order, Request $request) {
		$type = "Order";
        $action = "Delete";
        $description = Auth::User()->name . " has succesful delete a order " . $order->order_number;
        ActivitylogHelpers::generate($type,$action,$description);

		$order->delete();
		if ($request->ajax() || $request->wantsJson()) {
			return response('ok.', 200);
		}
		SessionHelper::setMessage("Record Has Been Deleted!");
		return redirect()->route($this->route.'.index');
	}

	public function viewOrderInformation($id) {
		$config['api_key'] = '56950ca5aa637a0c5efa1b266e28f160';
		$config['account_type'] = 'pro';
		$rajaongkir = new Rajaongkir($config);
		$order = Order::where('id','=', $id)->first();
		if ($order->address_id != null) {
			$address = array(
				'province' => $rajaongkir->get_province($order->address->province)['province'],
				'city' => $rajaongkir->get_city($order->address->city)['city_name'],
				'district' => $rajaongkir->get_subdistrict($order->address->district)['subdistrict_name'],
				);
		} else {
			$address = null;
		}
		$existShipment = Shipment::where('order_id','=',$order->id)->first();
		$shipment = new Shipment();
		return view('backend.order.information')
		->with('shipment', $shipment)
		->with('existShipment', $existShipment)
		// ->with('getOrderList', $this->getOrderList())
		->with('id', $id)
		->with('order', $order)
		->with('title', 'Order Information')
		->with('order', $order)
		->with('address', $address)
		->with('small_title', 'order information');
	}

	public function changeStatus(Request $request) {
		$order = Order::where('id','=',$request->id)->first();
		$point = $order->point;
		$orderDetail = OrderDetail::where('order_id','=',$order->id)->get();
		$user = User::where('id','=',$order->user_id)->first();
		$config = Config::first();
		$email = $config->about_us_email;

		$subTotal = 0;
		for ($i = 0; $i < count($orderDetail); $i++) {
			$subTotal = $subTotal + ($orderDetail[$i]->qty * $orderDetail[$i]->price);
		}

		if ($order->cod_address == null) {
			$address = Address::where('id','=',$order->address_id)->first();
			$data = array('user' => $user,'order' => $order, 'orderDetail' => $orderDetail, 'subTotal' => $subTotal, 'address' => $address, 'point' => $point,'email' => $email);
		}

		if ($request->type == "confirmPayment") {	
			$order->status = 1;

			$type = "Order";
	        $action = "Confirmation";
	        $description = Auth::User()->name . " has succesful confirm a order " . $order->order_number;
	        ActivitylogHelpers::generate($type,$action,$description);

			if ($order->cod_address == null) {
				$pdf = PDF::loadView('frontend.pdf.confirm_payment', $data);
				Mail::send('frontend.pdf.confirm_payment', $data, function ($message) use($pdf, $data) {
					$emailCustomer = $data['user']->email;

					$message->from('iskandarjonny65@gmail.com')->subject('Invoice order no. '.$data['order']->order_number.' !');
					$message->attachData($pdf->output(), "invoice.pdf");
					$message->to($emailCustomer);

				});
			}
		}

		if ($request->type == "cancelOrder") {
			$type = "Order";
	        $action = "Cancel";
	        $description = Auth::User()->name . " has succesful cancel a order " . $order->order_number;
	        ActivitylogHelpers::generate($type,$action,$description);

			$orderDetail = OrderDetail::where('order_id','=',$order->id)->get();
			foreach ($orderDetail as $value) {
				if ($value->size == 'null') {
					$variant = Variant::where('product_id','=',$value->product_id)->where('size','=',null)->first();
				} else {
					$variant = Variant::where('product_id','=',$value->product_id)->where('size','=',$value->size)->first();
				}
				$variant->qty = $variant->qty + $value->qty;
				$variant->save();
			}
			$order->status = 5;
			$order->remark = $request->remark;

			$userPoint = User::find($order->user_id);
			$userPoint->point = $userPoint->point + $order->point;
			$userPoint->save();

			if ($order->cod_address == null) {
				Mail::send('frontend.pdf.cancel_order', $data, function ($message) use($data) {
					$emailCustomer = $data['user']->email;
					$message->from('iskandarjonny65@gmail.com')->subject('Cancel order no. '.$data['order']->order_number.' !');
					$message->to($emailCustomer);

				});
			}
		}
		if ($request->type == "completed") {
			$type = "Order";
	        $action = "Completed";
	        $description = Auth::User()->name . " has succesful completed a order " . $order->order_number;
	        ActivitylogHelpers::generate($type,$action,$description);

			$order->status = 7;
			$totalPoint = 0;
			// if ($order->status == OrderStatus::COMPLETED || $order->status == OrderStatus::DELIVERED) {
   			$userPoint = User::find($order->user_id);
  			$orderDetail = OrderDetail::where('order_id','=',$order->id)->get();
  			foreach ($orderDetail as $value) {
  				$product = Product::where('id','=',$value->product_id)->first();
  				$userPoint->point = $userPoint->point + $product->point;
  				$totalPoint += $product->point;
  			}
   			$point = new Point();
   			$point->user_id = $order->user_id;
   			$point->amount = $totalPoint;
   			$point->description = 'Purchase Order No. '.$order->order_number.' got';
   			$point->save();
  			$userPoint->save();
       		// }
			if ($order->cod_address == null) {
				Mail::send('frontend.pdf.complete_order', $data, function ($message) use($data) {
					$emailCustomer = $data['user']->email;
					$message->from('iskandarjonny65@gmail.com')->subject('Completed order no. '.$data['order']->order_number.' !');
					$message->to($emailCustomer);
				});
			}
		}

		$order->save();
		return response()->json(['message' => 'success']);
	}

	public function addShipment(Request $request) {
		$config = Config::first();
		$email = $config->about_us_email;
		$shipment = new Shipment();
		$shipment->order_id = $request->id;
		$shipment->shipping_date = $request->shipping_date;
		$shipment->tracking_number = $request->tracking_number;
		$shipment->created_by = Auth::user()->id;
		$shipment->save();

		$order = Order::where('id','=',$request->id)->first();
		$order->status = 2;

		$type = "Shipment";
        $action = "Create";
        $description = Auth::User()->name . " has succesful create a shipment for " . $order->order_number;
        ActivitylogHelpers::generate($type,$action,$description);

		$user = User::where('id','=',$order->user_id)->first();  

		$data = array('user' => $user, 'order' => $order, 'shipment' => $shipment, 'email' => $email);

		Mail::send('frontend.pdf.tracking_number', $data, function ($message) use ($data)
		{
			$emailCustomer = $data['user']->email;
			$message->from('iskandarjonny65@gmail.com')->subject('Your shipment for order no. '.$data['order']->order_number.' !');
			$message->to($emailCustomer);
		});

		$order->save();
		return response()->json(['message' => 'success']);
	}

	public function listAllOrder() {
		$orderList = Order::with('user','orderDetails')->get();
		return Datatables::of($orderList)
		->addColumn('action', function ($model) {
			return '
			<a href="'.route('admin.orders.view', ['order' => $model->id]).'"
				class="btn btn-info btn-outline btn-sm">
				<span class="fa fa-edit"></span> View Order Information
			</a>
			<a data-url="'.route('admin.orders.destroy', ['order' => $model->id]).'"
				data-toggle="modal" data-target=" #modalDelete" data-title="Confirmation" data-table-name="#order-list" data-message="Would you like to delete this order?"
				class="btn btn-danger btn-outline btn-sm" >
				<span class="fa fa-trash-o"></span> Delete
			</a>';
		})->editColumn('user_id', function ($model) { 
			return $model->user->first_name.' '.$model->user->last_name;
		})->editColumn('status', function ($model) { 
			return OrderStatus::getString($model->status);
		})->editColumn('shipment_type', function ($model) { 
			return TypeShipmentHelper::getString($model->shipment_type);
		})->editColumn('payment_type', function ($model) { 
			return PaymentType::getString($model->payment_type);
		})->editColumn('product_id', function ($model) { 
			$product = [];
			foreach ($model->orderDetails as $value) {
				array_push($product, ' '.$value->product->name);
			}
			return $product;
		})
		->make(true);
	}
}
