<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\FileUploadHelper;
use App\Helpers\SessionHelper;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Picture;
use App\Models\Slider;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Session;
use Validator;
use Yajra\Datatables\Datatables;
use App\Helpers\ActivitylogHelpers;

class SliderController extends Controller
{
	protected $view = 'backend.slider';
	protected $route = 'admin.sliders';

	public function index() {
		$slider = Slider::all();
		$pictures = Picture::all();
		$categories = Category::all()->pluck('name','id');
		return view($this->view.'.index')
		->with('sliders', $slider)
		->with('pictures', $pictures)
		->with('listOfCategory', $categories)
		->with('title', 'Slider')
		->with('manageSystem', 'active root-level opened')
		->with('small_title', 'slider information')
		->with('sliderList', 'active root-level opened')
		->with('breadcrumbs', array('Slider'));
	}

	public function save() {
		$category = Category::first();
		$input = Input::all();
		$file = Input::file('file');
		$filename = md5(Carbon::now().rand(1111111,9999999)).'-'.str_replace(" ", "-", $file->getClientOriginalName() );
		$picture = new Picture();
		$folder = 'uploads';
		$directory = public_path().'/'. $folder;
		$picture->url = $folder.'/'.$filename;
		$file->move($directory, $filename);
		$picture->category_id = $category->id;
		$picture->save();

		$type = "Slider";
        $action = "Create";
        $description = Auth::User()->name . " has succesful create a slider for category " . $category->name;
        ActivitylogHelpers::generate($type,$action,$description);

		SessionHelper::setMessage("Picture(s) added to slider");
	}

	public function delete(Request $request){
		$picture = Picture::findOrFail($request->id);
		$category = Category::where('id','=',$picture->category_id)->first();

		$type = "Slider";
        $action = "Delete";
        $description = Auth::User()->name . " has succesful delete a slider for category " . $category->name;
        ActivitylogHelpers::generate($type,$action,$description);


		$picture->delete();
	}

	public function update(Request $request){
		$active = Picture::where('is_first','=',1)->first();
		if($active){
			$active->is_first = 0;
			$active->save();
		}
		$first = Picture::find($request->id);
		$first->is_first = 1;
		$first->save();
	}

	public function updateCategory(Request $request, $picture){
		$picture = Picture::find($picture);
		$picture->category_id = $request->input('category_id');
		$picture->save();
		$category = Category::where('id','=',$request->input('category_id'))->first();

		$type = "Slider";
        $action = "Update";
        $description = Auth::User()->name . " has succesful update a slider for category " . $category->name;
        ActivitylogHelpers::generate($type,$action,$description);

		SessionHelper::setMessage("Category added to slider");
		return redirect()->route($this->route.'.index');
	}
}
