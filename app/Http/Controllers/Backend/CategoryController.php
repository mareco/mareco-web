<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\FileUploadHelper;
use App\Helpers\SessionHelper;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\CategoryLang;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Session;
use Validator;
use Yajra\Datatables\Datatables;
use App\Helpers\ActivitylogHelpers;

class CategoryController extends Controller
{
	protected $view = 'backend.category';
	protected $route = 'admin.categories';

	public function index() {
		$category = Category::all();
		return view($this->view.'.index')
		->with('categories', $category)
		->with('title', 'Category')
		->with('small_title', 'category information')
		->with('manageProduct', 'active root-level opened')
		->with('categoryList', 'active')
		->with('breadcrumbs', array('Category'));
	}
	
	public function newForm() {
		$category = new Category();
		return view($this->view.'.form')
		->with('method','POST')
		->with('title', 'Category')
		->with('small_title', 'category information')
		->with('manageProduct', 'active root-level opened')
		->with('categoryList', 'active')
		->with('breadcrumbs', array('Category', 'Register'));

	}

	public function edit($category) {
		if(count($category) > 1) {
			foreach ($category as $value){
				if($value->is_active == 1) {
					$result = 'checked';
				} else {
					$result = '';
				}
			}
		} else if($category->is_active == 1) {
			$result = 'checked';
		} else {
			$result = '';
		}
		return view($this->view.'.form')
		->with('result', $result)
		->with('category',$category)
		->with('method', 'PUT')
		->with('title', 'Category')
		->with('small_title', 'category information')
		->with('manageProduct', 'active root-level opened')
		->with('categoryList', 'active')
		->with('breadcrumbs', array('Category', 'Update'));
	}
	

	public function save(Request $request) {

		try{

			$category = new Category();
			$category->name = $request->input('name');
			$category->slug_url = str_slug($category->name, '-');
			$category->is_active = 1;
			$category->description = $request->input('description');
			if ($request->file('photo')) {
				$upload = FileUploadHelper::upload($request, 'photo', 'jpeg,bmp,png,jpg');
				$category->photo = $upload;
			}
			if(!$category->save()){
				return back()->withInput()->withErrors($category->errors());
			}

			$categoryLang = new CategoryLang();
			$categoryLang->name = $request->input('name');
			$categoryLang->description = $request->input('description');
			$categoryLang->lang = 'en';
			$categoryLang->category_id = $category->id;
			$categoryLang->save();

			if ($request->category_name) {
				$categoryLang = new CategoryLang();
				$categoryLang->name = $request->input('category_name');
				$categoryLang->description = $request->input('category_description');
				$categoryLang->lang = 'id';
				$categoryLang->category_id = $category->id;
				$categoryLang->save();
			}

			$type = "Category";
	        $action = "Create";
	        $description = Auth::User()->name . " has succesful create a category " . $category->name;
	        ActivitylogHelpers::generate($type,$action,$description);

			SessionHelper::setMessage("Form succesfully created");
			return redirect()->route($this->route.'.index');

		} catch (\Exception $e) {
			DB::rollBack();
			\Log::error($e);
			return back()->withInput()->withErrors('Something wrong');
		}
	}


	public function update(Request $request, $category) {
		$this->validate($request, Category::editRules($category->id));

		try{

			$category->name = $request->input('name');
			$category->slug_url = str_slug($category->name, '-');
			$category->is_active = (isset($request->is_active) ? 1 : 0);
			$category->description = $request->input('description');
			if ($request->file('photo')) {
				$upload = FileUploadHelper::upload($request, 'photo', 'jpeg,bmp,png,jpg');
				$category->photo = $upload;
			}
			if(!$category->save()){
				return back()->withInput()->withErrors($category->errors());
			}

			if($request->category_name) {
				if($category->categoryLang('id')){
					$categoryLang = $category->categoryLang('id');
					$categoryLang->name = $request->input('category_name');
					$categoryLang->description = $request->input('category_description');
				} else {
					$categoryLang = new CategoryLang();
					$categoryLang->name = $request->input('category_name');
					$categoryLang->description = $request->input('category_description');
				}
				$categoryLang->lang = 'id';
				$categoryLang->category_id = $category->id;
				$categoryLang->save();
			}

			$type = "Category";
	        $action = "Update";
	        $description = Auth::User()->name . " has succesful update a category " . $category->name;
	        ActivitylogHelpers::generate($type,$action,$description);

			SessionHelper::setMessage("Form succesfully updated");
			return redirect()->route($this->route.'.index');
			
		} catch (\Exception $e) {
			DB::rollBack();
			\Log::error($e);
			return back()->withInput()->withErrors('Something wrong');
		}
	}

	public function destroy($category, Request $request) {
		$type = "Category";
        $action = "Delete";
        $description = Auth::User()->name . " has succesful delete a category " . $category->name;
        ActivitylogHelpers::generate($type,$action,$description);

		$category->categoryLangs()->delete();
		$category->delete();

		if ($request->ajax() || $request->wantsJson()) {
			return response('ok.', 200);
		}
		SessionHelper::setMessage("Form succesfully deleted");
		return redirect()->route($this->route.'.index');
	}

	public function listAllCategory() {
		$categoryList = Category::query();
		return Datatables::of($categoryList)
		->addColumn('action', function ($model) {
			return '
			<a href="'.route('admin.categories.edit', ['category' => $model->id]).'"
				class="btn btn-info btn-outline btn-sm">
				<span class="fa fa-edit"></span> Edit
			</a>
			<a data-url="'.route('admin.categories.destroy', ['category' => $model->id]).'"
				data-toggle="modal" data-target=" #modalDelete" data-title="Confirmation" data-table-name="#category-list" data-message="Would you like to delete this category?" 
				class="btn btn-danger btn-outline btn-sm" >
				<span class="fa fa-trash-o"></span> Delete
			</a>';
		})
		->make(true);
	}
}