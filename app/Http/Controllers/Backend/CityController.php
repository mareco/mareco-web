<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\FileUploadHelper;
use App\Helpers\SessionHelper;
use App\Http\Controllers\Controller;
use App\Models\City;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Session;
use Validator;

class CityController extends Controller
{
	protected $view = 'backend.city';
	protected $route = 'admin.cities';

	public function index() {
		$city = City::all();
		return view($this->view.'.index')
		->with('cities', $city)
		->with('title', 'City')
		->with('small_title', 'city information')
		->with('manageShipping', 'active root-level opened')
		->with('cityList', 'active')
		->with('breadcrumbs', array('City'));
	}
	
	public function newForm() {
		$city = new City();
		return view($this->view.'.form')
		->with('method','POST')
		->with('title', 'City')
		->with('small_title', 'city information')
		->with('listOfProvinces', $this->getProvinceList() )
		->with('manageShipping', 'active root-level opened')
		->with('cityList', 'active')
		->with('breadcrumbs', array('City', 'Register'));

	}

	public function edit($city) {
		return view($this->view.'.form')
		->with('city',$city)
		->with('method', 'PUT')
		->with('title', 'City')
		->with('listOfProvinces', $this->getProvinceList() )
		->with('small_title', 'city information')
		->with('manageShipping', 'active root-level opened')
		->with('cityList', 'active')
		->with('breadcrumbs', array('City', 'Update'));
	}
	

	public function save(Request $request) {

		$this->validate($request, City::addRules());

		try{

			$city = new City();
			$city->name = $request->input('name');
			$city->province_id = $request->input('province_id');
			if(!$city->save()){
				return back()->withInput()->withErrors($city->errors());
			}
			SessionHelper::setMessage("Form succesfully created");
			return redirect()->route($this->route.'.index');

		} catch (\Exception $e) {
			DB::rollBack();
			\Log::error($e);
			return back()->withInput()->withErrors('Something wrong');
		}
	}


	public function update(Request $request, $city) {
		$this->validate($request, City::editRules($city->id));

		try{

			$city->name = $request->input('name');
			$city->province_id = $request->input('province_id');
			if(!$city->save()){
				return back()->withInput()->withErrors($city->errors());
			}
			SessionHelper::setMessage("Form succesfully updated");
			return redirect()->route($this->route.'.index');
			
		} catch (\Exception $e) {
			DB::rollBack();
			\Log::error($e);
			return back()->withInput()->withErrors('Something wrong');
		}
	}

	public function destroy($city, Request $request) {
		$city->delete();
		if ($request->ajax() || $request->wantsJson()) {
			return response('ok.', 200);
		}
		SessionHelper::setMessage("Record Has Been Deleted!");
		return redirect()->route($this->route.'.index');
	}

}