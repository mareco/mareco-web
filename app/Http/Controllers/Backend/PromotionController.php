<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\Enums\ProductStatus;
use App\Helpers\Enums\PromotionType;
use App\Helpers\FcmHelper;
use App\Helpers\FileUploadHelper;
use App\Helpers\SessionHelper;
use App\Http\Controllers\Controller;
use App\Models\Installation;
use App\Models\Product;
use App\Models\ProductPromotion;
use App\Models\Promotion;
use App\Models\User;
use App\Models\Variant;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Session;
use Validator;
use App\Helpers\ActivitylogHelpers;

class PromotionController extends Controller
{
	protected $view = 'backend.promotion';
	protected $route = 'admin.promotions';

	public function index() {
		
		$promotion = Promotion::all();
		$promoProduct = Product::where('status','<>', ProductStatus::OUT_OF_STOCK)->pluck('name','id');
		
		return view($this->view.'.form')
		->with('method','POST')
		->with('title', 'Promotion')
		->with('promotions', $promotion)
		->with('promoProduct', $promoProduct)
		->with('promotionType', PromotionType::getArray())
		->with('small_title', 'promotion information')
		->with('manageSystem', 'active root-level opened')
		->with('promotionList', 'active')
		->with('breadcrumbs', array('Promotion', 'Register'));

	}


	public function storePromotion(Request $request) {
		try {
			$allPromotion = ProductPromotion::all();
			$total = 0;
			foreach ($allPromotion as $key) {
				if ($key->size == 1) {
					$total = $total + 1;
				} else if ($key->size == 0) {
					$total = $total + 4;
				}
			}
			$checkIfExist = ProductPromotion::where('product_id','=', $request->product_id)->get();
			if(count($checkIfExist) >= 1){
				SessionHelper::errorMessage("Selected product already been promoted");
				return redirect('admin/promotions');
			}
			// if ($total ==  8) {
			// 	SessionHelper::errorMessage("You cannot add another promotion, please delete another promotion to create a new one");
			// 	return redirect('admin/promotions');
			// } else {
			// 	if ($total > 4 && $request->size == 0) {
			// 		SessionHelper::errorMessage("The size is not suitable with your data, please choose a small size");
			// 		return redirect('admin/promotions');
			// 	}
			// }

			$promotion = new Promotion();
			$promotion->title = $request->input('title');
			$promotion->product_id = $request->input('product_id');
			$promotion->content = $request->input('content');
			$promotion->start_date = $request->input('start_date');
			$promotion->end_date = $request->input('end_date');
			$promotion->remark = $request->input('remark');
			$promotion->discount_type = $request->input('discount_type');
			$promotion->discount_value = $request->input('discount_value');
			$promotion->font_color = $request->input('font_color');
			$promotion->bg_color = $request->input('bg_color');
			if ($request->file('photo')) {
				$upload = FileUploadHelper::upload($request, 'photo', 'jpeg,bmp,png,jpg');
				$promotion->photo = $upload;
			}
			$promotion->size = 0;
			if(!$promotion->save()){
				return back()->withInput()->withErrors($promotion->errors());
			}

			$productPromotion = new ProductPromotion();
			$productPromotion->product_id = $promotion->product_id;
			$productPromotion->promotion_id = $promotion->id;
			$productPromotion->save();

			$variants = Variant::where('product_id','=', $promotion->product_id)->get();
			if($variants) {
				foreach($variants as $variant) {
					if($promotion->discount_type == 0) {
						// dd('0.'.$promotion->discount_value);
						$variant->discount_price = $variant->price-($variant->price*('0.'.$promotion->discount_value));
					} else {
						$variant->discount_price = $variant->price-$promotion->discount_value;
					}
					$variant->is_discount = 1;
					$variant->save();
				}
			}

			$type = "Promotion";
	        $action = "Create";
	        $description = Auth::User()->name . " has succesful create a promotion " . $promotion->title;
	        ActivitylogHelpers::generate($type,$action,$description);

			SessionHelper::setMessage("Promotion Update!");
			return redirect()->route($this->route.'.index');
		} catch (\Exception $e) {
			DB::rollBack();
			\Log::error($e);
			return back()->withInput()->withErrors('Something wrong');
		}
	}

	public function destroyPromotion($promotion, Request $request) {
		$type = "Promotion";
        $action = "Delete";
        $description = Auth::User()->name . " has succesful delete a promotion " . $promotion->title;
        ActivitylogHelpers::generate($type,$action,$description);

		$promotion->productPromotions()->delete();
		$promotion->delete();
		if ($request->ajax() || $request->wantsJson()) {
			return response('ok.', 200);
		}
		SessionHelper::setMessage("Promotion deleted!",'warning');
		return redirect()->route($this->route.'.index');
	}

	public function pushNotifProduct($promotion) {
		$users = User::where('promotion','=',1)->get();
		if($users) {
			foreach($users as $user) {
				$tokens = Installation::where('user_id','=', $user->id)
				->whereNotNull('device_token')
				->where('device_token','<>', '')
				->pluck('device_token')->toArray();

				$setting = [
				'title' => 'Mareco Store',
				'body' => 'Hi! you deserve '.$promotion->remark.' just for you',
				'data' => ['promotion' => $promotion->id],
				'sound' => 'default', 
				];

				$response = FcmHelper::pushNotification($tokens, $setting);
			}
		}
		SessionHelper::setMessage("Push notification success");
		return redirect()->route($this->route.'.index');
	}

}