<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\FileUploadHelper;
use App\Helpers\SessionHelper;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Unit;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Session;
use Yajra\Datatables\Datatables;
use Validator;
use App\Helpers\ActivitylogHelpers;

class UnitController extends Controller
{
	protected $view = 'backend.unit';
	protected $route = 'admin.units';

	public function index() {
		$unit = Unit::all();
		return view($this->view.'.index')
		->with('units', $unit)
		->with('title', 'Unit')
		->with('small_title', 'unit information')
		->with('manageProduct', 'active root-level opened')
		->with('unitList', 'active')
		->with('breadcrumbs', array('Unit'));
	}
	
	public function newForm() {
		$unit = new Unit();
		return view($this->view.'.form')
		->with('method','POST')
		->with('title', 'Unit')
		->with('small_title', 'unit information')
		->with('manageProduct', 'active root-level opened')
		->with('unitList', 'active')
		->with('breadcrumbs', array('Unit', 'Register'));

	}

	public function edit($unit) {
		return view($this->view.'.form')
		->with('unit',$unit)
		->with('method', 'PUT')
		->with('title', 'Unit')
		->with('small_title', 'unit information')
		->with('manageProduct', 'active root-level opened')
		->with('unitList', 'active')
		->with('breadcrumbs', array('Unit', 'Update'));
	}
	

	public function save(Request $request) {

		$this->validate($request, Unit::addRules());

		try{
			
			$unit = new Unit();
			$unit->description = $request->input('description');
			$unit->name = $request->input('name');
			
			if(!$unit->save()){
				return back()->withInput()->withErrors($unit->errors());
			}
			
			$type = "Unit";
	        $action = "Create";
	        $description = Auth::User()->name . " has succesful create a unit " . $unit->name;
	        ActivitylogHelpers::generate($type,$action,$description);

			SessionHelper::setMessage("Form succesfully created");
			return redirect()->route($this->route.'.index');


		} catch (\Exception $e) {
			DB::rollBack();
			\Log::error($e);
			return back()->withInput()->withErrors('Something wrong');
		}
	}


	public function update(Request $request, $unit) {
		$this->validate($request, Unit::editRules($unit->id));

		try{

			$unit->description = $request->input('description');
			$unit->name = $request->input('name');
			
			if(!$unit->save()){
				return back()->withInput()->withErrors($unit->errors());
			}

			$type = "Unit";
	        $action = "Update";
	        $description = Auth::User()->name . " has succesful update a unit " . $unit->name;
	        ActivitylogHelpers::generate($type,$action,$description);

			SessionHelper::setMessage("Form succesfully updated");
			return redirect()->route($this->route.'.index');
			
		} catch (\Exception $e) {
			DB::rollBack();
			\Log::error($e);
			return back()->withInput()->withErrors('Something wrong');
		}
	}

	public function destroy($unit, Request $request) {
		$type = "Unit";
        $action = "Delete";
        $description = Auth::User()->name . " has succesful delete a unit " . $unit->name;
        ActivitylogHelpers::generate($type,$action,$description);

		$unit->delete();
		if ($request->ajax() || $request->wantsJson()) {
			return response('ok.', 200);
		}
		SessionHelper::setMessage("Record Has Been Deleted!");
		return redirect()->route($this->route.'.index');
	}

	public function listAllUnit() {
		$unitList = Unit::query();
		return Datatables::of($unitList)
		->addColumn('action', function ($model) {
			return '
			<a href="'.route('admin.units.edit', ['unit' => $model->id]).'"
				class="btn btn-info btn-outline btn-sm">
				<span class="fa fa-edit"></span> Edit
			</a>
			<a data-url="'.route('admin.units.destroy', ['unit' => $model->id]).'"
				data-toggle="modal" data-target=" #modalDelete" data-title="Confirmation" data-table-name="#unit-list" data-message="Would you like to delete this unit?" 
				class="btn btn-danger btn-outline btn-sm" >
				<span class="fa fa-trash-o"></span> Delete
			</a>';
		})
		->make(true);
	}
}