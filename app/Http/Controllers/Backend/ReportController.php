<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\FileUploadHelper;
use App\Helpers\SessionHelper;
use App\Http\Controllers\Controller;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Session;
use Validator;
use Yajra\Datatables\Datatables;
use Excel;
use App\Models\Product;
use App\Models\Variant;
use App\Models\User;
use App\Models\Order;

class ReportController extends Controller
{
	protected $view = 'backend.report';

	public function index() {
		return view($this->view.'.index')
		->with('title', 'Report')
		->with('small_title', 'Report')
		->with('report', 'active root-level opened')
		->with('breadcrumbs', array('Report'));
	}

	public function listReport(Request $request) {
		if ($request->report_type == null) {
			SessionHelper::errorMessage("Please choose the report type");
			return redirect('/admin/report');
		}
		$start_date = $request->start_date;
		$end_date = $request->end_date;
		if ($request->report_type == 1 || $request->report_type == 2) {
			if ($start_date == null || $end_date == null) {
				SessionHelper::errorMessage("Please fill the date");
				return redirect('/admin/report');
			}
		}
		if ($start_date == $end_date) {
			Session::put('same_date','same');
		} else {
			Session::put('same_date','');
		}
		Session::put('start_date',$start_date);
		Session::put('end_date',$end_date);
		Session::put('typePayment',$request->typePayment);

		if($request->report_type == 0) {
			Excel::create('CustomerData'.Carbon::now()->format('Y-m-d-His').Auth::user()->first_name, function($excel) {
				$excel->sheet('New sheet', function($sheet) {
					$customer = User::all();
					$mostSale = User::join('orders','users.id','=','orders.user_id')->get();
					foreach ($customer as $value) {
						$count = 0;
						$grandTotal = 0;
						foreach ($mostSale as $sale) {
							if ($value->email == $sale->email) {
								$count++;
							}
						}
						$listOrder = Order::where('user_id','=',$value->id)->get();

						foreach ($listOrder as $order) {
							$grandTotal += $order->grand_total;
						}
						$value->grand_total = $grandTotal;
						$value->count = $count;
					}

					$customer = $customer->sortByDesc('count');

					$order = Order::all();
					$product = Product::all();
					$variant = Variant::all();

					$sheet->loadView('backend.report.customer_data')
					->with('customer', $customer)
					->with('order', $order)
					->with('product', $product);
				});
			})->download('xls');
		} else if ($request->report_type == 2) {
			Excel::create('Order'.Carbon::now()->format('Y-m-d-His').Auth::user()->first_name, function($excel) {
				$excel->sheet('New sheet', function($sheet) {
					$customer = User::all();
					$same_date = Session::get('same_date');
					if ($same_date == '') {
						$order = Order::whereBetween('created_at',[Session::get('start_date'),Session::get('end_date')])->get();
					} else {
						$order = Order::whereDate('created_at', DB::raw('CURDATE()'))->get();
					}
					$product = Product::all();
					$variant = Variant::all();
					$start_date = Session::get('start_date');
					$end_date = Session::get('end_date');

					$sheet->loadView('backend.report.order')
					->with('customer', $customer)
					->with('order', $order)
					->with('product', $product)
					->with('start_date', $start_date)
					->with('end_date', $end_date);
				});
			})->download('xls');
		} else if ($request->report_type == 3) {
			Excel::create('Stock'.Carbon::now()->format('Y-m-d-His').Auth::user()->first_name, function($excel) {
				$excel->sheet('New sheet', function($sheet) {
					$customer = User::all();
					$order = Order::all();
					$product = Product::all();
					$variant = Variant::all();

					$sheet->loadView('backend.report.stock')
					->with('customer', $customer)
					->with('order', $order)
					->with('variants', $variant)
					->with('products', $product);
				});
			})->download('xls');
		} else {
			Excel::create('Order'.Carbon::now()->format('Y-m-d-His').Auth::user()->first_name, function($excel) {
				$excel->sheet('New sheet', function($sheet) {
					$typePayment = Session::get('typePayment');
					if ($typePayment == 'cod') {
						$order = Order::where('payment_type','=',2)->get();
					} else if ($typePayment == 'bank') {
						$order = Order::where('payment_type','=',1)->get();
					} else  {
						$order = Order::where('payment_type','=',0)->get();
					}
					$customer = User::all();
					$product = Product::all();
					$variant = Variant::all();
					$start_date = Session::get('start_date');
					$end_date = Session::get('end_date');

					$sheet->loadView('backend.report.payment')
					->with('customer', $customer)
					->with('order', $order)
					->with('product', $product)
					->with('start_date', $start_date)
					->with('end_date', $end_date);
				});
			})->download('xls');
		}
	}
}