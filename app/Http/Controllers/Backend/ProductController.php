<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\ActivitylogHelpers;
use App\Helpers\Enums\ProductStatus;
use App\Helpers\FcmHelper;
use App\Helpers\FileUploadHelper;
use App\Helpers\SessionHelper;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Installation;
use App\Models\Product;
use App\Models\ProductLang;
use App\Models\Review;
use App\Models\SubCategory;
use App\Models\Term;
use App\Models\User;
use App\Models\Variant;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Session;
use Validator;
use Yajra\Datatables\Datatables;

class ProductController extends Controller
{
	protected $view = 'backend.product';
	protected $route = 'admin.products';

	public function index() {
		$product = Product::all();
		$categoryList = Category::all()->pluck('name','id');
		return view($this->view.'.index')
		->with('products', $product)
		->with('title', 'Product')
		->with('small_title', 'product information')
		->with('productStatus', ProductStatus::getArray())
		->with('manageProduct', 'active root-level opened')
		->with('listOfCategories', $this->getCategoryList() )
		->with('listOfUnits', $this->getUnitList() )
		->with('categoryList', $categoryList)
		->with('productList', 'active')
		->with('breadcrumbs', array('Product'));
	}
	
	public function newForm() {
		$product = new Product();
		return view($this->view.'.form')
		// ->with('product', $product)
		->with('method','POST')
		->with('title', 'Product')
		->with('small_title', 'product information')
		->with('listOfCategories', $this->getCategoryList() )
		->with('listOfUnits', $this->getUnitList() )
		->with('manageProduct', 'active root-level opened')
		->with('productList', 'active')
		->with('breadcrumbs', array('Product', 'Register'));

	}

	public function edit($product) {
		// if(count($product) >= 1) {
		// 	foreach ($product->productVariants as $value){
		// 		if(isset($value->is_discount)){
		// 			if($value->is_discount == 1) {
		// 				$result = 'checked';
		// 			} else {
		// 				$result = '';
		// 			}
		// 		} else {
		// 			$result = '';
		// 		}
		// 	}
		// } else {
		// 	$result = '';
		// }
		return view($this->view.'.form')
		->with('product',$product)
		// ->with('result', $result)
		->with('method', 'PUT')
		->with('title', 'Product')
		->with('small_title', 'product information')
		->with('listOfCategories', $this->getCategoryList() )
		->with('listOfUnits', $this->getUnitList() )
		->with('manageProduct', 'active root-level opened')
		->with('productList', 'active')
		->with('breadcrumbs', array('Product', 'Update'));
	}
	

	public function save(Request $request) {
		$this->validate($request, Product::addRules());
		$featuredProduct = Product::where('is_featured','=',1)->get();
		if(count($featuredProduct) > 4 ) {
			if($request->is_featured == "on") {
				SessionHelper::setMessage("Maximal of featured product is 5","warning");
				return redirect()->route($this->route.'.index');
			}
		}
		try{
			$product = new Product();
			$product->created_by = Auth::user()->id;
			$product->category_id = $request->input('category_id');
			
			if ($request->file('product_image')) {
				$upload = FileUploadHelper::upload($request, 'product_image', 'jpeg,bmp,png,jpg');
				$product->product_image = $upload;
				$product->rotate = $request->input('previewImageRotate');
			}
			if ($request->file('product_image_second')) {
				$upload = FileUploadHelper::upload($request, 'product_image_second', 'jpeg,bmp,png,jpg');
				$product->product_image_second = $upload;
				$product->secondRotate = $request->input('previewImage2Rotate');
			}
			if ($request->file('product_image_third')) {
				$upload = FileUploadHelper::upload($request, 'product_image_third', 'jpeg,bmp,png,jpg');
				$product->product_image_third = $upload;
				$product->thirdRotate = $request->input('previewImage3Rotate');
			}
			if ($request->file('product_image_fourth')) {
				$upload = FileUploadHelper::upload($request, 'product_image_fourth', 'jpeg,bmp,png,jpg');
				$product->product_image_fourth = $upload;
				$product->fourRotate = $request->input('previewImage4Rotate');
			}
			if ($request->file('product_image_fifth')) {
				$upload = FileUploadHelper::upload($request, 'product_image_fifth', 'jpeg,bmp,png,jpg');
				$product->product_image_fifth = $upload;
				$product->fiveRotate = $request->input('previewImage5Rotate');
			}
			$product->name = $request->input('name');
			$product->slug_url = str_slug($product->name, '-');
			$product->code = $request->input('code');
			$product->point = $request->input('point');
			$product->eta = $request->input('eta');
			if($request->is_featured == "on") {
				$product->is_featured = 1;
				$product->status = ProductStatus::FEATURED;
			} else {
				$product->is_featured = 0;
			}
			$product->description = $request->input('description');
			if(!$product->save()){
				return back()->withInput()->withErrors($product->errors());
			}

			$productLang = new ProductLang();
			$productLang->name = $request->input('name');
			$productLang->description = $request->input('description');
			$productLang->lang = 'en';
			$productLang->product_id = $product->id;
			$productLang->save();

			if ($request->product_name) {
				$productLang = new ProductLang();
				$productLang->name = $request->input('product_name');
				$productLang->description = $request->input('product_description');
				$productLang->lang = 'id';
				$productLang->product_id = $product->id;
				$productLang->save();
			}

			$this->saveProductVariant($product, $request);

			$type = "Product";
			$action = "Create";
			$description = Auth::User()->name . " has succesful create a product " . $product->name;
			ActivitylogHelpers::generate($type,$action,$description);

			SessionHelper::setMessage("Form succesfully created");
			return redirect()->route($this->route.'.index');
		} catch (\Exception $e) {
			DB::rollBack();
			\Log::error($e);
			return back()->withInput()->withErrors('Fail to submit product, Please try again later.');
		}
	}

	public function pushNotifProduct($product) {
		$users = User::where('new_product','=',1)->get();
		if($users) {
			foreach($users as $user) {
				$tokens = Installation::where('user_id','=', $user->id)
				->whereNotNull('device_token')
				->where('device_token','<>', '')
				->pluck('device_token')->toArray();

				$setting = [
				'title' => 'Mareco Store',
				'body' => 'Hi! telah tiba produk baru! '.$product->name.' Rp. '.$product->productVariants->first()->price,
				'sound' => 'default', 
				'data' => ['productId' => $product->id],
				'data2' => ['product' => $product],
				];

				$response = FcmHelper::pushNotification($tokens, $setting);
			}
		}
		SessionHelper::setMessage("Push notification success");
		return redirect()->route($this->route.'.index');
	}

	public function saveProductVariant(Product $product, Request $request) {
		foreach($request->productVariant as $productVariant){
			$variant = new Variant($productVariant);
			$variant->sku = $productVariant['sku'];
			$variant->qty = $productVariant['qty'];
			if($productVariant['size']) $variant->size = $productVariant['size'];
			$variant->unit_id = $productVariant['unit_id'];
			if($productVariant['color']) $variant->color = $productVariant['color'];
			$variant->weight = $productVariant['weight'];
			$variant->price = $productVariant['price'];
			// if(isset($request->is_discount)) {
			// 	$variant->is_discount = 1;
			// 	$variant->discount_price = $productVariant['discount_price'];
			// } else{
			// 	$variant->is_discount = 0;
			// 	$variant->discount_price = 0;
			// }
			if (!$product->productVariants()->save($variant)) {
				throw new ValidationException($variant->errors());
			}
		}
		return redirect()->route($this->route.'.index');
	}


	public function update(Request $request, $product) {
		$this->validate($request, Product::editRules($product->id));
		$featuredProduct = Product::where('is_featured','=',1)->get();
		if(count($featuredProduct) > 4 ) {
			if($request->is_featured == "on") {
				if($product->is_featured == 0) {
					SessionHelper::setMessage("Maximal of featured product is 5","warning");
					return redirect()->route($this->route.'.index');
				}
			}
		}
		try{

			$product->category_id = $request->input('category_id');
			if ($request->file('product_image')) {
				$upload = FileUploadHelper::upload($request, 'product_image', 'jpeg,bmp,png,jpg');
				$product->product_image = $upload;
				$product->rotate = $request->input('previewImageRotate');
			} 
			if ($request->file('product_image_second')) {
				$upload = FileUploadHelper::upload($request, 'product_image_second', 'jpeg,bmp,png,jpg');
				$product->product_image_second = $upload;
				$product->secondRotate = $request->input('previewImage2Rotate');
			} 
			if ($request->file('product_image_third')) {
				$upload = FileUploadHelper::upload($request, 'product_image_third', 'jpeg,bmp,png,jpg');
				$product->product_image_third = $upload;
				$product->thirdRotate = $request->input('previewImage3Rotate');
			} 
			if ($request->file('product_image_fourth')) {
				$upload = FileUploadHelper::upload($request, 'product_image_fourth', 'jpeg,bmp,png,jpg');
				$product->product_image_fourth = $upload;
				$product->fourRotate = $request->input('previewImage4Rotate');
			}
			if ($request->file('product_image_fifth')) {
				$upload = FileUploadHelper::upload($request, 'product_image_fifth', 'jpeg,bmp,png,jpg');
				$product->product_image_fifth = $upload;
				$product->fiveRotate = $request->input('previewImage5Rotate');
			}
			if ($request->product_image == null){
				$product->product_image = '';
			}
			if ($request->product_image_second == null){
				$product->product_image_second = '';
			}
			if ($request->product_image_third == null){
				$product->product_image_third = '';
			}
			if ($request->product_image_fourth == null){
				$product->product_image_fourth = '';
			}
			if ($request->product_image_fifth == null){
				$product->product_image_fifth = '';
			}
			$product->name = $request->input('name');
			$product->slug_url = str_slug($product->name, '-');
			$product->code = $request->input('code');
			$product->eta = $request->input('eta');
			$product->point = $request->input('point');
			if($request->is_featured == "on") {
				$product->is_featured = 1;
				$product->status = ProductStatus::FEATURED;
			} else {
				$product->is_featured = 0;
			}
			$product->description = $request->input('description');
			if(!$product->save()){
				return back()->withInput()->withErrors($product->errors());
			}

			if($request->product_name) {
				if($product->productLang('id')){
					$productLang = $product->productLang('id');
					$productLang->name = $request->input('product_name');
					$productLang->description = $request->input('product_description');
				} else {
					$productLang = new ProductLang();
					$productLang->name = $request->input('product_name');
					$productLang->description = $request->input('product_description');
				}
				$productLang->lang = 'id';
				$productLang->product_id = $product->id;
				$productLang->save();
			}

			foreach ($request->productVariant as $value) {
				if (!isset($value['id'])) {
					$variant = new Variant();
					$variant->sku = $value['sku'];
					$variant->qty = $value['qty'];
					$variant->size = $value['size'];
					$variant->unit_id = $value['unit_id'];
					$variant->color = $value['color'];
					$variant->weight = $value['weight'];
					$variant->price = $value['price'];
					// if(isset($request->is_discount)) {
					// 	$variant->is_discount = 1;
					// 	$variant->discount_price = $productVariant['discount_price'];
					// } else{
					// 	$variant->is_discount = 0;
					// 	$variant->discount_price = 0;
					// }
				} else {
					$variant = Variant::find($value['id']);
				}
				$this->updateProductVariant($variant, $value, $product);
			}

			$type = "Product";
			$action = "Update";
			$description = Auth::User()->name . " has succesful update a product " . $product->name;
			ActivitylogHelpers::generate($type,$action,$description);

			SessionHelper::setMessage("Form succesfully updated");
			return redirect()->route($this->route.'.index');
			
		} catch (\Exception $e) {
			DB::rollBack();
			\Log::error($e);
			return back()->withInput()->withErrors('Something wrong');
		}
	}

	public function updateProductVariant(Variant $variant, $value, $product) {
		if (isset($value['product_id'])) {
			$variant->product_id = $value['product_id'];
		}
		$variant->fill($value);
		if (!$product->productVariants()->save($variant)) {
			throw new ValidationException($variant->errors());
		}
	}

	public function destroy($product, Request $request) {
		$type = "Product";
		$action = "Delete";
		$description = Auth::User()->name . " has succesful delete a product " . $product->name;
		ActivitylogHelpers::generate($type,$action,$description);
		$product->productLanguages()->delete();
		$product->productVariants()->delete();
		$product->delete();
		if ($request->ajax() || $request->wantsJson()) {
			return response('ok.', 200);
		}
		SessionHelper::setMessage("Record Has Been Deleted!");
		return redirect()->route($this->route.'.index');
	}
	
	public function listAllProduct() {
		$productList = Product::query();
		return Datatables::of($productList)
		->addColumn('action', function ($model) {
			$value = $model->reviews->count();
			if($value != 0) {
				return '
				<a href="'.route('admin.products.push', ['product' => $model->id]).'"
					class="btn btn-warning btn-outline btn-sm" style="width: 100%">
					<span class="fa fa-edit"></span> Notification
				</a>
				<a href="'.route('admin.products.edit', ['product' => $model->id]).'"
					class="btn btn-info btn-outline btn-sm" style="width: 100%">
					<span class="fa fa-edit"></span> Edit
				</a>
				<a data-url="'.route('admin.products.destroy', ['product' => $model->id]).'"
					data-toggle="modal" data-target=" #modalDelete" data-title="Confirmation" data-table-name="#product-list" data-message="Would you like to delete this product?" 
					class="btn btn-danger btn-outline btn-sm" style="width: 100%">
					<span class="fa fa-trash-o"></span> Delete
				</a>
				<a href="'.route('admin.reviews.index', ['product' => $model->id]).'"
					class="btn btn-green btn-outline btn-sm" style="width: 100%">
					<span class="fa fa-edit"></span> Review
				</a>';
			} else {
				return '
				<a href="'.route('admin.products.push', ['product' => $model->id]).'"
					class="btn btn-warning btn-outline btn-sm" style="width: 100%">
					<span class="fa fa-edit"></span> Notification
				</a>
				<a href="'.route('admin.products.edit', ['product' => $model->id]).'"
					class="btn btn-info btn-outline btn-sm" style="width: 100%">
					<span class="fa fa-edit"></span> Edit
				</a>
				<a data-url="'.route('admin.products.destroy', ['product' => $model->id]).'"
					data-toggle="modal" data-target=" #modalDelete" data-title="Confirmation" data-table-name="#product-list" data-message="Would you like to delete this product?" 
					class="btn btn-danger btn-outline btn-sm" style="width: 100%">
					<span class="fa fa-trash-o"></span> Delete
				</a>
				';
			}
		}) 
		->editColumn('category_id', function($model){
			return $model->category->name;
		})
		->editColumn('status', function($model){
			return ProductStatus::getString($model->status);
		})
		->make(true);
	}

	public function productReview($product) {
		$review = Review::where('product_id','=', $product->id)->get();
		return view($this->view.'.review')
		->with('reviews', $review)
		->with('product', $product)
		->with('title', 'Product')
		->with('small_title', 'product information')
		->with('manageProduct', 'active root-level opened')
		->with('productList', 'active')
		->with('breadcrumbs', array('Product'));
	}

	public function productReviewSave($product, Request $request) {
		$review = new Review();
		$review->user_id = Auth::user()->id;
		$review->product_id = $product->id;
		$review->remarks = $request->input('remarks');
		$review->save();

		SessionHelper::setMessage("Review updated!");
		return redirect()->route('admin.reviews.index', $product->id);
	}

	public function productReviewDestroy($review, Request $request) {
		$type = "Review";
		$action = "Delete";
		$description = Auth::User()->first_name . " has succesful delete a review";
		ActivitylogHelpers::generate($type,$action,$description);

		$review->delete();
		if ($request->ajax() || $request->wantsJson()) {
			return response('ok.', 200);
		}
		SessionHelper::setMessage("Record Has Been Deleted!");
		return redirect()->route('admin.reviews.index', $product->id);
	}
}