<?php

namespace App\Http\Controllers\Api\V1;

use App\Helpers\Enums\CODStatus;
use App\Helpers\Enums\OrderStatus;
use App\Helpers\Enums\PaymentType;
use App\Helpers\Enums\TypeShipmentHelper;
use App\Helpers\FcmHelper;
use App\Helpers\FileUploadHelper;
use App\Helpers\PaypalHelper;
use App\Http\Controllers\Controller;
use App\Models\Address;
use App\Models\BankAccount;
use App\Models\Cart;
use App\Models\CashOnDelivery;
use App\Models\Category;
use App\Models\CategoryLang;
use App\Models\Comment;
use App\Models\Config;
use App\Models\Installation;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\PaypalPayment;
use App\Models\Picture;
use App\Models\Point;
use App\Models\Product;
use App\Models\Promotion;
use App\Models\Review;
use App\Models\Term;
use App\Models\User;
use App\Models\Variant;
use App\Models\Wishlist;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use JWTAuth;
use JeroenDesloovere\Distance\Distance;
use Paypal;
use Steevenz\Rajaongkir;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class ApiController extends Controller
{
    private $_apiContext;

    public function __construct()
    {
        $this->_apiContext = Paypal::ApiContext(
            config('services.paypal.client_id'),
            config('services.paypal.secret'));

        $this->_apiContext->setConfig(array(
            'mode' => 'sandbox',
            'service.EndPoint' => 'https://api.sandbox.paypal.com',
            'http.ConnectionTimeOut' => 30,
            'log.LogEnabled' => true,
            'log.FileName' => storage_path('logs/paypal.log'),
            'log.LogLevel' => 'FINE'
            ));
    }

    // public function getTableColumns($table)
    // {
    //     return DB::getSchemaBuilder()->getColumnListing($table);
    // }

    public function product(Request $request) {
        $name   = $request->name;
        $offset = $request->offset;
        $limit  = $request->limit;
        $category_id = $request->category;
        // $product = Product::where('name', 'LIKE', '%'.$name.'%');
        // if($category_id) $product = $product->where('category_id','=',$category_id);
        // $product = $product->with(['category' => function($query) {
        //     $query->select('id', 'name');
        // }])
        // ->with('productVariants.unit')
        // ->orderBy('created_at','desc');
        $lang = $request->header('x-language');
        // $fields = $this->getTableColumns('products');

        $product = Product::leftJoin('product_langs AS b', function($query) use ($lang) {
            $query->on('products.id', '=', 'b.product_id');
            $query->on('b.lang', '=', DB::raw("'$lang'"));
        })
        ->select('products.id','products.category_id','products.code','products.slug_url','products.product_image','products.product_image_second','products.product_image_third','products.product_image_fourth','products.product_image_fifth','products.point','products.sold','products.status','products.sale_type','products.sale_amount','products.grand_amount',
            DB::raw('CASE WHEN b.name IS NULL THEN products.name ELSE b.name END AS name'),
            DB::raw('CASE WHEN b.description IS NULL THEN products.description ELSE b.description END AS description'))

        ->where('products.name', 'LIKE', '%'.$name.'%');
        if($category_id) $product = $product->where('products.category_id','=',$category_id);
        $product = $product->with(['category' => function($query) use ($lang) {
            // $query->select('id', 'name');
            $query->leftJoin('category_langs AS b', function($join) use ($lang) {
                $join->on('categories.id', '=', 'b.category_id');
                $join->on('b.lang', '=', DB::raw("'$lang'"));
            })
            ->select('categories.id',
                DB::raw('CASE WHEN b.name IS NULL THEN categories.name ELSE b.name END AS name'));
        }])
        ->with('productVariants.unit')
        ->orderBy('products.created_at','desc');

        if($limit) {
            $product = $product->limit($limit)
            ->offset($offset);
        }
        $product = $product->get();

        return response()->json($product, 200);
    }

    public function productDetail(Request $request, $id) { 
        $lang = $request->header('x-language');

        $product = Product::leftJoin('product_langs AS b', function($query) use ($lang) {
            $query->on('products.id', '=', 'b.product_id');
            $query->on('b.lang', '=', DB::raw("'$lang'"));
        })
        ->select('products.id','products.category_id','products.code','products.slug_url','products.product_image','products.product_image_second','products.product_image_third','products.product_image_fourth','products.product_image_fifth','products.point','products.sold','products.status','products.sale_type','products.sale_amount','products.grand_amount','eta',
            DB::raw('CASE WHEN b.name IS NULL THEN products.name ELSE b.name END AS name'),
            DB::raw('CASE WHEN b.description IS NULL THEN products.description ELSE b.description END AS description'))

        ->with(['category' => function($query) use ($lang) {
            $query->leftJoin('category_langs AS b', function($join) use ($lang) {
                $join->on('categories.id', '=', 'b.category_id');
                $join->on('b.lang', '=', DB::raw("'$lang'"));
            })
            ->select('categories.id',
                DB::raw('CASE WHEN b.name IS NULL THEN categories.name ELSE b.name END AS name'));
        }])
        ->with(['unit' => function($query) {
            $query->select('id', 'name');
        }])
        ->with('productVariants.unit')
        ->find($id);
        if(!$product) return response()->json(['message'=>'Not Found'], 404);
        return response()->json($product, 200);
    }

    public function category(Request $request) {
        $lang = $request->header('x-language');
        // $categories = Category::select('id','name','description','photo')->where('is_active','=', 1)->get();
        $categories = Category::leftJoin('category_langs AS b', function($query) use ($lang) {
            $query->on('categories.id', '=', 'b.category_id');
            $query->on('b.lang', '=', DB::raw("'$lang'"));
        })
        ->select('categories.id','categories.photo', 
            DB::raw('CASE WHEN b.name IS NULL THEN categories.name ELSE b.name END AS name'),
            DB::raw('CASE WHEN b.description IS NULL THEN categories.description ELSE b.description END AS description'))
        ->where('categories.is_active','=', 1)
        ->get();
            // dd($categories);
        // $categories = CategoryLang::select('id','name','description','photo')->where('is_active','=', 1)->get();
        if(!$categories) return response()->json(['message'=>'Not Found'], 404);
        return response()->json($categories, 200);
    }

    public function productComment(Request $request){
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser();
        $product = Product::find($request->product_id);
        try {
            $comment = new Review();
            $comment->user_id = $user->id;
            if(!$product) return response()->json(['message'=>'Product Not Found'], 404);
            $comment->product_id = $request->input('product_id');
            $comment->remarks = $request->input('remarks');
            $comment->rate = $request->input('rate');
            $comment->save();
        } catch (\Exception $e) {
            return response()->json(['message' => 'Not Found.'], 400);
        }
        if (!$comment) return response()->json(['message'=>'Not Found'], 404);
        return response()->json($comment, 200);
    }

    public function getProductComment(Request $request, $id) {
        $lang = $request->header('x-language');
        
        $comment = Review::with(['user' => function ($query) {
            $query->select('id','first_name','last_name','email','phone','photo','point');
        }])
        ->with(['product' => function($query) use ($lang) {
            $query->leftJoin('product_langs AS b', function($join) use ($lang) {
                $join->on('products.id', '=', 'b.product_id');
                $join->on('b.lang', '=', DB::raw("'$lang'"));
            })
            ->select('products.id', 'products.point','products.code',
                DB::raw('CASE WHEN b.name IS NULL THEN products.name ELSE b.name END AS name'),
                DB::raw('CASE WHEN b.description IS NULL THEN products.description ELSE b.description END AS description')
                );
        }])
        ->select('id','remarks','rate','user_id','product_id')
        ->where('product_id','=',$id)
        ->orderBy('created_at','desc')
        ->get();
        if(count($comment) == 0) return [];
        return response()->json($comment, 200);
    }

    public function getCart(Request $request) {
        $lang = $request->header('x-language');

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser();
        // $carts = Cart::where('user_id','=', $user->id)->with(['variant.product'])->get();
        $carts = Cart::where('user_id','=', $user->id)->with(['variant.product' => function($query) use ($lang) {
            $query->leftJoin('product_langs AS b', function($join) use ($lang) {
                $join->on('products.id', '=', 'b.product_id');
                $join->on('b.lang', '=', DB::raw("'$lang'"));
            })
            ->select('products.id', 'products.point','products.code','products.product_image',
                DB::raw('CASE WHEN b.name IS NULL THEN products.name ELSE b.name END AS name'),
                DB::raw('CASE WHEN b.description IS NULL THEN products.description ELSE b.description END AS description')
                );
        }])->get();
        return response()->json($carts, 200);
    }

    public function addCart(Request $request) {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser();
        $this->validateJson($request, Cart::addRules());
        try {
            $cart = Cart::where('user_id','=', $user->id)->where('variant_id','=',$request->variant_id)->first();
            $variant = Variant::where('id','=',$request->variant_id)->first();
            if($variant->qty > 0) {
                if(!$cart) {
                    $cart = new Cart();
                    $cart->variant_id = $request->variant_id;
                    $cart->qty = $request->qty;
                    $cart->user_id = $user->id;
                    $cart->status = $cart->variant->product->status;
                } else {
                    $cart->qty += $request->qty;
                }
                $cart->save();
                $variant->qty -= $request->qty;
                $variant->save();
            }
            if($variant->qty == 0) return response()->json(['message' => 'Product out of stock.'], 400);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Not Found.'], 400);
        }
        return response()->json($cart, 201);
    }

    public function editCart(Request $request) {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser();
        try {
            $cart = Cart::where('user_id','=', $user->id)->where('variant_id','=',$request->variant_id)->first();
            $variant = Variant::where('id','=',$request->variant_id)->first();
            if($variant->qty > 0) {
                if (!$cart) {
                    $cart = new Cart();
                    $cart->variant_id = $request->variant_id;
                    $cart->user_id = $user->id;
                }
                $cart->qty = $request->qty;
                $cart->save();

                $variant->qty -= $request->qty;
                $variant->save();
                
            }
            if($variant->qty == 0) return response()->json(['message' => 'Product out of stock.'], 400);

        } catch (\Exception $e) {
            return response()->json(['message' => 'Not Found.'], 400);
        }
        return response()->json($cart, 200);
    }

    public function deleteCart(Request $request) {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser();

        $cart = Cart::where('user_id','=', $user->id)->where('variant_id','=',$request->variant_id)->first();
        $variant = Variant::where('id','=',$request->variant_id)->first();

        if (!$cart) return response()->json(['message' => 'Not Found.'], 400);
        if ($cart) $cart->delete();
        $variant->qty += $cart->qty;
        $variant->save();   
        return response()->json($cart, 204);
    }

    public function getWishlist(Request $request) {
        $lang = $request->header('x-language');

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser();
        $wishlists = Wishlist::where('user_id','=', $user->id)->with(['variant.product' => function($query) use ($lang) {
            $query->leftJoin('product_langs AS b', function($join) use ($lang) {
                $join->on('products.id', '=', 'b.product_id');
                $join->on('b.lang', '=', DB::raw("'$lang'"));
            })
            ->select('products.id', 'products.point','products.code',
                DB::raw('CASE WHEN b.name IS NULL THEN products.name ELSE b.name END AS name'),
                DB::raw('CASE WHEN b.description IS NULL THEN products.description ELSE b.description END AS description')
                );
        }])->get();
        return response()->json($wishlists, 200);
    }

    public function addWishlist(Request $request) {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser();
        $this->validateJson($request, Wishlist::addRules());
        try {
            $wishlist = Wishlist::where('variant_id',$request->variant_id)->first();
            if($wishlist) return response()->json(['message'=>'Selected variant already added']);
            if(!$wishlist){
                $wishlist = new Wishlist();
                $wishlist->variant_id = $request->variant_id;
                $wishlist->user_id = $user->id;
                $wishlist->status = $wishlist->variant->product->status;
                $wishlist->save();
            }
        } catch (\Exception $e) {
            return response()->json(['message' => 'Not Found.'], 400);
        }
        return response()->json($wishlist, 201);
    }

    public function editWishlist(Request $request) {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser();
        try {
            $wishlist = Wishlist::where('user_id','=', $user->id)->where('variant_id','=',$request->variant_id)->first();
            if (!$wishlist) {
                $wishlist = new Wishlist();
                $wishlist->variant_id = $request->variant_id;
                $wishlist->user_id = $user->id;
            }
            $wishlist->save();
        } catch (\Exception $e) {
            return response()->json(['message' => 'Not Found.'], 400);
        }
        return response()->json($wishlist, 200);
    }

    public function deleteWishlist(Request $request) {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser();

        $wishlist = Wishlist::where('user_id','=', $user->id)->where('variant_id','=',$request->variant_id)->first();
        if (!$wishlist) return response()->json(['message' => 'Not Found.'], 400);
        if ($wishlist) $wishlist->delete();
        return response()->json($wishlist, 204);
    }

    public function checkOut(Request $request) {

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser();
        $carts = Cart::where('user_id','=', $user->id)->get();
        if (count($carts) == 0) return response()->json(['message' => 'No Cart.'], 404);

        $this->validateJson($request, Order::addRules());

        try {
            DB::beginTransaction();

            $order = Order::orderBy('order_number','desc')->first();
            $number = 1;
            if($order) $number = $order->order_number+1;
            $generateNumber = str_pad($number, 5, '0', STR_PAD_LEFT);

            $order = new Order();
            $order->order_number = $generateNumber;
            $order->shipment_service = $request->input('shipment_service');
            $order->shipment_type = $request->input('shipment_type');
            $order->grand_total = $request->input('grand_total');
            $order->bank_account_id = $request->input('bank_account_id');
            $order->shipment_fee = $request->input('shipment_fee');
            $order->address_id = $request->input('address_id');
            $order->payment_type = $request->input('payment_type');
            if($request->input('payment_type') == PaymentType::CASH_ON_DELIVERY) {
                $order->status = OrderStatus::ON_VERIFICATION;
                $order->cod_address = $request->input('cod_address');
                $order->cod_lat = $request->input('cod_lat');
                $order->cod_lng = $request->input('cod_lng');
            } else {
                $order->status = OrderStatus::WAITING_FOR_PAYMENT;
            }   
            $order->user_id = $user->id;
            //Below is Usage Point
            $order->point = $request->input('point');
            $order->save();


            if($request->input('point')) {
                $point = new Point();
                $point->user_id = $user->id;
                $point->amount = -$order->point;
                $point->description = 'Use Point Order No'.' '.$order->order_number.' '.'lost ';
                $point->save();
            }

            foreach($carts as $cart){

                $variant = Variant::find($cart->variant_id);
                $variant->qty -= $cart->qty;
                $variant->save();

                $detail = new OrderDetail();
                $detail->order()->associate($order);
                $detail->qty = $cart->qty;
                $detail->checkOrderDetail($cart->variant);
                $detail->save();
                $cart->delete();
            }

            $order->getGrandTotal();
            $detail = $this->saveOrderDetail($order, $request);

            $result = Order::where('id',$order->id)->with(['orderDetails.product','address','bankAccount'])->first();

            if ($request->point) { 
                $minPoint = User::find($user->id); 
                $minPoint->point = $minPoint->point-$request->point; 
                $minPoint->save(); 
            } 

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            Log::error($e->getMessage());
            return response()->json(['message' => 'Not Found.'], 400);
        }
        return response()->json($result, 200);
    }

    public function saveOrderDetail(Order $order, Request $request) {
        if($request->orderDetail){
            foreach($request->orderDetail as $orderDetail){
                $detail = new OrderDetail();
                $detail->order_id = $order;
                $detail->qty = $orderDetail['qty'];
                $detail->product_id = $orderDetail['product_id'];
                $detail->price = $orderDetail['price'];
                $detail->weight = $orderDetail['weight'];
                $detail->size = $orderDetail['size'];
                $detail->sku = $orderDetail['sku'];
                $detail->color = $orderDetail['color'];
                $detail->amount = $detail->qty*$detail->price;
                if (!$order->orderDetails()->save($detail)) return false;

            }
        }
        return true;
    }

    public function getAddress() {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser();
        $address = Address::where('user_id','=', $user->id)->with(['user'])->get();
        if (!$address) return response()->json(['message' => 'Not Found.'], 400);
        return response()->json($address, 200);
    }

    public function postAddress(Request $request){
        $config['api_key'] = '56950ca5aa637a0c5efa1b266e28f160';
        $config['account_type'] = 'pro';
        $rajaongkir = new Rajaongkir($config);

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser();
        $this->validateJson($request, Address::addRules());
        try {
            $address = new Address();
            $address->user_id = $user->id;
            $address->city = $request->input('city');
            $address->province = $request->input('province');
            $address->district = $request->input('district');
            $address->address = $request->input('address');
            $address->postal_code = $request->input('postal_code');
            $address->state = $request->input('state');
            $address->city_name = $rajaongkir->get_city($request->input('city'))['city_name'];
            $address->province_name = $rajaongkir->get_province($request->input('province'))['province'];
            $address->district_name = $rajaongkir->get_subdistrict($request->input('district'))['subdistrict_name'];
            $address->save();
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return response()->json(['message' => 'Not Found.'], 400);
        }
        return response()->json($address, 201);
    }

    public function editAddress(Request $request, $address){
        $config['api_key'] = '56950ca5aa637a0c5efa1b266e28f160';
        $config['account_type'] = 'pro';
        $rajaongkir = new Rajaongkir($config);

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser();
        try {
            $address = Address::find($address);
            $address->user_id = $user->id;
            $address->city = $request->input('city');
            $address->province = $request->input('province');
            $address->district = $request->input('district');
            $address->address = $request->input('address');
            $address->postal_code = $request->input('postal_code');
            $address->state = $request->input('state');
            $address->city_name = $rajaongkir->get_city($request->input('city'))['city_name'];
            $address->province_name = $rajaongkir->get_province($request->input('province'))['province'];
            $address->district_name = $rajaongkir->get_subdistrict($request->input('district'))['subdistrict_name'];
            $address->save();
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return response()->json(['message' => 'Not Found.'], 400);
        }
        return response()->json($address, 200);
    }

    public function deleteAddress($id) {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser();

        $address = Address::where('user_id','=', $user->id)->where('id','=', $id)->first();
        if (!$address) return response()->json(['message' => 'Not Found.'], 400);
        $address->delete();
        return response()->json($address, 204);
    }

    public function orderHistory(Request $request) {
        $offset = $request->offset;
        $limit  = $request->limit;
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser();

        $orders = Order::where('user_id','=',$user->id)->with(['orderDetails.product','address','bankAccount','shipment'])
        ->orderBy('created_at','desc');
        if($limit) {
            $orders = $orders->limit($limit)
            ->offset($offset);
        }
        $orders = $orders->get();
        if(count($orders) == 0) return [];
        return response()->json($orders, 200);
    }

    public function paymentType() {
        $paymentType = PaymentType::getArray();
        return response()->json($paymentType, 200);
    }

    public function shippingMethod() {
        $shippingMethod = TypeShipmentHelper::getArray();
        return response()->json($shippingMethod, 200);
    }

    public function getBank(){
        $bank = BankAccount::where('is_active','=',1)->get();
        if(!$bank) return response()->json(['message'=>'Not Found'], 404);
        return response()->json($bank, 200);
    }

    public function orderHistoryChangeStatus(Request $request) {
        $order = Order::find($request->order_id);
        if (!$order) return response()->json(['message'=>'Not Found'], 404);
        if ($order->payment_type == PaymentType::BANK_TRANSFER) {
            $order->account_name = $request->input('account_name');
            $order->bank_account_id = $request->input('bank_account_id');
            $order->account_number = $request->input('account_number');
            if ($request->file('receipt_url')) {
                $upload = FileUploadHelper::upload($request, 'receipt_url', 'jpeg,bmp,png,jpg,mp3,mp4,wmv,ogg,3gp,avi');
                $order->receipt_url = $upload;
            } else {
                $order->receipt_url = '';
            }
        }
        $order->status = $request->input('status');
        $order->remark = $request->input('remark');
        $order->save();



        $tokens = Installation::where('user_id','=', $order->user_id)
        ->whereNotNull('device_token')
        ->where('device_token','<>', '')
        ->pluck('device_token')->toArray();

        $setting = [
        'title' => 'Mareco Store',
        'body' => 'Your Order status was changed',
        'sound' => 'default', 
        'data' => ['order_id' => $order->id], 
        ];

        $response = FcmHelper::pushNotification($tokens, $setting);
        return response()->json($order, 200);
    }

    public function slider(){
        $picture = Picture::orderBy('is_first','dec')->get();
        if(!$picture) return response()->json(['message'=>'Not Found'], 404);
        return response()->json($picture, 200);
    }

    public function codPayment(Request $request) {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser();
        try {

            $config = Config::first();
            if(!$config) return response()->json(['message'=>'Not Found'], 404);
            $configLat = $config->storage_lat;
            $configLng = $config->storage_lng;
            $customerLat = $request->user_lat;
            $customerLng = $request->user_lng;
            $customerAddress = $request->user_address;

            $distance = Distance::between($configLat, $configLng, $customerLat, $customerLng);
            $rate = $distance*$config->cod_amount;

            $cod = new CashOnDelivery();
            $cod->user_address = $customerAddress;
            $cod->rate = $rate;
            $cod->user_id = $user->id;
            $cod->save();

            $result = $cod;
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return response()->json(['message' => 'Not Found.'], 400);
        }
        return response()->json($result, 200);
    }

    public function aboutUs(Request $request) {
        $lang = $request->header('x-language');
        $config = Config::leftJoin('about_us_lang AS b', function($query) use ($lang) {
            $query->on('configs.id', '=', 'b.config_id');
            $query->on('b.lang', '=', DB::raw("'$lang'"));
        })
        ->select('configs.id','configs.about_us_email','configs.about_us_phone','configs.facebook_account','configs.twitter_account','configs.instagram_account','configs.storage_address','configs.about_us_mobile','configs.cod_amount','configs.storage_lat','configs.storage_lng',
            DB::raw('CASE WHEN b.title IS NULL THEN configs.about_us_title ELSE b.title END AS about_us_title'),
            DB::raw('CASE WHEN b.about_us_content IS NULL THEN configs.about_us_content ELSE b.about_us_content END AS about_us_content'))
        ->get();
        if(!$config) return response()->json(['message'=>'Config Not Found'], 404);
        return response()->json($config, 200);
    }

    public function updateProductReview(Request $request, $id){
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser();
        $review = Review::where('user_id',$user->id)->find($id);
        if(!$review) return response()->json(['message'=>'Review Not Found'], 404);
        try {
            $review->user_id = $user->id;
            $review->product_id = $request->input('product_id');
            $review->remarks = $request->input('remarks');
            $review->rate = $request->input('rate');
            $review->save();
        } catch (\Exception $e) {
            return response()->json(['message' => 'Not Found.'], 400);
        }
        if (!$review) return response()->json(['message'=>'Not Found'], 404);
        return response()->json($review, 200);
    }

    public function paypalPayment(Request $request) {
        $this->validateJson($request, PaypalPayment::addRules());

        $paypal = new PaypalHelper();
        $paypal = $paypal->checkPaymentId($request->payment_id);
        if ($paypal) {
            if ($paypal->state == "approved") {

                // $id = $request->get('payment_id');
                // $payment = PayPal::getById($id, $this->_apiContext);
                $paymentExecution = Paypal::PaymentExecution();
                $paymentExecution->setPayerId($paypal->payer->payer_info->payer_id);
                // $executePayment = $payment->execute($paymentExecution, $this->_apiContext);

                $paypalPayment = new PaypalPayment();
                $paypalPayment->payment_id = $request->payment_id;
                $paypalPayment->order_id = $request->order_id;
                $paypalPayment->payment_status = $paypal->state;
                $paypalPayment->payment_amount = $paypal->transactions[0]->amount->total;
                // $paypalPayment->payer_id = $paypal->payer->payer_info->payer_id;
                $paypalPayment->save();

                $order = Order::find($request->order_id);
                $order->status = OrderStatus::PACKING;
                $order->reference_id = $request->payment_id;
                $order->save();

                $tokens = Installation::where('user_id','=', $order->user_id)
                ->whereNotNull('device_token')
                ->where('device_token','<>', '')
                ->pluck('device_token')->toArray();

                $setting = [
                'title' => 'Mareco Store',
                'body' => 'Your Order status was changed',
                'sound' => 'default', 
                'data' => ['order_id' => $order->id], 
                ];

                $response = FcmHelper::pushNotification($tokens, $setting);

                return response()->json(['message' => 'Paypal Transaction Completed!'], 200);
            }
            return response()->json(['message' => 'Paypal Transaction fail!'], 400);

        }
        // return response($paypal);
    }

    public function notificationSetting(Request $request) {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser();

        $user = User::find($user->id);
        if(!$user) return response()->json(['message'=>'User Not Found'], 404);
        try {
            $user->new_product = $request->input('new_product');
            $user->promotion = $request->input('promotion');
            $user->save();
        } catch (\Exception $e) {
            return response()->json(['message' => 'Not Found.'], 400);
        }
        if (!$user) return response()->json(['message'=>'Not Found'], 404);
        return response()->json(['message'=>'Settings Updated!'], 200);
    }

    public function term(Request $request){
        $lang = $request->header('x-language');
        if($lang == 'id') {
            $term = Term::where('lang','=','id')->first();
        } else {
            $term = Term::where('lang','=','en')->first();
        }
        if(!$term) return response()->json(['message' => ' '], 400);
        return response()->json($term, 200);
    }

    public function promotionList() {
        $today = Carbon::today()->format('Y-m-d');
        $promotion = Promotion::with('product')
            ->whereDate('start_date','<=', $today)
            ->whereDate('end_date','>=',$today)->get();
        if(!$promotion) return response()->json(['message'=>'Not Found'], 404);
        return response()->json($promotion, 200);
    }

}
