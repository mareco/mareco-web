<?php

namespace App\Http\Controllers\Api\V1;

use App\Helpers\Enums\UserRole;
use App\Helpers\FileUploadHelper;
use App\Http\Controllers\Controller;
use App\Mail\ForgotPassword;
use App\Mail\Registration;
use App\Models\Installation;
use App\Models\User;
use App\Models\UserSocialAccount;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Password; 
use Hash;
use Illuminate\Http\Request;
use JWTAuth;
use Mail;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class AuthController extends Controller
{
	function userProfile($token)
	{
		$user = JWTAuth::toUser($token);
		return [
			'id' => $user->id,
            'first_name' => $user->first_name,
			'last_name' => $user->last_name,
			'email' => $user->email,
            'phone' => $user->phone,
            'photo' => $user->photo,
            'role' => $user->role,
            'address_id' => $user->addresses,
            'point' => $user->point,
            'promotion' => $user->promotion,
            'new_product' => $user->new_product,
		];
	}

    function responseLogin($token)
    {
        $user = JWTAuth::toUser($token);
        $installation = new Installation();
        $installation->session_token = $token;
        $installation->user_id = $user->id;
        $installation->save();

        $user = $this->userProfile($token);
        return response()->json(compact('token', 'user'));
    }

    public function login(Request $request)
    {
        $this->validateJson($request, [
            'email' => 'required',
            'password' => 'required',
        ]);
        $credentials = $request->only('email', 'password');

        if (!$token = JWTAuth::attempt($credentials)) {
            return response()->json(['message' => 'Invalid credentials'], 401);
        }
        return $this->responseLogin($token);
    }

     public function register(Request $request) {
        $request->offsetSet('role', UserRole::USER);
        $this->validateJson($request, User::addRules());

        try {

            $user = new User();
            $user->first_name = '';
            if ($request->first_name) $user->first_name = $request->first_name;
            $user->last_name = '';
            if ($request->last_name) $user->last_name = $request->last_name;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->role = UserRole::USER;
            $user->phone = $request->phone;
            if ($request->file('photo')) {
                $upload = FileUploadHelper::upload($request, 'photo', 'jpeg,bmp,png,jpg');
                $user->photo = $upload;
            }
            $user->save();

        } catch (\Exception $e) {
            return response()->json(['message' => 'This operation has been canceled, Please contact system administrator'], 400);
        }

        Mail::to($user->email)->send(new Registration($user));

        $token = JWTAuth::fromUser($user);
        return $this->responseLogin($token);
    }

	public function facebookLogin(Request $request)
	{
		$token = $request->token;
		if (!$token) return response()->json(['message' => 'Token is required'], 400);

		$client = new Client();
        try {
            $res = $client->request('GET', 'https://graph.facebook.com/me?fields=id,name,email&access_token='.$token, ['verify' => false]);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Invalid facebook access token'], 400);
        }
        $facebook = json_decode($res->getBody());

        $provider = 'facebook';
        $user = UserSocialAccount::where('provider', '=', $provider)
            ->where('provider_user_id', '=', $facebook->id)
            ->first();
        if ($user) {
            $user = $user->user;
        }
        else {
            if (isset($facebook->email)) $user = User::where('email', '=', $facebook->email)->first();
        }
        if (!$user) {
            $password = strtoupper(str_random(8));
            $user = new User();
            $user->first_name = $facebook->name;
            $user->email = '';
	        if (isset($facebook->email)) $user->email = $facebook->email;
	        $user->password = bcrypt($password);
	        $user->role = UserRole::USER;
            $user->photo = 'http://graph.facebook.com/'.$facebook->id.'/picture?type=large';
	        $user->save();

	        $userSocialAccount = new UserSocialAccount();
	        $userSocialAccount->user_id = $user->id;
	        $userSocialAccount->provider = $provider;
	        $userSocialAccount->provider_user_id = $facebook->id;
	        $userSocialAccount->save();

            Mail::to($user->email)->send(new Registration($user));
        }

        $token = JWTAuth::fromUser($user);
        return $this->responseLogin($token);
	}

    public function googleLogin(Request $request) {
        $token = $request->token;
        if (!$token) return response()->json(['message' => 'Token is required'], 400);

        $client = new Client();
        try {
            $res = $client->request('GET', 'https://www.googleapis.com/oauth2/v3/tokeninfo?fields=sub,email,name,picture,given_name,family_name&id_token='.$token, ['verify' => false]);
        }catch (\Exception $e) {
            return response()->json(['message' => 'Invalid Google access token!'], 400);
        }
        $google = json_decode($res->getBody());
        $provider = 'google';
        $user = UserSocialAccount::where('provider', '=', $provider)
            ->where('provider_user_id', '=', $google->sub)
            ->first();
        if ($user) {
            $user = $user->user;
        }
        else {
            if (isset($google->email)) $user = User::where('email', '=', $google->email)->first();
        }
        if (!$user) { 
            $password = str_random(8);

            $user = new User();
            $user->first_name = $google->given_name;
            $user->last_name = $google->family_name;
            $user->email = $google->email;
            $user->password = Hash::make($password);
            $user->photo = $google->picture;
            $user->role = UserRole::USER;
            $user->phone = '';
            $user->save();

            $userSocialAccount = new UserSocialAccount();
            $userSocialAccount->user_id = $user->id;
            $userSocialAccount->provider = $provider;
            $userSocialAccount->provider_user_id = $google->sub;
            $userSocialAccount->save();

            Mail::to($user->email)->send(new Registration($user));
        }
        // return response()->json(compact('api_token', 'user'));
        $token = JWTAuth::fromUser($user);
        return $this->responseLogin($token);

    }

    public function broker()
    {
        return Password::broker();
    }

    public function forgotPassword(Request $request) {
        $this->validateJson($request, ['email' => 'required|email']);
        $user = User::where('email', '=', $request->email)->first();
        if (!$user) return response()->json(['message' => 'Email not registered'], 404);

        $response = $this->broker()->sendResetLink(
            $request->only('email')
        );
        if ($response == Password::RESET_LINK_SENT) return response()->json(['message' => 'Email sent']);
        return response()->json(['message' => 'Fail to sent email'], 400);
    }

    public function updateInstallation(Request $request)
    {
        $this->validateJson($request, Installation::editRules());

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        Installation::where('device_token', '=', $request->device_token)
        ->where('user_id', '<>', $user->id)
        ->update(['device_type' => '', 'device_token' => '']);

        $installation = Installation::where('session_token', '=', $token)->first();
        if (!$installation) return response()->json(['message' => 'Not found'], 404);
        $installation->device_type = $request->device_type;
        $installation->device_token = $request->device_token;
        $installation->save();

        return response()->json(['message' => 'Installation updated']);
    }

    public function logout()
    {
        $token = JWTAuth::getToken();
        Installation::where('session_token', '=', $token)->delete();
        JWTAuth::invalidate($token);

        return response()->json(['message' => 'Log out']);
    }

	public function profile()
	{
	    $token = JWTAuth::getToken();
	    $user = $this->userProfile($token);
	    return response()->json($user);
	}

    public function updateProfile(Request $request) {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $rules = User::updateProfileRules($user->id);
        if (!$request->email) unset($rules['email']);
        $this->validateJson($request, $rules);

        try {
            $user = User::find($user->id);
            if ($request->first_name) $user->first_name = $request->first_name;
            if ($request->last_name) $user->last_name = $request->last_name;
            if ($request->email) $user->email = $request->email;
            if ($request->phone) $user->phone = $request->phone;
            if ($request->file('photo')) {
                $upload = FileUploadHelper::upload($request, 'photo', 'jpeg,bmp,png');
                $user->photo = $upload;
            }
            if ($request->old_password || $request->new_password) {
                if (!$request->old_password) return response()->json(['message' => 'Old password is required'], 400);
                if (!$request->new_password) return response()->json(['message' => 'New password is required'], 400);
                if (!Hash::check($request->old_password, $user->password)) {
                    return response()->json(['message' => 'Old password is wrong'], 400);
                }
                $user->password = bcrypt($request->new_password);
            }
            $user->save();

        } catch (\Exception $e) {
            return response()->json(['message' => 'This operation has been canceled, Please contact system administrator'], 400);
        }
        
        $token = JWTAuth::getToken();
        $user = $this->userProfile($token);
        return response()->json($user);
    }

}
