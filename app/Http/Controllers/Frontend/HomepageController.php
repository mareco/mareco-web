<?php

namespace App\Http\Controllers\Frontend;

use App\Helpers\SessionHelper;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Config;
use App\Models\FeatureBrand;
use App\Models\Picture;
use App\Models\Product;
use App\Models\Promotion;
use App\Models\UserService;
use App\Models\Variant;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Session;
use Validator;

class HomepageController extends Controller
{

	public function showHomepage($locale) {
		if($locale == "en") {
			App::setLocale('en');
		} elseif ($locale == "id") {
			App::setLocale('id');
		}
		$firstPicture = Picture::where('is_first','=',1)->first();
		if ($firstPicture == null) {
			$firstPicture = Picture::where('is_first','!=',1)->first();
			if ($firstPicture != null) {
				$picture = Picture::where('is_first','!=',1)->where('id','!=',$firstPicture->id)->get();
			} else {
				$picture = null;
			}
		} else {
			$picture = Picture::where('is_first','!=',1)->get();
		}
		$today = Carbon::today()->format('Y-m-d');
		$product = Product::where('is_featured','=',1)->get();
		$promotion = Promotion::whereDate('start_date','<=', $today)
		->whereDate('end_date','>=',$today)->orderBy('size','asc')->limit(2)->get();
		$allPromotion = Promotion::whereDate('start_date','<=', $today)
		->whereDate('end_date','>=',$today)->orderBy('size','asc')->get();
		if(count($promotion) == 0 ){
			$variants = Variant::all();
			foreach ($variants as $variant) {
				$variant->is_discount = 0;
				$variant->discount_price = 0;
				$variant->save();
			}
		}
		$brands = FeatureBrand::all();

		return view('frontend.homepage')->with('homepage', 'active')->with('firstPicture', $firstPicture)
		->with('picture', $picture)
		->with('promotion', $promotion)
		->with('allPromotion', $allPromotion)
		->with('locale', $locale)
		->with('brands', $brands)
		->with('product', $product);
	}

	public function showShoppingCart() {
		return view('frontend.cart');
	}
	public function showCheckout() {
		return view('frontend.checkout');
	}

	public function showMorePromotion($locale) {
		$today = Carbon::today()->format('Y-m-d');
		$promotion = Promotion::whereDate('start_date','<=', $today)
		->whereDate('end_date','>=',$today)->orderBy('size','asc')->paginate(8);

		return view('frontend.show_more_promotion')->with('promotion', $promotion)
		->with('locale', $locale)
		->with('breadcrumb', 'Promotion');
	}

	public static function showFooter() {
		$config = Config::first();
		if($config == null) {
			$config = '';
		}
		return $config;
	}

	public function getInTouch($locale, Request $request) {
		if($locale == "en") {
			App::setLocale('en');
		} elseif ($locale == "id") {
			App::setLocale('id');
		}
		$cs = new UserService();
		$cs->name = $request->name;
		$cs->email = $request->email;
		$cs->message = $request->message;
		$cs->save();

		return view('frontend.contact-us.get-in-touch')->with('locale', $locale);
	}

	public function searchProduct($locale, Request $request) {
		if($locale == "en") {
			App::setLocale('en');
		} elseif ($locale == "id") {
			App::setLocale('id');
		}
		$products = Product::where('name', 'LIKE', '%'.$request->search.'%')->get();
		$content = '<ul>';
		if(count($products) < 1) $content .= '<li style="list-style : none">No Product Found</li>';
		foreach ($products as $product) {
			$content .= '<li><a href="'.route('product.details',["locale" => $locale,"slug" => $product->slug_url]).'">'.$product->name.'</a></li>';
			// $content .= '<li><a href="'.$locale.'/product/detail/'.$product->slug_url.'">'.$product->name.'</a></li>';
		}

		$content .= '</ul>';

		return $content;
	}
}
