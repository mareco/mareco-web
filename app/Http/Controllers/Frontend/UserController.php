<?php

namespace App\Http\Controllers\Frontend;

use App\Helpers\FileUploadHelper;
use App\Helpers\SessionHelper;
use App\Http\Controllers\Controller;
use App\Models\Address;
use App\Models\Order;
use App\Models\Point;
use App\Models\User;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Session;
use Validator;

class UserController extends Controller
{

	public function account($locale) {
		if($locale == "en") {
			App::setLocale('en');
		} elseif ($locale == "id") {
			App::setLocale('id');
		}
		$user = User::where('id','=',Auth::user()->id)->first();
		$shipping_address = Address::where('user_id','=',Auth::user()->id)->where('is_shipping_address','=',1)->orderBy('id','desc')->first();
		$billing_address = Address::where('user_id','=',Auth::user()->id)->where('is_billing_address','=',1)->orderBy('id','desc')->first();

		// $pointHistory = Order::where('user_id','=', Auth::user()->id)->where('point','>',1)->get();
        $pointHistory = Point::where('user_id','=', $user->id)->orderBy('created_at','desc')->paginate(10);
        $totalPoint = DB::table('points')->where('user_id','=', $user->id)->sum('amount');
		
		return view('frontend.account.account-dashboard')->with('user', $user)
		->with('shipping_address', $shipping_address)
		->with('locale', $locale)
		->with('pointHistory', $pointHistory)
		->with('totalPoint', $totalPoint)
		->with('billing_address', $billing_address)
		->with('homepage', 'active');
	}

	public function accountDetail($locale) {
		if($locale == "en") {
			App::setLocale('en');
		} elseif ($locale == "id") {
			App::setLocale('id');
		}
		$user = User::where('id','=',Auth::user()->id)->first();
		return view('frontend.account.account-detail')
		->with('locale', $locale)
		->with('user', $user)
		->with('homepage', 'active');
	}

	public function updateAccount($locale, Request $request) {
		if($locale == "en") {
			App::setLocale('en');
		} elseif ($locale == "id") {
			App::setLocale('id');
		}
		$user = User::where('id','=',Auth::user()->id)->first();
		$user->first_name = $request->first_name;
		$user->last_name = $request->last_name;
		$user->email = $request->email;
		$user->phone = $request->phone;
		if ($request->file('photo')) {
			$upload = FileUploadHelper::upload($request, 'photo', 'jpeg,bmp,png,jpg');
			if ($upload == false) {
				Session::flash("Error","The type image is not suitable");
				return redirect()->back();
			} else {
				$user->photo = $upload;
			}
		}
		$user->save();

		Session::flash("Success","You have updated your account");
		return redirect("/customer/".$locale."/account/detail");
	}

	public function updatePassword($locale, Request $request) {
		if($locale == "en") {
			App::setLocale('en');
		} elseif ($locale == "id") {
			App::setLocale('id');
		}
		$checkOldPassword = Hash::check($request->old_password, Auth::user()->password);
		if ($checkOldPassword == true) {
			if ($request->password != $request->confirm_password) {
				Session::flash("Error","Your new password is not same with the current one, please check it");
				return redirect('/customer/'.$locale.'/change/password');
			}
		}
		else if ($checkOldPassword == false) {
			if($locale == "en") {
				Session::flash("Error","Please check your old password");
			} elseif ($locale == "id") {
				Session::flash("Error","Kata sandi lama anda salah");
			}
			return redirect('/customer/'.$locale.'/change/password');
		}
		$user = User::findOrFail(Auth::user()->id);
		$user->password = Hash::make($request->password);
		$user->save();
		if($locale == "en") {
			Session::flash("Success","You have updated your password");
		} elseif ($locale == "id") {
			Session::flash("Success","Kata sandi anda berhasil diubah");
		}
		return redirect('/customer/'.$locale.'/change/password');
	}

	public static function getUser() {
		$user = User::where('id','=',Auth::user()->id)->first();
		return $user;
	} 

	public function changeFrontPassword($locale) {
		if($locale == "en") {
			App::setLocale('en');
		} elseif ($locale == "id") {
			App::setLocale('id');
		}
		return view('frontend.account.change-password')
		->with('locale', $locale);
	}

	public function activateAccount(Request $request) {
		$user = User::where('email','=',$request->email)->first();
		if ($user->is_activate == 1) {
			Session::flash("Error","Your account is allready activated");
		} else {
			if ($user->activate_code == $request->activate_code) {
				$user->is_activate = 1;
				$user->save();
				Session::flash("Success","Your account is activated");
			} else {
				Session::flash("Error","Your activated code is wrong");
			}
		}
		return redirect($request->locale.'/login');
	}

}