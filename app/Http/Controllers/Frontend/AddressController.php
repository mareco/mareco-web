<?php

namespace App\Http\Controllers\Frontend;

use App\Helpers\SessionHelper;
use App\Http\Controllers\Controller;
use App\Models\Address;
use App\Models\User;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Session;
use Steevenz\Rajaongkir;
use Validator;

class AddressController extends Controller
{
	public function showAddress($locale) {
		if($locale == "en") {
			App::setLocale('en');
		} elseif ($locale == "id") {
			App::setLocale('id');
		}
		$shipping_address = Address::where('user_id','=',Auth::user()->id)->where('is_shipping_address','=',1)->orderBy('id','desc')->first();
		$billing_address = Address::where('user_id','=',Auth::user()->id)->where('is_billing_address','=',1)->orderBy('id','desc')->first();
		$additional_address = Address::where('user_id','=',Auth::user()->id)->where('is_shipping_address','=',null)->get();
		return view('frontend.account.customer-address')->with('shipping_address', $shipping_address)
		->with('billing_address', $billing_address)
		->with('locale', $locale)
		->with('additional_address', $additional_address)
		->with('homepage', 'active');
	}

	public function createBillingAddress() {
		return view('frontend.account.billing-address');
	}

	public function showCreateForm($locale) {
		if($locale == "en") {
			App::setLocale('en');
		} elseif ($locale == "id") {
			App::setLocale('id');
		}
		$config['api_key'] = '56950ca5aa637a0c5efa1b266e28f160';
		$config['account_type'] = 'pro';
		$rajaongkir = new Rajaongkir($config);
		$provinces = $rajaongkir->get_provinces();
		$destinationIN = $rajaongkir->get_international_destinations();
		$address = null;

		return view('frontend.account.shipping-address')->with('provinces', $provinces)
		->with('destination', $destinationIN)
		->with('locale', $locale)
		->with('address', $address)
		->with('homepage', 'active');
	}

	public function showUpdateForm($locale, $id) {
		if($locale == "en") {
			App::setLocale('en');
		} elseif ($locale == "id") {
			App::setLocale('id');
		}
		$config['api_key'] = '56950ca5aa637a0c5efa1b266e28f160';
		$config['account_type'] = 'pro';
		$rajaongkir = new Rajaongkir($config);
		$address = Address::where('id','=',$id)->first();
		$user = User::where('id','=',$address->user_id)->first();
		$provinces = $rajaongkir->get_provinces();
		$city = $rajaongkir->get_cities($address->province);
		$district = $rajaongkir->get_subdistricts($address->city);
		$destinationIN = $rajaongkir->get_international_destinations();
		return view('frontend.account.shipping-address')->with('provinces', $provinces)
		->with('city', $city)
		->with('district', $district)
		->with('destination', $destinationIN)
		->with('locale', $locale)
		->with('user', $user)
		->with('address', $address)
		->with('id', $id)
		->with('homepage', 'active');
	}

	public function getCity(Request $request) {
		$config['api_key'] = '56950ca5aa637a0c5efa1b266e28f160';
		$config['account_type'] = 'pro';
		$rajaongkir = new Rajaongkir($config);
		$city = $rajaongkir->get_cities($request->province_id);

		return response()->json(['city' => $city,'count' => count($city)]);
	}

	public function getDistrict(Request $request) {
		$config['api_key'] = '56950ca5aa637a0c5efa1b266e28f160';
		$config['account_type'] = 'pro';
		$rajaongkir = new Rajaongkir($config);
		$city = $rajaongkir->get_city($request->city_id);
		$district = $rajaongkir->get_subdistricts($request->city_id);

		return response()->json(['district' => $district,'count' => count($district)]);
	}

	public function saveAddress($locale, Request $request) {
		if($locale == "en") {
			App::setLocale('en');
		} elseif ($locale == "id") {
			App::setLocale('id');
		}
		$config['api_key'] = '56950ca5aa637a0c5efa1b266e28f160';
		$config['account_type'] = 'pro';
		$rajaongkir = new Rajaongkir($config);

		$province_name = $rajaongkir->get_province($request->province)['province'];
		$city_name = $rajaongkir->get_city($request->city)['city_name'];
		$district_name = $rajaongkir->get_subdistrict($request->district)['subdistrict_name'];

		$address = new Address();
		$address->province = $request->province;
		$address->city = $request->city;
		$address->district = $request->district;
		$address->state = 'Indonesia';
		$address->address = $request->address;
		$address->postal_code = $request->postal_code;
		$address->user_id = Auth::user()->id;
		$address->city_name = $city_name;
		$address->province_name = $province_name;
		$address->district_name = $district_name;
		if ($request->is_shipping_address == 'true') {
			$address->is_shipping_address = 1;
			// $address->is_billing_address = 0;

			$shipping_address = Address::where('user_id','=',Auth::user()->id)->where('is_shipping_address','=',1)->orderBy('id','desc')->first();
			if ($shipping_address != null) {
				$shipping_address->is_shipping_address = null;
				$shipping_address->save();
			}
		}
		$address->save();
		if($locale == "en") {
			Session::flash("Success","You have successful create address");
		} elseif ($locale == "id") {
			Session::flash("Success","Tambah alamat sukses");
		}
		return redirect("/customer/$locale/address");
	}

	public function updateAddress(Request $request, $locale) {
		if($locale == "en") {
			App::setLocale('en');
		} elseif ($locale == "id") {
			App::setLocale('id');
		}
		$config['api_key'] = '56950ca5aa637a0c5efa1b266e28f160';
		$config['account_type'] = 'pro';
		$rajaongkir = new Rajaongkir($config);

		$province_name = $rajaongkir->get_province($request->province)['province'];
		$city_name = $rajaongkir->get_city($request->city)['city_name'];
		$district_name = $rajaongkir->get_subdistrict($request->district)['subdistrict_name'];

		$address = Address::findOrFail($request->id);
		$address->province = $request->province;
		$address->city = $request->city;
		$address->district = $request->district;
		// $address->state = $request->country;
		$address->address = $request->address;
		$address->postal_code = $request->postal_code;
		$address->city_name = $city_name;
		$address->province_name = $province_name;
		$address->district_name = $district_name;
		if ($request->is_shipping_address == 'true') {
			$address->is_shipping_address = 1;
			// $address->is_billing_address = 0;
		}
		$address->save();
		if($locale == "en") {
			Session::flash("Success","You have successful update address");
		} elseif ($locale == "id") {
			Session::flash("Success","Update alamat sukses");
		}
		return redirect("/customer/$locale/address");
	}	

	public function delete($locale, $id) {
		if($locale == "en") {
			App::setLocale('en');
		} elseif ($locale == "id") {
			App::setLocale('id');
		}
		$address = Address::findOrFail($id);
		$address->delete();
		if($locale == "en") {
			Session::flash("Success","You have deleted the address");
		} elseif ($locale == "id") {
			Session::flash("Success","Alamat sukses dihapus");
		}
		return redirect("/customer/$locale/address");
	}

	// public static function getCountryName($id) {
	// 	$config['api_key'] = '56950ca5aa637a0c5efa1b266e28f160';
 // 		$config['account_type'] = 'pro';
 // 		$rajaongkir = new Rajaongkir($config);

 // 		if ($id == 236) {
 // 			$country = "Indonesia";
 // 		} else {
 // 			$country = $rajaongkir->get_international_destination($id)['country_name'];
 // 		}
 // 		return $country;
	// }
}