<?php

namespace App\Http\Controllers\Frontend;

use App\Helpers\SessionHelper;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Address;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Laravel\Socialite\Facades\Socialite;
use Redirect;
use RedirectsUsers;
use Session;
use Validator;
use Mail;
use Steevenz\Rajaongkir;

class LoginController extends Controller
{
	use ThrottlesLogins;

	public function showLoginForm($locale) {
        if($locale == "en") {
            App::setLocale('en');
        } elseif ($locale == "id") {
            App::setLocale('id');
        }
        
		return view('frontend.login')->with('locale', $locale)->with('homepage', 'active');;
	}

	public function showRegistrationForm($locale) {
        if($locale == "en") {
            App::setLocale('en');
        } elseif ($locale == "id") {
            App::setLocale('id');
        }

        $config['api_key'] = '56950ca5aa637a0c5efa1b266e28f160';
        $config['account_type'] = 'pro';
        $rajaongkir = new Rajaongkir($config);
        $provinces = $rajaongkir->get_provinces();
		return view('frontend.register')->with('locale', $locale)->with('homepage', 'active')->with('provinces', $provinces);
	}

	public function login(Request $request, $locale)
    {
        $this->validateLogin($request);

        $user = User::where('email','=',$request->email)->first();
        if (count($user) != 0) {
            if ($user->google_id == null && $user->facebook_id == null) {
                if ($user->is_activate == 1) {
                    // If the class is using the ThrottlesLogins trait, we can automatically throttle
                    // the login attempts for this application. We'll key this by the username and
                    // the IP address of the client making these requests into this application.
                    if ($this->hasTooManyLoginAttempts($request)) {
                        $this->fireLockoutEvent($request);

                        return $this->sendLockoutResponse($request);
                    }

                    if ($this->attemptLogin($request)) {
                        return $this->sendLoginResponse($locale, $request);
                    }

                    // If the login attempt was unsuccessful we will increment the number of attempts
                    // to login and redirect the user back to the login form. Of course, when this
                    // user surpasses their maximum number of attempts they will get locked out.
                    $this->incrementLoginAttempts($request);

                    return $this->sendFailedLoginResponse($request);
                } else {
                    Session::flash("Error","You need to activate your account !");
                    return redirect()->back();
                }
            } else {
                if ($this->hasTooManyLoginAttempts($request)) {
                    $this->fireLockoutEvent($request);

                    return $this->sendLockoutResponse($request);
                }

                if ($this->attemptLogin($request)) {
                    return $this->sendLoginResponse($locale, $request);
                }

                // If the login attempt was unsuccessful we will increment the number of attempts
                // to login and redirect the user back to the login form. Of course, when this
                // user surpasses their maximum number of attempts they will get locked out.
                $this->incrementLoginAttempts($request);

                return $this->sendFailedLoginResponse($request);
            }
        }
    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            $this->username() => 'required|string',
            'password' => 'required|string',
        ]);
    }
    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        return $this->guard()->attempt(
            $this->credentials($request), $request->has('remember')
        );
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        return $request->only($this->username(), 'password');
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse($locale, Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        return $this->authenticated($request, $this->guard()->user())
                ?: redirect()->intended($this->redirectPath($locale));
    }

    public function redirectPath($locale)
    {
        if($locale == "en") {
            App::setLocale('en');
        } elseif ($locale == "id") {
            App::setLocale('id');
        }
        if (method_exists($this, 'redirectTo')) {
            return $this->redirectTo();
        }

        return property_exists($this, 'redirectTo') ? $this->redirectTo : '/customer/'.$locale.'/account';
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        //
    }

    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        $errors = [$this->username() => trans('auth.failed')];

        if ($request->expectsJson()) {
            return response()->json($errors, 422);
        }

        return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors($errors);
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'email';
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request, $locale)
    {
        if($locale == "en") {
            App::setLocale('en');
        } elseif ($locale == "id") {
            App::setLocale('id');
        }
        $this->guard()->logout();

        $request->session()->flush();

        $request->session()->regenerate();

        return redirect($locale.'/login');
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'phone' => 'required|max:15',
            'address' => 'required',
            'province' => 'required',
            'city' => 'required',
            'district' => 'required',
            'postal_code' => 'required'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'role' => $data['role'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'password' => bcrypt($data['password']),
            'password' => bcrypt($data['password']),
        ]);
    }

    public function redirectToProvider($provider,$locale)
    {
        Session::put('locale',$locale);
        return Socialite::driver($provider)->redirect();
    }

    public function socialLogin($provider)
    {
        //notice we are not doing any validation, you should do it
        $locale = Session::get('locale');
        if($locale == "en") {
            App::setLocale('en');
        } elseif ($locale == "id") {
            App::setLocale('id');
        }
        $user = Socialite::driver($provider)->user();
        $email = '';
        $first_name = '';
        $last_name = '';
        if ($provider == "google") {
            $field_name = 'google_id';
            $email = $user->email;
            $first_name = $user->user['name']['givenName'];
            $last_name = $user->user['name']['familyName'];
        }
        else if ($provider == "facebook") {
            $field_name = 'facebook_id';
            if ($user->email) $email = $user->email;
            $first_name = $user->user['name'];
        }
        $customer = User::where('email','=',$email)->first();

        if (!$customer) {
            $hasPassword = true;
            $password = str_random(8);
            $customer = new User();
            $customer->first_name = $first_name;
            $customer->last_name = $last_name;
            $customer->email = $email;
            $customer->password = Hash::make($password);
            $customer->photo = $user->avatar;
            $customer->phone = '';
            if ($provider == "google") {
                $customer->google_id = $user->id;
            } else {
                $customer->facebook_id = $user->id;
            }
            $customer->role = 'user';
            $customer->save();
        } else {
            $hasPassword = false;
            $customer->photo = $user->avatar;
            if ($provider == "google") {
                $customer->google_id = $user->id;
            } else {
                $customer->facebook_id = $user->id;
            }
            $customer->save();
        }

        if ($hasPassword == true) {
            $data = array('first_name' => $first_name, 'email' => $email,'password' => $password);
            Mail::send('frontend.pdf.changePassword', $data, function ($message) use ($data)
            {
                $emailCustomer = $data['email'];
                $message->from('iskandarjonny65@gmail.com')->subject('Information Password');
                $message->to($emailCustomer);
            });
        }

        //after login to home page
        Auth()->login($customer, true);
        return redirect()->to('/customer'.'/'.$locale.'/account');
    }

    public function register($locale, Request $request)
    {
        if($locale == "en") {
            App::setLocale('en');
        } elseif ($locale == "id") {
            App::setLocale('id');
        }
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $user = new User;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->phone = $request->phone;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->role = 'user';
        if ($request->file('photo')) {
            $upload = FileUploadHelper::upload($request, 'photo', 'jpeg,bmp,png,jpg');
            $user->photo = $upload;
        }
        $user->password = Hash::make($request->password);
        $randomCode = str_random(6);
        $user->activate_code = $randomCode;
        $user->save();

        $config['api_key'] = '56950ca5aa637a0c5efa1b266e28f160';
        $config['account_type'] = 'pro';
        $rajaongkir = new Rajaongkir($config);

        $province_name = $rajaongkir->get_province($request->province)['province'];
        $city_name = $rajaongkir->get_city($request->city)['city_name'];
        $district_name = $rajaongkir->get_subdistrict($request->district)['subdistrict_name'];

        $address = new Address();
        $address->province = $request->province;
        $address->city = $request->city;
        $address->district = $request->district;
        $address->state = 'Indonesia';
        $address->address = $request->address;
        $address->postal_code = $request->postal_code;
        $address->user_id = $user->id;
        $address->city_name = $city_name;
        $address->province_name = $province_name;
        $address->district_name = $district_name;
        $address->is_shipping_address = 1;
        $address->save();

        $data = array('user' => $user,'locale' => $locale);
        Mail::send('frontend.activate.activate_link', $data, function ($message) use ($data)
        {
            $emailCustomer = $data['user']->email;
            $message->from('iskandarjonny65@gmail.com')->subject('Activate Your Account');
            $message->to($emailCustomer);
        });

        return redirect($locale.'/login')->with('status', 'We have send you activation link, you need to activate your account');
    }

    public function forgot($locale) {
        if($locale == "en") {
            App::setLocale('en');
        } elseif ($locale == "id") {
            App::setLocale('id');
        }
        return view('frontend.forgot')->with('locale', $locale);
    }

}