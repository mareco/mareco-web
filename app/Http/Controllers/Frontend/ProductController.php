<?php

namespace App\Http\Controllers\Frontend;

use App\Helpers\SessionHelper;
use App\Http\Controllers\Controller;
use App\Models\Address;
use App\Models\BankAccount;
use App\Models\Category;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Point;
use App\Models\Product;
use App\Models\Review;
use App\Models\Term;
use App\Models\User;
use App\Models\Variant;
use App\Models\Wishlist;
use Auth;
use Carbon\Carbon;
use Cart;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Ncaneldiee\Rajaongkir as International;
use Session;
use Steevenz\Rajaongkir;
use Validator;

class ProductController extends Controller
{
	public function showProduct(Request $request, $locale) {
		if($locale == "en") {
			App::setLocale('en');
		} elseif ($locale == "id") {
			App::setLocale('id');
		}
		$product = new Product;
		if ($request->category) {
			$category = Category::where('slug_url', $request->category)->first();
			$categoryId = null;
			if ($category) $categoryId = $category->id;
			$product = $product->where('category_id', $categoryId);
		}
		$product = $product->paginate(15);
		return view('frontend.product')->with('product', $product)
		->with('locale', $locale)
		->with('shop', 'active')
		->with('breadcrumb', 'Shop');
	}

	public function showProductDetails($locale, $slug) {
		if($locale == "en") {
			App::setLocale('en');
		} elseif ($locale == "id") {
			App::setLocale('id');
		}

		$product = Product::where('slug_url','=',$slug)->first();
		$variant = Variant::where('product_id','=',$product->id)->first();
		$allVariant = Variant::where('product_id','=',$product->id)->get();
		$reviews = Review::where('product_id','=',$product->id)->get();
		$enTerm = Term::where('lang','=','en')->first();
		$idTerm = Term::where('lang','=','id')->first();
		if (Auth::user()) {
			$checkReview = Order::where('user_id','=',Auth::user()->id)->get();
			foreach ($checkReview as $detail) {
				foreach ($detail->orderDetails as $value) {
					if($value->product_id == $product->id) {
						$detail->existInUser = 'exist';
					} 
				}
			}
			$isExist = $checkReview->where('existInUser','=','exist')->first();
		} else {
			$isExist = null;
		}
		$arrSize = [];
		for ($i = 0; $i < count($allVariant); $i++) {
			if (count($arrSize) == 0) {
				if ($allVariant[$i]->size != null) {
					array_push($arrSize, $allVariant[$i]->size);
				}
			} else {
				for ($n = 0; $n < count($arrSize); $n++) {
					if ($allVariant[$i]->size != null) {
						if ($allVariant[$i]->size == $arrSize[$n]) {
							$push = false;
						} else {
							$push = true;
						}
					}
				}
				if ($push == true) {
					array_push($arrSize, $allVariant[$i]->size);
				}
			}
		}
		if(Auth::user()){
			$review = Review::where('user_id',Auth::user()->id)
			->where('product_id',$product->id)
			->first();
		} else {
			$review = '';
		}

		return view('frontend.product-details')->with('product', $product)
		->with('variant', $variant)
		->with('allVariant', $allVariant)
		->with('arrSize', $arrSize)
		->with('enTerm', $enTerm)
		->with('idTerm', $idTerm)
		->with('locale', $locale)
		->with('isExist', $isExist)
		->with('review', $review)
		->with('reviews', $reviews)
		->with('shop', 'active')
		->with('breadcrumb', 'Shop')
		->with('secondBreadcrumb', $product->name);
	}

	public function editReview($locale, $product) {
		if($locale == "en") {
			App::setLocale('en');
		} elseif ($locale == "id") {
			App::setLocale('id');
		}
		$variant = Variant::where('product_id','=',$product->id)->first();
		$review = Review::where('product_id','=', $product->id)->where('user_id','=',Auth::user()->id)->first();
		return view('frontend.reviews')
		->with('breadcrumb', 'Shop')
		->with('product', $product)
		->with('locale', $locale)
		->with('variant', $variant)
		->with('review', $review)
		;
	}

	public function updateReview($review, Request $request, $locale) {
		if($locale == "en") {
			App::setLocale('en');
		} elseif ($locale == "id") {
			App::setLocale('id');
		}
		try{

			$review->remarks = $request->input('remarks');
			$review->rate = $request->input('rate');
			$review->save();

			SessionHelper::setMessage("Review successfully updated");
			return redirect()->route('product', $locale);
			
		} catch (\Exception $e) {
			DB::rollBack();
			\Log::error($e);
			return back()->withInput()->withErrors('Something wrong');
		}
	}

	public function saveReview($product, Request $request, $locale) {
		if($locale == "en") {
			App::setLocale('en');
		} elseif ($locale == "id") {
			App::setLocale('id');
		}
		try{
			$review = new Review();
			$review->user_id = Auth::user()->id;
			$review->product_id = $product->id;
			$review->remarks = $request->input('remarks');
			$review->rate = $request->input('rate');
			$review->save();

			SessionHelper::setMessage("Review successfully added");
			return redirect()->route('product', $locale);
			
		} catch (\Exception $e) {
			DB::rollBack();
			\Log::error($e);
			return back()->withInput()->withErrors('Something wrong');
		}
	}

	public function getColor(Request $request) {
		$variant = Variant::where('product_id','=',$request->product_id)->where('size','=',$request->size)->get();
		return response()->json(['variant' => $variant, 'countVariant' => count($variant)]);
	}

	public function showShoppingCart($locale) {
		if($locale == "en") {
			App::setLocale('en');
		} elseif ($locale == "id") {
			App::setLocale('id');
		}
		$arrQuantity = [1,2,3,4,5,6,7,8,9,10];
		$cart = Cart::content();
		$subtotal = 0;

		foreach ($cart as $value) {
			$subtotal = $subtotal + ($value->price * $value->qty);	
		}

		if (Auth::user()) {
			$point = DB::table('points')->where('user_id','=', Auth::user()->id)->sum('amount');
			$totalPoint = DB::table('points')->where('user_id','=', Auth::user()->id)->sum('amount');
			if (Auth::user()->use_point == 0) {
				$point = '-';
			} else {
				if ($subtotal < Auth::user()->point) {
					$point = $subtotal;
				} else {
					$point = Auth::user()->amount_point;
				}
			}
		} else {
			$point = '-';
			$totalPoint = '';
		}

		return view('frontend.cart')->with('cart', $cart)
		->with('subtotal', $subtotal)
		->with('locale', $locale)
		->with('point', $point)	
		->with('totalPoint', $totalPoint)	
		->with('arrQuantity', $arrQuantity)
		->with('shop', 'active');
	}

	public function showWishlist($locale) {
		if($locale == "en") {
			App::setLocale('en');
		} elseif ($locale == "id") {
			App::setLocale('id');
		}
		$wishlist = Wishlist::where('user_id','=',Auth::user()->id)->get();
		return view('frontend.wishlist')->with('locale', $locale)->with('wishlist', $wishlist);
	}

	public function deleteWishlist($locale, $id) {
		$wishlist = Wishlist::findOrFail($id);
		$wishlist->delete();

		if($locale == "en") {
			App::setLocale('en');
			Session::flash("Success","You have successful deleted wishlist");

		} elseif ($locale == "id") {
			App::setLocale('id');
			Session::flash("Sukses","Produk sukses hapus dari wishlist");
		}
		return redirect('/customer/'.$locale.'/wishlist');
	}

	public function showCheckout($locale) {
		if($locale == "en") {
			App::setLocale('en');
		} elseif ($locale == "id") {
			App::setLocale('id');
		}

		$cart = Cart::content();
		$listOutStock = '';
		foreach ($cart as $value) {
			$product = Variant::where('id','=',$value->options->variant_id)->first();
			if ($value->qty > $product->qty) {
				if ($listOutStock == '') {
					$listOutStock = $value->name . ' ' . $listOutStock;
				} else {
					$listOutStock = $value->name . ', ' . $listOutStock;
				}
			}
		}

		if ($listOutStock != '') {
			Session::flash("Error", 'Product ' . $listOutStock . ' is out of stock');
			return redirect($locale.'/checkout/cart');
		}

		$user = User::findOrFail(Auth::user()->id);
		$config['api_key'] = '56950ca5aa637a0c5efa1b266e28f160';
		$config['account_type'] = 'pro';
		$rajaongkir = new Rajaongkir($config);
		$bank = BankAccount::all();
		$address = Address::where('user_id','=',$user->id)->where('is_shipping_address','=',1)->first();
		$additionalAddress = Address::where('user_id','=',$user->id)->orderBy('is_shipping_address','desc')->get();
		$provinces = $rajaongkir->get_provinces();
		$destinationIN = $rajaongkir->get_international_destinations();

		if ($address) {
			$cities = $rajaongkir->get_cities($address->province);
			$districts = $rajaongkir->get_subdistricts($address->city);
		} else {
			$cities = null;
			$districts = null;
		}

		if ($address) {
			$city = $rajaongkir->get_cities($address->province);
			$district = $rajaongkir->get_subdistricts($address->city);
		}

		$subtotal = 0;

		foreach ($cart as $value) {
			$subtotal = $subtotal + ($value->price * $value->qty);	
		}

		if (Auth::user()) {
			if (Auth::user()->use_point == 0) {
				$point = '-';
			} else {
				if ($subtotal < Auth::user()->point) {
					$point = $subtotal;
				} else {
					$point = Auth::user()->amount_point;
				}
			}
		} else {
			$point = '-';
		}

		return view('frontend.checkout')->with('user', $user)
		->with('bank', $bank)
		->with('provinces', $provinces)
		->with('destination', $destinationIN)
		->with('address', $address)
		->with('additionalAddress', $additionalAddress)
		->with('districts', $districts)
		->with('cities', $cities)
		->with('subtotal', $subtotal)
		->with('locale', $locale)
		->with('point', $point)
		->with('shop', 'active');
	}

	public function addCart($locale, Request $request) {
		if($locale == "en") {
			App::setLocale('en');
		} elseif ($locale == "id") {
			App::setLocale('id');
		}

		if ($request->size == 'null') {
			$size = null;
		} else {
			$size = $request->size;
		}

		if ($request->color == 'null') {
			$color = null;
		} else {
			$color = $request->color;
		}

		$product = Product::where('id','=',$request->product_id)->first();
		if ($request->color == 'null') {
			$variant = Variant::where('product_id','=',$product->id)->where('size','=',$size)->where('color','=',null)->first();
		} else {
			$variant = Variant::where('product_id','=',$product->id)->where('size','=',$size)->where('color','=',$color)->first();
		}


		if ($variant->qty < 1) {
			if($locale == 'en') {
				$message = 'Sorry, this product is out of stock';
			} else if ($locale == 'id') {
				$message = 'Maaf, produk ini sedang kosong';
			} 
		} else {
			$message = null;
		}

		if ($variant->is_discount == 0) {
			$price = $variant->price;
		} else {
			$price = $variant->discount_price;
		}

		Cart::add(array('id' => $request->product_id,'name' => $product->name,'qty' => $request->quantity,'price' => $price,'options' => ['variant_id' => $variant->id,'photo' => $product->product_image, 'weight' => $variant->weight, 'size' => $request->size, 'color' => $request->color]));
		$qty = 0;
		$cart = Cart::content();
		foreach ($cart as $cartQty) {
			if($cartQty->qty > $variant->qty) {
				if($locale == 'en') {
					$message = 'Sorry, this product is out of stock';
					return response()->json(['data' => $cart, 'qty' => $qty, 'message' => $message]);
				} else if ($locale == 'id') {
					$message = 'Maaf, produk ini sedang kosong';
					return response()->json(['data' => $cart, 'qty' => $qty, 'message' => $message]);
				} 
			} else {
				$qty = $qty + $cartQty->qty;
			}
		}
		
		return response()->json(['data' => $cart, 'qty' => $qty, 'message' => $message]);
	}

	public function addWishlist($locale, Request $request) {
		if($locale == "en") {
			App::setLocale('en');
		} elseif ($locale == "id") {
			App::setLocale('id');
		}

		if ($request->size == 'null') {
			$size = null;
		} else {
			$size = $request->size;
		}

		if ($request->color == 'null') {
			$color = null;
		} else {
			$color = $request->color;
		}

		$product = Product::where('id','=',$request->product_id)->first();
		$message = null;
		if ($request->color == 'null') {
			$variant = Variant::where('product_id','=',$product->id)->where('size','=',$size)->where('color','=',null)->first();
		} else {
			$variant = Variant::where('product_id','=',$product->id)->where('size','=',$size)->where('color','=',$color)->first();
		}
		$dataWishlist = Wishlist::where('user_id','=',Auth::user()->id)->get();

		foreach ($dataWishlist as $value) {
			if ($value->variant_id == $variant->id) {
				if($locale == 'en') {
					$message = 'This product has been already added to wishlist';
				}else if($locale == 'id') {
					$message = 'Produk ini sudah ada di wishlist';
				}
			}
		}

		if ($message == null) {
			$wishlist = new Wishlist();
			$wishlist->user_id = Auth::user()->id;
			$wishlist->variant_id = $variant->id;
			$wishlist->save();
		}

		$countWishlist = Wishlist::where('user_id','=',Auth::user()->id)->get();
		return response()->json(['data' => $dataWishlist,'countWishlist' => count($countWishlist), 'message' => $message]);
	}

	public function updateCart($locale, Request $request) {
		if($locale == "en") {
			App::setLocale('en');
		} elseif ($locale == "id") {
			App::setLocale('id');
		}
		$variant = Variant::findOrFail($request->variant_id);
		$message = null;
		if ($variant->qty > $request->qty) {
			Cart::update($request->id,$request->qty);
		} else {
			if($locale == 'en') {
				$message = 'This product already out of stock';
			} else if($locale == 'id') {
				$message = 'Produk ini sedang kosong';
			}
		}

		return response()->json(['message' => $message]);
	}

	public function deleteCart($locale, $id) {
		if($locale == "en") {
			App::setLocale('en');
		} elseif ($locale == "id") {
			App::setLocale('id');
		}
		Cart::remove($id);
		if (count(Cart::content()) == 0) {
			if (Auth::user()) {
				$user = User::findOrFail(Auth::user()->id);
				$user->use_point = 0;
				$user->amount_point = null;
				$user->save();
			}
		}
		return redirect($locale.'/checkout/cart');
	}

	public function usePoint($locale, Request $request) {
		if($locale == "en") {
			App::setLocale('en');
		} elseif ($locale == "id") {
			App::setLocale('id');
		}
		$user = User::findOrFail(Auth::user()->id);

		if ($request->point == null) {
			if($locale == "en") {
				Session::flash("Error","You need to input point");
			} elseif ($locale == "id") {
				Session::flash("Error","Anda perlu memasukin poin");
			}
			return redirect($locale.'/checkout/cart');
		}
		if ($request->point > $user->point) {
			if($locale == "en") {
				Session::flash("Error","You don't have enough point");
			} elseif($locale == "id") {
				Session::flash("Error","Poin anda tidak mencukupi");
			} 
			return redirect($locale.'/checkout/cart');
		}
		if ($user->point == 0) {
			if($locale == "en") {
				Session::flash("Error","You don't have a point right now");
			} elseif($locale == "id") {
				Session::flash("Error","Sementara anda tidak memiliki poin");
			}
			return redirect($locale.'/checkout/cart');
		}
		$user->amount_point = $request->point;
		$user->use_point = 1;
		$user->save();
		if($locale == "en") {
			Session::flash("Success","You have successful using your point");
		} elseif($locale == "id") {
			Session::flash("Success","Pemakaian poin sukses");
		}
		return redirect($locale.'/checkout/cart');
	}	

	public function cancelPoint($locale) {
		if($locale == "en") {
			App::setLocale('en');
		} elseif ($locale == "id") {
			App::setLocale('id');
		}
		$user = User::findOrFail(Auth::user()->id);
		$user->use_point = 0;
		$user->amount_point = null;
		$user->save();

		if($locale == "en") {
			Session::flash("Success","You have successful cancel using your point");
		} elseif ($locale == "id") {
			Session::flash("Success","Pemakaian poin sukses dibatalkan");
		}
		return redirect($locale.'/checkout/cart');
	}

	public function getCost(Request $request) {
		$config['api_key'] = '56950ca5aa637a0c5efa1b266e28f160';
		$config['account_type'] = 'pro';
		$rajaongkirInterntional = new International\Domestic('56950ca5aa637a0c5efa1b266e28f160');
		$rajaongkir = new Rajaongkir($config);
		if ($request->weight == null) {
			$dataCart = array();
			$totalWeight = 0;
			if ($request->type = 'admin') {
				$dataCart = array();
				$totalWeight = 0;
				foreach(Cart::content() as $row) {
					array_push($dataCart,$row);
					$totalWeight = $totalWeight + $row->options->weight * $row->qty;
				}	
			}
			foreach(Cart::content() as $row) {
				array_push($dataCart,$row);
				$totalWeight = $totalWeight + $row->options->weight * $row->qty;
			}
		} else {
			$totalWeight = $request->weight;
		}
		if ($request->district_id == null) {
			$address = Address::where('id','=',$request->address_id)->first();
			$request->city_id = $address->city_id;
			$request->district_id = $address->district_id;
		}

		$costJNE = $rajaongkir->get_cost(['city' => $request->city_id], ['subdistrict' => $request->district_id], ceil($totalWeight) * 1000, 'jne');
		$costPOS = $rajaongkir->get_cost(['city' => $request->city_id], ['subdistrict' => $request->district_id], ceil($totalWeight) * 1000, 'pos');
		$costTIKI = $rajaongkir->get_cost(['city' => $request->city_id], ['subdistrict' => $request->district_id], ceil($totalWeight) * 1000, 'tiki');

		return response()->json(['costJNE' => $costJNE,'costPOS' => $costPOS,'costTIKI' => $costTIKI,'countJNE' => count($costJNE[0]->costs),'countPOS' => count($costPOS[0]->costs),'countTIKI' => count(count($costTIKI[0]->costs))]);

		// if ($request->country_id == 236) {
		// 	$costJNE = $rajaongkir->get_cost(['city' => $request->city_id], ['subdistrict' => $request->district_id], ceil($totalWeight) * 1000, 'jne');
		// 	$costPOS = $rajaongkir->get_cost(['city' => $request->city_id], ['subdistrict' => $request->district_id], ceil($totalWeight) * 1000, 'pos');
		// 	$costTIKI = $rajaongkir->get_cost(['city' => $request->city_id], ['subdistrict' => $request->district_id], ceil($totalWeight) * 1000, 'tiki');

		// 	return response()->json(['costJNE' => $costJNE,'costPOS' => $costPOS,'costTIKI' => $costTIKI,'countJNE' => count($costJNE[0]->costs),'countPOS' => count($costPOS[0]->costs),'countTIKI' => count(count($costTIKI[0]->costs))]);
		// } else {
		// 	$costJNE = $rajaongkirInterntional->cost($request->city_id, $request->country_id, ceil($totalWeight) * 1000, 'jne');
		// 	$costPOS = $rajaongkirInterntional->cost($request->city_id, $request->country_id, ceil($totalWeight) * 1000, 'pos');
		// 	$costTIKI = $rajaongkirInterntional->cost($request->city_id, $request->country_id, ceil($totalWeight) * 1000, 'tiki');

		// 	return response()->json(['costJNE' => $costJNE->data,'costPOS' => $costPOS->data,'costTIKI' => $costTIKI->data, 'countJNE' => count($costJNE->data[0]->costs),'countPOS' => count($costPOS->data[0]->costs),'countTIKI' => count($costTIKI->data[0]->costs)]);
		// }
	}

	public static function getVariant($id) {
		$variant = Variant::where('product_id','=',$id)->first();
		return $variant;
	}

	public static function getWishlist() {
		$wishlist = Wishlist::where('user_id','=',Auth::user()->id)->get();
		return count($wishlist);
	}

	public static function getRotate($id) {
		$product = Product::where('id','=',$id)->first();
		return $product;
	}
}
