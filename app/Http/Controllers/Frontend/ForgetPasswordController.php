<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Contracts\Auth\PasswordBroker as PasswordBrokerContract;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Password;
use Mail;
use Session;

class ForgetPasswordController extends Controller
{
   /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showLinkRequestForm($locale)
    {
        if($locale == "en") {
            App::setLocale('en');
        } elseif ($locale == "id") {
            App::setLocale('id');
        }
        return view('frontend.reset.reset')->with('locale', $locale);
    }

    public function sendResetLinkEmail($locale, Request $request)
    {
        if($locale == "en") {
            App::setLocale('en');
            Session::flash("Success","We have send you the email to reset your password");

        } elseif ($locale == "id") {
            App::setLocale('id');
            Session::flash("Success","Kami telah mengirim email reset kata sandi anda");
        }
        $user = User::where('email','=',$request->email)->first();
        if(!$user) {
            if($locale == "en") {
                Session::flash("Error","Email not found");
            } elseif ($locale == "id") {
                Session::flash("Error","Email tidak ditemukan");
            }
            return redirect($locale.'/login');
        }
        $this->validateEmail($request);

        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        

        Mail::send('frontend.reset.reset_link', ['user' => $user, 'token' => $request->_token, 'locale' => $locale], function ($m) use ($user) {
            $m->from('iskandarjonny65@gmail.com');

            $m->to($user->email)->subject('Reset Password!');
        });

        return redirect($locale.'/login');
    }
}
