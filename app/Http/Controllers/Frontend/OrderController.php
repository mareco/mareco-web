<?php

namespace App\Http\Controllers\Frontend;

use App\Helpers\BackEndHelper;
use App\Helpers\SessionHelper;
use App\Http\Controllers\Controller;
use App\Models\Order;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Session;
use Steevenz\Rajaongkir;
use Validator;

class OrderController extends Controller
{

	public function listOrder($locale) {
		if($locale == "en") {
			App::setLocale('en');
		} elseif ($locale == "id") {
			App::setLocale('id');
		}
		$order = Order::orderBy('created_at','desc')->where('user_id',Auth::user()->id)->paginate(10);
		return view('frontend.account.customer-order')->with('order',$order)
		->with('homepage', 'active')
		->with('locale', $locale);
	}

	public function confirmationOrder($locale, $order) {
		if($locale == "en") {
			App::setLocale('en');
		} elseif ($locale == "id") {
			App::setLocale('id');
		}
		$config['api_key'] = '56950ca5aa637a0c5efa1b266e28f160';
		$config['account_type'] = 'pro';
		$rajaongkir = new Rajaongkir($config);
		$confirmation = Order::where('id','=',$order->id)->first();
		if ($confirmation->address_id != null) {
			$address = array(
				'province' => $rajaongkir->get_province($confirmation->address->province)['province'],
				'city' => $rajaongkir->get_city($confirmation->address->city)['city_name'],
				'district' => $rajaongkir->get_subdistrict($confirmation->address->district)['subdistrict_name'],
				);
		} else {
			$address = null;
		}
		return view('frontend.account.confirmation-order')
		->with('locale', $locale)
		->with('address',$address)
		->with('getBankList', $this->getBankList())
		->with('order',$confirmation);
	}
}