<?php

namespace App\Http\Controllers\Frontend;

use App\Helpers\Enums\OrderStatus;
use App\Helpers\FileUploadHelper;
use App\Helpers\SessionHelper;
use App\Http\Controllers\Controller;
use App\Models\Address;
use App\Models\BankAccount;
use App\Models\CashOnDelivery;
use App\Models\Category;
use App\Models\Config;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Point;
use App\Models\Product;
use App\Models\User;
use App\Models\Variant;
use App\Models\Wishlist;
use Auth;
use Carbon\Carbon;
use Cart;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use JeroenDesloovere\Distance\Distance;
use Mail;
use Ncaneldiee\Rajaongkir as International;
use PDF;
use Paypal;
use Session;
use Steevenz\Rajaongkir;
use Validator;

class PaymentController extends Controller
{
	private $_apiContext;

    public function __construct()
    {
        $this->_apiContext = PayPal::ApiContext(
            config('services.paypal.client_id'),
            config('services.paypal.secret'));

        $this->_apiContext->setConfig(array(
            'mode' => 'sandbox',
            'http.ConnectionTimeOut' => 30,
            'log.LogEnabled' => true,
            'log.FileName' => storage_path('logs/paypal.log'),
            'log.LogLevel' => 'FINE'
            ));

    }

    public function confirmPayment($locale, Request $request) {
        if($locale == "en") {
            App::setLocale('en');
        } elseif ($locale == "id") {
            App::setLocale('id');
        } else {
           $locale = "en";
        }
        if ($request->date_payment > Carbon::now()) {
            $message = 'Date payment cannot more than today';
            $request->session()->flash('alert-warning',$message);
            return redirect('/customer/'.$locale.'/order/confirmation/' . $request->id);
        }
        $order = Order::where('id','=',$request->id)->first();
        $order->status = 6;
        $order->date_payment = $request->date_payment;
        $order->account_name = $request->account_name;
        $order->account_number = $request->account_number;
        $order->bank_account_id = $request->bank_id;
        $order->amount_payment = $request->amount;
        if ($request->file('receipt_url')) {
            $upload = FileUploadHelper::upload($request, 'receipt_url', 'jpeg,bmp,png,jpg');
            $order->receipt_url = $upload;
        }
        $order->save();

        $config = Config::first();
        if ($config->about_us_email == null) {
            $email = 'Not have a email yet';
        } else {
            $email = $config->about_us_email;
        }

        $data = array('first_name' => Auth::user()->first_name,'last_name' => Auth::user()->last_name, 'order' => $order,'email' => $email);
        if ($config->about_us_email != null || $config->about_us_email != '') {
            Mail::send('frontend.pdf.proof_payment', $data, function ($message) use ($data)
            {
                $config = Config::first();
                $emailCustomer = $config->about_us_email;
                $message->from('iskandarjonny65@gmail.com')->subject('Received Payment');
                $message->to($emailCustomer);
            });
        }


        return redirect('/customer/'.$locale.'/order/confirmation/' . $request->id);
    }

    public function saveCheckout($locale, Request $request) {
        if($locale == "en") {
            App::setLocale('en');
        } elseif ($locale == "id") {
            App::setLocale('id');
        }
		//Save Address
        $config['api_key'] = '56950ca5aa637a0c5efa1b266e28f160';
        $config['account_type'] = 'pro';
        $rajaongkir = new Rajaongkir($config);
        if ($request->cod_address != '') {
            $cod = new CashOnDelivery();
            $cod->user_address = $request->cod_address;
            $cod->rate = $request->rate;
            $cod->user_id = Auth::user()->id;
            $cod->save();
        } else {
            if ($request->shipping_address_id != '') {
                $address_id = $request->shipping_address_id;
                $address = Address::findOrFail($request->shipping_address_id);
            } else {

                $province_name = $rajaongkir->get_province($request->province)['province'];
                $city_name = $rajaongkir->get_city($request->city)['city_name'];
                $district_name = $rajaongkir->get_subdistrict($request->district)['subdistrict_name'];

                $address = new Address();
                $address->province = $request->province;
                $address->city = $request->city;
                $address->district = $request->district;
                $address->state = 'Indonesia';
                $address->address = $request->address;
                $address->postal_code = $request->postal_code;
                $address->city_name = $city_name;
                $address->province_name = $province_name;
                $address->district_name = $district_name;
                $address->User_id = Auth::user()->id;
                $address->save();

                $address_id = $address->id;
            }
        }

		// Update User
        $user = User::where('email','=',$request->email)->first();
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->use_point = 0;
        $user->amount_point = 0;
        $user->point = $user->point - $request->point;
        $user->save();

		// Save Order
        $allOrder = Order::all();
        if (count($allOrder) == 0) {
            $randomNumber = str_pad(1, 5, '0', STR_PAD_LEFT);
        }
        else {
            $number = Order::orderBy('order_number','desc')->value('order_number') + 1;
            $randomNumber = str_pad($number, 5, '0', STR_PAD_LEFT);;
        }

        if ($request->radioPayment == 'paypal') {
        	$payment_type = 0;
        } else if ($request->radioPayment == 'bank-transfer') {
        	$payment_type = 1;
        } else {
        	$payment_type = 2;
        }

        if ($request->cod_address == '') {
            $order = new Order();
            $order->address_id = $address_id;
            $order->order_number = $randomNumber;
            $order->status = 0;
            $order->grand_total = $request->grandTotal;
            $order->payment_type = $payment_type;
            $order->shipment_service = $request->serviceType;
            $order->shipment_type = $request->radioShipping;
            $order->shipment_fee = $request->serviceFee;
            $order->point = $request->point;
            $order->user_id = $user->id;
            $order->save();

            if($request->point > 0) {
                $point = new Point();
                $point->user_id = $user->id;
                $point->amount = -$request->point;
                $point->description = 'Use Point Order No. '.$order->order_number.' lost';
                $point->save();
            }
        } else {
            $order = new Order();
            $order->order_number = $randomNumber;
            $order->status = 0;
            $order->grand_total = $request->grandTotal;
            $order->payment_type = $payment_type;
            $order->shipment_type = $request->radioShipping;
            $order->shipment_fee = $request->rate;
            $order->point = $request->point;
            $order->cod_address = $request->cod_address;
            $order->cod_lat = $request->lat;
            $order->cod_lng = $request->lng;
            $order->user_id = $user->id;
            $order->additional_note = $request->additional_note;
            $order->save();

            if($request->point > 0) {
                $point = new Point();
                $point->user_id = $user->id;
                $point->amount = -$request->point;
                $point->description = 'Use Point Order No.'.$order->order_number.'lost';
                $point->save();
            }
        }


        $dataCart = array();
        foreach(Cart::content() as $row) {
            array_push($dataCart,$row);
        }

        $subTotal = 0;
        for ($i = 0; $i < count($dataCart); $i++) {
            $subTotal = $subTotal + ($dataCart[$i]->qty * $dataCart[$i]->price);

            $sku = Variant::where('id','=',$dataCart[$i]->options->variant_id)->value('sku');
            $orderDetail = new OrderDetail();
            $orderDetail->order_id = $order->id;
            $orderDetail->product_id = $dataCart[$i]->id;
            $orderDetail->qty = $dataCart[$i]->qty;
            $orderDetail->size = $dataCart[$i]->options->size;
            $orderDetail->color = $dataCart[$i]->options->color;
            $orderDetail->sku = $sku;
            $orderDetail->price = $dataCart[$i]->price;
            $orderDetail->amount = $dataCart[$i]->qty * $dataCart[$i]->price;
            $orderDetail->weight = $dataCart[$i]->qty * $dataCart[$i]->options->weight;
            $orderDetail->save();

            $variant = Variant::where('id','=',$dataCart[$i]->options->variant_id)->first();
            $variant->qty = $variant->qty - $dataCart[$i]->qty;
            $variant->save();
        }

        $bank = BankAccount::all();
        $config = Config::first();
        if ($config->about_us_email == null) {
            $email = 'Not have a email yet';
        } else {
            $email = $config->about_us_email;
        }

        if ($request->cod_address == '') {
            $data = array('user' => $user, 'bank' => $bank, 'order' => $order, 'orderDetail' => $dataCart, 'subTotal' => $subTotal, 'address' => $address, 'point' => $request->point,'email' => $email);

            Mail::send('frontend.pdf.order-confirmation', $data, function ($message) use ($data) {
                $emailCustomer = $data['user']->email;

                $message->from('iskandarjonny65@gmail.com')->subject('Order Information !');
                // $message->attachData($pdf->output(), "invoice.pdf");
                $message->to($emailCustomer);

            });
        }

        $data = array('user' => $user, 'bank' => $bank, 'order' => $order, 'orderDetail' => $dataCart, 'subTotal' => $subTotal, 'address' => $address, 'point' => $request->point,'email' => $email);

            Mail::send('frontend.pdf.order-confirmation-admin', $data, function ($message) use ($data) {
                $config = Config::first();
                $emailCustomer = $config->about_us_email;

                $message->from('iskandarjonny65@gmail.com')->subject('Order Information !');
                // $message->attachData($pdf->output(), "invoice.pdf");
                $message->to($emailCustomer);

            });

        for ($i = 0; $i < count($dataCart); $i++) {
            $rowId = $dataCart[$i]->rowId;
            Cart::remove($rowId);
        }

        if ($request->radioPayment == 'bank-transfer' || $request->radioPayment == 'COD') {
        	return redirect('/customer/'.$locale.'/order/history');
        }

        if ($request->radioPayment == 'paypal') {
            Session::put('locale', $locale);
            Session::put('order_id',$order->id);

            $payer = PayPal::Payer();
            $payer->setPaymentMethod('paypal');
            $total = number_format($order->grand_total / $rajaongkir->get_currency()['value'],2,'.',',');
	        // $valueNOK = Currency::conv($from = 'USD', $to = 'NOK', $value = 10, $decimals = 2);
            $amount = PayPal::Amount();
            $amount->setCurrency('USD');
	        $amount->setTotal((float)$total); // This is the simple way,
	        // you can alternatively describe everything in the order separately;
	        // Reference the PayPal PHP REST SDK for details.
	        $transaction = PayPal::Transaction();
	        $transaction->setAmount($amount);
	        $transaction->setDescription('Mareco Store');

	        $redirectUrls = PayPal:: RedirectUrls();
	        $redirectUrls->setReturnUrl(route('payment.getDone'));
	        $redirectUrls->setCancelUrl(route('payment.getCancel'));

	        $payment = PayPal::Payment();
	        $payment->setIntent('sale');
	        $payment->setPayer($payer);
	        $payment->setRedirectUrls($redirectUrls);
	        $payment->setTransactions(array($transaction));

	        $response = $payment->create($this->_apiContext);
	        
	        $redirectUrl = $response->links[1]->href;

	        return Redirect::to( $redirectUrl );
        }
    }

    public function paypalCheckOut($id, $locale) {
        Session::put('locale', $locale);
        Session::put('order_id',$id);

        // if($locale == "en") {
        //     App::setLocale('en');
        // } elseif ($locale == "id") {
        //     App::setLocale('id');
        // }
        //Save Address
        $config['api_key'] = '56950ca5aa637a0c5efa1b266e28f160';
        $config['account_type'] = 'pro';
        $rajaongkir = new Rajaongkir($config);

        $order = Order::find($id);
        $payer = PayPal::Payer();
        $payer->setPaymentMethod('paypal');
        $total = number_format($order->grand_total / $rajaongkir->get_currency()['value'],2,'.',',');
        $amount = PayPal::Amount();
        $amount->setCurrency('USD');
        $amount->setTotal((float)$total); 
        $transaction = PayPal::Transaction();
        $transaction->setAmount($amount);
        $transaction->setDescription('Mareco Store');

        $redirectUrls = PayPal:: RedirectUrls();
        $redirectUrls->setReturnUrl(route('payment.getDone'));
        $redirectUrls->setCancelUrl(route('payment.getCancel'));

        $payment = PayPal::Payment();
        $payment->setIntent('sale');
        $payment->setPayer($payer);
        $payment->setRedirectUrls($redirectUrls);
        $payment->setTransactions(array($transaction));

        $response = $payment->create($this->_apiContext);

        $redirectUrl = $response->links[1]->href;

        return Redirect::to( $redirectUrl );

    }

    public function getDone(Request $request) {
        $locale = Session::get('locale');

        if($locale == "en") {
            App::setLocale('en');
        } elseif ($locale == "id") {
            App::setLocale('id');
        }
        $id = $request->get('paymentId');
        $token = $request->get('token');
        $payer_id = $request->get('PayerID');

        $payment = PayPal::getById($id, $this->_apiContext);

        $paymentExecution = PayPal::PaymentExecution();

        $paymentExecution->setPayerId($payer_id);
        $executePayment = $payment->execute($paymentExecution, $this->_apiContext);

        $order = Order::where('id','=',Session::get('order_id'))->first();
        $order->status = OrderStatus::ON_VERIFICATION;
        $order->reference_id = $id;
        $order->date_payment = Carbon::now();
        $order->save();

        $config = Config::first();
        if ($config->about_us_email == null) {
            $email = 'Not have a email yet';
        } else {
            $email = $config->about_us_email;
        }

        $data = array('first_name' => Auth::user()->first_name,'last_name' => Auth::user()->last_name, 'order' => $order,'email' => $email);
        if ($config->about_us_email != null || $config->about_us_email != '') {
            Mail::send('frontend.pdf.proof_payment', $data, function ($message) use ($data)
            {
                $config = Config::first();
                $emailCustomer = $config->about_us_email;
                $message->from('iskandarjonny65@gmail.com')->subject('Received Payment');
                $message->to($emailCustomer);
            });
        }

        // Clear the shopping cart, write to database, send notifications, etc.
        // Thank the user for the purchase
        return redirect('/customer/'.$locale.'/order/history');
    }

    public function getCancel() {
    // if($locale == "en") {
    //     App::setLocale('en');
    // } elseif ($locale == "id") {
    //     App::setLocale('id');
    // }
        // Curse and humiliate the user for cancelling this most sacred payment (yours)
        return redirect('/customer/order/history');
    }

    public function getDistance(Request $request) {
        $config = Config::first();
        $configLat = $config->storage_lat;
        $configLng = $config->storage_lng;
        $customerLat = $request->lat;
        $customerLng = $request->lng;

        $distance = Distance::between($configLat, $configLng, $customerLat, $customerLng);
        $rate = $distance*$config->cod_amount;

        return response()->json(['rate' => $rate]);
    }
}
