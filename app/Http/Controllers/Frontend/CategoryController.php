<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Category;
use App\Helpers\SessionHelper;
use App\Http\Controllers\Controller;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Session;
use Validator;

class CategoryController extends Controller
{

	public static function getCategory() {
		$category = Category::all();
		return $category;
	}
}