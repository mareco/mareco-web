<?php

namespace App\Http\Controllers\Frontend;

use App\Helpers\SessionHelper;
use App\Http\Controllers\Controller;
use App\Models\Config;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Session;
use Validator;

class AboutController extends Controller
{

	public function showAboutUs($locale) {
		if($locale == "en") {
			App::setLocale('en');
		} elseif ($locale == "id") {
			App::setLocale('id');
		}
		$config = Config::first();
		return view('frontend.about-us.about-us')->with('config', $config)
		->with('locale', $locale)
		->with('about', 'active')
		->with('breadcrumb', 'About Us');
	}
	public function showContactUs($locale) {
		if($locale == "en") {
			App::setLocale('en');
		} elseif ($locale == "id") {
			App::setLocale('id');
		}
		$config = Config::first();
		return view('frontend.contact-us.contact-us')->with('config', $config)
		->with('locale', $locale)
		->with('contact', 'active')
		->with('breadcrumb', 'Contact Us');
	}
}