<?php

namespace App\Http\Controllers;

use App\Helpers\ApiHelper;
use App\Helpers\BackEndHelper;
use App\Helpers\RedirectWhenError;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    use RedirectWhenError;
    use ApiHelper;
    use BackEndHelper;
}
