<?php

namespace App\Http\Controllers;

use App\Helpers\Enums\UserRole;
use App\Helpers\FileUploadHelper;
use App\Helpers\SessionHelper;
use App\Models\Order;
use App\Models\Payment;
use App\Models\Product;
use App\Models\User;
use App\Models\Activity_log;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Helpers\ActivitylogHelpers;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $registeredUsers = User::where('role','=', UserRole::USER)->count();
        $totalProducts = Product::all()->count();
        $totalOrders = Order::all()->count();
        $recentOrders = Order::orderBy('created_at','desc')->limit(5)->get();
        $lifeTimeSales = DB::table('orders')->sum('grand_total');
        $recentRegistered = User::where('role','=', UserRole::USER)->orderBy('created_at','desc')->limit(5)->get();
        $activity_logs = Activity_log::limit(10)->get();
        //Charts
        $chartOrders = array();
        $chartPayments = array();
        $startDate = new Carbon('first day of this month');
        $endDate = new Carbon('last day of this month');
        $tempSD = $startDate;
        $i = 0;
        while ( $tempSD <= $endDate) {
            $tempOrder = Order::whereDate('created_at', '=', $tempSD->toDateString())->sum('grand_total');
            $tempPayment = Payment::whereDate('created_at', '=', $tempSD->toDateString())->sum('total');
            // array_push($chartOrders, array($tempSD->day, $tempOrder));
            // array_push($chartPayments, array($tempSD->day, $tempPayment));

            // foreach($tempOrder as $result) {
            //     echo $result."<br />";
            // }

            $chartOrders['tempOrder'][$i] = $tempOrder;
            $chartOrders['tempPayment'][$i] = $tempPayment;
            $tempSD = $tempSD->addDays(1);
            $i++;
        }
        // dd($chartOrders);
        // echo count( $chartOrders['tempOrder']);
        // for($z = 0; $z < count($chartOrders['tempOrder']); $z++) {
        //     echo $chartOrders['tempPayment'][$z]."<br />";
        // }
        // exit;
        //End Charts
        return view('backend.dashboard.index')
        ->with('home', 'active')
        ->with('chartOrders', $chartOrders)
        ->with('tempOrder', $tempOrder)
        ->with('tempPayment', $tempPayment)
        ->with('chartPayments', $chartPayments)
        ->with('registeredUsers', $registeredUsers)
        ->with('totalProducts', $totalProducts)
        ->with('recentRegistered', $recentRegistered)
        ->with('lifeTimeSales', $lifeTimeSales)
        ->with('totalOrders', $totalOrders)
        ->with('recentOrders', $recentOrders)
        ->with('activity_logs', $activity_logs)
        ->with('title', 'Home')
        ->with('dashboard','active'); 
    }

    public function changePassword(Request $request) {
        return view('auth.passwords.change')
        ->with('user', $request->user())
        ->with('title', 'Change Password')
        ->with('small_title', 'password')
        ->with('currentClass', '')
        ->with('userList', '')
        ->with('userClass', '')
        ->with('formAction', 'Change Password');
    }

    public function updateChangePassword(Request $request) {
        $this->validate($request, [
            'old_password' => 'required',
            'password' => 'required|min:3|confirmed',
            'password_confirmation' => 'required|min:3',
            ], [
            'old_password.required' => 'Please enter current password',
            'password.required' => 'Please enter new password',
            ]);
        $user = $request->user();
        if ($request->file('photo')) {
            $upload = FileUploadHelper::upload($request, 'photo', 'jpeg,bmp,png,jpg');
            $user->photo = $upload;
        }
        if (!Hash::check($request->old_password, $user->password)) {
            return back()->withErrors(['old_password' => 'Incorrect Current password']);
        }
        $user->password = bcrypt($request->password);
        $user->save();

        $type = "Admin";
        $action = "Update";
        $description = Auth::User()->name . " has succesful update a profile";
        ActivitylogHelpers::generate($type,$action,$description);

        SessionHelper::setMessage('Password Successfully Changed');
        return redirect()->route('admin.dashboard');
    }

}
