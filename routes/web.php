<?php
Route::get('/', function () {
	return redirect('/en');
});
Route::get('/admin/login', 'Auth\LoginController@showLoginForm')->name('admin.login.form');
Route::post('/admin/login', 'Auth\LoginController@login')->name('admin.login');

Route::group(['middleware' => ['auth', 'admin']], function(){
	Route::group(['prefix' => 'admin'], function(){
		Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
		Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
		Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('admin.password.reset');
		Route::post('/password/reset', 'Auth\ResetPasswordController@reset');

		Route::group(['middleware' => 'auth'], function () {
			Route::get('/', 'HomeController@index')->name('admin.dashboard');
			Route::group(['prefix' => 'change-password'], function () {
				Route::get('/', 'HomeController@changePassword')->name('admin.password.change.index');
				Route::put('/', 'HomeController@updateChangePassword')->name('admin.password.change.update');
			});
			Route::group(['namespace' => 'Auth'], function () {
				Route::get('/logout', 'LoginController@logout')->name('admin.logout');
			});
			Route::group(['prefix' => 'users'], function(){
				Route::get('/', 'Backend\UserController@index')->name('admin.users.index');
				Route::get('/new', 'Backend\UserController@newForm')->name('admin.users.new');
				Route::post('/save', 'Backend\UserController@save')->name('admin.users.save');
				Route::get('/edit/{user}', 'Backend\UserController@edit')->name('admin.users.edit');
				Route::put('/save/{user}', 'Backend\UserController@update')->name('admin.users.update');
				Route::delete('/delete/{user}', 'Backend\UserController@destroy')->name('admin.users.destroy');
				Route::post('/list', 'Backend\UserController@listAllUser')->name('admin.users.list');
				Route::get('/detail/{user}', 'Backend\PointController@index')->name('admin.users.point.index');
				Route::get('/point/{user}', 'Backend\PointController@newForm')->name('admin.users.point.form');
				Route::post('/detail/save/{user}', 'Backend\PointController@save')->name('admin.point.save');

			});
			Route::group(['prefix' => 'admins'], function(){
				Route::get('/', 'Backend\AdminController@index')->name('admin.admins.index');
				Route::get('/new', 'Backend\AdminController@newForm')->name('admin.admins.new');
				Route::post('/save', 'Backend\AdminController@save')->name('admin.admins.save');
				Route::get('/edit/{user}', 'Backend\AdminController@edit')->name('admin.admins.edit');
				Route::put('/save/{user}', 'Backend\AdminController@update')->name('admin.admins.update');
				Route::delete('/delete/{user}', 'Backend\AdminController@destroy')->name('admin.admins.destroy');
				Route::post('/list', 'Backend\AdminController@listAllAdmin')->name('admin.admins.list');
			});
			Route::group(['prefix' => 'banks'], function(){
				Route::get('/', 'Backend\BankAccountController@index')->name('admin.banks.index');
				Route::get('/new', 'Backend\BankAccountController@newForm')->name('admin.banks.new');
				Route::post('/save', 'Backend\BankAccountController@save')->name('admin.banks.save');
				Route::get('/edit/{bank}', 'Backend\BankAccountController@edit')->name('admin.banks.edit');
				Route::put('/save/{bank}', 'Backend\BankAccountController@update')->name('admin.banks.update');
				Route::delete('/delete/{bank}', 'Backend\BankAccountController@destroy')->name('admin.banks.destroy');
				Route::post('/list', 'Backend\BankAccountController@listAllBank')->name('admin.banks.list');
			});

			Route::group(['prefix' => 'shipping-methods'], function(){
				Route::get('/', 'Backend\ShippingMethodController@index')->name('admin.shipping.methods.index');
				Route::get('/new', 'Backend\ShippingMethodController@newForm')->name('admin.shipping.methods.new');
				Route::post('/save', 'Backend\ShippingMethodController@save')->name('admin.shipping.methods.save');
				Route::get('/edit/{shippingMethod}', 'Backend\ShippingMethodController@edit')->name('admin.shipping.methods.edit');
				Route::put('/save/{shippingMethod}', 'Backend\ShippingMethodController@update')->name('admin.shipping.methods.update');
				Route::delete('/delete/{shippingMethod}', 'Backend\ShippingMethodController@destroy')->name('admin.shipping.methods.destroy');
			});
			Route::group(['prefix' => 'categories'], function(){
				Route::get('/', 'Backend\CategoryController@index')->name('admin.categories.index');
				Route::get('/new', 'Backend\CategoryController@newForm')->name('admin.categories.new');
				Route::post('/save', 'Backend\CategoryController@save')->name('admin.categories.save');
				Route::get('/edit/{category}', 'Backend\CategoryController@edit')->name('admin.categories.edit');
				Route::put('/save/{category}', 'Backend\CategoryController@update')->name('admin.categories.update');
				Route::delete('/delete/{category}', 'Backend\CategoryController@destroy')->name('admin.categories.destroy');
				Route::post('/list', 'Backend\CategoryController@listAllCategory')->name('admin.categories.list');
			});
			Route::group(['prefix' => 'sub-categories'], function(){
				Route::get('/', 'Backend\SubCategoryController@index')->name('admin.sub-categories.index');
				Route::get('/new', 'Backend\SubCategoryController@newForm')->name('admin.sub-categories.new');
				Route::post('/save', 'Backend\SubCategoryController@save')->name('admin.sub-categories.save');
				Route::get('/edit/{subCategory}', 'Backend\SubCategoryController@edit')->name('admin.sub-categories.edit');
				Route::put('/save/{subCategory}', 'Backend\SubCategoryController@update')->name('admin.sub-categories.update');
				Route::delete('/delete/{subCategory}', 'Backend\SubCategoryController@destroy')->name('admin.sub-categories.destroy');
				Route::post('/list', 'Backend\SubCategoryController@listAllSubCategory')->name('admin.sub-categories.list');
			});
			Route::group(['prefix' => 'units'], function(){
				Route::get('/', 'Backend\UnitController@index')->name('admin.units.index');
				Route::get('/new', 'Backend\UnitController@newForm')->name('admin.units.new');
				Route::post('/save', 'Backend\UnitController@save')->name('admin.units.save');
				Route::get('/edit/{unit}', 'Backend\UnitController@edit')->name('admin.units.edit');
				Route::put('/save/{unit}', 'Backend\UnitController@update')->name('admin.units.update');
				Route::delete('/delete/{unit}', 'Backend\UnitController@destroy')->name('admin.units.destroy');
				Route::post('/list', 'Backend\UnitController@listAllUnit')->name('admin.units.list');
			});
			Route::group(['prefix' => 'products'], function(){
				Route::get('/', 'Backend\ProductController@index')->name('admin.products.index');
				Route::get('/new', 'Backend\ProductController@newForm')->name('admin.products.new');
				Route::post('/save', 'Backend\ProductController@save')->name('admin.products.save');
				Route::get('/edit/{product}', 'Backend\ProductController@edit')->name('admin.products.edit');
				Route::post('/save/{product}', 'Backend\ProductController@update')->name('admin.products.update');
				Route::delete('/delete/{product}', 'Backend\ProductController@destroy')->name('admin.products.destroy');
				Route::post('/list', 'Backend\ProductController@listAllProduct')->name('admin.products.list');
				Route::get('/push-notif-product/{product}', 'Backend\ProductController@pushNotifProduct')->name('admin.products.push');
				Route::get('/review/{product}', 'Backend\ProductController@productReview')->name('admin.reviews.index');
				Route::delete('/review/delete/{review}', 'Backend\ProductController@productReviewDestroy')->name('admin.product.reviews.delete');
				Route::post('/review/save/{product}', 'Backend\ProductController@productReviewSave')->name('admin.product.reviews.save');

			});
			Route::group(['prefix' => 'orders'], function(){
				Route::get('/', 'Backend\OrderController@index')->name('admin.orders.index');
				Route::get('/new', 'Backend\OrderController@newForm')->name('admin.orders.new');
				Route::post('/save', 'Backend\OrderController@save')->name('admin.orders.save');
				Route::get('/edit/{order}', 'Backend\OrderController@edit')->name('admin.orders.edit');
				Route::delete('/delete/{order}', 'Backend\OrderController@destroy')->name('admin.orders.destroy');
				Route::post('/list', 'Backend\OrderController@listAllOrder')->name('admin.orders.list');
				Route::get('/order-information/{id}', 'Backend\OrderController@viewOrderInformation')->name('admin.orders.view');
				Route::get('get/size', 'Backend\OrderController@getSize')->name('admin.orders.size');
				Route::get('get/color', 'Backend\OrderController@getColor')->name('admin.orders.color');
				Route::get('get/product', 'Backend\OrderController@getDataProduct')->name('admin.orders.product');
				Route::get('add/product', 'Backend\OrderController@addCart')->name('admin.orders.add.cart');
				// Route::get('/delete/cart/{id}', 'Backend\OrderController@deleteCart')->name('admin.orders.delete.cart');
				Route::get('get/address', 'Backend\OrderController@getAddress')->name('admin.orders.get.address');
				Route::get('get/city', 'Backend\OrderController@getCity')->name('admin.orders.get.city');
				Route::get('get/district', 'Backend\OrderController@getDistrict')->name('admin.orders.get.district');
				Route::get('get/cost', 'Backend\OrderController@getCost')->name('admin.orders.get.cost');
				Route::put('change/status', 'Backend\OrderController@changeStatus')->name('admin.orders.change.status');
				Route::put('add/shipment', 'Backend\OrderController@addShipment')->name('admin.orders.add.shipment');
			});
			Route::group(['prefix' => 'wishlists'], function(){
				Route::get('/', 'Backend\WishlistController@index')->name('admin.wishlists.index');
				Route::get('/get/variant', 'Backend\WishlistController@getVariant')->name('admin.wishlists.getData');
				Route::delete('/delete/{category}', 'Backend\WishlistController@destroy')->name('admin.wishlists.destroy');
				Route::post('/list', 'Backend\WishlistController@listAllWishlist')->name('admin.wishlists.list');
			});
			Route::group(['prefix' => 'sliders'], function(){
				Route::get('/', 'Backend\SliderController@index')->name('admin.sliders.index');
				Route::post('/save', 'Backend\SliderController@save')->name('admin.sliders.save');
				Route::get('/delete', 'Backend\SliderController@delete')->name('admin.sliders.delete');
				Route::get('/update', 'Backend\SliderController@update')->name('admin.sliders.update');
				Route::put('/update/category/{picture}', 'Backend\SliderController@updateCategory')->name('admin.sliders.category.update');
			});
			// Route::group(['prefix' => 'catalogs'], function(){
			// 	Route::get('/', 'Backend\CatalogController@index')->name('admin.catalogs.index');
			// 	Route::get('/setting', 'Backend\CatalogController@settingCatalog')->name('admin.catalog.setting');
			// 	Route::post('/list', 'Backend\CatalogController@listAllCatalog')->name('admin.catalogs.list');
			// 	Route::put('/save', 'Backend\CatalogController@update')->name('admin.catalogs.update');
			// });
			Route::group(['prefix' => 'promotions'], function(){
				Route::get('/', 'Backend\PromotionController@index')->name('admin.promotions.index');
				Route::post('/promotion/save', 'Backend\PromotionController@storePromotion')->name('admin.promotions.save');
				Route::delete('/promotion/{promotion}/destroy', 'Backend\PromotionController@destroyPromotion')->name('admin.promotions.destroy');
				Route::get('/push-notif/{promotion}', 'Backend\PromotionController@pushNotifProduct')->name('admin.promotions.push');

			});
			Route::group(['prefix' => 'configs'], function(){
				Route::get('/', 'Backend\ConfigurationController@newForm')->name('admin.configs.new');
				Route::post('/save', 'Backend\ConfigurationController@save')->name('admin.configs.save');
				Route::put('/update/{config}', 'Backend\ConfigurationController@update')->name('admin.configs.update');
				Route::get('/terms-conditions','Backend\ConfigurationController@termCondition')->name('admin.term.condition');
				Route::post('/store/terms','Backend\ConfigurationController@storeTermCondition')->name('admin.store.term');
				Route::post('/store/terms/id','Backend\ConfigurationController@storeTermConditionId')->name('admin.store.term.id');
				Route::put('/update/terms/{term}','Backend\ConfigurationController@updateTermCondition')->name('admin.update.term');
				Route::delete('/delete/term/{term}', 'Backend\ConfigurationController@termDestroy')->name('admin.term.destroy');
			});
			Route::group(['prefix' => 'brands'], function(){
				Route::get('/', 'Backend\FeatureBrandController@index')->name('admin.brands.index');
				Route::post('/save', 'Backend\FeatureBrandController@save')->name('admin.brands.save');
				Route::get('/delete', 'Backend\FeatureBrandController@delete')->name('admin.brands.delete');
				Route::get('/update', 'Backend\FeatureBrandController@update')->name('admin.brands.update');
			});
			Route::group(['prefix' => 'report'], function() {
				Route::get('/', 'Backend\ReportController@index')->name('admin.report');
				Route::post('/generate', 'Backend\ReportController@listReport')->name('report.generate');
			});
			Route::get('/activity_log', 'Backend\ActivityLogController@index')->name('admin.log.index');
			Route::post('/activity_log/list', 'Backend\ActivityLogController@listAllLog')->name('admin.log.list');
			Route::get('/get-in-touch','Backend\UserController@getInTouch')->name('admin.user.services.index');
			

		});
});
});



Route::group(['prefix' => '{locale}'], function() {

	Route::get('/', 'Frontend\HomepageController@showHomepage')->name('customer.homepage');
	Route::get('/about-us', 'Frontend\AboutController@showAboutUs')->name('about');
	Route::get('/contact-us', 'Frontend\AboutController@showContactUs')->name('contact');
	Route::get('/login', 'Frontend\LoginController@showLoginForm')->name('customer.login.form');
	Route::get('/register', 'Frontend\LoginController@showRegistrationForm')->name('customer.register.form');
	Route::post('/login', 'Frontend\LoginController@login')->name('customer.login');
	Route::get('/login/{provider}', 'Frontend\LoginController@socialLogin');
	Route::post('/register', 'Frontend\LoginController@register')->name('customer.register');
	Route::get('/product', 'Frontend\ProductController@showProduct')->name('product');
	Route::get('/product/{slug}', 'Frontend\ProductController@showProductDetails')->name('product.details');
	Route::get('/promotion', 'Frontend\HomepageController@showMorePromotion')->name('show.promotion');
	Route::get('/checkout/cart','Frontend\ProductController@showShoppingCart')->name('checkout.cart');
	Route::get('/checkout','Frontend\ProductController@showCheckout')->name('checkout');
	Route::post('/customer/checkout/save', 'Frontend\PaymentController@saveCheckout')->name('save.checkout');
	Route::get('/distance/address', 'Frontend\PaymentController@getDistance')->name('customer.address.distance');
	Route::post('/payment/confirm/save', 'Frontend\PaymentController@confirmPayment')->name('customer.confirm.payment');
	Route::post('/get-in-touch','Frontend\HomepageController@getInTouch')->name('get.in.touch');

	Route::get('/product/review/{product}','Frontend\ProductController@editReview')->name('product.reviews');
	Route::get('/forgot', 'Frontend\LoginController@forgot')->name('forgot');
	
	Route::post('/customer/password/email', 'Frontend\ForgetPasswordController@sendResetLinkEmail')->name('customer.password.email');
	Route::get('/customer/password/reset/{token}', 'Frontend\ResetPasswordController@showResetForm')->name('customer.password.reset');
	Route::get('/customer/password/reset', 'Frontend\ForgetPasswordController@showLinkRequestForm')->name('customer.password.request');
	Route::post('/customer/password/reset', 'Frontend\ResetPasswordController@reset');
	Route::get('/delete/cart/{id}', 'Frontend\ProductController@deleteCart');
	Route::post('/product/search/', 'Frontend\HomepageController@searchProduct')->name('search.product');

	Route::put('/update/cart', 'Frontend\ProductController@updateCart')->name('product.update.cart');
	Route::post('/add/cart', 'Frontend\ProductController@addCart')->name('product.cart');
	Route::post('/add/wishlist', 'Frontend\ProductController@addWishlist')->name('product.wishlist');

});

Route::get('/paypal/checkout/{id}/{locale}', 'Frontend\PaymentController@paypalCheckOut')->name('paypal.double.check');

Route::get('login/redirect/{provider}/{locale}', ['uses' => 'Frontend\LoginController@redirectToProvider', 'as' => 'social.login']);
Route::get('login/{provider}', 'Frontend\LoginController@socialLogin');
Route::get('/password/reset', 'Frontend\LoginController@showRegisterPage')->name('customer.register');


Route::get('/product/review/update/{review}/{locale}','Frontend\ProductController@updateReview')->name('product.reviews.update');
Route::post('/product/review/save/{product}/{locale}','Frontend\ProductController@saveReview')->name('product.reviews.save');
Route::put('/user/activate/account','Frontend\UserController@activateAccount')->name('user.activate');

Route::get('/variant/color', 'Frontend\ProductController@getColor')->name('product.color');

Route::get('/address/city', 'Frontend\AddressController@getCity')->name('register.address.city');
Route::get('/address/district', 'Frontend\AddressController@getDistrict')->name('register.address.district');


Route::get('/distance/address', 'Frontend\PaymentController@getDistance')->name('customer.address.distance');

Route::group(['prefix' => 'customer'], function() {
	Route::group(['middleware' => 'auth'], function () {
		Route::get('/{locale}/logout', 'Frontend\LoginController@logout')->name('customer.logout');
		Route::get('/{locale}/account', 'Frontend\UserController@account')->name('customer.account');
		Route::get('/{locale}/account/detail', 'Frontend\UserController@accountDetail')->name('customer.account.detail');
		Route::get('/{locale}/order/history', 'Frontend\OrderController@listOrder')->name('customer.order');
		Route::get('/{locale}/order/confirmation/{order}', 'Frontend\OrderController@confirmationOrder')->name('customer.order.confirmation');
		Route::get('/{locale}/address', 'Frontend\AddressController@showAddress')->name('customer.address');
		// Route::get('/billing/address', 'Frontend\AddressController@createBillingAddress')->name('customer.billing.address');
		Route::get('/{locale}/create/address', 'Frontend\AddressController@showCreateForm')->name('customer.create.address');
		Route::get('/{locale}/edit/address/{id}', 'Frontend\AddressController@showUpdateForm')->name('customer.edit.address');
		Route::get('/{locale}/change/password', 'Frontend\UserController@changeFrontPassword')->name('customer.change.password');

		Route::put('/account/detail/{locale}', 'Frontend\UserController@updateAccount')->name('customer.update.account');
		Route::put('/{locale}/update/password', 'Frontend\UserController@updatePassword')->name('customer.update.password');
		Route::get('/address/city', 'Frontend\AddressController@getCity')->name('customer.address.city');
		Route::get('/address/district', 'Frontend\AddressController@getDistrict')->name('customer.address.district');
		Route::get('/address/cost', 'Frontend\ProductController@getCost')->name('customer.orders.get.cost');
		Route::post('/{locale}/address', 'Frontend\AddressController@saveAddress')->name('customer.save.address');
		Route::put('/{locale}/address', 'Frontend\AddressController@updateAddress')->name('customer.update.address');
		Route::get('/{locale}/wishlist', 'Frontend\ProductController@showWishlist')->name('customer.wishlist');
		Route::get('/{locale}/delete/wishlist/{id}', 'Frontend\ProductController@deleteWishlist')->name('customer.delete.wishlist');
		Route::put('/{locale}/use/point', 'Frontend\ProductController@usePoint')->name('customer.use.point');
		Route::put('/{locale}/cancel/point', 'Frontend\ProductController@cancelPoint')->name('customer.cancel.point');
		Route::get('/payment/getdone', 'Frontend\PaymentController@getDone')->name('payment.getDone');
		Route::get('/payment/getcancel', 'Frontend\PaymentController@getCancel')->name('payment.getCancel');
		Route::post('/payment/confirm/save', 'Frontend\PaymentController@confirmPayment')->name('customer.confirm.payment');
		Route::get('/{locale}/delete/address/{id}', 'Frontend\AddressController@delete')->name('customer.delete.address');
	});
});


