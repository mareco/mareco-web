<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api'], function () {
	Route::group(['prefix' => 'v1', 'namespace' => 'V1'], function () {
		Route::post('/login', 'AuthController@login');
		Route::post('/register', 'AuthController@register');
		Route::post('/login/facebook', 'AuthController@facebookLogin');
		Route::post('/login/google', 'AuthController@googleLogin');
		Route::post('/forgot-password', 'AuthController@forgotPassword');

		Route::group(['middleware' => 'jwt.auth'], function () {
			Route::put('/installations', 'AuthController@updateInstallation');
			Route::get('/logout', 'AuthController@logout');
			Route::get('/me', 'AuthController@profile');
			Route::post('/me', 'AuthController@updateProfile');
			Route::get('/products', 'ApiController@product');
			Route::get('/products/{id}', 'ApiController@productDetail');
			Route::get('/categories', 'ApiController@category');
			Route::get('/products/{id}/reviews','ApiController@getProductComment');
			Route::put('/reviews/{id}/update','ApiController@updateProductReview');
			Route::post('/products/comments','ApiController@productComment');
			Route::post('/checkout','ApiController@checkout');
			Route::group(['prefix' => 'carts'], function () {
				Route::get('/','ApiController@getCart');
				Route::post('/','ApiController@addCart');
				Route::put('/','ApiController@editCart');
				Route::delete('/','ApiController@deleteCart');
			});
			Route::group(['prefix' => 'wishlists'], function () {
				Route::get('/','ApiController@getWishlist');
				Route::post('/','ApiController@addWishlist');
				Route::put('/','ApiController@editWishlist');
				Route::delete('/','ApiController@deleteWishlist');
			});
			Route::group(['prefix' => 'address'], function () {
				Route::get('/','ApiController@getAddress');
				Route::post('/','ApiController@postAddress');
				Route::put('/{id}','ApiController@editAddress');
				Route::delete('/{id}','ApiController@deleteAddress');
			});
			Route::get('/order-history', 'ApiController@orderHistory');
			Route::get('/payment-type', 'ApiController@paymentType');
			Route::get('/shipping-method', 'ApiController@shippingMethod');
			Route::get('/banks','ApiController@getBank');
			Route::get('/sliders','ApiController@slider');
			Route::post('/order-history', 'ApiController@orderHistoryChangeStatus');
			Route::post('/cod-payment', 'ApiController@codPayment');
			Route::post('/paypal-payment', 'ApiController@paypalPayment');
			Route::get('/about-us', 'ApiController@aboutUs');
			Route::get('/term', 'ApiController@term');
			Route::put('/settings', 'ApiController@notificationSetting');
			Route::get('/promotions', 'ApiController@promotionList');

		});
	});
});