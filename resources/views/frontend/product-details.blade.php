@extends('layouts.frontend-dashboard')
@section('content')
<section class="content">
    <style type="text/css">
        @import url(//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css);

        fieldset, label { margin: 0; padding: 0; }
        body{ margin: 20px; }
        h1 { font-size: 1.5em; margin: 10px; }

        /****** Style Star Rating Widget *****/

        .rating {
            border: none;
            float: right;
        }

        .rating > input { display: none; }
        .rating > label:before {
            margin: 5px;
            font-size: 1.25em;
            font-family: FontAwesome;
            display: inline-block;
            content: "\f005";
        }

        .rating > .half:before {
            content: "\f089";
            position: absolute;
        }

        .rating > label {
            color: #ddd;
            float: right;
        }

        .rating > input:checked ~ label, /* show gold star when clicked */
        .rating:not(:checked) > label:hover, /* hover current star */
        .rating:not(:checked) > label:hover ~ label { color: #FFD700;  } /* hover previous stars in list */

        .rating > input:checked + label:hover, /* hover current star when changing rating */
        .rating > input:checked ~ label:hover,
        .rating > label:hover ~ input:checked ~ label, /* lighten current selection */
        .rating > input:checked ~ label:hover ~ label { color: #FFED85;  }
        .magnify-large {
            position: absolute;
            display: none;
            width: 300px;
            height: 300px;

            -webkit-box-shadow: 0 0 0 7px rgba(255, 255, 255, 0.85), 0 0 7px 7px rgba(0, 0, 0, 0.25), inset 0 0 40px 2px rgba(0, 0, 0, 0.25);
            -moz-box-shadow: 0 0 0 7px rgba(255, 255, 255, 0.85), 0 0 7px 7px rgba(0, 0, 0, 0.25), inset 0 0 40px 2px rgba(0, 0, 0, 0.25);
            box-shadow: 0 0 0 7px rgba(255, 255, 255, 0.85), 0 0 7px 7px rgba(0, 0, 0, 0.25), inset 0 0 40px 2px rgba(0, 0, 0, 0.25);

            -webkit-border-radius: 100%;
            -moz-border-radius: 100%;
            border-radius: 100%
        }
    </style>
    <?php

    use App\Helpers\Enums\ProductStatus;

    ?>
    @include('includes.breadcrumb')
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-xs-12">
                <div id="slider" class="flexslider main-slider">
                    <ul class="slides">
                        @if ($product->product_image)
                        <li>
                            @if ($product->product_image != null)
                            <div class="mag">
                                <img data-toggle="magnify" src="{{asset($product->product_image)}}" alt="" class="img-responsive rotate{{$product->rotate}}"/>
                            </div>
                            @else
                            <img src="{{asset('assets/frontend-image/No-Picture-Available.jpg')}}" alt="" class="img-responsive"/>
                            @endif
                        </li>
                        @endif
                        @if ($product->product_image_second)
                        <li>
                            @if ($product->product_image_second != null)
                            <div class="mag">
                                <img data-toggle="magnify" src="{{asset($product->product_image_second)}}" alt="" class="img-responsive rotate{{$product->secondRotate}}"/>
                            </div>
                            @else
                            <img src="{{asset('assets/frontend-image/No-Picture-Available.jpg')}}" alt="" class="img-responsive"/>
                            @endif
                        </li>
                        @endif
                        @if ($product->product_image_third)
                        <li>
                            @if ($product->product_image_third != null)
                            <div class="mag">
                                <img data-toggle="magnify" src="{{asset($product->product_image_third)}}" alt="" class="img-responsive rotate{{$product->thirdRotate}}"/>
                            </div>
                            @else
                            <img src="{{asset('assets/frontend-image/No-Picture-Available.jpg')}}" alt="" class="img-responsive"/>
                            @endif
                        </li>
                        @endif
                        @if ($product->product_image_fourth)
                        <li>
                            @if ($product->product_image_fourth != null)
                            <div class="mag">
                                <img data-toggle="magnify" src="{{asset($product->product_image_fourth)}}" alt="" class="img-responsive rotate{{$product->fourRotate}}"/>
                            </div>
                            @else
                            <img src="{{asset('assets/frontend-image/No-Picture-Available.jpg')}}" alt="" class="img-responsive"/>
                            @endif
                        </li>
                        @endif
                        @if ($product->product_image_fifth)
                        <li>
                            @if ($product->product_image_fifth != null)
                            <div class="mag">
                                <img data-toggle="magnify" src="{{asset($product->product_image_fifth)}}" alt="" class="img-responsive rotate{{$product->fiveRotate}}"/>
                            </div>
                            @else
                            <img src="{{asset('assets/frontend-image/No-Picture-Available.jpg')}}" alt="" class="img-responsive"/>
                            @endif
                        </li>
                        @endif
                    </ul>
                </div>
                <div id="carousel" class="flexslider thumbnails-slider">
                    <ul class="slides">
                        @if ($product->product_image)
                        <li>
                            @if ($product->product_image != null)
                            <img src="{{asset($product->product_image)}}" alt="" class="img-responsive rotate{{$product->rotate}}"/>
                            @else
                            <img src="{{asset('assets/frontend-image/No-Picture-Available.jpg')}}" alt="" class="img-responsive"/>
                            @endif
                        </li>
                        @endif
                        @if ($product->product_image_second)
                        <li>
                            @if ($product->product_image_second != null)
                            <img src="{{asset($product->product_image_second)}}" alt="" class="img-responsive rotate{{$product->secondRotate}}"/>
                            @else
                            <img src="{{asset('assets/frontend-image/No-Picture-Available.jpg')}}" alt="" class="img-responsive"/>
                            @endif
                        </li>
                        @endif
                        @if ($product->product_image_third)
                        <li>
                            @if ($product->product_image_third != null)
                            <img src="{{asset($product->product_image_third)}}" alt="" class="img-responsive rotate{{$product->thirdRotate}}"/>
                            @else
                            <img src="{{asset('assets/frontend-image/No-Picture-Available.jpg')}}" alt="" class="img-responsive"/>
                            @endif
                        </li>
                        @endif
                        @if ($product->product_image_fourth)
                        <li>
                            @if ($product->product_image_fourth != null)
                            <img src="{{asset($product->product_image_fourth)}}" alt="" class="img-responsive rotate{{$product->fourRotate}}"/>
                            @else
                            <img src="{{asset('assets/frontend-image/No-Picture-Available.jpg')}}" alt="" class="img-responsive"/>
                            @endif
                        </li>
                        @endif
                        @if ($product->product_image_fifth)
                        <li>
                            @if ($product->product_image_fifth != null)
                            <img src="{{asset($product->product_image_fifth)}}" alt="" class="img-responsive rotate{{$product->fiveRotate}}"/>
                            @else
                            <img src="{{asset('assets/frontend-image/No-Picture-Available.jpg')}}" alt="" class="img-responsive"/>
                            @endif
                        </li>
                        @endif
                    </ul>
                </div>
            </div>
            <div class="col-md-6 col-xs-12">
                @if($variant->qty == 0 || $product->status == ProductStatus::OUT_OF_STOCK)
                <div class="alert alert-warning">@lang('messages.outOfStock')</div>
                @endif            
                <div class="product-description border-box big-padding">
                    <div class="sku">
                        <h3>#{{$variant->sku}}</h3>
                    </div>
                    <div class="product-name">
                        @if($locale == 'id')
                        @if($product->productLang('id'))
                        <h1>{{$product->productLang('id')->name}}</h1>
                        @else
                        <h1>{{$product->name}}</h1>
                        @endif
                        @else
                        <h1>{{$product->name}}</h1>
                        @endif
                    </div>

                    <div class="product-price">
                        @if ($variant->is_discount == 1)
                        <span class="light price Scratch ">IDR {{number_format($variant->price)}}</span>
                        <span class="extra-bold price">IDR {{number_format($variant->discount_price)}}</span>
                        @else
                        <span class="light price">IDR {{number_format($variant->price)}}</span>
                        @endif

                        <i><h6 id="stock_qty">(Stock : {{$variant->qty}})</h6></i>
                    </div>
                    <div class="product-description-text">
                        {{$product->description}}
                    </div>
                    @if($variant->size != null)
                    <div class="select-size">
                        <div class="col-md-12 pd-left-0">
                            <div class="col-md-6 mg-bot-25 pd-left-0">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="@lang('messages.selSize')" id="inputSize">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> <i class="fa fa-location-arrow" aria-hidden="true"></i> </button>
                                        <ul class="dropdown-menu dropdown">
                                            @if (count($arrSize) != 0)
                                            @foreach ($arrSize as $value)
                                            <li onclick="getSize(event)">
                                                <a>{{$value}}</a>
                                            </li>
                                            <li class="divider"></li>
                                            @endforeach
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <input type="text" class="form-control" id="quantity" value="" placeholder="Qty">
                            </div>
                        </div>
                    </div>
                    @else 
                    <div class="col-md-2 no-padding mg-bot-15">
                        <input type="text" class="form-control" id="quantity" value="" placeholder="Qty">
                    </div>
                    <input type="hidden" class="form-control" id="getColor" value="null">
                    <input type="hidden" class="form-control" id="inputSize" value="null">
                    @endif
                    <div class="select-color"></div>
                    {{ csrf_field() }}
                    @if($variant->qty == 0)
                    <button class="btn btn-red width-full h-45 mg-bot-15 text-white" disabled="disabled">@lang('messages.addToCart')</button>
                    <button class="btn btn-grey width-full h-45 mg-bot-15 text-white" disabled="disabled">@lang('messages.addToWishlist')</button>
                    @elseif($product->status != ProductStatus::OUT_OF_STOCK)
                    <input type="hidden" name="" id="product_id" value="{{$product->id}}">
                    <button onclick="addCart()" class="btn btn-red width-full h-45 mg-bot-15 text-white">@lang('messages.addToCart')</button>
                    @if (Auth::user())
                    <button onclick="addWishlist()" class="btn btn-grey width-full h-45 mg-bot-15 text-white">@lang('messages.addToWishlist')</button>
                    @else
                    <button onclick="location.href='/{{$locale}}/login'" class="btn btn-grey width-full h-45 mg-bot-15 text-white">@lang('messages.addToWishlist')</button>
                    @endif
                    @endif
                    <span id="message"></span>
                    <span style="font-size: 10px !important; color: grey"> Estimated Time Arrive : {{$product->eta}}</span>
                    <a href="#" style="font-size: 10px !important" class="pull-right" data-toggle="modal" data-target="#myModal">*Terms & Conditions Apply</a>
                    <div id="myModal" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Terms & Conditions</h4>
                                </div>
                                <div class="modal-body">
                                    <table>
                                        @if ($enTerm)
                                        @if($locale == 'en')
                                        <tr>
                                            <td>
                                               {!!$enTerm->message!!}
                                           </td>
                                       </tr>
                                       @endif
                                       @if($locale == 'id')
                                       <tr>
                                        <td>
                                           {!!$idTerm->message!!}
                                       </td>
                                   </tr>
                                   @endif
                                   @endif
                               </table>
                           </div>
                           <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            <br><br>
        </div>
    </a>
</div>
</div>
<br>
@if(Auth::user())
@if($isExist)
@if(!$review)
<a href="/{{$locale}}/product/review/{{$product->id}}" class="btn btn-red text-white pull-right">Review </a>
@endif
@endif
@endif

<div class="row">
    <div class="col-md-12 col-xs-12">
        <div class="red text-white review">
            <span>REVIEW</span>
        </div>
    </div>
</div>
@if(count($reviews) == 0)
<div class="product-comment border-box small-padding">
    No reviews yet
</div>
@else
@foreach($reviews as $review)
<div class="product-comment border-box small-padding">
    <div class="row">
        <div class="col-md-2 col-sm-2">
            <div class="picture-profile">
                <img src="{{$review->user->photo}}" class="img-responsive" style="width: 100px">
            </div>
        </div>
        <div class="col-md-7 col-sm-7">
            <h4 class="title-comment bold">{{$review->user->first_name.' '.$review->user->last_name}}</h4>
            <p>
                {{$review->remarks}}
            </p>
        </div>
        <div class="col-md-3 col-sm-3 text-right">
            <span class="date">{{Carbon\Carbon::parse($review->created_at)->format('Y-m-d')}}</span>
            <div class="product-review">
                @for ($i = 0; $i < $review->rate; $i++)
                <span class="active"><img src="{{asset('assets/frontend-image/star-yellow.png')}}" alt="" /></span>
                @endfor
                @if($review->rate < 5)
                @for ($j = $i; $j < 5; $j++)
                <span class="unactive"><img src="{{asset('assets/frontend-image/star-white.png')}}" alt="" /></span>
                @endfor
                @endif
            </div>
            @if(Auth::user())
            @if($review->user_id == Auth::user()->id)
            <a class="btn btn-red mg-bot-15 text-white" href="/{{$locale}}/product/review/{{$product->id}}">Edit</a>
            @endif
            @endif
        </div>
    </div>
</div>
@endforeach
@endif

</div>
<p style="display: none;" id="countSize">{{count($arrSize)}}</p>
<p style="display: none;" id="getColor"></p>
</section>

<script type="text/javascript">
    $(document).ready( function() {
        // $('#myModal').on('shown.bs.modal', function(e) {
        //    alert('asd')
        // }).on('hidden.bs.modal', function (e) {

        // });
    });
    function addCart() {
        product_id = document.getElementById('product_id').value;
        size = document.getElementById('inputSize').value;
        color = document.getElementById('getColor').value;
        quantity = document.getElementById('quantity').value;
        if (quantity == '') {
            $.gritter.add(
            {
                title: '<i class="entypo-check"></i> You must fill quantity!',
                sticky: false,
                time: "3000"
            });
        } else {
            if (quantity <= 30 || quantity == 'null') {
                $.ajax({
                headers: {
                    'X-CSRF-Token': $('input[name="_token"]').val()
                },
                url: "{{ route('product.cart', $locale) }}",
                method: "POST",
                data: { product_id : product_id,
                    size : size,
                    color : color,
                    quantity: quantity },
                    success: function (data) {
                        if (data.message == null) {
                            document.getElementById('message').innerHTML = '<div class="alert alert-success">@lang('messages.succAddToCart')</div>';
                            document.getElementById('shoppingBag').innerHTML = data.qty;
                            $('#shoppingBag').removeClass('hide');
                            $('#shoppingBag').addClass('show');
                        } else {
                            document.getElementById('message').innerHTML = '<div class="alert alert-warning">'+data.message+'</div>';
                        }
                        setTimeout(function() {
                            $('.alert').fadeOut('fast');
                        }, 5000);
                    }
                });
            } else {
                $.gritter.add(
                {
                    title: '<i class="entypo-check"></i> Quantity cannot more than 30!',
                    sticky: false,
                    time: "3000"
                });
            }
        }
    }

    function addWishlist() {
        product_id = document.getElementById('product_id').value;
        size = document.getElementById('inputSize').value;
        color = document.getElementById('getColor').value;
        if (size == '') {
            $.gritter.add(
            {
                title: '<i class="entypo-check"></i> You must fill size!',
                sticky: false,
                time: "3000"
            });
        } else {
            $.ajax({
                headers: {
                    'X-CSRF-Token': $('input[name="_token"]').val()
                },
                url: "{{ route('product.wishlist', $locale) }}",
                method: "POST",
                data: { product_id : product_id,
                    size : size,
                    color : color },
                    success: function (data) {
                        if (data.message == null) {
                            document.getElementById('message').innerHTML = '<div class="alert alert-success">@lang('messages.succAddToWish')</div>';
                            document.getElementById('wishlistBag').innerHTML = data.countWishlist;
                            $('#wishlistBag').removeClass('hide');
                            $('#wishlistBag').addClass('show');
                        } else {
                            document.getElementById('message').innerHTML = '<div class="alert alert-warning">'+data.message+'</div>';
                        }
                        setTimeout(function() {
                            $('.alert').fadeOut('fast');
                        }, 5000);
                    }
                });
        }
    }

    $(window).on('load', function() {
      $('#carousel').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        itemWidth: 210,
        itemMargin: 5,
        asNavFor: '#slider'
    });

      $('#slider').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        sync: "#carousel"
    });
  });

    function getSize($event) {
        product_id = document.getElementById('product_id').value;
        countSize = document.getElementById('countSize').innerHTML;
        size = $($event.currentTarget).children()[0].innerHTML;
        document.getElementById('inputSize').value = size;

        if (countSize != 0) {
            $.ajax({
                headers: {
                    'X-CSRF-Token': $('input[name="_token"]').val()
                },
                url: "{{ route('product.color') }}",
                method: "GET",
                data: { product_id : product_id,
                    size : size },
                    success: function (data) {
                        console.log(data);
                        if (data.countVariant != 0) {
                            $('.product-price').empty();
                            $('.select-color').empty();

                            if (data.variant[0].is_discount == 1) {
                                var divPrice = '<span class="light price Scratch ">IDR '+data.variant[0].price.toLocaleString('en-US')+'</span> <span class="extra-bold price">IDR '+data.variant[0].discount_price.toLocaleString('en-US')+'</span>';
                            } else {
                                var divPrice = '<span class="light price">IDR '+data.variant[0].price.toLocaleString('en-US')+'</span>';
                            }
                            $('.product-price').append(divPrice);

                            var divColor = [];
                            for (var i = 0; i < data.countVariant; i++) {
                                var color = data.variant[i].color;
                                divColor += '<div class="col-md-2 mg-bot-25"><div onclick="selectColor(event)" class="div-color" style="background-color: '+color+';"><input type="hidden" value="'+color+'"></div></div>'
                            }
                            $('.select-color').append(divColor);
                        }
                    }
                });
        }
    }

    function selectColor($event) {
        var color = $($event.currentTarget).children()[0].value;
        document.getElementById('getColor').value = color;
        if ($($event.currentTarget)[0].className == 'div-color') {
            $('.div-color').removeClass('active');
            $($event.currentTarget)[0].className = 'div-color active';
        }
    }
</script>
@endsection
