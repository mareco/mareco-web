@extends('layouts.frontend-dashboard')
@section('content')
<section class="content">
        @include('includes.breadcrumb')
	<div class="container">
        <div class="col-md-12 pd-left-0">
            <div class="col-md-12 pd-left-0 map mg-bot-15" id="googleMap">

            </div>
            <div class="col-md-6 pd-left-0">
                <h4 class="f-20">@lang('messages.contact')</h4>
                <div class="profile-box contact">
                    @if (isset($config))
                    <ul>
                        <li>
                            <!-- <i class="fa fa-envelope-o f-16" aria-hidden="true"></i> -->
                            <img src="{{asset('assets/frontend-image/marker.png')}}">
                            <span class="f-30">{{$config->storage_address}}</span>
                        </li>
                        <li>
                            <!-- <i class="fa fa-envelope-o f-30" aria-hidden="true"></i> -->
                            <img src="{{asset('assets/frontend-image/service.png')}}">
                            <span class="f-30">{{$config->about_us_phone}}</span>
                        </li>
                        <li>
                            <!-- <i class="fa fa-envelope-o f-30" aria-hidden="true"></i> -->
                            <img src="{{asset('assets/frontend-image/phone.png')}}">
                            <span class="f-30">{{$config->about_us_mobile}}</span>
                        </li>
                        <li>
                            <i class="fa fa-envelope-o f-27" aria-hidden="true"></i>
                            <span class="f-30 content-icon">{{$config->about_us_email}}</span>
                        </li>
                    </ul>
                    @else
                    <h3>Don't have a data contact us yet</h3>
                    @endif
                </div>
            </div>
            <div class="col-md-6 pd-left-0">
                <h4 class="mg-bot-15 f-20">@lang('messages.getInTouch')</h4>
                {!! Form::open(['url' => route('get.in.touch', $locale),'method' => 'POST','data-parsley-validate' => 'true','enctype'=>'multipart/form-data']) !!}
                <div class="profile-box form-getintouch">
                    <div class="mg-bot-20">
                        <label class="label pd-left-0"><span class="f-20">@lang('messages.name')</span></label>
                        <input type="text" name="name" class="form-control">
                    </div>
                    <div class="mg-bot-20">
                        <label class="label pd-left-0"><span class="f-20">Email</span></label>
                        <input type="email" name="email" class="form-control">
                    </div>
                    <div class="col-md-12 pd-left-0 pd-right-0 mg-bot-20">
                        <label class="label pd-left-0"><span class="f-20">@lang('messages.message')</span></label>
                        <textarea class="form-control" rows="5" name="message"></textarea>
                    </div>
                    <button type="submit" class="btn btn-save btn-red width-full f-16">@lang('messages.send')</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    @if(isset($config->storage_lat) || isset($config->storage_lng))
    <input type="hidden" name="storage_lat" value="{{$config->storage_lat}}" id="storage_lat">
    <input type="hidden" name="storage_lng" value="{{$config->storage_lng}}" id="storage_lng">
    @endif
</section>
<script type="text/javascript">
    var storageLat = $('#storage_lat').val();
    var storageLng = $('#storage_lng').val();
    function myMap() {
        var mapProp= {
            center:new google.maps.LatLng(storageLat,storageLng),
            zoom:15,
            draggable: false,
            scrollwheel: false,
            disableDoubleClickZoom: true,
        };
        var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
        var infoWindow = new google.maps.InfoWindow;

        infoWindow.setPosition(new google.maps.LatLng(storageLat,storageLng));
        infoWindow.setContent('We are here!');
        infoWindow.open(map);
    }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCSlAK_5cMtJS9jDISX0WIG3VDnWpQiUjk&callback=myMap"></script>
@endsection
