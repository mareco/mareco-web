@extends('layouts.frontend-dashboard')
@section('content')
<section class="content">
	<div class="container" style="text-align: center">
		@if($locale == 'en')
		<h2>Thank you for getting in touch!</h2>

		<h5>We appreciate you contacting us. We will get back to you soon.</h5>
		<h5>Have a great day ahead!</h5>
		@endif
		@if($locale == 'id')
		<h2>Terima Kasih telah menghubungi kami!</h2>

		<h5>Kami sangat senang anda menghubungi kami. Kami akan segera membalas pesanan anda</h5>
		<h5>Semoga harimu menyenangkan!</h5>
		@endif
	</div>
</section>
@endsection
