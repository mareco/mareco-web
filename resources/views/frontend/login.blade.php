@extends('layouts.frontend-dashboard')
@section('content')
<section class="content">
	<div class="container">
		<div class="col-md-12">
			<div class="login-page">
				<h3 class="f-30">@lang('messages.login')</h3>
				<hr class="modif-hr mg-bot-15">

				@if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
                @endif
                @if (Session::has('Success'))
                    <div class="alert alert-success" id="SuccessAlert">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Success! </strong> {{ Session::get('Success') }}
                    </div>
                @endif
                @if (Session::has('Error'))
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Error! </strong> {{ Session::get('Error') }}
                </div>
                @endif
				<form class="form-horizontal" method="POST" action="{{ route('customer.login', $locale) }}">
					{{ csrf_field() }}
					<div class="profile-box mg-bot-20">
						<label class="label pd-left-0"><span>Email</span></label>
                        <input type="text" name="email" class="form-control">
                        @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
					</div>
					<div class="profile-box mg-bot-20">
						<label class="label pd-left-0"><span>@lang('messages.pw')</span></label>
                        <input type="password" name="password" class="form-control">
                        @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
					</div>

					<button class="btn btn-save width-full h-45 mg-bot-15 f-16">@lang('messages.logEmail')</button>
					<button type="button" onclick="location.href='/{{$locale}}/forgot';" class="btn btn-grey width-full h-45 mg-bot-15 f-16 text-black">@lang('messages.forgotpw')</button>

                    <div class="col-md-6 no-padding">
                    	<a href="/login/redirect/facebook/{{$locale}}" class="mg-auto">
                        	<button type="button" class="btn btn-fb btn-lg social-button facebook mg-auto width-full f-14"><i class="fa fa-facebook-square left"></i> @lang('messages.logFB')</button>
                    	</a>
                    </div>
                    <div class="col-md-6 no-padding">
	                    <a href="/login/redirect/google/{{$locale}}" class="mg-auto">
	                        <button type="button" class="btn btn-fb btn-lg social-button google mg-auto width-full fl-right f-14"><i class="fa fa-google-plus-square" aria-hidden="true"></i> @lang('messages.logGoogle') <i class="fa fa-plus plus-position text-white f-16" aria-hidden="true"></i></button>
	                    </a>
                    </div>
				</form>
			</div>
		</div>
    </div>
</section>
@endsection
