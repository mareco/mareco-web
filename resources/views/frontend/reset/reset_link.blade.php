<p>Hi, {{$user->first_name}}  {{$user->last_name}}</p>

<p>You are receiving this email because we received a password reset request for your account.</p>

Click here to reset your password: <a href="{{ $link = url($locale.'/customer/password/reset/'.$user->remember_token).'?email='.urlencode($user->getEmailForPasswordReset()) }}"> {{ $link }} </a>

<p>Regards, Mareco Store</p>