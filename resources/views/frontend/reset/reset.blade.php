@extends('layouts.frontend-dashboard')
@section('content')
<section class="content">
	<div class="container">
		<div class="col-md-12 no-padding">
			<div class="login-page">
				<h3 class="f-30">@lang('messages.resetPW')</h3>
				<hr class="modif-hr mg-bot-15">

				<form class="form-horizontal" role="form" method="POST" action="{{ url($locale.'/customer/password/reset') }}">
                {{ csrf_field() }}
                <!-- <input type="hidden" name="token" value="{{ $token }}"> -->
                <input type="hidden" name="token" value="{{ $token }}">
                <h3 class="form-title font-green">@lang('messages.resetPW')</h3>
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                <div class="col-md-12">
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label class="label f-20 real-black fw-normal no-padding"><span>Email</span></label>
                        <input type="email" class="form-control placeholder-no-fix" name="email" value="{{ $email or old('email') }}">
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label class="label f-20 real-black fw-normal no-padding"><span>@lang('messages.pw')</span></label>
                        <input class="form-control placeholder-no-fix" type="password" placeholder="@lang('messages.pw')" name="password" />
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    

                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        <label class="label f-20 real-black fw-normal no-padding"><span>@lang('messages.reTypePW')</span></label>
                        <input class="form-control placeholder-no-fix" type="password" placeholder="@lang('messages.reTypePW')" name="password_confirmation" /> 
                        @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </span>
                        @endif    
                    </div>
                </div>

                <div class="form-actions no-padding">
                    <button type="submit" class="btn btn-save">Reset</button>
                </div>
            </form>
			</div>
		</div>
    </div>
</section>
@endsection
