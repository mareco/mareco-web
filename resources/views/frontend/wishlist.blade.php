@extends('layouts.frontend-dashboard')
@section('content')
<section class="content">
    <div class="container">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Wishlist</li>
        </ol>
    </div>
    <div class="container">
        <div class="wishlist-box">
            <div class="row">
                    @if (Session::has('Success'))
                        <div class="alert alert-success" id="SuccessAlert">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong>Success! </strong> {{ Session::get('Success') }}
                        </div>
                    @endif
                    @if (Session::has('Error'))
                        <div class="alert alert-danger" id="errorAlert">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong>Error! </strong> {{ Session::get('Error') }}
                        </div>
                    @endif

                    @if (count($wishlist) != 0)
                        @foreach ($wishlist as $value)
                            <!-- <a href="/product/{{$value->variant->product->slug_url}}" class="no-hover">
                                <div class="col-md-3">
                                    <div class="wishlist-box">
                                        <img src="{{asset($value->variant->product->product_image)}}" alt="" class="img-wishlist">
                                        <h5 class="text-center real-black">
                                            <span class="pd-left-25 bd-color" style="background-color: {{$value->variant->color}};"></span>
                                            <span class="wishlist-detail"> {{$value->variant->size}} / IDR {{number_format($value->variant->price)}}</span>
                                        </h5>

                                        <div class="hover-btn">
                                            <a href="/customer/delete/wishlist/{{$value->id}}" type="button" class="close-round" data-dismiss="alert">
                                                <svg class="" viewBox="0 0 24 24"><path d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"></path><path d="M0 0h24v24h-24z" fill="none"></path></svg>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </a> -->

                                <div class="col-md-15 col-xs-6 col-sm-4">
                                    <a href="/{{$locale}}/product/{{$value->variant->product->slug_url}}" class="no-hover">
                                        <div class="product-img">
                                            <img src="{{asset($value->variant->product->product_image)}}" alt="" class="img-wishlist rotate{{$value->variant->product->rotate}}">
                                        </div>

                                        <div class="product-name">
                                            <span>{{$value->variant->product->name}}</span>
                                        </div>
                                        <div class="product-price">
                                            <span class="light price">IDR {{number_format($value->variant->price)}}</span>
                                        </div>
                                        <div class="product-variant">
                                            <span class="color bd-color pd-left-25" style="background-color: {{$value->variant->color}};"></span>
                                            <span class="size">{{$value->variant->size}}</span>
                                        </div>
                                    </a>
                                    <a href="/customer/{{$locale}}/delete/wishlist/{{$value->id}}" data-dismiss="alert" class="remove-wishlist">@lang('messages.removeFromWishlist')</a>
                                </div>
                        @endforeach
                    @else
                    <div class="col-md-12">
                        <h3>@lang('messages.noWishlist')</h3>
                    </div>
                    @endif
                </div>
        </div>
    </div>
</section>
<script type="text/javascript">
  setTimeout(function() {
      $('.alert').fadeOut('fast');
  }, 5000);
</script>
@endsection
