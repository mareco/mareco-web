@extends('layouts.frontend-dashboard')
@section('content')
<section class="content">
    <div class="container">
        <div class="cart-block">
            <div class="row">
                @if (count($cart) != 0)
                <div class="col-md-8">
                  <div class="flash-message">
                      @if (Session::has('Success'))
                          <div class="alert alert-success" id="SuccessAlert">
                              <button type="button" class="close" data-dismiss="alert">&times;</button>
                              <strong>Success! </strong> {{ Session::get('Success') }}
                          </div>
                      @endif
                      @if (Session::has('Error'))
                          <div class="alert alert-danger" id="errorAlert">
                              <button type="button" class="close" data-dismiss="alert">&times;</button>
                              <strong>Error! </strong> {{ Session::get('Error') }}
                          </div>
                      @endif
                      <p id="message"></p>
                  </div>
                    <div class="shopping-cart border-box medium-padding font-16">
                        <div class="table-responsive">
                            <table class="table cart">
                                <thead>
                                  <tr>
                                    <th colspan="2" class="product-item">Item</th>
                                    <th class="product-qty">@lang('messages.qty')</th>
                                    <th class="product-size">@lang('messages.size')</th>
                                    <th class="product-price">@lang('messages.price')</th>
                                    <th class="product-subtotal">Subtotal</th>
                                    <th></th>
                                  </tr>
                                </thead>
                                <tbody>
                                  @if (count($cart) != 0)
                                    @foreach ($cart as $value)
                                    <?php
                                      $rotate = App\Http\Controllers\Frontend\ProductController::getRotate($value->id);
                                    ?>
                                      <tr>
                                          <td class="product-img">
                                              <img src="{{asset($value->options->photo)}}" alt="" class="img-responsive rotate{{$rotate->rotate}}"/>
                                          </td>
                                          <td class="product-name">
                                              <p>{{$value->name}}</p>
                                          </td>
                                          <td class="product-qty">
                                            <p>
                                              <select class="Quantity">
                                                @foreach ($arrQuantity as $qty)
                                                  <option value="{{$qty}}" {{$value->qty == $qty ? 'selected="selected"' : ''}}>{{$qty}}</option>
                                                @endforeach
                                              </select>
                                              <input type="hidden" value="{{$value->rowId}}" id="id" name="id">
                                              <input type="hidden" value="{{$value->options->variant_id}}" id="variant_id" name="variant_id">
                                            </p>
                                          </td>
                                          <td class="product-size">
                                            @if($value->options->size != 'null')
                                              <p>
                                                {{$value->options->size}}
                                                <span style="background-color: {{$value->options->color}};"></span>
                                              </p>
                                            @else
                                              -
                                            @endif
                                          </td>
                                          <td class="product-price">
                                              <p>IDR {{number_format($value->price)}}</p>
                                          </td>
                                          <td class="product-subtotal">
                                              <p>IDR {{number_format($value->price * $value->qty)}}</p>
                                          </td>
                                          <td class="text-center">
                                            <p>
                                              <a href="/{{$locale}}/delete/cart/{{$value->rowId}}"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                            </p>
                                          </td>
                                      </tr>
                                    @endforeach
                                  @endif
                                </tbody>
                          </table>

                          <div class="coupon-block">
                              <div class="col-md-6">
                              @if (count($cart) != 0)
                                @if (Auth::user())
                                  @if (Auth::user()->point == 0)
                                      <p class="point-remaining" id="remainingPoint">@lang('messages.noPoint')</p>
                                  @else
                                    @if (Auth::user()->amount_point != null)
                                      <p class="point-remaining" id="remainingPoint">@lang('messages.youHave') <b>{{Auth::user()->point - Auth::user()->amount_point}}</b> @lang('messages.point') @lang('messages.remaining')</p>
                                    @else
                                      <p class="point-remaining" id="remainingPoint">@lang('messages.youHave') <b>{{Auth::user()->point}}</b> @lang('messages.point') @lang('messages.remaining')</p>
                                    @endif
                                  @endif
                                  @if (Auth::user()->use_point == 0)
                                    {!! Form::open(['url' => '/customer/'.$locale.'/use/point', 'method' => 'PUT','data-parsley-validate' => 'true']) !!}
                                      <div class="input-group">
                                          <input type="text" class="form-control" name="point">
                                          <span class="input-group-btn">
                                            <button class="btn btn-red text-white font-16" type="submit">@lang('messages.usePoint')</button>
                                          </span>
                                      </div><br>
                                    {!! Form::close() !!}
                                  @else
                                    {!! Form::open(['url' => '/customer/'.$locale.'/cancel/point', 'method' => 'PUT','data-parsley-validate' => 'true']) !!}
                                      <button class="btn btn-red text-white font-16" type="submit">@lang('messages.cancel') @lang('messages.usePoint')</button>
                                    {!! Form::close() !!}
                                  @endif
                                @else
                                  <div class="input-group">
                                      <input type="text" class="form-control" data-parsley-required = "true">
                                      <span class="input-group-btn">
                                        <a href="/{{$locale}}/login" class="btn btn-red text-white font-16" type="button">@lang('messages.usePoint')</a>
                                      </span>
                                  </div>
                                @endif
                              @endif
                              </div>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                  <p></p>
                    <div class="summary border-box medium-padding font-16">
                        <table class="table summary">
                            <thead>
                              <tr>
                                <th colspan="2">@lang('messages.summary')</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                  <td>
                                      Subtotal
                                  </td>
                                  <td>
                                      IDR {{number_format($subtotal)}}
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                      @lang('messages.point') @lang('messages.discount')
                                  </td>
                                  <td>
                                    @if($point != '-')
                                      IDR {{number_format($point)}}
                                    @else
                                      {{$point}}
                                    @endif
                                  </td>
                              </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    @if($locale == 'en')
                                    <td>
                                        Order Total
                                    </td>
                                    @endif
                                    @if($locale == 'id')
                                    <td>
                                        Total Belanja
                                    </td>
                                    @endif
                                    <td>
                                      @if ($point != '-')
                                        IDR {{number_format($subtotal - $point)}}
                                      @else
                                        IDR {{number_format($subtotal)}}
                                      @endif
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                @else
                  <div class="col-md-12">
                      <p class="empty-bag">@lang('messages.noItemCart')</p>
                      <p class="empty-bag"><a href="/{{$locale}}/product">@lang('messages.clickHere') </a> @lang('messages.toContShop')</p>
                  </div>
                @endif
            </div>
            <div class="button-block">
                <div class="row">
                    <div class="col-md-6">
                        <a class="btn btn-grey mg-bot-15 text-white font-16">CONTINUE SHOPPING</a>
                    </div>
                    <div class="col-md-6">
                      @if (count($cart) != 0)
                      @if (Auth::user())
                        <a href="{{ route ('checkout', $locale) }}" class="btn btn-red mg-bot-15 text-white font-16 right">CHECKOUT</a>
                        @else
                        <a href="{{ route ('customer.login.form', $locale) }}" class="btn btn-red mg-bot-15 text-white font-16 right">CHECKOUT</a>
                        @endif
                      @endif
                    </div>
                </div>
            </div>
            {{ csrf_field() }}
        </div>
    </div>
</section>
<script type="text/javascript">
  setTimeout(function() {
      $('.alert').fadeOut('fast');
  }, 5000);

  $('.Quantity').change(function($event) {
    qty = $($event.currentTarget)[0].value;
    id = $($event.currentTarget).siblings()[0].value;
    variant_id = $($event.currentTarget).siblings()[1].value;

    return updateQuantity();
  });

  function updateQuantity() {
    $.ajax({
      headers: {
          'X-CSRF-Token': $('input[name="_token"]').val()
      },
      url: "{{ route('product.update.cart', $locale) }}",
      method: "PUT",
      data: { id : id,
              qty : qty,
              variant_id : variant_id},
      success: function (data) {
        console.log(data.message);
          if (data.message == null) {
            window.location.reload();
          } else {
            $('#message')[0].innerHTML = '<div class="alert alert-warning">'+data.message+'</div>';
          }
      }
    });
  }
</script>
@endsection
