@extends('layouts.frontend-dashboard')
@section('content')
<section class="content">
	<div class="container">
        @include('includes.breadcrumb')
		<div class="col-md-12 pd-left-0">
            <div class="col-md-12 pd-left-0 mg-bot-20">
            	<img src="{{asset('assets/frontend-image/about-us-red.png')}}" class="img-responsive img-about-us">
            </div>
            <div class="col-md-12 pd-left-0 about-f-size">
                @if(isset($config))
                    <p>{!!$config->about_us_content!!}</p>
                @endif
            </div>
        </div>
    </div>
</section>
@endsection
