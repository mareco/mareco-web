@extends('layouts.frontend-dashboard')
@section('content')
<section class="content">
  @include('includes.breadcrumb')
  <div class="container">
    <div class="product-list">
        <div class="row">
            @if (count($promotion) != 0)
                @foreach ($promotion as $image)
                    @if ($image->size == 0)
                    <a href="/{{$locale}}/product/{{$image->product->slug_url}}">
                        <div class="col-md-6 mg-bot-20">
                            <div class="big-sale">
                                <img src="{{asset($image->photo)}}" alt="" class="img-responsive"/>
                                @if ($image->title != null || $image->title != '')
                                <div class="red-box" style="background-color: {{$image->bg_color}};">
                                    <span class="light" style="color: {{$image->font_color}};">{{$image->content}}</span>
                                    <div class="sale-box" style="color: {{$image->font_color}}; border-color: {{$image->font_color}};">
                                        <span class="extra-bold">{{$image->title}}</span>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                    </a>
                    @endif
                @endforeach
            @endif
        </div> 
    </div>
</div>
</div>

<div class="container">
    <div class="text-center">
        {{ $promotion->links() }}
        </div>
    </div>
</section>
@endsection
