@extends('layouts.frontend-dashboard')
@section('content')
<section class="content">
	<div class="container">
		<div class="col-md-12">
			<div class="login-page">
				<h3>@lang('messages.createAccount')</h3>
				<h5>@lang('messages.haveAccount')<a href="/login" class="f-green"> @lang('messages.logInstead')</a></h5>
				<hr class="modif-hr mg-bot-15">

				<form class="form-horizontal" role="form" enctype="multipart/form-data" method="POST" action="{{ url($locale.'/register') }}">
					{!! csrf_field() !!}
					<div class="col-md-6 pd-left-0 mg-bot-20">
						<div class="profile-box">
							<label class="label pd-left-0"><span>@lang('messages.firstName')</span></label>
	                        <input type="text" name="first_name" class="form-control" id="firstName">
						</div>
	                    @if ($errors->has('first_name'))
	                        <span class="help-block">
	                            <strong>{{ $errors->first('first_name') }}</strong>
	                        </span>
	                    @endif
					</div>
					<div class="col-md-6 pd-right-0 mg-bot-20">
						<div class="profile-box">
							<label class="label pd-left-0"><span>@lang('messages.lastName')</span></label>
	                        <input type="text" name="last_name" class="form-control" id="lastName">
						</div>
						@if ($errors->has('last_name'))
	                        <span class="help-block">
	                            <strong>{{ $errors->first('last_name') }}</strong>
	                        </span>
	                    @endif
					</div>
					<div class="col-md-8 no-padding mg-bot-20">
						<div class="profile-box">
							<label class="label pd-left-0"><span>Email</span></label>
	                        <input type="email" name="email" class="form-control" id="email">
						</div>
						@if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
					</div>
					<div class="col-md-4 pd-right-0 mg-bot-20">
						<div class="profile-box">
							<label class="label pd-left-0"><span>@lang('messages.phone')</span></label>
	                        <input type="phone" name="phone" class="form-control" id="phone">
						</div>
						@if ($errors->has('phone'))
                            <span class="help-block">
                                <strong>{{ $errors->first('phone') }}</strong>
                            </span>
                        @endif
					</div>
					<div class="col-md-6 pd-left-0 mg-bot-20">
						<div class="profile-box">
							<label class="label pd-left-0"><span>@lang('messages.pw')</span></label>
	                        <input type="password" name="password" class="form-control" id="password">
						</div>
						@if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
					</div>
					<div class="col-md-6 pd-right-0 mg-bot-20">
						<div class="profile-box">
							<label class="label pd-left-0"><span>@lang('messages.conPW')</span></label>
	                        <input type="password" name="password_confirmation" class="form-control" id="confirmPassword">
						</div>
						@if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </span>
                        @endif
					</div>

					<div class="col-md-12 pd-left-0 pd-right-0 mg-bot-20">
						<div class="profile-box">
	                    	<label class="label pd-left-0"><span>@lang('messages.address')</span></label>
	                        <textarea rows="3" name="address" id="address" class="form-control mg-bot-15" data-parsley-required = "true"></textarea>
						</div>
						@if ($errors->has('address'))
                            <span class="help-block">
                                <strong>{{ $errors->first('address') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-6 pd-left-0 mg-bot-20">
                    	<div class="profile-box">
	                        <label class="label pd-left-0"><span>@lang('messages.province')</span></label>
	                        <select class="form-control" name="province" id="province" data-parsley-required = "true">
	                            <option></option>
	                            @foreach ($provinces as $value)
	                                <option value="<?php echo $value['province_id'] ?>"><?php echo $value['province']  ?></option>
	                            @endforeach
	                        </select>
                    	</div>
                    	@if ($errors->has('province'))
                            <span class="help-block">
                                <strong>{{ $errors->first('province') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-6 pd-right-0 mg-bot-20">
                        <div class="profile-box">
	                        <label class="label pd-left-0"><span>@lang('messages.city')</span></label>
	                        <select class="form-control" name="city" id="city" data-parsley-required = "true">
	                            <option></option>
	                        </select>
                        </div>
                        @if ($errors->has('city'))
                            <span class="help-block">
                                <strong>{{ $errors->first('city') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-6 pd-left-0 mg-bot-20">
                        <div class="profile-box">
	                        <label class="label pd-left-0"><span>@lang('messages.district')</span></label>
	                        <select class="form-control" name="district" id="district" data-parsley-required = "true">
	                            <option></option>
	                        </select>
                        </div>
                        @if ($errors->has('district'))
                            <span class="help-block">
                                <strong>{{ $errors->first('district') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-6 pd-right-0 mg-bot-20">
                        <div class="profile-box">
	                        <label class="label pd-left-0"><span>@lang('messages.postal')</span></label>
	                        <input type="text" name="postal_code" class="form-control" id="postal_code" data-parsley-required = "true">
                        </div>
                        @if ($errors->has('postal_code'))
                            <span class="help-block">
                                <strong>{{ $errors->first('postal_code') }}</strong>
                            </span>
                        @endif
                    </div>
                    <input type="hidden" name="is_shipping_address" value="true" class="form-control" data-parsley-required = "true">

                    <div class="col-md-12 no-padding">
	                    <button class="btn btn-save w-inherit h-45 mg-r-10 w-150">@lang('messages.createAccount')</button>
						<button type="button" class="btn btn-reset h-45 w-85" onclick="reset()">RESET</button>
                    </div>

				</form>
			</div>
		</div>
    </div>
</section>
<script type="text/javascript">
	function reset() {
		document.getElementById('firstName').value = '';
		document.getElementById('lastName').value = '';
		document.getElementById('phone').value = '';
		document.getElementById('email').value = '';
		document.getElementById('password').value = '';
		document.getElementById('confirmPassword').value = '';
		document.getElementById('address').value = '';
		document.getElementById('postal_code').value = '';
		var selectCity = document.getElementById('city');
        var selectDistrict = document.getElementById('district');
        var selectProvince = document.getElementById('province');
        selectProvince.selectedIndex = 0; 
        selectCity.options.length = 0;
        selectDistrict.options.length = 0;
	}

	$(document).ready(function() {
        $('#province').change(function($event) {
            province_id = $($event.currentTarget)[0].value;
            return getDataCity($event);
        });

        $('#city').change(function($event) {
            city_id = $($event.currentTarget)[0].value;
            return getDataDistrict($event);
        });

        var getDataCity = function() {
            $.ajax({
                headers: {
                    'X-CSRF-Token': $('input[name="_token"]').val()
                },
                url: "{{ route('register.address.city') }}",
                method: "GET",
                data: { province_id : province_id},
                success: function (data) {
                    var select = document.getElementById('city');
                    var selectDistrict = document.getElementById('district');
                    select.options.length = 0;
                    selectDistrict.options.length = 0;
                    options = data.city;
                    select.options.add(new Option(""));
                    for (var i = 0; i < data.count; i++) {
                      option = options[i];
                      select.options.add(new Option(option.city_name, option.city_id));
                    }
                }
            });
        };

        var getDataDistrict = function() {
            $.ajax({
                headers: {
                    'X-CSRF-Token': $('input[name="_token"]').val()
                },
                url: "{{ route('register.address.district') }}",
                method: "GET",
                data: { city_id : city_id},
                success: function (data) {
                    var select = document.getElementById('district');
                    select.options.length = 0;
                    options = data.district;
                    select.options.add(new Option(""));
                    for (var i = 0; i < data.count; i++) {
                      option = options[i];
                      select.options.add(new Option(option.subdistrict_name, option.subdistrict_id));
                    }
                }
            });
        };
    });
</script>
@endsection