@extends('layouts.frontend-dashboard')
@section('content')

<section class="content homepage">
    <svg width="0" height="0">
      <clipPath id="polygon-tag" class="svg-clip">
          <polygon points="-1 119,103 75,150 -1,0 0">
          </polygon>
      </clipPath>
  </svg>
  <svg width="0" height="0" class="svg-clip">
      <clipPath id="polygon-tag-mobile">
          <polygon points="-1 119,50 75,80 -1,0 0">
          </polygon>
      </clipPath>
  </svg>
  <div id="myCarousel" class="carousel slide homepage-slider" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
        @if (isset($firstPicture))
        <div class="item active">
            <img src="{{asset($firstPicture->url)}}" alt="Chania">
            <div class="shop-slider">
                @if (isset($firstPicture->category))
                <h1>@lang('messages.allNew')</h1>
                <h1 class="item-cat red text-white text-center">{{$firstPicture->category->name}}</h1>
                <a href="{{ url("/$locale/product?category=" . $firstPicture->category->slug_url) }}" class="btn btn-default">@lang('messages.shop') @lang('messages.now')</a>
                @endif
            </div>
        </div>
        @endif
        @if (!isset($firstPicture) && count($picture) == 0)
        <div class="item active">
            <img src="{{asset('assets/frontend-image/slider.jpg')}}" alt="Chania">
            <div class="shop-slider">
                <h1>@lang('messages.allNew')</h1>
                <h1 class="item-cat red text-white">@lang('messages.accesories')</h1>
                <a href="#" class="btn btn-default">@lang('messages.shop') @lang('messages.now')</a>
            </div>
        </div>
        @endif

        @if (count($picture) != 0)
        @foreach ($picture as $image)
        <div class="item">
            <img src="{{asset($image->url)}}" alt="Chania">
            <div class="shop-slider">
                @if (isset($image->category))
                <h1>@lang('messages.allNew')</h1>

                <h1 class="item-cat red text-white text-center">{{$image->category->name}}</h1>
                <a href="{{ url("/$locale/product?category=" . $image->category->slug_url) }}" class="btn btn-default">@lang('messages.shop') @lang('messages.now')</a>
                @endif
            </div>
        </div>
        @endforeach
        @endif
    </div>
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">@lang('messages.previous')</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">@lang('messages.next')</span>
    </a>
</div>

<div class="promo-title-center">
    @lang('messages.weeklyproduct')
</div>

<div class="top-5">
    <div class="container">
        <div class="row">
            @if (count($product) != 0)
            <div class="owl-carousel owl-theme">
                @foreach ($product as $item)
                <?php
                $variant = App\Http\Controllers\Frontend\ProductController::getVariant($item->id);
                ?>
                <div class="product-grid">

                    @if($locale == "en")
                    <a href="{{$locale}}/product/{{$item->slug_url}}">
                        @if ($item->status == 1)
                        <div class="product-tag">
                            <div class="polygon-tag arrival-tag text-white">
                                <span class="bold">New</span><br>
                                <span class="f-16">Arrival</span>
                            </div>
                        </div>
                        @endif
                        @if ($item->status == 2)
                        <div class="product-tag">
                            <div class="polygon-tag sale-tag text-white">
                                <span class="bold">Now</span><br>
                                <span class="f-16">Sale</span>
                            </div>
                        </div>
                        @endif
                        @if ($item->status == 3)
                        <div class="product-tag">
                            <div class="polygon-tag best-selling green text-white">
                                <span class="bold">Best</span><br>
                                <span class="f-16">Selling</span>
                            </div>
                        </div>
                        @endif
                        @if ($item->status == 4 || $variant->qty == 0)
                        <div class="product-tag">
                            <div class="polygon-tag out-of-stock green text-white">
                                <span class="bold">Sold</span><br>
                                <span class="f-14">Out</span>
                            </div>
                        </div>
                        @endif
                        <div class="product-img">
                            <img src="{{asset($item->product_image)}}" alt="" class="img-responsive"/>
                        </div>
                    </a>
                    @endif
                    @if($locale == "id")
                    <a href="{{$locale}}/product/{{$item->slug_url}}">
                        @if ($item->status == 1)
                        <div class="product-tag">
                            <div class="polygon-tag arrival-tag text-white">
                                <span class="bold" style="font-size: 14px !important">Baru</span><br>
                                <span class="f-14"> </span>
                            </div>
                        </div>
                        @endif
                        @if ($item->status == 2)
                        <div class="product-tag">
                            <div class="polygon-tag sale-tag text-white">
                                <span class="bold" style="font-size: 14px !important">Diskon</span><br>
                                <span class="f-14"> </span>
                            </div>
                        </div>
                        @endif
                        @if ($item->status == 3)
                        <div class="product-tag">
                            <div class="polygon-tag best-selling green text-white">
                                <span class="bold" style="font-size: 14px !important">Popular</span><br>
                                <span class="f-14"> </span>
                            </div>
                        </div>
                        @endif
                        @if ($item->status == 4 || $variant->qty == 0)
                        <div class="product-tag">
                            <div class="polygon-tag out-of-stock green text-white">
                                <span class="bold" style="font-size: 14px !important">Habis</span><br>
                                <span class="f-14"> </span>
                            </div>
                        </div>
                        @endif
                        <div class="product-img">
                            <img src="{{asset($item->product_image)}}" alt="" class="img-responsive"/>
                        </div>
                    </a>
                    @endif
                    <a href="{{$locale}}/product/{{$item->slug_url}}">
                        <div class="product-name">
                            @if($locale == 'id')
                            @if($item->productLang('id'))
                            <span>{{$item->productLang('id')->name}}</span>
                            @else
                            <span>{{$item->name}}</span>
                            @endif
                            @else
                            <span>{{$item->name}}</span>
                            @endif
                        </div>
                        <div class="product-price no-margin">
                            @if ($variant->is_discount == 1)
                            <span class="light price Scratch ">IDR {{number_format($variant->price)}}</span>
                            <span class="extra-bold price">IDR {{number_format($variant->discount_price)}}</span>
                            @else
                            <span class="light price">IDR {{number_format($variant->price)}}</span>
                            @endif
                        </div>
                    </a>
                </div>
                @endforeach
            </div>
            @else
            <h3 class="text-center">@lang('messages.noProducts')</h3>
            @endif
        </div>
    </div>
</div>

<div class="promo-title-center">
    <div class="container">
        @lang('messages.all4you')
    </div>
</div>

<div class="promo">
    <div class="container">
        <div class="row">
            @if (count($promotion) != 0)
                @foreach ($promotion as $image)
                    @if ($image->size == 0)
                    <a href="{{$locale}}/product/{{$image->product->slug_url}}">
                        <div class="col-md-6">
                            <div class="big-sale">
                                <img src="{{asset($image->photo)}}" alt="" class="img-responsive"/>
                                @if ($image->title != null || $image->title != '')
                                <div class="red-box">
                                    <span class="light" style="color: {{$image->font_color}};">{{$image->content}}</span>
                                    <div class="sale-box" style="color: {{$image->font_color}}; border-color: {{$image->font_color}};">
                                        <span class="extra-bold">{{$image->title}}</span>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                    </a>
                    @endif
                <!-- @if ($image->size == 1)
                <a href="{{$locale}}/product/{{$image->product->slug_url}}">

                    <div class="col-md-3 h-174 mg-bot-20">
                        <div class="small-img">
                            <img src="{{asset($image->photo)}}" alt="" class="img-responsive"/>
                            <div class="default-box">
                                <span class="text-white light">{{$image->content}}</span>
                                <div class="text-white" style="border: 3px solid #f3f3f3; text-align: center; font-size: 30px; font-weight: 100px; margin-top: 15px">
                                    <span class="extra-bold">{{$image->title}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>

                @endif -->
                @endforeach
                @if (count($allPromotion) > 2) 
                    <div class="col-md-12 text-center mg-top-20">
                        <a href="{{ route ('show.promotion', $locale) }}" class="f-16"><u>Show More</u></a>
                    </div>
                @endif
            @else
            <h3 class="text-center">@lang('messages.noPromo')</h3>
            @endif
        </div>
    </div>
</div>

<div class="promo-title-center">
    <div class="container">
        @lang('messages.featuredBrands')
    </div>
</div>

<div class="featured-brands">
    <div class="container">
        <div class="row">
            @if(count($brands) != 0)
            <div class="owl-carousel owl-theme">
                @foreach($brands as $brand)
                <img src="{{$brand->feature_url}}" alt="" class="img-responsive" />
                @endforeach
            </div>
            @endif
            @if (count($brands) == 0)
            <h3 class="text-center">@lang('messages.noBrands')</h3>
            @endif
        </div>
    </div>
</div>

</section>
<script type="text/javascript">
    $(document).ready(function(){
        $('.owl-carousel').owlCarousel({
            loop:true,
            margin:10,
            responsiveClass:true,
            responsive:{
                0:{
                    items:2,
                    nav:true
                },
                600:{
                    items:3,
                    nav:false
                },
                1000:{
                    items:5,
                    nav:true,
                    loop:false,
                    mouseDrag  : false
                }
            }
        })
    });

</script>
@endsection
