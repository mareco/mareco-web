@extends('layouts.frontend-dashboard')
@section('content')
<section class="content">
    <svg width="0" height="0" class="svg-clip">
      <clipPath id="polygon-tag">
          <polygon points="-1 119,103 75,150 -1,0 0">
          </polygon>
      </clipPath>
  </svg>

  <svg width="0" height="0" class="svg-clip">
      <clipPath id="polygon-tag-mobile">
          <polygon points="-1 119,50 75,80 -1,0 0">
          </polygon>
      </clipPath>
  </svg>
  @include('includes.breadcrumb')
  <div class="container">
    <div class="product-list">
        <div class="row">
            @if (count($product) != 0)
            @foreach ($product as $item)
            
            <?php
            $variant = App\Http\Controllers\Frontend\ProductController::getVariant($item->id);
            ?>
            <div class="col-md-15 col-xs-6 col-sm-4">
                <div class="product-grid">
                    @if($locale == "en")
                    <a href="/en/product/{{$item->slug_url}}">
                        @if ($item->status == 1)
                        <div class="product-tag">
                            <div class="polygon-tag arrival-tag text-white">
                                <span class="bold">New</span><br>
                                <span class="f-16">Arrival</span>
                            </div>
                        </div>
                        @endif
                        @if ($item->status == 2)
                        <div class="product-tag">
                            <div class="polygon-tag sale-tag text-white">
                                <span class="bold">Now</span><br>
                                <span class="f-16">Sale</span>
                            </div>
                        </div>
                        @endif
                        @if ($item->status == 3)
                        <div class="product-tag">
                            <div class="polygon-tag best-selling green text-white">
                                <span class="bold">Best</span><br>
                                <span class="f-16">Selling</span>
                            </div>
                        </div>
                        @endif
                        @if ($item->status == 4)
                        <div class="product-tag">
                            <div class="polygon-tag out-of-stock green text-white">
                                <span class="bold">Sold</span><br>
                                <span class="f-14">Out</span>
                            </div>
                        </div>
                        @endif
                        <div class="product-img">
                            <img src="{{asset($item->product_image)}}" alt="" class="img-responsive rotate{{$item->rotate}}"/>
                        </div>
                    </a>
                    @endif
                    @if($locale == "id")
                    <a href="/id/product/{{$item->slug_url}}">
                        @if ($item->status == 1)
                        <div class="product-tag">
                            <div class="polygon-tag arrival-tag text-white">
                                <span class="bold">Baru</span><br>
                                <span class="f-16"> </span>
                            </div>
                        </div>
                        @endif
                        @if ($item->status == 2)
                        <div class="product-tag">
                            <div class="polygon-tag sale-tag text-white">
                                <span class="bold">Diskon</span><br>
                                <span class="f-16"> </span>
                            </div>
                        </div>
                        @endif
                        @if ($item->status == 3)
                        <div class="product-tag">
                            <div class="polygon-tag best-selling green text-white">
                                <span class="bold">Popular</span><br>
                                <span class="f-16"> </span>
                            </div>
                        </div>
                        @endif
                        @if ($item->status == 4)
                        <div class="product-tag">
                            <div class="polygon-tag out-of-stock green text-white">
                                <span class="bold">Habis</span><br>
                                <span class="f-14"> </span>
                            </div>
                        </div>
                        @endif
                        <div class="product-img">
                            <img src="{{asset($item->product_image)}}" alt="" class="img-responsive rotate{{$item->rotate}}"/>
                        </div>
                    </a>
                    @endif
                    <a href="/product/{{$item->slug_url}}">
                        <div class="product-name">
                           @if($locale == 'id')
                           @if($item->productLang('id'))
                           <span>{{$item->productLang('id')->name}}</span>
                           @else
                           <span>{{$item->name}}</span>
                           @endif
                           @else
                           <span>{{$item->name}}</span>
                           @endif
                       </div>
                       <div class="product-price no-margin">
                        @if ($variant->is_discount == 1)
                        <span class="light price Scratch ">IDR {{number_format($variant->price)}}</span>
                        <span class="extra-bold price">IDR {{number_format($variant->discount_price)}}</span>
                        @else
                        <span class="light price">IDR {{number_format($variant->price)}}</span>
                        @endif
                    </div>
                </a>
            </div>
        </div>
        @endforeach
        @else
        <h3>Currently we don't have a product</h3>
        @endif
    </div>
</div>
</div>

<div class="container">
    <div class="text-center">
        {{ $product->links() }}
            <!-- <ul class="pagination">
                <li class="page-item">
                  <a class="page-link" href="#" aria-label="Previous">
                    <span aria-hidden="true">&lt;</span>
                    <span class="sr-only">Previous</span>
                  </a>
                </li>
                <li class="page-item"><a class="page-link active" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item">
                  <a class="page-link" href="#" aria-label="Next">
                    <span aria-hidden="true">&gt;</span>
                    <span class="sr-only">Next</span>
                  </a>
                </li>
            </ul> -->
        </div>
    </div>
</section>
@endsection
