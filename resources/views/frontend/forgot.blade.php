@extends('layouts.frontend-dashboard')
@section('content')
<section class="content">
	<div class="container">
		<div class="col-md-12">
			<div class="login-page">
				<h3 class="f-30">@lang('messages.forPW')</h3>
				<hr class="modif-hr mg-bot-15">
                @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
                @endif
                
                <form class="forget-form" method="POST" action="{{ url($locale.'/customer/password/email') }}">
                    {{ csrf_field() }}
                    <h3 class="font-green">@lang('messages.forgotpw')</h3>
                    <p> @lang('messages.forgotEmail') </p>
                    <div class="form-group">
                        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" />
                    </div>
                    <div class="form-actions">
                        <button onclick=location.href="{{ url($locale.'/login') }}" type="button" id="back-btn" class="btn btn-default">Back</button>
                        <button type="submit" class="btn btn-success uppercase pull-right mg-top-r">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection
