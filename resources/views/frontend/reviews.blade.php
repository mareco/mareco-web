@extends('layouts.frontend-dashboard')
@section('content')
<section class="content">
    <style type="text/css">
        @import url(//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css);

        fieldset, label { margin: 0; padding: 0; }
        body{ margin: 20px; }
        h1 { font-size: 1.5em; margin: 10px; }

        /****** Style Star Rating Widget *****/

        .rating {
            border: none;
            float: right;
        }

        .rating > input { display: none; }
        .rating > label:before {
            margin: 5px;
            font-size: 1.25em;
            font-family: FontAwesome;
            display: inline-block;
            content: "\f005";
        }

        .rating > .half:before {
            content: "\f089";
            position: absolute;
        }

        .rating > label {
            color: #ddd;
            float: right;
        }

        .rating > input:checked ~ label, /* show gold star when clicked */
        .rating:not(:checked) > label:hover, /* hover current star */
        .rating:not(:checked) > label:hover ~ label { color: #FFD700;  } /* hover previous stars in list */

        .rating > input:checked + label:hover, /* hover current star when changing rating */
        .rating > input:checked ~ label:hover,
        .rating > label:hover ~ input:checked ~ label, /* lighten current selection */
        .rating > input:checked ~ label:hover ~ label { color: #FFED85;  }
    </style>
    @include('includes.breadcrumb')
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-xs-12">
                @if (isset($product))
                <img src="{{asset($product->product_image)}}" alt="" class="img-responsive"/>
                @else
                <img src="{{asset('assets/frontend-image/No-Picture-Available.jpg')}}" alt="" class="img-responsive"/>
                @endif
            </div>
            <div class="col-md-6">
                <div class="sku">
                    <h3>#{{$variant->sku}}</h3>
                </div>
                <div class="product-name">
                    <h1>{{$product->name}}</h1>
                </div>
                <div class="product-price">
                    @if ($variant->is_discount == 1)
                    <span class="light price Scratch ">IDR {{number_format($variant->price)}}</span>
                    <span class="extra-bold price">IDR {{number_format($variant->discount_price)}}</span>
                    @else
                    <span class="light price">IDR {{number_format($variant->price)}}</span>
                    @endif
                </div>
                <div class="product-description-text">
                    {{$product->description}}
                </div>
            </div>
        </div>
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="red text-white review">
                        <span>REVIEW</span>
                    </div>
                </div>
            </div>
            <div class="product-comment border-box small-padding">
                @if (isset($review))
                {!! Form::model($review, ['url' => route('product.reviews.update', ['review'=> $review, $locale]),'method' => 'get','data-parsley-validate' => 'true','enctype'=>'multipart/form-data']) !!}
                @else
                {!! Form::open(['url' => route('product.reviews.save',['product'=>$product, $locale]),'method' => 'POST','data-parsley-validate' => 'true','enctype'=>'multipart/form-data']) !!}
                @endif
                <div class="row">
                    <div class="col-md-2">
                        <div class="review-picture">
                            <img src="{{Auth::user()->photo}}" class="img-responsive">
                        </div>
                    </div>
                    <div class="col-md-7">
                        <h4 class="title-comment bold">{{Auth::user()->first_name.' '.Auth::user()->last_name}}</h4>
                        <p>
                            {{ Form::textarea('remarks', null, array('class'=>'form-group','cols'=>70,'rows'=>5)) }}
                        </p>
                    </div>
                    <div class="col-md-3 text-right">
                        <span class="date">{{Carbon\Carbon::now()->format('Y-m-d')}}</span>
                        <div class="product-review">
                            <fieldset class="rating">
                                <input type="radio" id="star5" name="rate" value="5" /><label class = "full" for="star5" title="Awesome - 5 stars"></label>
                                <input type="radio" id="star4" name="rate" value="4" /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
                                <input type="radio" id="star3" name="rate" value="3" /><label class = "full" for="star3" title="Meh - 3 stars"></label>
                                <input type="radio" id="star2" name="rate" value="2" /><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
                                <input type="radio" id="star1" name="rate" value="1" /><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
                            </fieldset>
                        </div>
                    </div>
                    <button class="btn btn-red text-white pull-right" type="submit" style="margin-right: 18px">Save</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
</section>
@endsection
