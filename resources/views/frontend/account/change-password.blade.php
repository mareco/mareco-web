@extends('layouts.frontend-dashboard')
@section('content')
<section class="content">
	<div class="container">
		<div class="col-md-3 pd-left-0">
	    	<div class="profile-box">
	    		<h3 class="title">@lang('messages.myAcc')</h3>
	    		<hr class="modif-hr">
	            <ul class="sidebar-menu">
	                <li><a href="{{ route('customer.account',$locale) }}">@lang('messages.account')</a></li>
                    <li><a href="{{ route('customer.order', $locale) }}">@lang('messages.order')</a></li>
                    <li><a href="{{ route('customer.address', $locale) }}">@lang('messages.addressBook')</a></li>
                    {{-- <li><a href="/customer/account/detail">Account Details</a></li> --}}
                    <li><a href="{{ route('customer.change.password', $locale) }}"  class="active">@lang('messages.changePW')</a></li>
	            </ul>
	        </div>
    	</div>
    	<div class="col-md-9">
    		<div class="profile-box">
    			<h3 class="title">@lang('messages.changePW')</h3>
    			<hr class="modif-hr">

                @if (Session::has('Success'))
                    <div class="alert alert-success" id="SuccessAlert">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Success! </strong> {{ Session::get('Success') }}
                    </div>
                @endif
                @if (Session::has('Error'))
                    <div class="alert alert-danger" id="errorAlert">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Error! </strong> {{ Session::get('Error') }}
                    </div>
                @endif
                
                {!! Form::open(['url' => route('customer.update.password', ['locale'=>$locale]),'method' => 'PUT','data-parsley-validate' => 'true','enctype'=>'multipart/form-data']) !!}
                    <div class="col-md-12 pd-left-0 pd-right-0 mg-bot-20">
                        <label class="label pd-left-0"><span>@lang('messages.oldPW')</span></label>
                        <input type="password" name="old_password" class="form-control" data-parsley-required = "true">
                    </div>

                    <div class="col-md-12 pd-left-0 pd-right-0 mg-bot-20">
                        <label class="label pd-left-0"><span>@lang('messages.pw')</span></label>
                        <input type="password" name="password" class="form-control" data-parsley-required = "true">
                    </div>
                    <div class="col-md-12 pd-left-0 pd-right-0 mg-bot-20">
                        <label class="label pd-left-0"><span>@lang('messages.conPW')</span></label>
                        <input type="password" name="confirm_password" class="form-control" data-parsley-required = "true">
                    </div>

        			<button class="btn btn-save fl-right f-16">@lang('messages.save')</button>
                {!! Form::close() !!}
    		</div>
    	</div>
    </div>
</section>

<script type="text/javascript">
  setTimeout(function() {
      $('.alert').fadeOut('fast');
  }, 5000);
</script>
@endsection