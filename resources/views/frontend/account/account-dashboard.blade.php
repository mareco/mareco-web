@extends('layouts.frontend-dashboard')
@section('content')
<section class="content">
	<div class="container">
		<div class="col-md-3 pd-left-0">
            <div class="profile-box">
                <h3 class="title">@lang('messages.myAcc')</h3>
                <hr class="modif-hr">
                <ul class="sidebar-menu">
                    <li><a href="{{ route('customer.account',$locale) }}" class="active">@lang('messages.account')</a></li>
                    <li><a href="{{ route('customer.order', $locale) }}">@lang('messages.order')</a></li>
                    <li><a href="{{ route('customer.address', $locale) }}">@lang('messages.addressBook')</a></li>
                    <li><a href="{{ route('customer.change.password', $locale) }}">@lang('messages.changePW')</a></li>
                </ul>
            </div>
        </div>
        <div class="col-md-9">
            <div class="profile-box">
                <h3 class="title">@lang('messages.accInfo')</h3>
                <hr class="modif-hr">
                <ul>
                    <li><span>{{$user->first_name}} {{$user->last_name}}</span></li>
                    <li><span>{{$user->email}}</span></li>
                    <li><a href="account/detail/" class="color-light-blue">@lang('messages.editInfo')</a></li>
                </ul>
            </div>
            <div class="profile-box">
                <h3 class="title">@lang('messages.pointHistory')</h3>
                @if(count($pointHistory) != 0)
              {{--   <table class="table">
                    <thead>
                        <tr>
                            <th style="font-size: 20px;">@lang('messages.code')</th>
                            <th style="font-size: 20px;">@lang('messages.productPoint')</th>
                            <th style="font-size: 20px;">@lang('messages.pointUsage')</th>
                        </tr>
                    </thead>
                    @foreach($pointHistory as $history)
                    @foreach($history->orderDetails as $detail)
                    <tr>
                        <td>
                            {{$detail->product->code}}
                        </td>
                        <td>
                            {{$detail->product->name}}
                        </td>
                        <td>
                            {{$history->point}}
                        </td>
                    </tr> 
                    @endforeach
                    @endforeach
                </table> --}}
                <table class="table">
                    <tbody>
                        @if(isset($pointHistory))
                        @foreach($pointHistory as $point)
                        <tr>
                            <td> <i>{{ $point->created_at->diffForHumans() }}</i> {{ $point->description }} {{$point->amount}} point</td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
                <b>@lang('messages.remainPoint') {{number_format($totalPoint,2,',','.')}}</b><br>
                {{ $pointHistory->links() }}
                @else
                <div class="pd-left-0">
                    <ul>
                        <li><span class="fw-normal">@lang('messages.noTransaction')</span></li>
                    </ul>
                </div>
                @endif
            </div>
            <div class="profile-box">
                <h3 class="title">@lang('messages.addInfo')</h3>
                <hr class="modif-hr">
                <div class="col-md-6 col-xs-12 pd-left-0">
                    @if (count($shipping_address) == 0)
                    <ul>
                        <li><span class="fw-normal">@lang('messages.noAddress')</span></li>
                    </ul>
                    @else
                    <ul>
                        <li><span class="fw-normal">@lang('messages.shipAddress')</span></li>
                        <li><span>{{$shipping_address->user->first_name}} {{$shipping_address->user->last_name}}</span></li>
                        <li><span>{{$shipping_address->address}}</span></li>
                        <li><span>{{$shipping_address->city_name}}, {{$shipping_address->state}}</span></li>
                        <li><span>{{$shipping_address->user->phone}}</span></li>
                        <li><a href="/customer/{{$locale}}/edit/address/{{$shipping_address->id}}" class="color-light-blue">@lang('messages.editAddress')</a></li>
                    </ul>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
@endsection