@extends('layouts.frontend-dashboard')
@section('content')
<section class="content">
	<div class="container">
		<div class="col-md-3 pd-left-0">
          <div class="profile-box">
             <h3 class="title">@lang('messages.myAcc')</h3>
             <hr class="modif-hr">
             <ul class="sidebar-menu">
                <li><a href="{{ route('customer.account',$locale) }}" class="active">@lang('messages.account')</a></li>
                <li><a href="{{ route('customer.order', $locale) }}" >@lang('messages.order')</a></li>
                <li><a href="{{ route('customer.address', $locale) }}">@lang('messages.addressBook')</a></li>
                {{-- <li><a href="/customer/account/detail">Account Details</a></li> --}}
                <li><a href="{{ route('customer.change.password', $locale) }}">@lang('messages.changePW')</a></li>
            </ul>
        </div>
    </div>
    <div class="col-md-9">
      <div class="profile-box">
        @if($locale == 'en')
        <h3 class="title">Account Detail</h3>
        @endif
         @if($locale == 'id')
        <h3 class="title">Detail Akun</h3>
        @endif
        <hr class="modif-hr">

        @if (Session::has('Success'))
        <div class="alert alert-success" id="SuccessAlert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>Success! </strong> {{ Session::get('Success') }}
        </div>
        @endif
        @if (Session::has('Error'))
        <div class="alert alert-danger" id="errorAlert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>Error! </strong> {{ Session::get('Error') }}
        </div>
        @endif

        {!! Form::open(['url' => route('customer.update.account', ['locale'=>$locale]),'method' => 'PUT','data-parsley-validate' => 'true','enctype'=>'multipart/form-data']) !!}
        <div class="col-md-6 pd-left-0 mg-bot-20">
            <label class="label pd-left-0"><span>@lang('messages.firstName')</span></label>
            <input type="text" name="first_name" value="{{$user->first_name}}" class="form-control" data-parsley-required = "true">
        </div>
        <div class="col-md-6 pd-right-0 mg-bot-20">
            <label class="label pd-left-0"><span>@lang('messages.lastName')</span></label>
            <input type="text" name="last_name" value="{{$user->last_name}}" class="form-control" data-parsley-required = "true">
        </div>
        <div class="col-md-6 pd-left-0 mg-bot-20">
            <label class="label pd-left-0"><span>Email</span></label>
            <input type="text" name="email" value="{{$user->email}}" class="form-control" readonly>
        </div>
        <div class="col-md-6 pd-right-0 mg-bot-20">
            <label class="label pd-left-0"><span>@lang('messages.phone')</span></label>
            <input type="number" name="phone" value="{{$user->phone}}" class="form-control" data-parsley-required = "true">
        </div>
                    <!-- <div class="col-md-12 pd-left-0 pd-right-0 mg-bot-20">
                        <label class="label pd-left-0"><span>Password</span></label>
                        <input type="text" name="password" class="form-control">
                    </div>
                    <div class="col-md-12 pd-left-0 pd-right-0 mg-bot-20">
                        <label class="label pd-left-0"><span>Confirm Password</span></label>
                        <input type="text" name="confirm_password" class="form-control">
                    </div> -->
                    <div class="col-md-12 pd-right-0 pd-left-0 mg-bot-20">
                        <label class="label pd-left-0"><span>Photo</span></label>
                        <input type="file" name="photo" class="form-control">
                    </div>
                    <button class="btn btn-save fl-right f-20">Save</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>

    <script type="text/javascript">
      setTimeout(function() {
          $('.alert').fadeOut('fast');
      }, 5000);
  </script>
  @endsection