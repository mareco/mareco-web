@extends('layouts.frontend-dashboard')
@section('content')
<section class="content">
	<div class="container">
		<div class="col-md-3 pd-left-0">
	    	<div class="profile-box">
	    		<h3 class="title">My Account</h3>
	    		<hr class="modif-hr">
	            <ul class="sidebar-menu">
	               <li><a href="{{ route('customer.account',$locale) }}">@lang('messages.account')</a></li>
                    <li><a href="{{ route('customer.order', $locale) }}">@lang('messages.order')</a></li>
                    <li><a href="{{ route('customer.address', $locale) }}" class="active">@lang('messages.addressBook')</a></li>
                    {{-- <li><a href="/customer/account/detail">Account Details</a></li> --}}
                    <li><a href="{{ route('customer.change.password', $locale) }}">@lang('messages.changePW')</a></li>
	            </ul>
	        </div>
    	</div>
    	<div class="col-md-9 pd-0-mobile">
    		<div class="profile-box">
    			<h3 class="title">@lang('messages.createAddress')</h3>
    			<hr class="modif-hr">
                @if (Session::has('Success'))
                    <div class="alert alert-success" id="SuccessAlert">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Success! </strong> {{ Session::get('Success') }}
                    </div>
                @endif
                @if (Session::has('Error'))
                    <div class="alert alert-danger" id="errorAlert">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Error! </strong> {{ Session::get('Error') }}
                    </div>
                @endif


                @if (count($address) == 0)
                    {!! Form::open(['url' => route('customer.save.address', $locale),'method' => 'POST','data-parsley-validate' => 'true','enctype'=>'multipart/form-data']) !!}

                        <div class="col-md-12 pd-left-0 pd-right-0 mg-bot-20">
                            <label class="label pd-left-0"><span>@lang('messages.address')</span></label>
                            <textarea rows="3" name="address" class="form-control mg-bot-15" data-parsley-required = "true"></textarea>
                        </div>
                        <div class="col-md-6 pd-left-0 mg-bot-20">
                            <label class="label pd-left-0"><span>@lang('messages.province')</span></label>
                            <select class="form-control" name="province" id="province" data-parsley-required = "true">
                                <option></option>
                                @foreach ($provinces as $value)
                                    <option value="<?php echo $value['province_id'] ?>"><?php echo $value['province']  ?></option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6 pd-right-0 mg-bot-20">
                            <label class="label pd-left-0"><span>@lang('messages.city')</span></label>
                            <select class="form-control" name="city" id="city" data-parsley-required = "true">
                                <option></option>
                            </select>
                        </div>
                        <div class="col-md-6 pd-left-0 mg-bot-20">
                            <label class="label pd-left-0"><span>@lang('messages.district')</span></label>
                            <select class="form-control" name="district" id="district" data-parsley-required = "true">
                                <option></option>
                            </select>
                        </div>
                        <div class="col-md-6 pd-right-0 mg-bot-20">
                            <label class="label pd-left-0"><span>@lang('messages.postal')</span></label>
                            <input type="text" name="postal_code" class="form-control" data-parsley-required = "true">
                        </div>
                        <div class="col-md-12 pd-left-0 mg-bot-20">
                            <label class="width-full"><input type="radio" name="is_shipping_address" class="height-inherit" value="true"> 
                                <span class="f-20">@lang('messages.defaultAddress')</span>
                            </label>
                            <label class="width-full"><input type="radio" name="is_shipping_address" class="height-inherit" value="false" data-parsley-required = "true">
                                <span class="f-20">@lang('messages.altAddress')</span>
                            </label>
                        </div>

                        <button class="btn btn-save w-inherit fl-right f-20">Save</button>
                    {!! Form::close() !!}
                @else
                    {!! Form::open(['url' => route('customer.update.address', $locale),'method' => 'PUT','data-parsley-validate' => 'true','enctype'=>'multipart/form-data']) !!}

            			<div class="col-md-12 pd-left-0 pd-right-0 mg-bot-20">
            				<label class="label pd-left-0"><span>Street Address</span></label>
            				<textarea rows="3" name="address" class="form-control mg-bot-15" data-parsley-required = "true" value="{{$address->address}}">{{$address->address}}</textarea>
            			</div>
            			<div class="col-md-6 pd-left-0 mg-bot-20">
            				<label class="label pd-left-0"><span>Province</span></label>
            				<select class="form-control" name="province" id="province" data-parsley-required = "true">
                                <option></option>
                               @foreach ($provinces as $value)
                                    <option value="<?php echo $value['province_id'] ?>" {{$value['province_id'] == $address->province ? 'selected="selected"' : ''}}><?php echo $value['province']  ?></option>
                                @endforeach
                            </select>
            			</div>
                        <div class="col-md-6 pd-right-0 mg-bot-20">
                            <label class="label pd-left-0"><span>City</span></label>
                            <select class="form-control" name="city" id="city" data-parsley-required = "true">
                                @foreach ($city as $value)
                                    <option value="<?php echo $value['city_id'] ?>" {{$value['city_id'] == $address->city ? 'selected="selected"' : ''}}><?php echo $value['city_name']  ?></option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6 pd-left-0 mg-bot-20">
                            <label class="label pd-left-0"><span>District</span></label>
                            <select class="form-control" name="district" id="district" data-parsley-required = "true">
                                @foreach ($district as $value)
                                    <option value="<?php echo $value['subdistrict_id'] ?>" {{$value['subdistrict_id'] == $address->district ? 'selected="selected"' : ''}}><?php echo $value['subdistrict_name']  ?></option>
                                @endforeach
                            </select>
                        </div>
            			<div class="col-md-6 pd-right-0 mg-bot-20">
            				<label class="label pd-left-0"><span>ZIP / Postal Code</span></label>
            				<input type="text" name="postal_code" class="form-control" data-parsley-required = "true" value="{{$address->postal_code}}">
            			</div>
                        <div class="col-md-12 pd-left-0 mg-bot-20 ">
                            <label class="width-full"><input type="radio" name="is_shipping_address" class="height-inherit" value="true"> 
                                <span class="f-20">Use as default shipping address</span>
                            </label>
                            <label class="width-full"><input type="radio" name="is_shipping_address" class="height-inherit" value="false" data-parsley-required = "true">
                                <span class="f-20">Additional Address</span>
                            </label>
                        </div>

                        <input type="hidden" name="id" value="{{$id}}">
            			<button class="btn btn-save w-inherit fl-right f-20">Update</button>
                    {!! Form::close() !!}
                @endif

    		</div>
    	</div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function() {
        $('#province').change(function($event) {
            province_id = $($event.currentTarget)[0].value;
            return getDataCity($event);
        });

        $('#city').change(function($event) {
            city_id = $($event.currentTarget)[0].value;
            return getDataDistrict($event);
        });

        var getDataCity = function() {
            $.ajax({
                headers: {
                    'X-CSRF-Token': $('input[name="_token"]').val()
                },
                url: "{{ route('customer.address.city') }}",
                method: "GET",
                data: { province_id : province_id},
                success: function (data) {
                    var select = document.getElementById('city');
                    var selectDistrict = document.getElementById('district');
                    select.options.length = 0;
                    selectDistrict.options.length = 0;
                    options = data.city;
                    select.options.add(new Option(""));
                    for (var i = 0; i < data.count; i++) {
                      option = options[i];
                      select.options.add(new Option(option.city_name, option.city_id));
                    }
                }
            });
        };

        var getDataDistrict = function() {
            $.ajax({
                headers: {
                    'X-CSRF-Token': $('input[name="_token"]').val()
                },
                url: "{{ route('customer.address.district') }}",
                method: "GET",
                data: { city_id : city_id},
                success: function (data) {
                    var select = document.getElementById('district');
                    select.options.length = 0;
                    options = data.district;
                    select.options.add(new Option(""));
                    for (var i = 0; i < data.count; i++) {
                      option = options[i];
                      select.options.add(new Option(option.subdistrict_name, option.subdistrict_id));
                    }
                }
            });
        };
    });
</script>
@endsection
