<?php
use App\Helpers\Enums\OrderStatus;
use App\Helpers\Enums\OrderStatusLang;
?>

@extends('layouts.frontend-dashboard')
@section('content')
<section class="content">
	<div class="container">
		<div class="col-md-3 pd-left-0">
            <div class="profile-box">
                <h3 class="title">@lang('messages.myAcc')</h3>
                <hr class="modif-hr">
                <ul class="sidebar-menu">
                   <li><a href="{{ route('customer.account',$locale) }}">@lang('messages.account')</a></li>
                   <li><a href="{{ route('customer.order', $locale) }}" class="active">@lang('messages.order')</a></li>
                   <li><a href="{{ route('customer.address', $locale) }}">@lang('messages.addressBook')</a></li>
                   {{-- <li><a href="/customer/account/detail">Account Details</a></li> --}}
                   <li><a href="{{ route('customer.change.password', $locale) }}">@lang('messages.changePW')</a></li>
               </ul>
           </div>
       </div>
       <div class="col-md-9 pd-left-0">
        <div class="profile-box">
            <h3 class="title">@lang('messages.myOrder')</h3>
            <hr class="modif-hr">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <th>#@lang('messages.order')</th>
                        <th>@lang('messages.orderDate')</th>
                        <th class="width-25">@lang('messages.shipTo')</th>
                        <th>@lang('messages.orderTotal')</th>
                        <th>Status</th>
                    </thead>
                    <tbody>
                        @if (count($order) != 0)
                        @foreach ($order as $value)
                        <tr>
                            <td>#{{$value->order_number}}</td>
                            <td>{{$value->created_at->format('d/m/Y')}}</td>
                            @if ($value->address_id == null)
                            <td>{{$value->cod_address}}</td>
                            @else
                            <td>{{$value->address->address}}</td>
                            @endif
                            <td>IDR {{number_format($value->grand_total)}}</td>
                            @if($locale == "en")
                            <td>{{OrderStatus::getString($value->status)}}</td>
                            @else
                            <td>{{OrderStatusLang::getString($value->status)}}</td>
                            @endif
                            <td>
                                <a href="/customer/{{$locale}}/order/confirmation/{{$value->id}}" class="icon-eye"><i class="fa fa-eye"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        {{ $order->links() }}
            <!-- <ul class="pagination">
                <li class="page-item">
                  <a class="page-link" href="#" aria-label="Previous">
                    <i class="fa fa-angle-double-left f-14" aria-hidden="true"></i>
                  </a>
                </li>
                <li class="page-item">
                  <a class="page-link pd-left-0" href="#" aria-label="Previous">
                    <i class="fa fa-angle-left f-14" aria-hidden="true"></i>
                  </a>
                </li>
                <li class="page-item"><a class="page-link active" href="#">1</a></li>
                <li class="page-item">
                  <a class="page-link" href="#" aria-label="Next">
                    <i class="fa fa-angle-right f-14" aria-hidden="true"></i>
                  </a>
                </li>
                <li class="page-item">
                  <a class="page-link pd-left-0" href="#" aria-label="Next">
                    <i class="fa fa-angle-double-right f-14" aria-hidden="true"></i>
                  </a>
                </li>
            </ul> -->
        </div>
    </div>
</section>
@endsection
