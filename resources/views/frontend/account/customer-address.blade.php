@extends('layouts.frontend-dashboard')
@section('content')
<section class="content">
	<div class="container">
		<div class="col-md-3 pd-left-0">
	    	<div class="profile-box">
	    		<h3 class="title">@lang('messages.myAcc')</h3>
	    		<hr class="modif-hr">
	            <ul class="sidebar-menu">
	                <li><a href="{{ route('customer.account',$locale) }}">@lang('messages.account')</a></li>
	                <li><a href="{{ route('customer.order', $locale) }}">@lang('messages.order')</a></li>
	                <li><a href="{{ route('customer.address', $locale) }}" class="active">@lang('messages.addressBook')</a></li>
	                {{-- <li><a href="/customer/account/detail">Account Details</a></li> --}}
                    <li><a href="{{ route('customer.change.password', $locale) }}">@lang('messages.changePW')</a></li>
	            </ul>
	        </div>
    	</div>
    	<div class="col-md-9 pd-left-0">
    		<div class="profile-box">
    			<h3 class="title inline-box">@lang('messages.address')</h3>
				<a href="/customer/{{$locale}}/create/address" class="btn btn-save btn-create-address">@lang('messages.createNewAdd')</a>

    			<hr class="modif-hr mg-top-10">

    			@if (Session::has('Success'))
                    <div class="alert alert-success" id="SuccessAlert">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Success! </strong> {{ Session::get('Success') }}
                    </div>
                @endif
                @if (Session::has('Error'))
                    <div class="alert alert-danger" id="errorAlert">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Error! </strong> {{ Session::get('Error') }}
                    </div>
                @endif
    			<div class="col-md-12 col-xs-12 pd-left-0">
    				@if (count($shipping_address) == 0)
	    				<ul>
		    				<li><span class="fw-normal">@lang('messages.noAddress')</span></li>
		    			</ul>
    				@else
		    			<ul>
		    				<li><span class="fw-normal">@lang('messages.shipAddress')</span></li>
		    				<li><span>{{$shipping_address->user->first_name}} {{$shipping_address->user->last_name}}</span></li>
		    				<li><span>{{$shipping_address->address}}</span></li>
		    				<li><span>{{$shipping_address->city_name}}, {{$shipping_address->state}}</span></li>
		    				<li><span>{{$shipping_address->user->phone}}</span></li>
		    				<li><a href="/customer/{{$locale}}/edit/address/{{$shipping_address->id}}" class="color-light-blue">Change Shipping Address</a></li>
		    			</ul>
    				@endif
    			</div>
    			<br><br>

    			@if (count($additional_address) > 0)
	    			<h3>@lang('messages.address') @lang('messages.additional')</h3>
	    			<hr class="modif-hr">

	    			@foreach ($additional_address as $value)
	    				<div class="col-md-6 col-xs-12 pd-left-0">
			    			<ul>
			    				<li><span class="fw-normal">@lang('messages.shipAddress')</span></li>
			    				<li><span>{{$value->user->first_name}} {{$value->user->last_name}}</span></li>
			    				<li><span>{{$value->address}}</span></li>
			    				<li><span>{{$value->city_name}}, {{$value->state}}</span></li>
			    				<li><span>{{$value->user->phone}}</span></li>
			    				<li><a href="/customer/{{$locale}}/edit/address/{{$value->id}}" class="color-light-blue">@lang('messages.change') @lang('messages.address')</a></li>
			    				<li><a class="color-gray cursor-pointer" data-toggle="modal" data-target="#confirm-delete">@lang('messages.delAdd')</a></li>
			    			</ul>
		    			</div>

		    			<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						    <div class="modal-dialog">
						        <div class="modal-content">
						            <div class="modal-header f-20">
						                Are you sure want to delete this address ?
						            </div>
						            <div class="modal-footer">
						                <a type="button" class="btn btn-default" data-dismiss="modal">Cancel</a>
						                <a href="/customer/{{$locale}}/delete/address/{{$value->id}}" class="btn btn-danger btn-ok">Delete</a>
						            </div>
						        </div>
						    </div>
						</div>
	    			@endforeach
    			@endif
    		</div>
    	</div>
    </div>
</section>
@endsection
