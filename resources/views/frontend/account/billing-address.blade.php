@extends('layouts.frontend-dashboard')
@section('content')
<section class="content">
	<div class="container">
		<div class="col-md-3 pd-left-0">
	    	<div class="profile-box">
	    		<h3 class="title">My Account</h3>
	    		<hr class="modif-hr">
	            <ul class="sidebar-menu">
	                <li><a href="/customer/account">Account</a></li>
                    <li><a href="/customer/order/history">Orders</a></li>
                    <li><a href="/customer/address" class="active">Address Book</a></li>
                    {{-- <li><a href="/customer/account/detail">Account Details</a></li> --}}
                    <li><a href="/customer/change/password">Change Password</a></li>
	            </ul>
	        </div>
    	</div>
    	<div class="col-md-9">
    		<div class="profile-box">
    			<h3 class="title">Address</h3>
    			<hr class="modif-hr">
    			<p>Billing Address</p><br>
                
    			<div class="col-md-12 pd-left-0 pd-right-0 mg-bot-20">
    				<label class="label pd-left-0"><span>Street Address</span></label>
    				<input type="text" name="" class="form-control mg-bot-15">
    				<input type="text" name="" class="form-control">
    			</div>
    			<div class="col-md-6 pd-left-0 mg-bot-20">
    				<label class="label pd-left-0"><span>City</span></label>
    				<select class="form-control"></select>
    			</div>
    			<div class="col-md-6 pd-right-0 mg-bot-20">
    				<label class="label pd-left-0"><span>State / Province</span></label>
    				<select class="form-control"></select>
    			</div>
    			<div class="col-md-6 pd-left-0 mg-bot-20">
    				<label class="label pd-left-0"><span>ZIP / Postal Code</span></label>
    				<input type="text" name="" class="form-control">
    			</div>
    			<div class="col-md-6 pd-right-0 mg-bot-20">
    				<label class="label pd-left-0"><span>Phone Number</span></label>
    				<input type="text" name="" class="form-control">
    			</div>

    			<button class="btn btn-save btn-red fl-right f-20">Save</button>
    		</div>
    	</div>
    </div>
</section>
@endsection
