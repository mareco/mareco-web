<?php
use App\Helpers\Enums\OrderStatus;
use App\Helpers\Enums\PaymentType;
use App\Helpers\Enums\TypeShipmentHelper;
?>

@extends('layouts.frontend-dashboard')
@section('content')
<section class="content">
    <div class="container">
        @include('includes.modal_confirmation_order')
        <div class="col-md-3 pd-left-0">
            <div class="profile-box">
                <h3 class="title">@lang('messages.myAcc')</h3>
                <hr class="modif-hr">
                <ul class="sidebar-menu">
                 <li><a href="{{ route('customer.account',$locale) }}">@lang('messages.account')</a></li>
                 <li><a href="{{ route('customer.order', $locale) }}" class="active">@lang('messages.order')</a></li>
                 <li><a href="{{ route('customer.address', $locale) }}">@lang('messages.addressBook')</a></li>
                 {{-- <li><a href="/customer/account/detail">Account Details</a></li> --}}
                 <li><a href="{{ route('customer.change.password', $locale) }}">@lang('messages.changePW')</a></li>
             </ul>
         </div>
     </div>
     <div class="col-md-9 pd-left-0">
        <div class="profile-box">
            <h3 class="title">@lang('messages.myOrder')</h3>
            <hr class="modif-hr">
            <h2><b>@lang('messages.order') # {{$order->order_number}}</b></h2>
            {{Carbon\Carbon::parse($order->created_at)->format('d M y')}}
            @if($order->status == OrderStatus::WAITING_FOR_PAYMENT)
            @if ($order->payment_type == 1)
            <b class="mg-l-20">{{OrderStatus::getString($order->status)}}</b>
            <button type="button" data-toggle="modal" data-target="#modalConfirmOrder" class="btn btn-xs btn-info fl-right" style="margin-left: 20px">
                @lang('messages.confPayment')
            </button>
            @endif

            @if ($order->payment_type == 0)
            <b class="mg-l-20">{{OrderStatus::getString($order->status)}}</b>
            <a href="{{ route('paypal.double.check', [$order,$locale]) }}" class="btn btn-xs btn-info fl-right" style="margin-left: 20px">
               Go to Payment
           </a>
           @endif

           @if ($order->payment_type == 2)
           <b class="mg-l-20">Waiting</b>
           @endif
           @elseif($order->status == OrderStatus::ON_VERIFICATION)
                    <!-- <button class="btn btn-xs btn-gold no-action" style="margin-left: 20px">
                        {{OrderStatus::getString(OrderStatus::ON_VERIFICATION)}}
                    </button> -->
                    <b class="mg-l-20">{{OrderStatus::getString(OrderStatus::ON_VERIFICATION)}}</b>
                    @elseif($order->status == OrderStatus::PACKING)
                    <b class="mg-l-20">{{OrderStatus::getString(OrderStatus::PACKING)}}</b>
                    <!-- <button class="btn btn-xs btn-gold no-action" style="margin-left: 20px">
                        {{OrderStatus::getString(OrderStatus::PACKING)}}
                    </button> -->
                    @elseif($order->status == OrderStatus::ON_SHIPPING)
                    <b class="mg-l-20">{{OrderStatus::getString(OrderStatus::ON_SHIPPING)}}</b>
                    <!-- button class="btn btn-xs btn-primary no-action" style="margin-left: 20px">
                        {{OrderStatus::getString(OrderStatus::ON_SHIPPING)}}
                    </button> -->
                    @elseif($order->status == OrderStatus::DELIVERED)
                    <b class="mg-l-20">{{OrderStatus::getString(OrderStatus::DELIVERED)}}</b>
                    <!-- <button class="btn btn-xs btn-success no-action" style="margin-left: 20px">
                        {{OrderStatus::getString(OrderStatus::DELIVERED)}}
                    </button> -->
                    @elseif($order->status == OrderStatus::CANCEL)
                    <b class="mg-l-20">{{OrderStatus::getString(OrderStatus::CANCEL)}}</b>
                    <!-- <button class="btn btn-xs btn-danger no-action" style="margin-left: 20px">
                        {{OrderStatus::getString(OrderStatus::CANCEL)}}
                    </button> -->
                    @elseif($order->status == OrderStatus::COMPLETED)
                    <b class="mg-l-20">{{OrderStatus::getString(OrderStatus::COMPLETED)}}</b>
                    <!-- <button class="btn btn-xs btn-success no-action" style="margin-left: 20px">
                        {{OrderStatus::getString(OrderStatus::COMPLETED)}}
                    </button> -->
                    @endif
                    <hr class="modif-hr">
                    <div class="row">
                        <div class="col-md-4 col-xs-4">
                            <h5><b>@lang('messages.shipAddress')</b></h5>
                            <div>{{$order->user->first_name.' '.$order->user->last_name}}</div>
                            @if ($address != null)
                            <div>{{$order->address->address}}</div>
                            <div>{{$address['province']}}</div>
                            <div>{{$address['district']}}</div>
                            <div>{{$address['city']}}, {{$order->address->postal_code}}</div>
                            @else
                            <div>{{$order->cod_address}}</div>
                            @endif
                        </div>
                        <div class="col-md-4 col-xs-4">
                            <h5><b>@lang('messages.shipMethod')</b></h5>
                            @if ($address != null)
                            <div>{{TypeShipmentHelper::getString($order->shipment_type)}}</div>
                            @endif
                            <div>{{PaymentType::getString($order->payment_type)}}</div>
                        </div>
                        <div class="col-md-4 col-xs-4">
                            <h5><b>@lang('messages.trackNo')</b></h5>
                            @if($order->shipment)
                            <div>{{$order->shipment->shipping_date}}</div>
                            <div>{{$order->shipment->tracking_number}}</div>
                            @else
                            <div>Not available yet</div>
                            @endif
                        </div>
                        @if($order->additional_note)
                        <div class="col-md-12 col-xs-12 mg-top-20">
                            <div>Additional note : {{ $order->additional_note }}</div>
                        </div>
                        @endif
                    </div>
                    <hr class="modif-hr">
                    <div>
                    <!-- <div class="row">
                        <div class="col-md-1"><b>SKU</b></div>
                        <div class="col-md-3"><b>Product</b></div>
                        <div class="col-md-1"><b>Size</b></div>
                        <div class="col-md-1"><b>Color</b></div>
                        <div class="col-md-1"><b>Qty</b></div>
                        <div class="col-md-1"><b>Weight</b></div>
                        <div class="col-md-2"><b>Price</b></div>
                        <div class="col-md-2"><b>Subtotal</b></div>
                    </div>
                    <hr class="modif-hr">
                    @foreach($order->orderDetails as $value)
                    <div class="row">
                        <div class="col-md-1">{{$value->sku}}</div>
                        <div class="col-md-3">{{$value->product->name}}</div>
                        <div class="col-md-1">{{$value->size}}</div>
                        <div class="col-md-1"><div style="background: {{$value->color}}; color: {{$value->color}}">. </div></div>
                        <div class="col-md-1">{{$value->qty}}</div>
                        <div class="col-md-1">{{$value->weight}}</div>
                        <div class="col-md-2">IDR {{number_format($value->price,2,'.',',')}}</div>
                        <div class="col-md-2">IDR {{number_format($value->amount,2,'.',',')}}</div>
                    </div>
                    @endforeach -->
                    <div class="table-responsive">
                        <table class="table order-details-confirmation">
                            <thead>
                                <tr>
                                    <th>Photo</th>
                                    <th>SKU</th>
                                    <th>@lang('messages.productPoint')</th>
                                    <th>@lang('messages.size')</th>
                                    <th>@lang('messages.color')</th>
                                    <th>@lang('messages.qty')</th>
                                    <th>@lang('messages.weight')</th>
                                    <th>@lang('messages.price')</th>
                                    <th>Subtotal</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($order->orderDetails as $value)
                                <tr>
                                    <td><img src="{{ asset($value->product->product_image) }}" class="img-responsive rotate{{$value->product->rotate}}" style="width: 80px; height: 80px;" /></td>
                                    <td>{{$value->sku}}</td>
                                    <td>{{$value->product->name}}</td>
                                    <td>{{$value->size}}</td>
                                    <td><div style="background: {{$value->color}}; color: {{$value->color}}">. </div></td>
                                    <td>{{$value->qty}}</td>
                                    <td>{{$value->weight}}</td>
                                    <td>IDR {{number_format($value->price,2,'.',',')}}</td>
                                    <td>IDR {{number_format($value->amount,2,'.',',')}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <br><br>
                <div class="row">
                    <div class="col-md-6"></div>
                    <div class="col-md-6">
                        <!-- <div class="col-md-12"><h5>Order Totals</h5></div>
                        <div class="col-md-8">Total</div>
                        <div class="col-md-4">IDR {{number_format($order->orderDetails->sum('amount'), 2,'.',',')}}</div>
                        <div class="col-md-8">Point</div>
                        <div class="col-md-4">IDR  {{number_format($order->point, 2,'.',',')}}
                        </div>
                        <div class="col-md-8">Shipment Fee</div>
                        <div class="col-md-4">IDR {{number_format($order->shipment_fee, 2,'.',',')}}</div>
                        <hr>
                        <div class="col-md-8">Grand Total</div>
                        <div class="col-md-4">
                            <b>IDR {{number_format($order->orderDetails->sum('amount')+$order->shipment_fee, 2,'.',',')}}</b>
                        </div> -->
                        <div class="table-responsive">
                            <h3 class="confirmation-order title" style="font-size: 20px !important">@lang('messages.orderTotal')</h3>
                            <table class="table order-details-total">
                                <tr>
                                    <td style="font-size: 15px !important">Total</td>
                                    <td style="font-size: 15px !important" class="light">IDR {{number_format($order->orderDetails->sum('amount'), 2,'.',',')}}</td>
                                </tr>
                                <tr>
                                    <td style="font-size: 15px !important">@lang('messages.point')</td>
                                    <td style="font-size: 15px !important" class="light">IDR  {{number_format($order->point, 2,'.',',')}}</td>
                                </tr>
                                <tr>
                                    <td style="font-size: 15px !important">@lang('messages.shipFee')</td>
                                    <td style="font-size: 15px !important" class="light">IDR {{number_format($order->shipment_fee, 2,'.',',')}}</td>
                                </tr>
                                <tr>
                                    <td>@lang('messages.grandTotal')</td>
                                    <td class="light">IDR {{number_format($order->grand_total, 2,'.',',')}}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
