@extends('layouts.frontend-dashboard')
@section('content')
<section class="content">
    <div class="container">
        {!! Form::open(['url' => '/'.$locale.'/customer/checkout/save', 'method' => 'POST','data-parsley-validate' => 'true']) !!}
        <div class="tab-content">
            <div id="checkout" class="tab-pane fade in active">
                <div class="checkout-title font-20 reguler">
                    Checkout
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="checkout border-box medium-padding font-20 profile-box">
                            <label for="" class="title-label">@lang('messages.personInfo')</label>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="first_name">@lang('messages.firstName') <label class="required">*</label></label>
                                        <input type="text" name="first_name" value="{{$user->first_name}}" class="form-control" id="first_name">
                                        <span id="validateFirstName" style="color:red;">This field is required</span>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="last_name">@lang('messages.lastName')<label class="required">*</label></label>
                                        <input type="text" name="last_name" value="{{$user->last_name}}" class="form-control" id="last_name">
                                        <span id="validateLastName" style="color:red;">This field is required</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email">Email <label class="required">*</label></label>
                                <input type="text" name="email" value="{{$user->email}}" class="form-control" id="email">
                            </div>
                            <div class="form-group">
                                @if($locale == 'en')
                                <label for="phone">Phone Number <label class="required">*</label></label>
                                @endif
                                @if($locale == 'id')
                                <label for="phone">Nomor Telepon <label class="required">*</label></label>
                                @endif
                                <input type="text" name="phone" value="{{$user->phone}}" class="form-control" id="phone">
                                <span id="validatePhone" style="color:red;">This field is required</span>
                            </div>

                            <div id="cod_map" style="display: none;">
                                <div class="form-group">
                                    <input type="text" name="cod_address" class="form-control" id="searchmap" placeholder="Enter a address" autocomplete="off">
                                </div>
                                <div>
                                    <div id="map-canvas" style="height:280px;width:100%;"></div>
                                    <input type="hidden" class="form-control" id="searchmap">
                                    {!! Form::hidden('latitude', '1.0828276', ['class' => 'form-control', 'id' => 'lat']) !!}
                                    {!! Form::hidden('longitude', '104.03045350000002', ['class' => 'form-control', 'id' => 'lng']) !!}
                                </div>
                                <div class="form-group mg-top-20">
                                    <label for="phone">Additional Note (Optional) </label>
                                    <input type="text" name="additional_note" class="form-control" id="additional_note" placeholder="Additional Note">
                                </div>
                            </div>
                            @if (count($additionalAddress) != 0)
                            <div id="additionalAddress">
                                <div class="form-group select-address">
                                    <label for="phone">@lang('messages.select') @lang('messages.address')<label class="required">*</label></label>
                                </div>
                                <div class="profile-box">
                                    @foreach ($additionalAddress as $shipping_address)
                                    @if (count($address) != 0)
                                    <div class="col-md-6 address-box {{$shipping_address->id == $address->id ? 'active' : ''}}" onclick="clickAddressBox({{$shipping_address->city}}, {{$shipping_address->district}}, {{$shipping_address->id}}, event)">
                                        <ul>
                                            <li><span>{{$shipping_address->address}}</span></li>
                                            <li><span>{{$shipping_address->province_name}}, {{$shipping_address->city_name}}, {{$shipping_address->district_name}}</span></li>
                                        </ul>
                                        <div class="hover-btn">
                                            <a href="/customer/{{$locale}}/edit/address/{{$shipping_address->id}}" type="button" class="close-round" data-dismiss="alert">
                                                <i class="fa fa-pencil-square-o color-white" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </div>
                                    @else
                                    <div class="col-md-6 address-box" onclick="clickAddressBox({{$shipping_address->city}}, {{$shipping_address->district}}, {{$shipping_address->id}}, event)">
                                        <ul>
                                            <li><span>{{$shipping_address->address}}</span></li>
                                            <li><span>{{$shipping_address->province_name}}, {{$shipping_address->city_name}}, {{$shipping_address->district_name}}</span></li>
                                        </ul>
                                        <div class="hover-btn">
                                            <a href="/customer/{{$locale}}/edit/address/{{$shipping_address->id}}" type="button" class="close-round" data-dismiss="alert">
                                                <i class="fa fa-pencil-square-o color-white" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </div>
                                    @endif
                                    @endforeach
                                </div>
                            </div>
                            @endif
                            <br>
                            <button type="button" id="InputAddress" class="btn btn-red w-inherit h-45 text-white font-16" onclick="newAddress()">@lang('messages.createNewAdd')</button>

                            <div id="createAddress">
                                <div class="form-group">
                                    <label for="province">@lang('messages.province') <label class="required">*</label></label>
                                    <select class="form-control" name="province" id="province">
                                        <option></option>
                                        @foreach ($provinces as $value)
                                        <option value="{{ $value['province_id'] }}"> {{ $value['province']}}</option> 
                                        @endforeach 
                                    </select>
                                    <span id="validateProvince" style="color:red;">This field is required</span>
                                </div>
                                <div class="form-group">
                                    <label for="city">@lang('messages.city') <label class="required">*</label></label>
                                    <select class="form-control" name="city" id="city">
                                        <option></option>
                                    </select>
                                    <span id="validateCity" style="color:red;">This field is required</span>
                                </div>

                                <div class="form-group">
                                    <label for="district">@lang('messages.district') <label class="required">*</label></label>
                                    <select class="form-control" name="district" id="district">
                                        <option></option>
                                    </select>
                                    <span id="validateDistrict" style="color:red;">This field is required</span>
                                </div>
                                <div class="form-group">
                                    <label for="postal_code">@lang('messages.postal') <label class="required">*</label></label>
                                    <input type="text" name="postal_code" value="" class="form-control" id="postal_code">
                                    <span id="validatePostal" style="color:red;">This field is required</span>
                                </div>
                                <div class="form-group">
                                    <label for="address">@lang('messages.address') <label class="required">*</label></label>
                                    <textarea class="form-control" rows="7" id="comment" name="address" value=""></textarea>
                                    <span id="validateAddress" style="color:red;">This field is required</span>
                                </div>

                                <button type="button" class="btn btn-reset w-inherit h-45 text-white font-16" type="submit" onclick="backToAddress()">@lang('messages.avaiAddress')</button>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="summary border-box medium-padding font-20">
                            <table class="table summary">
                                <thead>
                                  <tr>
                                    <th colspan="2">@lang('messages.summary')</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        Subtotal
                                    </td>
                                    <td>
                                        IDR {{number_format($subtotal)}}
                                    </td>
                                </tr>
                                <tr>
                                    <td id="shipmentHTML">
                                        @lang('messages.shipFee')
                                    </td>
                                    <td id="shipmentFee">
                                        -
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        @lang('messages.point') @lang('messages.discount')
                                    </td>
                                    <td id="pointDiscount">
                                        @if($point != '-')
                                        IDR {{number_format($point)}}
                                            <input type="hidden" name="point" value="{{$point}}">
                                        @else
                                        {{$point}}
                                            <input type="hidden" name="point" value="0">
                                        @endif
                                    </td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    @if($locale == 'en')
                                    <td>
                                        Order Total
                                    </td>
                                    @endif
                                    @if($locale == 'id')
                                    <td>
                                        Total Belanja
                                    </td>
                                    @endif
                                    <td id="orderTotal">
                                        @if ($point != '-')
                                        IDR {{number_format($subtotal - $point)}}
                                        @else
                                        {{number_format($subtotal)}}
                                        @endif
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>

                    <div class="shipping-method border-box medium-padding font-20" id="shippingMethod">
                        <div class="shipping-title">
                            @lang('messages.shipMethod')
                        </div>
                        <div class="shipping-list">
                            <div class="radio">
                                <input type="radio" name="radioShipping" id="radioShipping" value="jne">
                                <label for="radioShipping"><span>JNE</span></label>
                            </div>
                            <div class="radio">
                                <input type="radio" name="radioShipping" id="radioShipping1" value="tiki">
                                <label for="radioShipping1"><span>TIKI</span></label>
                            </div>
                            <div class="radio">
                                <input type="radio" name="radioShipping" id="radioShipping2" value="pos">
                                <label for="radioShipping2"><span>POS INDONESIA</span></label>
                            </div>
                            <div class="radio">
                                <input type="radio" name="radioShipping" id="radioShipping3" value="batam">
                                <label for="radioShipping3"><span>Batam</span></label>
                            </div>
                            <span id="validateShippingMethod" style="color:red;">This field is required</span>
                        </div>
                    </div>

                    <div class="shipping-method border-box medium-padding font-20" id="shippingFee">
                        <div>
                            <div class="jne box-content" id="jneFee">
                                <div class="alert alert-info">Please input all data address first !</div>
                            </div>

                            <div class="tiki box-content" id="tikiFee">
                                <div class="alert alert-info">Please input all data address first !</div>
                            </div>

                            <div class="pos box-content" id="posFee">
                                <div class="alert alert-info">Please input all data address first !</div>
                            </div>
                        </div>
                    </div>

                    <input type="hidden" id="serviceType" name="serviceType">
                    <input type="hidden" id="serviceFee" name="serviceFee">
                    <input type="hidden" id="grandTotal" name="grandTotal">
                    <input type="hidden" id="customer_lat" name="lat">
                    <input type="hidden" id="customer_lng" name="lng">
                    <input type="hidden" id="distance_rate" name="rate">
                    @if ($point != '-')
                    <input type="hidden" value="{{$subtotal - $point}}" id="total">
                    @else
                    <input type="hidden" value="{{$subtotal}}" id="total">
                    @endif
                    @if (count($address) != 0)
                    <input type="hidden" id="shipping_address_id" name="shipping_address_id" value="{{$address->id}}">
                    <input type="hidden" value="{{$address->id}}" name="address_id" id="address_id">
                    <input type="hidden" id="shipping_district_id" value="{{$address->district}}">
                    <input type="hidden" id="shipping_city_id" value="{{$address->city}}">
                    @else
                    <input type="hidden" value="null" name="address_id" id="address_id">
                    <input type="hidden" id="shipping_address_id" name="shipping_address_id" value="">
                    @endif

                    <a type="button" class="btn btn-red btn-checkout mg-bot-15 width-full h-45 text-white font-16 right pd-top-10" id="goToNextStep" href="#paymentMethod" onclick="changeTab('payment')">NEXT</a>

                </div>
            </div>
        </div>

        <div id="paymentMethod" class="tab-pane fade">
            <div class="checkout-title font-20 reguler">
                @lang('messages.payMethod')
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="checkout border-box medium-padding font-20 profile-box">
                        @if (count($bank) != 0)
                        <div class="shipping-list half-width mg-auto">
                            <div class="">
                                <div class="radio">
                                    <input type="radio" name="radioPayment" id="radioPayment" value="bank-transfer">
                                    <label for="radioPayment"><span class="font-20">Bank Transfer</span></label>
                                </div>
                            </div>
                            <div class="bank-transfer box-content">
                                @if (count($bank) != 0)
                                @foreach ($bank as $value)
                                <div class="logo-bank">
                                    @if ($value->logo == null)
                                    <img src="{{ asset('assets/global/image/No-Picture-Available.jpg')}}" width="50" /> 
                                    @else
                                    <img src="{{asset($value->logo)}}" width="50" >
                                    @endif  
                                    <span>{{$value->bank}} / {{$value->account_name}} / {{$value->account_number}}</span>
                                </div>
                                @endforeach 
                                @endif
                                <button type="submit" class="btn btn-red h-45 text-white font-16 mg-top-20">PROCEED</button>
                            </div>
                        </div>
                        <hr>
                        @endif
                        <div class="shipping-list half-width mg-auto">
                            <div class="">
                                <div class="radio">
                                    <input type="radio" name="radioPayment" id="radioPayment1" value="paypal">
                                    <label for="radioPayment1"><span class="font-20">PayPal Express Checkout</span></label>
                                </div>
                            </div>
                            <div class="paypal box-content">
                                <h5>@lang('messages.directToPaypal')</h5>
                                <button type="submit" class="btn btn-red h-45 text-white font-16">@lang('messages.continuePaypal')</button>
                            </div>
                        </div>
                        <hr>
                        <div class="shipping-list half-width mg-auto">    
                            <div class="">
                                <div class="radio">
                                    <input type="radio" name="radioPayment" id="radioPayment2" value="COD">
                                    <label for="radioPayment2"><span class="font-20">Cash On Delivery</span></label>
                                </div>
                            </div>
                            <div class="COD box-content">
                                <button type="submit" class="btn btn-red h-45 text-white font-16 mg-top-20">PROCEED</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="summary border-box medium-padding font-20">
                        <table class="table summary">
                            <thead>
                              <tr>
                                <th colspan="2">@lang('messages.summary')</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    Subtotal
                                </td>
                                <td>
                                    IDR {{number_format($subtotal)}}
                                </td>
                            </tr>
                            <tr>
                                <td id="shipmentHTML2">
                                    @lang('messages.shipFee')
                                </td>
                                <td id="shipmentFee2">
                                    -
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    @lang('messages.point') @lang('messages.discount')
                                </td>
                                <td id="pointDiscount2">
                                    @if($point != '-')
                                    IDR {{number_format($point)}}
                                    <input type="hidden" name="point" value="{{$point}}">
                                    @else
                                    {{$point}}
                                    <input type="hidden" name="point" value="0">
                                    @endif
                                </td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td>
                                    @lang('messages.orderTotal')
                                </td>
                                <td id="orderTotal2">
                                    @if ($point != '-')
                                    IDR {{number_format($subtotal - $point)}}
                                    @else
                                    {{number_format($subtotal)}}
                                    @endif
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>  
    <!-- <a class="btn btn-reset mg-bot-15 h-45 font-16 fl-l pd-top-10 mg-top-11" href="/checkout">BACK</a> -->
</div>   
{!! Form::close() !!}
</div>

<div id="myModal" class="modal fade modal-map" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">SELECT YOUR LOCATION</h4>
    </div>
    <div class="modal-body">
        <p>Some text in the modal.</p>
    </div>
    <div class="modal-footer">
        <button class="btn btn-red btn-checkout width-full text-white right">SUBMIT</button>
    </div>
</div>
</div>
</div>
</section>
{{-- <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCSlAK_5cMtJS9jDISX0WIG3VDnWpQiUjk&libraries=places"></script> --}}
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCyB6K1CFUQ1RwVJ-nyXxd6W0rfiIBe12Q&libraries=places" type="text/javascript"></script>

<script type="text/javascript">

    @include('includes.js.checkout');

    $(document).ready(function() {
      $(window).keydown(function(event){
        if(event.keyCode == 13) {
          event.preventDefault();
          return false;
        }
      });
    });

    $('#radioShipping3').click(function(){
        $('#additionalAddress').hide();
        $('#cod_map').show();
        $('#InputAddress').hide();
        initialize();
    });
    $('#radioShipping2').click(function(){
        $('#additionalAddress').show();
        $('#cod_map').hide();
        // $('#InputAddress').show();
    });
    $('#radioShipping1').click(function(){
        $('#additionalAddress').show();
        $('#cod_map').hide();
        // $('#InputAddress').show();
    });
    $('#radioShipping').click(function(){
        $('#additionalAddress').show();
        $('#cod_map').hide();
        // $('#InputAddress').show();
    });

    $('#createAddress').hide();
    $('#shippingFee').hide();
    $('#validateFirstName').hide();
    $('#validateLastName').hide();
    $('#validatePhone').hide();
    $('#validateProvince').hide();
    // $('#validateCountry').hide();
    $('#validateCity').hide();
    $('#validateDistrict').hide();
    $('#validatePostal').hide();
    $('#validateAddress').hide();
    $('#validateShippingMethod').hide();

    function resetShipmentMethod() {
        $('#selectJNE').remove();
        $('#selectPOS').remove();
        $('#selectTIKI').remove();
    }

    function showAlert() {
        document.getElementById('jneFee').innerHTML = '<div class="alert alert-info">Please input all data address first !</div>';
        document.getElementById('posFee').innerHTML = '<div class="alert alert-info">Please input all data address first !</div>';
        document.getElementById('tikiFee').innerHTML = '<div class="alert alert-info">Please input all data address first !</div>';
    }

    function changeTab(title) {
        var first_name = document.getElementById('first_name').value;
        var last_name = document.getElementById('last_name').value;
        var phone = document.getElementById('phone').value;
        var postal_code = document.getElementById('postal_code').value;
        var comment = document.getElementById('comment').value;
        var province = $('#province')[0].value;
        var city = $('#city')[0].value;
        var district = $('#district')[0].value;
        // var country = $('#country')[0].value;
        var serviceType = $('#serviceType')[0].value;
        if (document.getElementById('searchmap').value != '') {
            $('#validateShippingMethod').hide();
            window.location.hash = title;
            $('#goToNextStep').tab('show');
            var shipmentFee = document.getElementById('shipmentFee').innerHTML;
            var pointDiscount = document.getElementById('pointDiscount').innerHTML;
            var orderTotal = document.getElementById('orderTotal').innerHTML;
            document.getElementById('shipmentFee2').innerHTML = shipmentFee;
            document.getElementById('pointDiscount2').innerHTML = pointDiscount;
            document.getElementById('orderTotal2').innerHTML = orderTotal;
            document.getElementById('shipmentHTML2').innerHTML = 'Amount Distance';
            // $('#radioPayment').attr('disabled', true);
            // $('#radioPayment1').attr('disabled', true);
        } else {
            if (document.getElementById('shipping_address_id').value == null) {
                if (first_name == '') {
                    $('#validateFirstName').show();
                } else {
                    $('#validateFirstName').hide();
                }
                if (last_name == '') {
                    $('#validateLastName').show();
                } else {
                    $('#validateLastName').hide();
                }
                if (phone == '') {
                    $('#validatePhone').show();
                } else {
                    $('#validatePhone').hide();
                }
                if (postal_code == '') {
                    $('#validatePostal').show();
                } else {
                    $('#validatePostal').hide();
                }
                if (comment == '') {
                    $('#validateAddress').show();
                } else {
                    $('#validateAddress').hide();
                }
                if (province == '') {
                    $('#validateProvince').show();
                } else {
                    $('#validateProvince').hide();
                }
                if (city == '') {
                    $('#validateCity').show();
                } else {
                    $('#validateCity').hide();
                }
                if (district == '') {
                    $('#validateDistrict').show();
                } else {
                    $('#validateDistrict').hide();
                }
                if (serviceType == '') {
                    $('#validateShippingMethod').show();
                } else {
                    $('#validateShippingMethod').hide();
                }
                if (first_name != '' && last_name != '' && phone != '' && postal_code != '' && comment != '' && province != '' && city != '' &&  district != '' &&serviceType != '') {
                    window.location.hash = title;
                    $('#goToNextStep').tab('show');
                    var shipmentFee = document.getElementById('shipmentFee').innerHTML;
                    var pointDiscount = document.getElementById('pointDiscount').innerHTML;
                    var orderTotal = document.getElementById('orderTotal').innerHTML;
                    document.getElementById('shipmentFee2').innerHTML = shipmentFee;
                    document.getElementById('pointDiscount2').innerHTML = pointDiscount;
                    document.getElementById('orderTotal2').innerHTML = orderTotal;
                    $('#radioPayment2').attr('disabled', true);
                }
            } else {
                if (serviceType == '') {
                    $('#validateShippingMethod').show();
                } else {
                    $('#validateShippingMethod').hide();
                }

                if (serviceType != '') {
                    window.location.hash = title;
                    $('#goToNextStep').tab('show');
                    var shipmentFee = document.getElementById('shipmentFee').innerHTML;
                    var pointDiscount = document.getElementById('pointDiscount').innerHTML;
                    var orderTotal = document.getElementById('orderTotal').innerHTML;
                    document.getElementById('shipmentFee2').innerHTML = shipmentFee;
                    document.getElementById('pointDiscount2').innerHTML = pointDiscount;
                    document.getElementById('orderTotal2').innerHTML = orderTotal;
                    $('#radioPayment2').attr('disabled', true);
                }
            }
        }
    }

    function newAddress() {
        $('#createAddress').show();
        // $('#additionalAddress').hide();
        $('#InputAddress').hide();
        $('.hide-label').hide();
        $('#cod_map').hide();

        $('.address-box').removeClass("active");
        document.getElementById('shipping_address_id').value = '';
    }

    function backToAddress() {
        $('.hide-label').show();
        $('#createAddress').hide();
        $('#additionalAddress').show();
        $('#InputAddress').show();
    }

    function clickAddressBox(city, district, id, event) {
        $('.address-box').removeClass('active');
        if (event.srcElement.offsetParent.className != 'close-round') {
            event.srcElement.offsetParent.className = 'col-md-6 address-box active';
        }

        document.getElementById('shipping_address_id').value = id;
        city_id = city;
        district_id = district;
        resetShipmentMethod();

        return getDataCost();
    }

    function checkService() {
        serviceJNE = JSON.parse(localStorage.getItem('serviceJNE'));
        shipmentFeeJNE = JSON.parse(localStorage.getItem('shipmentFeeJNE'));
        servicePOS = JSON.parse(localStorage.getItem('servicePOS'));
        shipmentFeePOS = JSON.parse(localStorage.getItem('shipmentFeePOS'));
        serviceTIKI = JSON.parse(localStorage.getItem('serviceTIKI'));
        shipmentFeeTIKI = JSON.parse(localStorage.getItem('shipmentFeeTIKI'));
        if ($('#selectJNE')[0] != undefined) {
            if ($('#selectJNE')[0].value != "Select a service") {
                for (var i = 0; i < serviceJNE.length; i++) {
                    if (serviceJNE[i] == $('#selectJNE')[0].value) {
                        service = serviceJNE[i];
                        shipmentFee = shipmentFeeJNE[i];
                    }
                }
            }
        }
        if ($('#selectPOS')[0] != undefined) {
         if ($('#selectPOS')[0].value != "Select a service") {
            for (var i = 0; i < servicePOS.length; i++) {
                if (servicePOS[i] == $('#selectPOS')[0].value) {
                    service = servicePOS[i];
                    shipmentFee = shipmentFeePOS[i];
                }
            }
        }
    }
    if ($('#selectTIKI')[0] != undefined) {
     if ($('#selectTIKI')[0].value != "Select a service") {
        for (var i = 0; i < serviceTIKI.length; i++) {
            if (serviceTIKI[i] == $('#selectTIKI')[0].value) {
                service = serviceTIKI[i];
                shipmentFee = shipmentFeeTIKI[i];
            }
        }
    }
}
document.getElementById('serviceType').value = service;
document.getElementById('serviceFee').value = shipmentFee;
var total = document.getElementById('total').value;
var grandTotal = parseInt(total) + parseInt(shipmentFee);
console.log(total,grandTotal);
document.getElementById('grandTotal').value = grandTotal; 
document.getElementById('shipmentFee').innerHTML = 'IDR ' + shipmentFee.toLocaleString('en-US');
document.getElementById('orderTotal').innerHTML = 'IDR ' + grandTotal.toLocaleString('en-US');
}

$(document).ready(function() {
    initialize();

    if (document.getElementById('shipping_address_id').value != '') {
        district_id = document.getElementById('shipping_district_id').value;
        city_id = document.getElementById('shipping_city_id').value;
        console.log(district_id, city_id);
            // country_id = $('#country')[0].value;
            getDataCost();
        }
    });

$('#paymentMethod .shipping-list input[type="radio"]').click(function(){
    var inputValue = $(this).attr("value");
    var targetBox = $("." + inputValue);
    $(".box-content").not(targetBox).hide();
    $(targetBox).show();
});

$('#shippingMethod .shipping-list input[type="radio"]').click(function(){
    $('#shippingFee').show();
    var inputValue = $(this).attr("value");
    if (inputValue == 'batam') {
        backToAddress();
        $('#additionalAddress').hide();
        $('#shippingFee').hide();
        var jne = document.getElementById('selectJNE');
        var pos = document.getElementById('selectPOS');
        var tiki = document.getElementById('selectTIKI');
        if (pos != null) {
            pos.selectedIndex = 0;
        }
        if (tiki != null) {
            tiki.selectedIndex = 0;
        }
        if (jne != null) {
            jne.selectedIndex = 0;
        }
    }
    var targetBox = $("." + inputValue);
    $(".box-content").not(targetBox).hide();
    $(targetBox).show();
    if (inputValue = 'jne') {
        var pos = document.getElementById('selectPOS');
        var tiki = document.getElementById('selectTIKI');
        if (pos != null) {
            pos.selectedIndex = 0;
        }
        if (tiki != null) {
            tiki.selectedIndex = 0;
        }
    }
    if (inputValue = 'pos') {
        var jne = document.getElementById('selectJNE');
        var tiki = document.getElementById('selectTIKI');
        if (jne != null) {
            jne.selectedIndex = 0;
        }
        if (tiki != null) {
            tiki.selectedIndex = 0;
        }
    }
    if (inputValue = 'tiki') {
        var pos = document.getElementById('selectPOS');
        var jne = document.getElementById('selectJNE');
        if (pos != null) {
            pos.selectedIndex = 0;
        }
        if (jne != null) {
            jne.selectedIndex = 0;
        }
    }
});

$('#province').change(function($event) {
            // var selectCountry = document.getElementById('country');
            // selectCountry.selectedIndex = 0;
            province_id = $($event.currentTarget)[0].value; 
            resetShipmentMethod();
            showAlert();
            return getDataCity($event);
        });

$('#city').change(function($event) {
            // var selectCountry = document.getElementById('country');
            // selectCountry.selectedIndex = 0;
            city_id = $($event.currentTarget)[0].value; 
            resetShipmentMethod();
            showAlert();
            return getDataDistrict($event);
        });

$('#district').change(function($event) {
    district_id = $($event.currentTarget)[0].value;
    city_id = $('#city')[0].value;
            // var selectCountry = document.getElementById('country');
            // selectCountry.selectedIndex = 0;
            resetShipmentMethod();
            return getDataCost();
            // showAlert();
        });

$('#country').change(function($event) {
    country_id = $($event.currentTarget)[0].value;
    district_id = $('#district')[0].value;
    city_id = $('#city')[0].value;
    resetShipmentMethod();
    return getDataCost();
});

var getDataCity = function() {
    $.ajax({
        headers: {
            'X-CSRF-Token': $('input[name="_token"]').val()
        },
        url: "{{ route('customer.address.city') }}",
        method: "GET",
        data: { province_id : province_id},
        success: function (data) {
            var select = document.getElementById('city');
            var selectDistrict = document.getElementById('district');
            select.options.length = 0;
            selectDistrict.options.length = 0;
            options = data.city;
            select.options.add(new Option(""));
            for (var i = 0; i < data.count; i++) {
              option = options[i];
              select.options.add(new Option(option.city_name, option.city_id));
          }
      }
  });
};

var getDataDistrict = function() {
    $.ajax({
        headers: {
            'X-CSRF-Token': $('input[name="_token"]').val()
        },
        url: "{{ route('customer.address.district') }}",
        method: "GET",
        data: { city_id : city_id},
        success: function (data) {
            var select = document.getElementById('district');
            select.options.length = 0;
            options = data.district;
            select.options.add(new Option(""));
            for (var i = 0; i < data.count; i++) {
              option = options[i];
              select.options.add(new Option(option.subdistrict_name, option.subdistrict_id));
          }
      }
  });
};

function getDataCost() {
    $.ajax({
        headers: {
            'X-CSRF-Token': $('input[name="_token"]').val()
        },
        url: "{{ route('customer.orders.get.cost') }}",
        method: "GET",
        data: { district_id : district_id,
            city_id : city_id},
            success: function (data) {
                $('#jneFee').empty();
                $('#posFee').empty();
                $('#tikiFee').empty();
                    // if (data.countJNE == 0 && data.countPOS == 0 && data.countTIKI == 0) {
                    //     document.getElementById('#messageCost').innerHTML = '<div class="alert alert-warning">We do not have shipment for this place</div>';
                    //     $('#checkShipment').hide();
                    // }
                    if (data.countJNE != 0) {
                        var optionJNE = '';
                        var dataServiceJNE = [];
                        var feeServiceJNE = [];
                        localStorage.removeItem('serviceJNE');
                        localStorage.removeItem('shipmentFeeJNE');
                        for (var i = 0; i < data.countJNE; i++) {
                            if (data.costJNE[0].costs[i] != undefined) {
                                var service = data.costJNE[0].costs[i].service;
                                var etd = data.costJNE[0].costs[i].cost[0].etd;
                                var price = data.costJNE[0].costs[i].cost[0].value;
                                if (etd != "") {
                                    dataServiceJNE.push(service);
                                    feeServiceJNE.push(price);
                                    optionJNE += '<option value=' + service + '>' + service + ' (Estimate : ' + etd + ') (Price : ' + price.toLocaleString('en-US') + ')' + '</option>'; 
                                }
                            }  
                        }
                        localStorage.setItem('serviceJNE',JSON.stringify(dataServiceJNE));
                        localStorage.setItem('shipmentFeeJNE',JSON.stringify(feeServiceJNE));
                        if (dataServiceJNE.length > 0) {
                            $('#jneFee').append('<select class="form-control" id="selectJNE" onchange="checkService()">' + '<option hidden>Select a service</option>' + optionJNE + '</select>');
                        } else {
                            $('#jneFee').append('<div class="alert alert-info">Do not have a shipment for this place !</div>');
                        }
                    } else {
                        console.log("EF");
                        $('#jneFee').append('<div class="alert alert-info">Do not have a shipment for this place !</div>');
                    }

                    if (data.countPOS != 0) {
                        var optionPOS = '';
                        var dataServicePOS = [];
                        var feeServicePOS = [];
                        localStorage.removeItem('servicePOS');
                        localStorage.removeItem('shipmentFeePOS');
                        for (var i = 0; i < data.countPOS; i++) {
                            if (data.costPOS[0].costs[i] != undefined) {
                                var service = data.costPOS[0].costs[i].service;
                                var etd = data.costPOS[0].costs[i].cost[0].etd;
                                var price = data.costPOS[0].costs[i].cost[0].value;
                                if (etd != "") {
                                    if (service != "Express Sameday Dokumen" && service != "Surat Kilat Khusus" && service != "Express Next Day Dokumen") {
                                        dataServicePOS.push(service);
                                        feeServicePOS.push(price);
                                        optionPOS += '<option value=' + JSON.stringify(service) + '>' + service + ' (Estimate : ' + etd + ') (Price : ' + price.toLocaleString('en-US') + ')' + '</option>';
                                    }
                                }
                            }
                        }
                        localStorage.setItem('servicePOS',JSON.stringify(dataServicePOS));
                        localStorage.setItem('shipmentFeePOS',JSON.stringify(feeServicePOS));
                        if (dataServicePOS.length > 0) {
                            $('#posFee').append('<select class="form-control" id="selectPOS" onchange="checkService()">' + '<option hidden>Select a service</option>' + optionPOS + '</select>');
                        } else {
                            $('#posFee').append('<div class="alert alert-info">Do not have a shipment for this place !</div>');
                        }
                    } else {
                        $('#posFee').append('<div class="alert alert-info">Do not have a shipment for this place !</div>');
                    }

                    if (data.countTIKI != 0) {
                        var optionTIKI = '';
                        var dataServiceTIKI = [];
                        var feeServiceTIKI = [];
                        localStorage.removeItem('serviceTIKI');
                        localStorage.removeItem('shipmentFeeTIKI');
                        for (var i = 0; i < data.countTIKI; i++) {
                            if (data.costTIKI[0].costs[i] != undefined) {
                                var service = data.costTIKI[0].costs[i].service;
                                var etd = data.costTIKI[0].costs[i].cost[0].etd;
                                var price = data.costTIKI[0].costs[i].cost[0].value;
                                if (etd != "") {
                                    dataServiceTIKI.push(service);
                                    feeServiceTIKI.push(price);
                                    optionTIKI += '<option value=' + service + '>' + service + ' (Estimate : ' + etd + ') (Price : ' + price.toLocaleString('en-US') + ')' + '</option>';
                                }
                            }
                        }
                        localStorage.setItem('serviceTIKI',JSON.stringify(dataServiceTIKI));
                        localStorage.setItem('shipmentFeeTIKI',JSON.stringify(feeServiceTIKI));
                        if (dataServiceTIKI.length > 0) {
                            $('#tikiFee').append('<select class="form-control" id="selectTIKI" onchange="checkService()">' + '<option hidden>Select a service</option>' + optionTIKI + '</select>');
                        } else {
                            $('#tikiFee').append('<div class="alert alert-info">Do not have a shipment for this place !</div>');
                        }
                    } else {
                        $('#tikiFee').append('<div class="alert alert-info">Do not have a shipment for this place !</div>');
                    }
                }
            });
}
    // });
</script>
@endsection
