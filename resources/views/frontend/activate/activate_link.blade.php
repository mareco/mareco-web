<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style>
		input{
		    width: 100%;
		    padding: 12px 20px;
		    margin: 8px 0;
		    display: inline-block;
		    border: 1px solid #ccc;
		    border-radius: 4px;
		    box-sizing: border-box;
		}

		button{
		    width: 100%;
		    background-color: #4CAF50;
		    color: white;
		    padding: 14px 20px;
		    margin: 8px 0;
		    border: none;
		    border-radius: 4px;
		    cursor: pointer;
		}

		button:hover {
		    background-color: #45a049;
		}

		div {
		    border-radius: 5px;
		    background-color: #f2f2f2;
		    padding: 20px;
		}
	</style>
</head>
<body>
	<p>Hi, {{$user->first_name}}  {{$user->last_name}}</p>

	<p>Please activate your user account from here. Here is your activation code : <b>{{$user->activate_code}}</b></p>

	<div>
		{!! Form::open(['url' => route('user.activate'),'method' => 'PUT']) !!}
	    	<label for="activate_code">Activation Code</label>
	    	<input type="text" name="activate_code">
	  		<input type="hidden" name="email" value="{{$user->email}}">
	  		<input type="hidden" name="locale" value="{{$locale}}">
	  		{{ csrf_field() }}
		    <button type="submit">Submit</button>
		{!! Form::close() !!}
	</div>
	<p>Regards, Mareco Store</p>
</body>
</html>