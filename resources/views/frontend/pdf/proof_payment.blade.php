<html>
<head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title></title>
</head>
<body>
    <p>Your has received payment from {{$first_name}} {{$last_name}} with order {{$order->order_number}}, you can check the confirmation in <a href="{{url('customer/order/confirmation'.$order->id)}}">here</a></p>
    <p>If you have questions about your order, you can email us at {{$email}}.</p>
    <p>Thanks,</p>
    <p>Mareco Store</p>
</body>
</html>
