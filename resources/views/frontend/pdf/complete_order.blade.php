<html>
<head>
  <meta name="viewport" content="width=device-width" />
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title></title>
<style>
  .clearfix:after {
  content: "";
  display: table;
  clear: both;
}

a {
  color: #5D6975;
  text-decoration: underline;
}

body {
  position: relative;
  width: 21cm;  
  height: 29.7cm; 
  margin: 0 auto; 
  color: #001028;
  background: #FFFFFF; 
  font-family: Arial, sans-serif; 
  font-size: 12px; 
  font-family: Arial;
}

header {
  padding: 10px 0;
  margin-bottom: 30px;
}

#logo {
  text-align: center;
  margin-bottom: 10px;
}

h1 {
  border-top: 1px solid  #5D6975;
  border-bottom: 1px solid  #5D6975;
  color: #5D6975;
  font-size: 2.4em;
  line-height: 1.4em;
  font-weight: normal;
  text-align: center;
  margin: 0 0 20px 0;
  background: url(dimension.png);
}

#project {
  float: left;
}

#project span {
  color: #5D6975;
  text-align: right;
  width: 52px;
  margin-right: 10px;
  display: inline-block;
  font-size: 0.8em;
}

#company {
  float: right;
  text-align: right;
}

#project div,
#company div {
  white-space: nowrap;        
}

table {
  width: 100%;
  border-collapse: collapse;
  border-spacing: 0;
  margin-bottom: 20px;
}

table tr:nth-child(2n-1) td {
  background: #F5F5F5;
}

table th,
table td {
  text-align: center;
}

table th {
  padding: 5px 20px;
  color: #5D6975;
  border-bottom: 1px solid #C1CED9;
  white-space: nowrap;        
  font-weight: normal;
}

table .service,
table .desc {
  text-align: left;
}

table td {
  padding: 20px;
  text-align: right;
}

table td.service,
table td.desc {
  vertical-align: top;
}

table td.unit,
table td.qty,
table td.total {
  font-size: 1.2em;
}

table td.grand {
  border-top: 1px solid #5D6975;;
}

#notices .notice {
  color: #5D6975;
  font-size: 1.2em;
}

footer {
  color: #5D6975;
  width: 100%;
  height: 30px;
  position: absolute;
  bottom: 0;
  border-top: 1px solid #C1CED9;
  padding: 8px 0;
  text-align: center;
}
.line-height-30 {
  line-height: 30px;
}
.f-18 {
  font-size: 18px;
}
.shipping-f {
  font-size: 14px;
  line-height: 20px;
}
.bd-top {
  border-top: 1px solid #c1ced9;
}
.mg-bot-75 {
  margin-bottom: 75px;
}
</style>
</head>
<body>
<header class="clearfix">
  <div id="logo">
    <img src="http://marecostore.com/assets/frontend-image/logo-red.png" width="400">
  </div>
  <h1>Order Number {{$order->order_number}}</h1>
  <h4>Your order has been completed, If you have questions about your order, you can email us at {{$email}}.</h4>
  @if ($order->payment_type == 2)
  <div id="company" class="clearfix">
    <div></div>
    <div><br /></div>
    <div></div>
    <div></div>
  </div>
  @endif
  <div id="">
    <div class="line-height-30"><span class="f-18">Shipping To </span></div>
    <div class="shipping-f">{{$user->first_name}} {{$user->last_name}}</div>
    <div class="shipping-f">{{$address->address}}</div>
    <div class="shipping-f">{{$address->district_name}}</div>
    <div class="shipping-f">{{$address->city_name}}, {{$address->postal_code}}</div>
    <div class="shipping-f">{{$address->state}}</div>
    <div class="shipping-f">T: {{$user->phone}}</div>
  </div>
</header>
<main>
  <table>
    <thead>
      <tr>
        <th class="service">No</th>
        <th class="desc">PRODUCT NAME</th>
        <th>QTY</th>
        <th>PRICE</th>
      </tr>
    </thead>
    <tbody>
    @foreach ($orderDetail as $index => $value)
    <tr>
      <td class="service">{{$index + 1}}</td>
      <td class="desc">{{$value->product->name}}</td>
      <td class="qty" style="text-align: center;">{{$value->qty}}</td>
      <td class="total" style="text-align: center;">IDR {{number_format($value->price * $value->qty)}}</td>
    </tr>
    @endforeach
      <tr>
        <td colspan="3" class="bd-top">SUBTOTAL</td>
        <td class="total bd-top" style="text-align: center;">IDR {{number_format($subTotal)}}</td>
      </tr>
      <tr>
        <td colspan="3">SHIPMENT FEE</td>
        <td class="total" style="text-align: center;">IDR {{number_format($order->shipment_fee)}}</td>
      </tr>
      <tr>
        <td colspan="3">POINT</td>
        @if ($order->point != 0 && $order->point != null)
          <td class="total" style="text-align: center;">{{number_format($order->point)}}</td>
        @else
          <td class="total" style="text-align: center;">0</td>
        @endif
      </tr>
      <tr>
        <td colspan="3" class="grand total">GRAND TOTAL</td>
        <td class="grand total" style="text-align: center;">IDR {{number_format($order->grand_total)}}</td>
      </tr>
    </tbody>
  </table>
</main>
<footer>
  Mareco Store, © 2016 Prometics. All rights reserved.
</footer>
</body>
</html>