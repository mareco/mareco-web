@extends('layouts.master')
@section('content')
<div class="login-container"> 
    <div class="login-header login-caret"> 
        <div class="login-content"> 
            <a href="#" class="logo"> 
                <img src="{{ asset('assets/frontend-image/about-us-red.png') }}" width="280" alt="" /> 
            </a> 
            <p class="description">Dear user, log in to access the admin area!</p>
            <div class="login-progressbar-indicator"> 
                <h3>43%</h3> 
                <span>logging in...</span> 
            </div> 
        </div>
    </div> 
    <div class="login-progressbar">
        <div></div> 
    </div> 
    <div class="login-form"> 
        <div class="login-content"> 
            <div class="form-login-error">
                <h3>Invalid login</h3>
                <p>Enter
                    <strong>demo</strong>/<strong>demo</strong> as login and password.
                </p> 
            </div> 
            <form class="form-horizontal" method="POST" action="{{ route('admin.login') }}">
                {{ csrf_field() }}
                <div class="form-group"> 
                    <div class="input-group {{ $errors->has('email') ? ' has-error' : '' }}"> 
                        <div class="input-group-addon"> 
                            <i class="entypo-user"></i> 
                        </div> 
                        <input type="email" class="form-control" name="email" id="email" placeholder="Email" value="{{ old('email') }}" required autofocus/> 
                        @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                </div> 
                <div class="form-group"> 
                    <div class="input-group {{ $errors->has('password') ? ' has-error' : '' }}"> 
                        <div class="input-group-addon"> 
                            <i class="entypo-key"></i> 
                        </div> 
                        <input type="password" class="form-control" name="password" id="password" placeholder="Password" autocomplete="off" required /> 
                        @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div> 
                </div> 
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block btn-login"> <i class="entypo-login"></i>
                        Login In
                    </button> 
                </div> 
                {{-- <div class="form-group">
                    <em>- or -</em> 
                </div> 
                <div class="form-group"> 
                    <button type="button" class="btn btn-default btn-lg btn-block btn-icon icon-left facebook-button">
                        Login with Facebook
                        <i class="entypo-facebook"></i> 
                    </button> 
                </div>  --}}
            </form> 
            <div class="login-bottom-links"> 
                <a href="{{ route('admin.password.request') }}" class="link">Forgot your password?</a>
                <br /> 
                <a href="#">Term of Service</a> - <a href="#">Privacy Policy</a> 
            </div> 
        </div> 
    </div> 
</div> 
@endsection
