@extends('layouts.master')
@section('content')
<div class="login-container">
    <div class="login-header login-caret">
        <div class="login-content">
            <a href="#" class="logo">
                <img src="{{ asset('assets/images/logo-mareco.png') }}" width="280" alt="" /> 
            </a>
            <p class="description">Enter your email, and we will send the reset link.</p>
            <div class="login-progressbar-indicator">
                <h3>43%</h3> <span>logging in...</span> 
            </div>
        </div>
    </div>
    <div class="login-progressbar">
        <div></div>
    </div>
    <div class="login-form">
        <div class="login-content">
             <form class="forget-form" role="form" method="POST" action="{{ route('admin.password.email') }}">
                {{ csrf_field() }}
                <div class="form-forgotpassword-success"> <i class="entypo-check"></i>
                    <h3>Reset email has been sent.</h3>
                    <p>Please check your email, reset password link will expire in 24 hours.</p>
                </div>
                <div class="form-steps">
                    <div class="step current" id="step-1">
                        <div class="form-group">
                            <div class="input-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                <div class="input-group-addon"> <i class="entypo-mail"></i></div>
                                <input type="email" class="form-control" name="email" id="email" placeholder="Email" autocomplete="off" value="{{ $email or old('email') }}" required autofocus/>

                                @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-info btn-block btn-login">
                                Send Email
                                <i class="entypo-right-open-mini"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </form>
            <div class="login-bottom-links">
                <a href="/admin/login" class="link"> <i class="entypo-lock"></i> Return to Login Page
                </a>
                <br /> <a href="#">ToS</a> - <a href="#">Privacy Policy</a>
            </div>
        </div>
    </div>
</div>
@endsection
