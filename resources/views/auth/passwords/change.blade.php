@extends('layouts.dashboard')
@extends('layouts.dashboard_side_bar')
@include('layouts.dashboard_header_right')
@section('content')
@include('includes.page_header_form')
{!! Form::model($user, ['url' => route('admin.password.change.update'),'method' => 'PUT', 'enctype'=>"multipart/form-data"]) !!}
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="{{ URL::route('admin.dashboard') }}" class="btn btn-sm btn-primary pull-right"><span class="fa fa-reply"></span> Back</a>
                </div>
                <h4>Change Password</h4>
            </div>
            
            <div class="panel-body">
                <div class="modal-body">
                    <fieldset>
                        {{ csrf_field() }}
                        <legend>Change Password</legend>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    @if(isset($user->photo) && $user->photo != '')
                                    <img src="{{($user->photo) }}" style="width: 145px; height: 145px">
                                    @else
                                    {{ Form::label('photo', 'Photo')}}
                                    <img src="{{ asset('assets/images/nopic.png') }}" style="width: 145px; height: 145px">
                                    @endif
                                    {{ Form::file('photo', null, array('class' => 'form-control'))}}
                                </div>
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                    {{ Form::label('email', 'E-mail')}}
                                    {{ Form::text('email', $user->email, array('class' => 'form-control', 'placeholder' => 'Email', 'readonly' => 'readonly', 'maxlength'=> '250'))}}
                                    @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
                                <div class="form-group {{ $errors->has('old_password') ? ' has-error' : '' }}">
                                    {{ Form::label('old_password', 'Current Password')}}
                                    {{ Form::password('old_password', array('class' => 'form-control', 'placeholder' => 'Current Password', 'required' => 'required', 'maxlength'=> '250'))}}
                                    @if ($errors->has('old_password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('old_password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                    {{ Form::label('password', 'New Password')}}
                                    {{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'Password', 'required' => 'required', 'maxlength'=> '250'))}}
                                    @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                    {{ Form::label('password_confirmation', 'Re-type Password')}}
                                    {{ Form::password('password_confirmation', array('class' => 'form-control', 'placeholder' => 'Password Confirmation', 'required' => 'required', 'maxlength'=> '250'))}}
                                    @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                    </fieldset>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="submit" class="btn btn-sm btn-success"><span class="fa fa-save"></span> Save</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endsection