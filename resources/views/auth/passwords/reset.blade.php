@extends('layouts.master')
@section('content')
<div class="login-container"> 
    <div class="login-header login-caret"> 
        <div class="login-content"> 
            <a href="#" class="logo"> 
                <img src="{{ asset('assets/images/logo-mareco.png') }}" width="280" alt="" /> 
            </a> 
            <p class="description">Reset Password - Please complete the form below!</p>
        </div>
    </div> 
    <div class="login-form"> 
        <div class="login-content"> 
            <form class="form-horizontal" role="form" method="POST" action="{{ route('admin.password.request') }}">
                {{ csrf_field() }}
                <input type="hidden" name="token" value="{{ $token }}">

                <div class="form-group"> 
                    <div class="input-group {{ $errors->has('email') ? ' has-error' : '' }}"> 
                        <div class="input-group-addon"> 
                            <i class="entypo-user"></i> 
                        </div>
                        <input type="email" class="form-control" name="email" id="email" placeholder="Email" value="{{ old('email') }}" required autofocus/> 
                        @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                </div> 
                <div class="form-group"> 
                    <div class="input-group {{ $errors->has('password') ? ' has-error' : '' }}"> 
                        <div class="input-group-addon"> 
                            <i class="entypo-key"></i> 
                        </div>
                        <input type="password" class="form-control" name="password" id="password" placeholder="Password" autocomplete="off" required /> 
                        @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div> 
                </div>
                <div class="form-group"> 
                    <div class="input-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}"> 
                        <div class="input-group-addon"> 
                            <i class="entypo-key"></i> 
                        </div>
                        <input id="password-confirm" type="password" class="form-control" placeholder="Confirm Password" name="password_confirmation" required>

                        @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                        @endif
                    </div> 
                </div> 
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block btn-login"> <i class="entypo-login"></i>
                    Reset Password
                    </button> 
                </div> 
            </form> 
{{--             <div class="login-bottom-links"> 
                <a href="{{ route('password.request') }}" class="link">Forgot your password?</a>
                <br /> 
                <a href="#">Term of Service</a> - <a href="#">Privacy Policy</a> 
            </div>  --}}
        </div> 
    </div> 
</div>
@endsection
