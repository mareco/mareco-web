<section class="top-header">
    <div class="container">
        <div class="top-header-wrap">
            <div class="row">
                <div class="col-md-12">
                    <ul class="nav navbar-nav languages">
                        <li class="dropdown languages-switcher">
                            @if($locale == "en")
                            <a class="dropdown-toggle" data-toggle="dropdown" href="/en">EN <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="/id">ID</a></li>
                            </ul>
                            @endif
                            @if($locale == "id")
                            <a class="dropdown-toggle" data-toggle="dropdown" href="/id">ID <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="/en">EN</a></li>
                            </ul>
                            @endif
                        </li>
                    </ul>
                    <ul class="nav navbar-nav top-nav">
                        @if (Auth::user())
                        <li><a href="{{ route('customer.logout', $locale) }}">@lang('messages.logout')</a></li>
                        @else
                        <li><a href="{{route('customer.login.form', $locale)}}">@lang('messages.singin')</a></li>
                        <li><a href="{{route('customer.register.form', $locale)}}">@lang('messages.createAccount')</a></li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="nav-header">
  <?php
  $category = App\Http\Controllers\Frontend\CategoryController::getCategory();
  ?>
  <nav class="navbar navbar-default">
    <div class="container">
      <div class="navbar-header">
        <a class="navbar-brand" href="{{ route('customer.homepage',$locale) }}">
          <div class="store-name">
            <img src="{{asset('assets/frontend-image/logo-red.png')}}" alt="" class="img-responsive logo" />
        </div>
    </a>
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
  </button>
</div>
<div class="collapse navbar-collapse" id="myNavbar">
    <ul class="nav navbar-nav navbar-right">
      <li class="{{$homepage or ''}}"><a href="{{ route('customer.homepage',$locale) }}">@lang('messages.home')</a></li>
      <li class="{{$shop or ''}}"><a href="{{ route('product',$locale)}}">@lang('messages.shop')</a></li>
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#" onclick="changeIcon()">@lang('messages.category')
          <i id="icon-category" class="fa fa-angle-down f-20" aria-hidden="true"></i></a>
          <ul class="dropdown-menu">
            <div class="container">
              <li>
                <ul class="list-unstyled three-columns col-sm-12 category-list">
                  @foreach ($category as $value)
                  <li class="col-md-4 mg-bot-15">
                    <img src="{{asset($value->photo)}}" width="35" height="35">
                    <a class="" href="{{ url("/$locale/product?category=$value->slug_url") }}">{{$value->name}}</a>
                </li>
                @endforeach
            </ul>
        </li>
    </div>
</ul>
</li>
<li class="{{$about or ''}}"><a href="{{route('about', $locale)}}">@lang('messages.about')</a></li>
<li class="{{$contact or ''}}"><a href="{{route('contact', $locale)}}">@lang('messages.contact')</a></li>
<li class="dropdown mobile-none search-icon">
  <a class="dropdown-toggle hover-search" data-toggle="dropdown"><img src="{{asset('assets/frontend-image/search.png')}}" alt="" class="img-responsive nav-icon"/></a>
  <ul class="dropdown-menu search pd-top-bot-10">
    {!! Form::text('search_text', null, array('placeholder' => 'Search Text','class' => 'form-control half-width mg-auto h-40','id'=>'search_text')) !!}
    <ul id="searchResult" class="searchList"></ul>
  </ul>
</li>

<!-- <div style="display:none; overflow-y: scroll;position: absolute; width: 250px; height: 90px; background: #FFF; z-index: 100; margin-top: 127px; margin-left: 336px;" id="searchResult"> -->
<!-- </div> -->
<?php
$qty = 0;
$cart = Cart::content();
foreach ($cart as $value) {
  $qty = $qty + $value->qty;
}
if (Auth::user()) {
  $wishlist = App\Http\Controllers\Frontend\ProductController::getWishlist();
}
?>
<li class="mobile-none">
  @if (Auth::user())
  <a href="{{ route('customer.wishlist', $locale) }}">
    <img src="{{asset('assets/frontend-image/wishlist_fav.png')}}" alt="" class="img-responsive nav-icon"/>
    <div class="budge-cart {{$wishlist == 0 ? 'hide' : 'block'}}" id="wishlistBag">{{$wishlist}}</div>
</a>
@else
<a href="{{ route('customer.login.form', $locale) }}">
    <img src="{{asset('assets/frontend-image/wishlist_fav.png')}}" alt="" class="img-responsive nav-icon"/>
</a>
@endif
</li>
<li class="mobile-none">
  <a href="{{ route('checkout.cart', $locale) }}">
    <img src="{{asset('assets/frontend-image/bag.png')}}" alt="" class="img-responsive nav-icon"/>
    <div class="budge-cart {{$qty == 0 ? 'hide' : 'block'}}" id="shoppingBag">{{$qty}}</div>
</a>
</li>
@if (Auth::user())
@if (Auth::user()->photo == null)
<li class="mobile-none"><a href="{{ route('customer.account', $locale) }}"><img src="{{asset('assets/frontend-image/avatar.png')}}" alt="" class="img-responsive nav-icon bd-50"/></a></li>
@else
<li class="mobile-none"><a href="{{ route('customer.account', $locale) }}"><img src="{{asset(Auth::user()->photo)}}" alt="" class="img-responsive nav-icon bd-50"/></a></li>
@endif
@else
<li class="mobile-none"><a href="#"><img src="{{asset('assets/frontend-image/avatar.png')}}" alt="" class="img-responsive nav-icon"/></a></li>
@endif
</ul>
</div>
<div class="navbar-custom">
  <ul class="nav navbar-nav navbar-right">
    <li class="dropdown">
      <a class="dropdown-toggle hover-search" data-toggle="dropdown"><img src="{{asset('assets/frontend-image/search.png')}}" alt="" class="img-responsive nav-icon"/></a>
      <ul class="dropdown-menu pd-top-bot-10">
        {!! Form::text('search_text', null, array('placeholder' => 'Search Text','class' => 'form-control half-width mg-auto h-40','id'=>'search_text_mobile')) !!}
        <ul id="searchResultMobile" class="searchList mobile"></ul>
    </ul>
</li>
<?php
$qty = 0;
$cart = Cart::content();
foreach ($cart as $value) {
  $qty = $qty + $value->qty;
}
if (Auth::user()) {
  $wishlist = App\Http\Controllers\Frontend\ProductController::getWishlist();
}
?>
<li>
  @if (Auth::user())
  <a href="{{ route('customer.wishlist', $locale) }}">
    <img src="{{asset('assets/frontend-image/wishlist_fav.png')}}" alt="" class="img-responsive nav-icon"/>
    <div class="budge-cart {{$wishlist == 0 ? 'hide' : 'block'}}" id="wishlistBag">{{$wishlist}}</div>
</a>
@else
<a href="{{ route('customer.login.form',$locale) }}">
    <img src="{{asset('assets/frontend-image/wishlist_fav.png')}}" alt="" class="img-responsive nav-icon"/>
</a>
@endif
</li>
<li>
  <a href="{{ route('checkout.cart', $locale) }}">
    <img src="{{asset('assets/frontend-image/bag.png')}}" alt="" class="img-responsive nav-icon"/>
    <div class="budge-cart {{$qty == 0 ? 'hide' : 'block'}}" id="shoppingBag">{{$qty}}</div>
</a>
</li>
@if (Auth::user())
@if (Auth::user()->photo == null)
<li><a href="{{ route('customer.account', $locale) }}"><img src="{{asset('assets/frontend-image/avatar.png')}}" alt="" class="img-responsive nav-icon bd-50"/></a></li>
@else
<li><a href="{{ route('customer.account', $locale) }}"><img src="{{asset(Auth::user()->photo)}}" alt="" class="img-responsive nav-icon bd-50"/></a></li>
@endif
@else
<li><a href="#"><img src="{{asset('assets/frontend-image/avatar.png')}}" alt="" class="img-responsive nav-icon"/></a></li>
@endif
</div>
</div>
</div>
</nav>
</section>
<script type="text/javascript">
    function changeIcon() {
      icon = $('#icon-category').find('i').prevObject[0].className;
      if (icon == 'fa fa-angle-down f-20') {
        $('#icon-category').find('i').prevObject[0].className = 'fa fa-angle-up f-20';
    } else {
        $('#icon-category').find('i').prevObject[0].className = 'fa fa-angle-down f-20';
    }
}


$(document).ready(function() {
      $('#search_text').keyup(function(e){
        var search = $("#search_text").val();

        $.ajax({
            url:  "{{ route('search.product', $locale) }}",
          data: { 'search' : search, '_token' : '{{csrf_token() }}'},
          type: 'POST',
          success: function(result) {
            $("#searchResult").show();
            $("#searchResult").html(result);
            }
        });
    });
      $('#search_text_mobile').keyup(function(e){
        var search = $("#search_text_mobile").val();

        $.ajax({
            url:  "{{ route('search.product', $locale) }}",
          data: { 'search' : search, '_token' : '{{csrf_token() }}'},
          type: 'POST',
          success: function(result) {
            $("#searchResultMobile").show();
            $("#searchResultMobile").html(result);
            }
        });
    });
});
</script>
