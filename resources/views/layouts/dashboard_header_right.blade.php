@section('header-right')
<div class="row"> <!-- Profile Info and Notifications --> 
    <div class="col-md-6 col-sm-8 clearfix"> 
        <ul class="user-info pull-left pull-none-xsm"> <!-- Profile Info --> 
            <li class="profile-info dropdown"><!-- add class "pull-right" if you want to place this from right --> 
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
                    @if(Auth::user()->photo == '')
                    <img src="{{ asset('assets/images/nopropic.png') }}" alt="" class="img-circle" width="44" />
                    @else
                    <img src="{{ Auth::user()->photo }}" alt="" class="img-circle" width="44" />
                    @endif
                    {{Auth::user()->first_name.' '.Auth::user()->last_name}}
                </a> 
                <ul class="dropdown-menu"> <!-- Reverse Caret -->
                    <li class="caret"></li> <!-- Profile sub-links --> 
                    <li> 
                        <a href=" {{ route('admin.password.change.index') }} "> 
                            <i class="entypo-user"></i>Edit Profile
                        </a>
                    </li>
                </ul>
            </li> 
        </ul>
        <ul class="user-info pull-left pull-right-xs pull-none-xsm"> <!-- Raw Notifications --> 
            <li class="notifications dropdown" style="display:none;">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> 
                    <i class="entypo-attention"></i> 
                    <span class="badge badge-info">6</span> 
                </a> 
                <ul class="dropdown-menu">
                    <li class="top"> 
                        <p class="small">
                            <a href="#" class="pull-right">Mark all Read</a>
                            You have <strong>3</strong> new notifications.
                        </p>
                    </li>
                    <li>
                        <ul class="dropdown-menu-list scroller">
                            <li class="unread notification-success">
                                <a href="#"> 
                                    <i class="entypo-user-add pull-right"></i> 
                                    <span class="line"> 
                                        <strong>New user registered</strong> 
                                    </span> 
                                    <span class="line small">
                                        30 seconds ago
                                    </span> 
                                </a> 
                            </li> 
                            <li class="unread notification-secondary"> 
                                <a href="#"> 
                                    <i class="entypo-heart pull-right"></i>
                                    <span class="line"> <strong>Someone special liked this</strong> </span> 
                                    <span class="line small">
                                        2 minutes ago
                                    </span>
                                </a> 
                            </li>
                            <li class="notification-primary"> 
                                <a href="#">
                                    <i class="entypo-user pull-right"></i> 
                                    <span class="line"> 
                                        <strong>Privacy settings have been changed</strong> 
                                    </span> 
                                    <span class="line small">
                                        3 hours ago
                                    </span> 
                                </a> 
                            </li>
                            <li class="notification-danger"> 
                                <a href="#">
                                    <i class="entypo-cancel-circled pull-right"></i> 
                                    <span class="line">
                                        John cancelled the event
                                    </span> 
                                    <span class="line small">
                                        9 hours ago
                                    </span> 
                                </a> 
                            </li>
                            <li class="notification-info">
                                <a href="#"> 
                                    <i class="entypo-info pull-right"></i> 
                                    <span class="line">
                                        The server is status is stable
                                    </span>
                                    <span class="line small">
                                        yesterday at 10:30am
                                    </span> 
                                </a> 
                            </li>
                            <li class="notification-warning"> 
                                <a href="#">
                                    <i class="entypo-rss pull-right"></i> 
                                    <span class="line">
                                        New comments waiting approval
                                    </span>
                                    <span class="line small">
                                        last week
                                    </span>
                                </a> 
                            </li> 
                        </ul> 
                    </li> 
                    <li class="external">
                        <a href="#">View all notifications</a> 
                    </li>
                </ul> 
            </li> <!-- Message Notifications -->
            <li class="notifications dropdown" style="display:none;"> 
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                    <i class="entypo-mail"></i> 
                    <span class="badge badge-secondary">10</span>
                </a> 
                <ul class="dropdown-menu"> <!-- TS147375658816464: Xenon - Boostrap Admin Template created by Laborator / Please buy this theme and support the updates --> 
                    <li> 
                        <form class="top-dropdown-search"> 
                            <div class="form-group"> 
                                <input type="text" class="form-control" placeholder="Search anything..." name="s" /> 
                            </div> 
                        </form> 
                        <ul class="dropdown-menu-list scroller">
                            <li class="active">
                                <a href="#"> 
                                    <span class="image pull-right"> 
                                        <img src="{{ asset('assets/images/member-2.jpg') }}" width="44" alt="" class="img-circle" /> 
                                    </span> 
                                    <span class="line"> 
                                        <strong>Luc Chartier</strong>
                                        - yesterday
                                    </span> 
                                    <span class="line desc small">
                                        This ain’t our first item, it is the best of the rest.
                                    </span>
                                </a> 
                            </li> 
                        </ul> 
                    </li> 
                    <li class="external"> 
                        <a href="mailbox/main/index.html">All Messages</a>
                    </li>
                </ul> 
            </li> <!-- Task Notifications --> 
        </ul> 
    </div> <!-- Raw Links --> 
    <div class="col-md-6 col-sm-4 clearfix hidden-xs"> 
        <ul class="list-inline links-list pull-right"> 
            <li class="dropdown language-selector" style="display:none;">
                Language: &nbsp;
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-close-others="true"> 
                    <img src="{{ asset('assets/images/flags/flag-uk.png') }}" width="16" height="16" /> 
                </a> 
                <ul class="dropdown-menu pull-right">
                    <li> 
                        <a href="#"> 
                            <img src="{{ asset('assets/images/flags/flag-id.png') }}" width="16" height="16" />
                            <span>Indonesia</span>
                        </a>
                    </li>
                    <li class="active">
                        <a href="#">
                            <img src="{{ asset('assets/images/flags/flag-uk.png') }}" width="16" height="16" />
                            <span>English</span>
                        </a>
                    </li>
                </ul> 
            </li> 
            <li class="sep" style="display:none;"></li>
            <li style="display:none;">
                <a href="#" data-toggle="chat" data-collapse-sidebar="1">
                    <i class="entypo-chat"></i>
                    Chat
                    <span class="badge badge-success chat-notifications-badge is-hidden">0</span> 
                </a>
            </li> 
            <li class="sep" style="display:none;"></li> 
            <li> 
                <a href="{{ route('admin.logout') }}">
                    Log Out 
                    <i class="entypo-logout right"></i> 
                </a> 
            </li>
        </ul>
    </div>
</div> 
@endsection