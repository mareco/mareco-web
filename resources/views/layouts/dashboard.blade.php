<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="Laborator.co" />
    <link rel="icon" href="{{ asset('favicon.ico') }}">
    <title>Mareco | {{ $title or ''}}</title>
    <link rel="stylesheet" href="{{ asset('assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css') }}" id="style-resource-1">
    <link rel="stylesheet" href="{{ asset('assets/css/font-icons/entypo/css/entypo.css') }}" id="style-resource-2">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic" id="style-resource-3">
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.css') }}" id="style-resource-4">
    <link href="{{ asset('assets/global/plugins/gritter/css/jquery.gritter.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('assets/css/neon-core.css') }}" id="style-resource-5">
    <link rel="stylesheet" href="{{ asset('assets/css/neon-theme.css') }}" id="style-resource-6">
    <link rel="stylesheet" href="{{ asset('assets/css/neon-forms.css') }}" id="style-resource-7">
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}" id="style-resource-8">
    <link rel="stylesheet" href="{{ asset('css/backend/style.css') }}" id="style-resource-9">
    <link href="{{ asset('js/bootstrap-eonasdan-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('js/bootstrap-datepicker.min.css') }}" />
    <script src="{{ asset('assets/js/jquery-1.11.3.min.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('plugin/colorpicker/css/bootstrap-colorpicker.min.css') }}">
</head>
<body class="page-body">
    <div class="page-container">
        <div class="sidebar-menu">
            <div class="sidebar-menu-inner">
                <header class="logo-env"> <!-- logo -->
                    <div class="logo">
                        <a href="/admin">
                            <img src="{{ asset('assets/frontend-image/about-us-red.png') }}" width="120" alt="" /> 
                        </a>
                    </div> <!-- logo collapse icon -->
                    <div class="sidebar-collapse">
                        <a href="#" class="sidebar-collapse-icon">
                            <i class="entypo-menu"></i>
                        </a>
                    </div>
                    <div class="sidebar-mobile-menu visible-xs">
                        <a href="#" class="with-animation">
                            <i class="entypo-menu"></i>
                        </a>
                    </div>
                </header>

                @yield('sidebar')

            </div>
        </div>
        <div class="main-content">
            @yield('header-right')
            <hr />
            @yield('content')
            <footer class="main">
                &copy; 2017 <strong>Mareco</strong> Store All rights reserved
            </footer>
        </div>

        <div id="chat" class="fixed" data-current-user="Art Ramadani" data-order-by-status="1" data-max-chat-history="25">
            <div class="chat-inner">
                <h2 class="chat-header">
                    <a href="#" class="chat-close">
                        <i class="entypo-cancel"></i>
                    </a>
                    <i class="entypo-users"></i>
                    Chat
                    <span class="badge badge-success is-hidden">0</span>
                </h2>
                <div class="chat-group" id="group-1">
                    <strong>Favorites</strong>
                    <a href="#" id="sample-user-123" data-conversation-history="#sample_history">
                        <span class="user-status is-online"></span>
                        <em>Catherine J. Watkins</em>
                    </a>
                    <a href="#">
                        <span class="user-status is-online"></span>
                        <em>Nicholas R. Walker</em>
                    </a>
                    <a href="#">
                        <span class="user-status is-busy"></span>
                        <em>Susan J. Best</em>
                    </a>
                </div>
            </div> <!-- conversation template -->
            <div class="chat-conversation">
                <div class="conversation-header">
                    <a href="#" class="conversation-close">
                        <i class="entypo-cancel"></i>
                    </a>
                    <span class="user-status"></span>
                    <span class="display-name"></span>
                    <small></small>
                </div>
                <ul class="conversation-body"> </ul>
                <div class="chat-textarea">
                    <textarea class="form-control autogrow" placeholder="Type your message"></textarea>
                </div>
            </div>
        </div>
        <ul class="chat-history" id="sample_history_2">
            <li class="opponent unread">
                <span class="user">Daniel A. Pena</span>
                <p>I am going out.</p>
                <span class="time">08:21</span>
            </li>
            <li class="opponent unread">
                <span class="user">Daniel A. Pena</span>
                <p>Call me when you see this message.</p>
                <span class="time">08:27</span>
            </li>
        </ul>
    </div>
    <link rel="stylesheet" href="{{ asset('assets/js/jvectormap/jquery-jvectormap-1.2.2.css') }}" id="style-resource-1">
    <link rel="stylesheet" href="{{ asset('assets/js/rickshaw/rickshaw.min.css') }}" id="style-resource-2">
    <link rel="stylesheet" href="{{ asset('assets/js/dropzone/dropzone.css') }}" id="style-resource-3">
    <script src="{{ asset('assets/js/gsap/TweenMax.min.js') }}" id="script-resource-1"></script>
    <script src="{{ asset('assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js') }}" id="script-resource-2"></script>
    <script src="{{ asset('assets/js/bootstrap.js') }}" id="script-resource-3"></script>
    <script src="{{ asset('assets/js/joinable.js') }}" id="script-resource-4"></script>
    <script src="{{ asset('assets/js/resizeable.js') }}" id="script-resource-5"></script>
    <script src="{{ asset('assets/js/neon-api.js') }}" id="script-resource-6"></script>
    <script src="{{ asset('assets/global/plugins/gritter/js/jquery.gritter.js') }}"></script>
    <script src="{{ asset('assets/js/cookies.min.js') }}" id="script-resource-7"></script>
    <script src="{{ asset('assets/js/jquery.bootstrap.wizard.min.js') }}" id="script-resource-8"></script>
    <script src="{{ asset('assets/js/jquery.validate.min.js') }}" id="script-resource-9"></script>
    <script src="{{ asset('assets/js/jquery.sparkline.min.js') }}" id="script-resource-10"></script>
    <script src="{{ asset('assets/js/rickshaw/vendor/d3.v3.js') }}" id="script-resource-11"></script>
    <script src="{{ asset('assets/js/rickshaw/rickshaw.min.js') }}" id="script-resource-12"></script>
    {{-- <script src="{{ asset('assets/js/neon-charts.js') }}" id="script-resource-13"></script> --}}
    <script src="{{ asset('assets/js/bootstrap-switch.min.js') }}" id="script-resource-14"></script>
    <script src="{{ asset('assets/js/morris.min.js') }}" id="script-resource-15"></script>
    <script src="{{ asset('assets/js/neon-chat.js') }}" id="script-resource-16"></script> <!-- JavaScripts initializations and stuff -->
    <script src="{{ asset('assets/js/neon-custom.js') }}" id="script-resource-17"></script> <!-- Demo Settings -->
    <script src="{{ asset('assets/js/neon-demo.js') }}" id="script-resource-18"></script>
    <script src="{{ asset('assets/js/raphael-min.js') }}" id="script-resource-20"></script>
    <script src="{{ asset('assets/js/dropzone/dropzone.js') }}" id="script-resource-21"></script>
    <script src="{{ asset('plugin/colorpicker/js/bootstrap-colorpicker.js') }}" id="script-resource-21"></script>

    <script src="{{ asset('assets/js/custom.js') }}"></script>
    <script src="{{ asset('js/bootstrap-daterangepicker/moment.js') }}"></script>
    <script src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('js/bootstrap-eonasdan-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('.view-color-selector').colorpicker({
            colorSelectors: {
                '#000000': '#000000',
                '#ffffff': '#ffffff',
                '#FF0000': '#FF0000',
                '#777777': '#777777',
                '#337ab7': '#337ab7',
                '#5cb85c': '#5cb85c',
                '#5bc0de': '#5bc0de',
                '#f0ad4e': '#f0ad4e',
                '#d9534f': '#d9534f',
                '#fc31b7': '#fc31b7'
            }
        }).on('changeColor', function(e) {
            console.log(e);
            e.currentTarget.parentElement.previousElementSibling.value = e.color.toString(
                'hex');
            e.currentTarget.style.backgroundColor = e.color.toString('hex');
        });
            $('#session-message').fadeIn(2000, function () {
                $('#session-message').fadeOut(2000);
            });

            $(".datetimepicker").datetimepicker({ format: "HH:mm" });
            $(".datepicker").datetimepicker({ format: 'YYYY-MM-DD'});
            $(".dateonlypicker").datetimepicker({ format: 'YYYY-MM-DD'});

            jQuery.fn.preventDoubleSubmission = function() {
                $(this).on('submit',function(e){
                    var $form = $(this);

                    if ($form.data('submitted') === true) {
                        // Previously submitted - don't submit again
                        e.preventDefault();
                    } else {
                        // Mark it so that the next submit can be ignored
                        $form.data('submitted', true);
                    }
                });
                return this;
            };

            $('form').preventDoubleSubmission();


        });
        </script>
    @stack('pageRelatedJs')

</body>
<!-- Mirrored from demo.neontheme.com/?skin=default by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 13 Sep 2016 08:58:29 GMT -->
</html>
