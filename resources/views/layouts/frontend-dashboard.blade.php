<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Mareco </title>
	<link rel="stylesheet" type="text/css" href="{{asset('css/frontend/style.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/js/parsley/src/parsley.css')}}">
	<link href="{{ asset('js/bootstrap-eonasdan-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('js/bootstrap-datepicker.min.css') }}" />
	<link href="{{ asset('plugin/gritter/css/jquery.gritter.css') }}" rel="stylesheet" />
	<script src="{{ asset('css/frontend/js/jquery-3.1.1.min.js') }}"></script>
	<script src="{{ asset('css/frontend/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('css/frontend/js/jquery.flexslider-min.js') }}"></script>
	<script src="{{ asset('assets/js/parsley/dist/parsley.js') }}"></script>
	<script src="{{ asset('css/frontend/js/owl.carousel.js') }}"></script>
	<script src="{{ asset('css/frontend/js/owl.carousel2.thumbs.min.js') }}"></script>
	<script src="{{ asset('css/frontend/js/bootstrap-magnify.min.js') }}"></script>
	<script src="{{ asset('js/bootstrap-daterangepicker/moment.js') }}"></script>
	<script src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
	<script src="{{ asset('js/bootstrap-eonasdan-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
	<script src="{{ asset('plugin/gritter/js/jquery.gritter.js') }}"></script>
	<!-- hammer plugin here -->
</head>
<body>

@include('layouts.frontend-header')

<div class="container-full bd-top-gray">
	@yield('content')
</div>

@include('layouts.frontend-footer')

<script type="text/javascript">
	$(document).ready(function() {
        $(".datetimepicker").datetimepicker({ format: "HH:mm" });
        $(".datepicker").datetimepicker({ format: 'YYYY-MM-DD HH:mm:ss'});
        $(".dateonlypicker").datetimepicker({ format: 'YYYY-MM-DD'});
    });
</script>
</body>
</html>
