@section('sidebar')
<ul id="main-menu" class="main-menu">
    <li class="has-item start {{ $home or '' }}">
        <a href="/admin">
            <i class="entypo-home"></i>
            <span class="title">Home</span>
        </a>
    </li>
    <li class="has-sub {{ $manageAccount or '' }}">
        <a href="javascript:;">
            <i class="entypo-users"></i>
            <span class="title">Manage Account</span>
        </a>
        <ul class="sub-menu">
            <li class="nav-item {{ $userList or '' }} ">
                <a href="{{ route('admin.users.index') }}">
                    <span class="title">User</span>
                </a>
            </li>
            <li class="nav-item {{ $adminList or '' }} ">
                <a href="{{ route('admin.admins.index') }}">
                    <span class="title">Admin</span>
                </a>
            </li>
            <li class="nav-item {{ $userService or '' }} ">
                <a href="{{ route('admin.user.services.index') }}">
                    <span class="title">User Feedback</span>
                </a>
            </li>
        </ul>
    </li>
    {{-- <li class="has-sub {{ $manageShipping or '' }}">
        <a href="javascript:;">
            <i class="entypo-rocket"></i>
            <span class="title">Manage Shipping</span>
        </a>
        <ul class="sub-menu">
            <li class="nav-item {{ $provinceList or '' }} ">
                <a href="{{ route('admin.provinces.index') }}">
                    <span class="title">Province</span>
                </a>
            </li>
            <li class="nav-item {{ $cityList or '' }} ">
                <a href="{{ route('admin.cities.index') }}">
                    <span class="title">City</span>
                </a>
            </li>
            <li class="nav-item {{ $districtList or '' }} ">
                <a href="{{ route('admin.districts.index') }}">
                    <span class="title">Sub-district</span>
                </a>
            </li>
            <li class="nav-item {{ $courierList or '' }} ">
                <a href="{{ route('admin.couriers.index') }}">
                    <span class="title">Courier</span>
                </a>
            </li>
            <li class="nav-item {{ $shippingMethodList or '' }} ">
                <a href="{{ route('admin.shipping.methods.index') }}">
                    <span class="title">Shipping Method</span>
                </a>
            </li>
        </ul>
    </li> --}}
    <li class="has-sub {{ $manageProduct or '' }}">
        <a href="javascript:;">
            <i class="entypo-basket"></i>
            <span class="title">Manage Product</span>
        </a>
        <ul class="sub-menu">
            <li class="nav-item {{ $categoryList or '' }} ">
                <a href="{{ route('admin.categories.index') }}">
                    <span class="title">Category</span>
                </a>
            </li>
            <li class="nav-item {{ $subCategoryList or '' }} " style="display: none;">
                <a href="{{ route('admin.sub-categories.index') }}">
                    <span class="title">Sub Category</span>
                </a>
            </li>
            <li class="nav-item {{ $unitList or '' }} ">
                <a href="{{ route('admin.units.index') }}">
                    <span class="title">Unit</span>
                </a>
            </li>
            <li class="nav-item {{ $productList or '' }} ">
                <a href="{{ route('admin.products.index') }}">
                    <span class="title">Product</span>
                </a>
            </li>
        </ul>
    </li>
    <li class="{{ $manageOrder or '' }}">
        <a href="javascript:;">
            <i class="entypo-sound"></i>
            <span class="title">Manage Order</span>
        </a>
        <ul class="sub-menu">
            <li class="nav-item {{ $orderList or '' }} ">
                <a href="{{ route('admin.orders.index') }}">
                    <span class="title">Order</span>
                </a>
            </li>
            <!-- <li class="nav-item {{ $wishList or '' }} ">
                <a href="{{ route('admin.wishlists.index')}}">
                    <span class="title">Wishlist</span> 
                </a>
            </li> -->
        </ul>
    </li>
    <li class="{{ $manageSystem or '' }}">
        <a href="javascript:;">
            <i class="entypo-lock"></i>
            <span class="title">System Config</span>
        </a>
        <ul class="sub-menu">
            {{-- <li class="nav-item {{ $catalogList or '' }} ">
                <a href="{{ route('admin.catalogs.index') }}">
                    <span class="title">Catalog Sales</span> 
                </a>
            </li> --}}
            <li class="nav-item {{ $promotionList or '' }} ">
                <a href="{{ route('admin.promotions.index') }}">
                    <span class="title">Promotion</span> 
                </a>
            </li>
            <li class="nav-item {{ $bankList or '' }} ">
                <a href="{{ route('admin.banks.index') }}">
                    <span class="title">Bank Account</span>
                </a>
            </li>
            <li class="{{ $sliderList or '' }}">
                <a href="{{ route('admin.sliders.index') }}">
                    <span class="title">Slider</span>
                </a>
            </li>
            <li class="{{ $brandList or '' }}">
                <a href="{{ route('admin.brands.index') }}">
                    <span class="title">Feature Brand</span>
                </a>
            </li>
            <li class="nav-item {{ $configList or '' }} ">
                <a href="{{ route('admin.configs.new') }}">
                    <span class="title">Configuration</span> 
                </a>
            </li>
        </ul>
    </li>
    <li class="has-item start {{ $report or '' }}">
        <a href="{{ route('admin.report') }}">
            <i class="entypo-home"></i>
            <span class="title">Report</span>
        </a>
    </li>
</ul>
@endsection
