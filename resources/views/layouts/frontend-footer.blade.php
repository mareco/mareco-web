<section class="footer text-white">
    <?php
    $config = App\Http\Controllers\Frontend\HomepageController::showFooter();
    ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="text-white">@lang('messages.contact')</h1>
            </div>
            <div class="col-md-6">
                @if($config != '')
                <p><span class="bold">@lang('messages.address') : </span> {{$config->storage_address}}</p>
                <p><span class="bold">@lang('messages.mailUs') : </span> {{$config->about_us_email}}</p>
                <p><span class="bold">@lang('messages.phone') : </span> {{$config->about_us_phone}}</p>
                @endif
            </div>
            <div class="col-md-6">
                <p class="text-right"> 2017 Mareco Store All rights reserved.</p>
                <ul class="social-icon">
                        @if($config != '')
                        <li>
                            @if($config->facebook_account)
                            <a href="https://{{$config->facebook_account}}"><img src="{{ asset('assets/images/facebook.png') }}" style="width: 30px;"></a>
                            @endif
                        </li>
                        <li>
                            @if($config->instagram_account)
                            <a href="https://{{$config->instagram_account}}"><img src="{{ asset('assets/images/instagram.png') }}" style="width: 30px;"></a>
                            @endif
                        </li>
                        <li>
                            @if($config->twitter_account)
                            <a href="https://{{$config->twitter_account}}"><img src="{{ asset('assets/images/twitter.png') }}" style="width: 30px;"></a>
                            @endif
                        </li>
                        @endif
                </ul>
            </div>
        </div>
    </div>
</div>
</section>
