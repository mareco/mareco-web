@extends('layouts.dashboard')
@extends('layouts.dashboard_side_bar')
@include('layouts.dashboard_header_right')
@include('includes.modal_delete')
@section('content')
@include('includes.page_header_index')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <a href="{{route('admin.districts.new')}}" class="btn btn-xs btn-primary pull-right"><span class="fa fa-plus"></span> Add</a>
                <h4>Sub-district</h4>
            </div>

            <div class="panel-body">
                <table class="table table-striped table-hover fullTable">
                    <thead>
                        <tr>
                            <th>City</th>
                            <th>Sub-district</th>
                            <th colspan="2">Action</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($districts as $value)
                        <tr>
                            <td>{{$value->city->name}}</td>
                            <td>{{$value->name}}</td>
                            <td>
                                <a href="{{ route('admin.districts.edit', [$value->id]) }}" class="btn btn-info"><i class="fa fa-edit"></i> Edit</a>
                            </td>
                            <td>
                                <a data-url="{{ route('admin.districts.destroy', [$value->id]) }}" class="btn btn-warning fa fa-trash" data-title="Confirmation" data-message="This will delete the selected district. Do you want to continue?" data-toggle="modal" data-target="#modalDelete"> Delete</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
