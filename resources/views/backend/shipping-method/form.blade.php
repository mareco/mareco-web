
@extends('layouts.dashboard')
@extends('layouts.dashboard_side_bar')
@include('layouts.dashboard_header_right')
@section('content')
@include('includes.page_header_form')

@if (isset($shippingMethod))
{!! Form::model($shippingMethod, [
        'url' => route('admin.shipping.methods.update', ['shippingMethod' => $shippingMethod]),
        'method' => 'PUT',
        'data-parsley-validate' => 'true',
    ]) !!}
@else
{!! Form::open([
        'url' => route('admin.shipping.methods.save'),
        'method' => 'POST',
        'data-parsley-validate' => 'true',
    ]) !!}
@endif

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="{{ URL::previous() }}" class="btn btn-xs btn-primary pull-right"><span class="fa fa-reply"></span> Back</a>
                </div>
                <h4>Shipping Method Registration Form</h4>
            </div>
            
            <div class="panel-body">
                <div class="modal-body">
                    <fieldset>
                        {{ csrf_field() }}
                        <div class="row">
                           <div class="row">
                                <div class="form-group col-md-6">
                                {{ Form::label('courier_id', 'Courier')}}
                                {{ Form::select('courier_id', $listOfCouriers, null, array('class' => 'select2me form-control', 'placeholder' => 'Please select one...','data-parsley-required' => 'true'))}}
                            </div>
                            <div class="form-group col-md-6">
                                {{ Form::label('city_id', 'City')}}
                                {{ Form::select('city_id', $listOfCities, null, array('class' => 'select2me form-control', 'placeholder' => 'Please select one...','data-parsley-required' => 'true'))}}
                            </div>
                            <div class="form-group col-md-6">
                                {{ Form::label('type', 'Type')}}
                                {{ Form::select('type',$shippingType, null, array('class' => 'select2me form-control', 'placeholder' => 'Please select one...','data-parsley-required' => 'true'))}}
                            </div>
                            <div class="form-group col-md-6">
                                {{ Form::label('fee', 'Fee')}}
                                {{ Form::number('fee', null, array('class' => 'form-control', 'placeholder' => 'Fee','data-parsley-required' => 'true'))}}
                            </div>
                            <div class="form-group col-md-12">
                                {{ Form::label('name', 'Description')}}
                                {{ Form::textarea('name', null, array('class' => 'form-control', 'placeholder' => 'Description','data-parsley-required' => 'true'))}}
                            </div>
                        </div>
                        </div>
                    </fieldset>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-success"><span class="fa fa-save"></span> Save</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endsection
