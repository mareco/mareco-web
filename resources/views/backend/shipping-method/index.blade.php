<?php
use App\Helpers\Enums\ShippingType;
?>

@extends('layouts.dashboard')
@extends('layouts.dashboard_side_bar')
@include('layouts.dashboard_header_right')
@include('includes.modal_delete')
@section('content')
@include('includes.page_header_index')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <a href="{{route('admin.shipping.methods.new')}}" class="btn btn-xs btn-primary pull-right"><span class="fa fa-plus"></span> Add</a>
                <h4>Shipping Method</h4>
            </div>

            <div class="panel-body">
                <table class="table table-striped table-hover fullTable">
                    <thead>
                        <tr>
                            <th>Courier </th>
                            <th>City </th>
                            <th>Type </th>
                            <th>Fee </th>
                            <th>Description </th>
                            <th>Created By </th>
                            <th colspan="2">Action</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($shippingMethods as $value)
                            <tr>
                                <td>{{$value->courier->name}}</td>
                                <td>{{$value->city->name}}</td>
                                <td>{{ShippingType::getString($value->type)}}</td>
                                <td>{{$value->fee}}</td>
                                <td>{{$value->name}}</td>
                                <td>{{Auth::user()->first_name.' '.Auth::user()->last_name}}</td>
                                <td>
                                    <a href="{{ route('admin.shipping.methods.edit', [$value->id]) }}" class="btn btn-info"><i class="fa fa-edit"></i> Edit</a>
                                </td>
                                <td>
                                    <a data-url="{{ route('admin.shipping.methods.destroy', [$value->id]) }}" class="btn btn-warning fa fa-trash" data-title="Confirmation" data-message="This will delete the selected shipping method. Do you want to continue?" data-toggle="modal" data-target="#modalDelete"> Delete</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @endsection
