@extends('layouts.dashboard')
@extends('layouts.dashboard_side_bar')
@include('layouts.dashboard_header_right')
<?php 

use App\Helpers\Enums\PaymentType;

?>
@section('content')
<div id="session-message" style="display: none;">
	@include('flash::message')
</div>
<div class="page-title">
	<h1><b>Mareco</b> Home 
	</h1>
</div>
<div class="portlet light bordered">
	<div class="portlet-body form">
		<div class="row">
			<div class="col-md-12">
				<h4> Welcome Admin <b>{{Auth::user()->first_name.' '.Auth::user()->last_name}}</b> </h4>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-3 col-xs-6">
				<div class="tile-stats tile-red">
					<div class="icon"><i class="entypo-users"></i></div>
					<div class="num" data-start="0" data-end="{{$registeredUsers}}" data-postfix="" data-duration="1500" data-delay="0">0</div>
					<h3>Registered users</h3>
					<p>so far in our website.</p>
				</div>
			</div>
			<div class="col-sm-3 col-xs-6">
				<div class="tile-stats tile-green">
					<div class="icon"><i class="entypo-layout"></i></div>
					<div class="num" data-start="0" data-end="{{$totalProducts}}" data-postfix="" data-duration="1500" data-delay="600">0</div>
					<h3>Total Products</h3>
					<p>Overall product.</p>
				</div>
			</div>
			<div class="clear visible-xs"></div>
			<div class="col-sm-3 col-xs-6">
				<div class="tile-stats tile-aqua">
					<div class="icon"><i class="entypo-basket"></i></div>
					<div class="num" data-start="0" data-end="{{$totalOrders}}" data-postfix="" data-duration="1500" data-delay="1200">0</div>
					<h3>Total Orders</h3>
					<p>Overall ordered.</p>
				</div>
			</div>
			<div class="col-sm-3 col-xs-6">
				<div class="tile-stats tile-blue">
					<div class="icon"><i class="entypo-rss"></i></div>
					@if(isset($lifeTimeSales))
					<div class="num" data-start="0" data-end="{{$lifeTimeSales}}" data-postfix="" data-duration="1500" data-delay="1800">0</div>
					@endif
					<h3>Lifetime Sales in Rupiah</h3>
					<p>on our site right now.</p>
				</div>
			</div>
		</div>
	</div>
</div>
<br><br>
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-primary" id="charts_env">
			<div class="panel-heading">
				<div class="panel-title">Orders Vs Payments {{ \Carbon\Carbon::now()->format('F').' '.\Carbon\Carbon::now()->year }}</div>
			</div>
			<div class="panel-body">
				<div class="tab-content">
					<div class="tab-pane active" id="line-chart">
						<div id="line-chart-demo" class="morrischart" style="height: 300px"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<br>
<div class="row">
	<div class="col-sm-6">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<div class="panel-title">Recent Orders</div>
			</div>
			<table class="table table-bordered table-responsive">
				<thead>
					<tr>
						<th>Order No.</th>
						<th>Customer</th>
						<th>Payment Type</th>
						<th>Ordered Date</th>
					</tr>
				</thead>
				<tbody>
					@if(isset($recentOrders))
					@foreach($recentOrders as $recentOrder)
					<tr>
						<td>{{$recentOrder->order_number}}</td>
						<td>{{$recentOrder->user->first_name.' '.$recentOrder->user->last_name}}</td>
						<td>{{PaymentType::getString($recentOrder->payment_type)}}</td>
						<td><i>{{$recentOrder->created_at }} ({{$recentOrder->created_at->diffForHumans()}})</i></td>
					</tr>
					@endforeach
					@endif
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-sm-6">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<div class="panel-title">Recent Registered</div>
			</div>
			<table class="table table-bordered table-responsive">
				<thead>
					<tr>
						<th>Name</th>
						<th>Email</th>
						<th>Phone</th>
						<th>Registered Date</th>
					</tr>
				</thead>
				<tbody>
					@if(isset($recentRegistered))
					@foreach($recentRegistered as $recentRegister)
					<tr>
						<td>{{$recentRegister->first_name.' '.$recentRegister->last_name}}</td>
						<td>{{$recentRegister->email}}</td>
						<td>{{$recentRegister->phone}}</td>
						<td><i>{{$recentOrder->created_at}} ({{$recentOrder->created_at->diffForHumans()}})</i></td> 
					</tr>
					@endforeach
					@endif
				</tbody>
			</table>
		</div>
	</div>

	<div class="col-sm-6">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<div class="panel-title">Activity Log</div>
			</div>
			<table class="table table-bordered table-responsive">
				<thead>
					<tr>
						<th>Type</th>
						<th>Action</th>
						<th>Description</th>
						<th>User</th>
						<th>Date</th>
					</tr>
				</thead>
				<tbody>
					@if(isset($activity_logs))
					@foreach($activity_logs as $activity_log)
					<tr>
						<td>{{$activity_log->type}}</td>
						<td>{{$activity_log->action}}</td>
						<td>{{$activity_log->description}}</td>
						<td>{{$activity_log->user->first_name}} {{$activity_log->user->last_name}}</td>
						<td>{{$activity_log->created_at}}</td>
					</tr>
					@endforeach
					@endif
				</tbody>
			</table>
			<div style="width: 100%;" class="text-center">
				<a href="{{route('admin.log.index')}}" class="text-center"><b><u>View More</u></b></a>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		setTimeout(function() {
			var opts = {
				"closeButton": true,
				"debug": false,
				"positionClass": rtl() || public_vars.$pageContainer.hasClass('right-sidebar') ? "toast-top-left" : "toast-top-right",
				"toastClass": "black",
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"timeOut": "5000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
			};
		}, 3000);

        // Line Charts

        var line_chart_demo = $("#line-chart-demo");
        var line_chart = Morris.Line({
        	element: 'line-chart-demo',
        	data:
        	[
        		<?php $month = 1; ?>
        		@for($i = 0; $i < count($chartOrders['tempOrder']); $i++)
        			{ day: "{{ $month }}", a: {{ $chartOrders['tempOrder'][$i] }}, b: {{ $chartOrders['tempPayment'][$i] }} },
        			<?php $month++; ?>
        		@endfor
        	],
        	xkey: 'day',
        	ykeys: ['a', 'b'],
        	labels: ['Orders', 'Payments'],
        	redraw: true,
        	parseTime: false,
        });
        line_chart_demo.parent().attr('style', '');
    });

	function getRandomInt(min, max) {
		return Math.floor(Math.random() * (max - min + 1)) + min;
	}
</script>
@endsection