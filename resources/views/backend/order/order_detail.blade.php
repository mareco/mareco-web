<div class="row">
  <div class="col-md-12">
      <table class="table table-striped table-hover" id="orderDetail">
          <div class="panel-heading-btn">
          </div>
          <legend>Add Product</legend>
          <thead>
              <tr>
                  <th>Product</th>
                  <th>Size</th>
                  <th>Color</th>
                  <th>Price</th>
                  <th>Weight</th>
                  <th>Qty</th>
              </tr>
          </thead>
          <tbody id="countRows">
              <tr>
                <td>
                  <select class="form-control select-product" name="product" id="product">
                    <option selected="" hidden="" value="none">Select Product</option>
                    @foreach($product as $key)
                      <option value="{{$key->id}}">{{$key->name}}</option>
                    @endforeach
                  </select>
                </td>
                <td>
                  <select class="form-control select-size" name="size" id="size">
                    <option selected="" hidden="" value="none">Select Size</option>
                  </select>
                </td>
                <td>
                  <select class="form-control select-color" name="color" id="color">
                    <option selected="" hidden="" value="none">Select Color</option>
                  </select>
                </td>
                <td>
                  <input class="form-control" type="number" name="price" readonly="readonly" id="price" value="none">
                </td>
                <td>
                  <input class="form-control" type="number" name="weight" readonly="readonly" id="weight" value="none">
                </td>
                <td>
                  <input class="form-control" type="number" name="qty" id="qty">
                </td>
              </tr>
          </tbody>
      </table>
  </div>
</div>