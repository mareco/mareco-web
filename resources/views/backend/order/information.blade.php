<?php 
use App\Helpers\Enums\OrderStatus;
use App\Helpers\Enums\PaymentType;
use App\Helpers\Enums\TypeShipmentHelper;
?>

@extends('layouts.dashboard')
@extends('layouts.dashboard_side_bar')
@section('content')
@include('includes.page_header_form')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="{{ route('admin.orders.index') }}" class="btn btn-xs btn-primary pull-right"><span class="fa fa-reply"></span> Back</a>
                    @if ($order->payment_type == 2)
                        @if ($order->status != OrderStatus::COMPLETED && $order->status != OrderStatus::CANCEL)
                        <button class="btn btn-xs btn-success pull-right" data-toggle="modal" data-target="#completed-order" style="margin-right: 10px">Completed</button>
                        <button class="btn btn-xs btn-danger pull-right" data-toggle="modal" data-target="#cancel-order" style="margin-right: 10px">Cancel</button>
                        @endif
                    @endif

                    @if ($order->payment_type != 2)
                        @if ($order->status == OrderStatus::ON_VERIFICATION)
                        <button class="btn btn-xs btn-danger pull-right" data-toggle="modal" data-target="#cancel-order" style="margin-right: 10px">Cancel</button>
                        <button class="btn btn-xs btn-primary pull-right" data-toggle="modal" data-target="#confirm-payment" style="margin-right: 10px">Confirm Payment</button>
                        @endif
                        @if ($order->status == OrderStatus::PACKING)
                        <button class="btn btn-xs btn-info pull-right" data-toggle="modal" data-target="#shipment" style="margin-right: 10px">Shipment</button>
                        @endif
                        @if ($order->status == OrderStatus::ON_SHIPPING)
                        <button class="btn btn-xs btn-success pull-right" data-toggle="modal" data-target="#completed-order" style="margin-right: 10px">Completed</button>
                        @endif
                    @endif
                </div>
                <div id="loading-wrapper" style="display: none">
                    <div style="position: absolute;width: 120px;height:120px;margin-top: -60px; margin-left:-60px; left: 50%; top: 50%">
                        <img src="{{ asset('assets/img/ring-alt.gif') }}" class="img-responsive" />
                    </div>
                </div>
                <h4>Order Information</h4>
            </div>
            
            <div class="panel-body">
                <div class="modal-body">
                    <p id="message"></p>
                    <fieldset>
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-6">
                                <b><legend>Order #{{$order->order_number}} </legend></b>
                                <div class="col-md-5">Order Date</div>
                                <div class="col-md-7">{{\Carbon\Carbon::parse($order->created_at)->format('Y-m-d H:i:s')}}</div>
                                <div class="col-md-5">Order Status</div>
                                <div class="col-md-7">{{OrderStatus::getString($order->status)}}</div>
                            </div>
                            <div class="col-md-6">
                                <b><legend>Account Information</legend></b>
                                @if(isset($order->user->photo))
                                <div class="col-md-5">Photo</div>
                                <div class="col-md-7">
                                    <img src={{$order->user->photo}} class="table-img index-data-image" style="width: 70px; "/>
                                </div>
                                @endif
                                <div class="col-md-5">Customer Name</div>
                                <div class="col-md-7">{{$order->user->first_name.' '.$order->user->last_name}}</div>
                                <div class="col-md-5">Email</div>
                                <div class="col-md-7">{{$order->user->email}}</div>
                                <div class="col-md-5">Phone</div>
                                <div class="col-md-7">{{$order->user->phone}}</div>
                            </div>
                        </div>
                        <br><br>
                        <div class="row">
                            <div class="col-md-6">
                                <b><legend>Payment & Shipping Method</legend></b>
                                @if ($order->payment_type != 2)
                                <div class="col-md-5">Shipping Method</div>
                                <div class="col-md-7">{{TypeShipmentHelper::getString($order->shipment_type)}}</div>
                                @endif
                                <div class="col-md-5">Payment Type</div>
                                <div class="col-md-7">{{PaymentType::getString($order->payment_type)}}</div>
                                @if($order->payment_type != PaymentType::CASH_ON_DELIVERY && $order->payment_type != PaymentType::PAYPAL)
                                <div class="col-md-5">To Bank</div>
                                <div class="col-md-7">@if($order->bankAccount != NULL) {{$order->bankAccount->bank}} @else Not available yet @endif</div>
                                <div class="col-md-5">Account Name</div>
                                <div class="col-md-7">@if($order->account_name != NULL) {{$order->account_name}} @else Not available yet @endif</div>
                                <div class="col-md-5">Account Number</div>
                                <div class="col-md-7">@if($order->account_number != NULL) {{$order->account_number}} @else Not available yet @endif</div>
                                <div class="col-md-5">Transfer Receipt</div>
                                <div class="col-md-7">
                                    @if($order->receipt_url != NULL)
                                    <img src="{{$order->receipt_url}} " class="img-responsive" />
                                    @else 
                                    Not available 
                                    @endif
                                </div>
                                @endif
                                <input type="hidden" name="cod_lat" value="{{$order->cod_lat}}" id="cod_lat">
                                <input type="hidden" name="cod_lng" value="{{$order->cod_lng}}" id="cod_lng">
                            </div>
                            <div class="col-md-6">
                                <b><legend>Invoice</legend></b>
                                <div class="col-md-6">
                                @if ($order->payment_type != 2)
                                    @if ($order->status != 0 && $order->status != 5)
                                    Invoice {{$order->order_number}} has send to customer
                                    @else
                                    Not Available yet
                                    @endif
                                @else
                                    No invoice for this one
                                @endif
                                </div>
                            </div>
                        </div>
                        <br><br>
                        <div class="row">
                            <div class="col-md-6">
                                <b><legend>Address Information</legend></b>
                                @if ($address != null)
                                    <div>{{$order->address->address}}</div>
                                    <div>{{$address['province']}}</div>
                                    <div>{{$address['district']}}</div>
                                    <div>{{$address['city']}}, {{$order->address->postal_code}}</div>
                                @else
                                    @if($order->payment_type == PaymentType::CASH_ON_DELIVERY)
                                    <div class="col-md-5">COD Address</div>
                                    <div class="col-md-7">{{$order->cod_address}}</div>
                                    <div class="col-md-12" id="codMap" style="height: 100px; margin-bottom: 10px"></div>
                                    @endif
                                @endif
                                @if($order->additional_note)
                                    <div class="col-md-5">Additional Note </div>
                                    <div class="col-md-7">{{$order->additional_note}}</div>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <b><legend>Shipment</legend></b>
                                <div class="col-md-6">Tracking Number : @if (count($existShipment) != 0) {{$existShipment->tracking_number}} @else Not Available yet @endif </div>
                            </div>
                        </div>
                        <br><br>
                        <div class="row">
                            <legend>Item Ordered</legend>
                            <table class="table table-striped table-hover fullTable">
                                <thead>
                                    <tr>
                                        <th>Photo</th>
                                        <th>SKU</th>
                                        <th>Product</th>
                                        <th>Size</th>
                                        <th>Color</th>
                                        <th>Qty</th>
                                        <th>Weight</th>
                                        <th>Price</th>
                                        <th>Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($order->orderDetails as $value)
                                    <tr>
                                        <td><img src="{{ asset($value->product->product_image) }}" class="img-responsive" style="width: 80px; height: 80px;" /></td>
                                        <td>{{$value->sku}}</td>
                                        <td>{{$value->product->name}}</td>
                                        <td>
                                        @if($value->size == 'null')
                                        -
                                        @else
                                        {{$value->size}}
                                        @endif
                                        </td>
                                        <td>
                                        @if($value->color == 'null')
                                        -
                                        @else
                                        <div style="background: {{$value->color}}; color: {{$value->color}}">. </div></td>
                                        @endif
                                        <td>{{$value->qty}}</td>
                                        <td>{{$value->weight}}</td>
                                        <td>{{number_format($value->price,2,'.',',')}}</td>
                                        <td>{{number_format($value->amount,2,'.',',')}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-md-6"></div>
                            <div class="col-md-6">
                                <legend>Order Totals</legend>
                                <div class="col-md-8">Total</div>
                                <div class="col-md-4">{{number_format($order->orderDetails->sum('amount'), 2,'.',',')}}</div>
                                <div class="col-md-8">Point</div>
                                <div class="col-md-4"> {{number_format($order->point, 2,'.',',')}}
                                </div>
                                <div class="col-md-8">Shipment Fee</div>
                                <div class="col-md-4">{{number_format($order->shipment_fee, 2,'.',',')}}</div>
                                <hr>
                                <div class="col-md-8">Grand Total</div>
                                <div class="col-md-4">
                                    <b>{{number_format($order->grand_total, 2,'.',',')}}</b>
                                </div>

                            </div>
                        </div>
                    </fieldset>
                </div>
               {{--  <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-success"><span class="fa fa-save"></span> Save</button>
                </div> --}}
            </div>

            <!-- Modal -->
            <div>
                <div id="shipment" class="modal fade warning">
                    <div class="modal-dialog">
                        <form id="modal-form" action="/" method="post" class="form-horizontal" enctype="multipart/form-data">
                            <div class="modal-dialog modal-sm">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                        <h4 class="modal-title txt-center" id="myModalLabel"><b>Form Shipment</b></h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row mg-bot-20" style="margin-left:15px; margin-right:15px;">
                                            <div class="">
                                                {{ Form::label('tracking_number', 'Tracking Number')}}
                                                {{ Form::text('tracking_number', $shipment->tracking_number, array('class' => 'form-control','id' => 'tracking_number','data-parsley-required' => 'true','style'=>'text-transform: uppercase'))}}
                                            </div>
                                        </div>
                                        <div class="row" style="margin-left:15px; margin-right:15px;">    
                                            <div class="shipping-date">
                                                {{ Form::label('shipping_date', 'Shipping Date')}}
                                                {{ Form::text('shipping_date', $shipment->shipping_date, array('class' => 'dateonlypicker form-control', 'id' => 'shipping_date', 'placeholder' => ' Shipping Date','data-parsley-required' => 'true'))}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="addShipment({{$id}})">Yes</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div id="completed-order" class="modal fade warning">
                    <div class="modal-dialog">
                        <form id="modal-form" action="/" method="post" class="form-horizontal" enctype="multipart/form-data">
                            <div class="modal-dialog modal-sm">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                        <h4 class="modal-title txt-center" id="myModalLabel"><b>Complete this order</b></h4>
                                    </div>
                                    <input type="hidden" name="type" value="completed">
                                    <div class="modal-body alert alert-info">
                                        Are you sure want to complete this order?
                                        <p></p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="changeStatus('completed', {{$id}})">Yes</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div id="confirm-payment" class="modal fade warning">
                    <div class="modal-dialog">
                        <form id="modal-form" action="/" method="post" class="form-horizontal" enctype="multipart/form-data">
                            <div class="modal-dialog modal-sm">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                        <h4 class="modal-title txt-center" id="myModalLabel"><b>Confirm Payment</b></h4>
                                    </div>
                                    <div class="modal-body alert alert-info">
                                        Are you sure want to confirm this confirmation?
                                        <p></p>
                                    </div>
                                    <input type="hidden" name="type" value="confirmPayment">
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-primary" data-dismiss="modal" onclick="changeStatus('confirmPayment', {{$id}})">Yes</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div id="cancel-order" class="modal fade warning">
                    <div class="modal-dialog">
                        <form id="modal-form" action="/" method="post" class="form-horizontal" enctype="multipart/form-data">
                            <div class="modal-dialog modal-m">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                        <h4 class="modal-title txt-center" id="myModalLabel"><b>Cancel this Order</b></h4>
                                    </div>
                                    <input type="hidden" name="type" value="cancelOrder">
                                    <div class="modal-body">
                                        <div class="alert alert-danger">
                                            Are you sure want to cancel this order?
                                            <p></p>
                                        </div><br>
                                        {{ Form::label('remark', 'Remark')}}
                                        {{ Form::textarea('remark',null, array('class' => 'form-control','id' => 'remark','data-parsley-required' => 'true'))}}

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="changeStatus('cancelOrder', {{$id}})">Yes</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var storageLat = $('#cod_lat').val();
    var storageLng = $('#cod_lng').val();
    console.log(storageLat);
    console.log(storageLng);
    function myMap() {
        var mapProp = {
            center: new google.maps.LatLng(storageLat,storageLng),
            zoom:15,
        };

        var map = new google.maps.Map(document.getElementById("codMap"), mapProp);
        var marker = new google.maps.Marker({
            title: 'Click to zoom',
        });
        
        map.addListener('click', function(event) {
            geoCoder(event.latLng);
            marker.setMap(map);
            marker.setPosition(event.latLng);
        });
        var infoWindow = new google.maps.InfoWindow;

        infoWindow.setPosition(new google.maps.LatLng(storageLat,storageLng));
        infoWindow.setContent('COD Here!');
        infoWindow.open(map);
    }

</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCSlAK_5cMtJS9jDISX0WIG3VDnWpQiUjk&callback=myMap"></script>
<script type="text/javascript">
    function addShipment($id) {
        var tracking_number = document.getElementById('tracking_number').value;
        var shipping_date = document.getElementById('shipping_date').value;
        if (tracking_number == '' || shipping_date == '') {
            document.getElementById('message').innerHTML = '<div class="alert alert-warning">Please fill all the field</div>';
        } else {
            // $("#loading-wrapper").show(2000);
            // $('<div class="modal-brackdrop"></div>').appendTo(document.body);
            $.ajax({
                headers: {
                    'X-CSRF-Token': $('input[name="_token"]').val()
                },
                url: "{{ url('/admin/orders/add/shipment') }}",
                method: "PUT",
                data: { tracking_number : tracking_number,
                    shipping_date : shipping_date,
                    id : $id
                },
                success: function (data) {
                    // $(".modal-brackdrop").remove();
                    // $("#loading-wrapper").hide(2000);
                    window.location.reload();
                }
            }).error(function (error) {
                console.log("error");
            });
        }
    }
    function changeStatus($type,$id) {
        var remark = document.getElementById('remark').value;
        // $("#loading-wrapper").show(2000);
        // $('<div class="modal-brackdrop"></div>').appendTo(document.body);
        $.ajax({
            headers: {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            url: "{{ url('/admin/orders/change/status') }}",
            method: "PUT",
            data: { type : $type,
                remark : remark,
                id : $id
            },
            success: function (data) {
                // $(".modal-brackdrop").remove();
                // $("#loading-wrapper").hide(2000);
                window.location.reload();
            }
        }).error(function (error) {
            console.log("error");
        });
    }
    setTimeout(function() {
        $('.alert').fadeOut('fast');
    }, 5000);
</script>
@endsection
