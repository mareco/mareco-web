@extends('layouts.dashboard')
@extends('layouts.dashboard_side_bar')
@include('layouts.dashboard_header_right')
@extends('shared.datatables')
@include('includes.modal_delete')
@section('content')
@include('includes.page_header_index')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <a href="{{route('admin.orders.new')}}" class="btn btn-xs btn-primary pull-right"><span class="fa fa-plus"></span> Add</a>
                <h4>Order</h4>
            </div>
            <div class="panel-body">
                <table class="table table-bordered datatable" id="order-list" width="100%">
                    <thead>
                        <tr>
                            <th>Order No</th>
                            <th>Products</th>
                            <th>Name</th>
                            <th>Payment Type</th>
                            <th>Status</th>
                            <th>Grand Total</th>
                            <th>Order Date</th>
                            <th>Action</th>
                        </tr>
                        <tr id="filter-row">
                            <th style="text-align: center">Order No</th>
                            <th style="text-align: center">Products</th>
                            <th style="text-align: center">Name</th>
                            <th style="text-align: center">Payment Type</th>
                            <th style="text-align: center">Status</th>
                            <th style="text-align: center">Grand Total</th>
                            <th style="text-align: center">Order Date</th>
                            <th style="text-align: center"></th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@push('pageRelatedJs')
<script type="text/javascript">
    $(document).ready(function () {
        var orderDataTable = $('#order-list').DataTable(
        {
            dom: "lrtip",
            orderCellsTop: true,
            responsive: true,
            serverSide: true,
            order: [[5, "desc"]],
            ajax: {
                url:  "{{ route('admin.orders.list') }}",
                data: { '_token' : '{{csrf_token() }}'},
                type: 'POST',
            },
            columns: [
            
            { data: 'order_number', name: 'order_number',"className": 'text-center', },
            { data: 'product_id', name: 'product_id',"className": 'text-center', },
            { data: 'user_id', name: 'user.first_name',"className": 'text-center', },
            { data: 'payment_type', name: 'payment_type',"className": 'text-center', },
            { data: 'status', name: 'status',"className": 'text-center', },
            { data: 'grand_total', name: 'grand_total',"className": 'text-center', },
            { data: 'created_at', name: 'created_at',"className": 'text-center', },
            { data: 'action', name: 'action',"className": 'text-center', orderable: false, searchable: false  },
            ],
        }); 

        $("#order-list thead tr#filter-row th").each(function (index) { 
            var column = $(this).text();

            switch(column){ 
                case "Order No": 
                case "Products": 
                case "Name": 
                case "Address": 
                var input = '<input type="text" class="form-control" placeholder="Search By ' + column + '" />'; 

                $(this).html(input); 
                break; 

                case "Status": 
                var select = '{{ Form::select("status", $status, null, array("id" => "status", "class" => "select-input form-control", "style" => "width:165px")) }}' 

                $(this).html(select); 
                break; 

                case "Payment Type": 
                var select = '{{ Form::select("paymentType", $paymentType, null, array("id" => "paymentType", "class" => "select-input form-control")) }}' 

                $(this).html(select); 
                break; 
            } 
        });

        orderDataTable.columns().every(function() {
            var that = this;
            $('input', this.footer()).on('keyup change', function() {
                if (that.search() !== this.value) {
                    that
                    .search(this.value)
                    .draw();
                }
            });
            $('select', this.footer()).on('keyup change', function() {
                if (that.search() !== this.value) {
                    that
                    .search(this.value)
                    .draw();
                }
            });
        });

        $(".select-input").prepend('<option value="" selected="selected">All</option>');
        

    });
</script>
@endpush