<?php

use Carbon\Carbon;

?>

@extends('layouts.dashboard')
@extends('layouts.dashboard_side_bar')
@section('content')
@include('includes.page_header_form')
@include('includes.modal_delete_table')


{!! Form::open(['url' => route('admin.orders.save'),'method' => 'POST','data-parsley-validate' => 'true']) !!}
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">

            <p style="display: none;" id="countRow"></p>
            <p style="display: none;" id="totalWeight">{{$totalWeight}}</p>
            <p style="display: none;" id="totalPrice">{{$totalPrice}}</p> 

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-inverse panel-with-tabs mg-bot-0">
                        <div class="panel-heading p-0">
                            <div class="panel-heading-btn m-r-10 m-t-10">
                                <a href="{{ URL::previous() }}" class="btn btn-xs btn-primary pull-right"><span class="fa fa-reply"></span> Back</a>
                            </div>

                        </div>
                        <p id="message"></p>
                        <div class="panel-body">
                            <div class="" id="">
                                @include('backend.order.order_detail')
                            </div>
                            <button class="btn btn-sm btn-success mg-bot-20 fl-right" type="button" onclick="addProductWeight()">Add Selected Product to Order</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-12">
                        <table class="table">
                            @if(count($cart) != 0)
                            <tr>
                                <td><b>Product</b></td>
                                <td><b>Size</b></td>
                                <td><b>Color</b></td>
                                <td><b>Price</b></td>
                                <td><b>Weight</b></td>
                                <td><b>Quantity</b></td>
                                <td><b>Action</b></td>
                            </tr>
                            @foreach ($cart as $key)
                            <tr>
                                <td>{{$key->name}}</td>
                                <td>{{$key->options->variant}}</td>
                                <td><span class="detail-color" style="background-color:{{$key->options->color}}; "></span></td>
                                <td>{{number_format($key->price)}}</td>
                                <td>{{$key->options->weight}}</td>
                                <td>{{$key->qty}}</td>
                                <td><a href='{{url("admin/orders/delete/cart/$key->rowId")}}' class="color-delete">Delete</a>
                                </td>
                            </tr>
                            @endforeach
                            @endif
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-inverse">
            <div class="panel-body pd-top-0">
                <div class="modal-body pd-top-0">
                    <fieldset>
                        {{ csrf_field() }}
                        <div class="row">
                            <p id="messageCost"></p>
                            <legend>Create Order</legend>
                            <div class="form-group col-md-3">
                                {{ Form::label('user_id', 'Select Customer')}}
                                <select class="form-control" name="user_id" id="customer">
                                    <option selected="" hidden="" value="none">Select Customer</option>
                                    @foreach ($user as $key)
                                    <option value="{{$key->id}}">{{$key->first_name}} {{$key->last_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-3" id="existAddress">
                                {{ Form::label('address_id', 'Customer Address')}}
                                <select class="form-control" name="address_id" id="customerAddress">
                                    <option selected="" hidden="" value="none">Customer Address</option>
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-2">
                                <span><a id="createAddress" class="cursor-pointer point link" onclick="createAddress()">Create new address</a><a id="hideAddress" class="cursor-pointer point link" onclick="hideAddress()">Hide Address</a></span>
                            </div>
                            <br>
                            <div class="col-md-2">
                                <span>
                                    <a id="useCOD" class="cursor-pointer point link" onclick="useCOD()">Use COD</a>
                                    <a id="hideCOD" class="cursor-pointer point link" onclick="hideCOD()">Hide COD</a>
                                </span>
                            </div>
                        </div>

                        <div class="row" id="address">
                            <legend>Create New Address</legend>
                            <div class="form-group col-md-3">
                                <label for="province">Province</label>
                                <select class="form-control" name="province" id="province">
                                    <option selected="" hidden="" value="none">Select Province</option>
                                    @foreach ($province as $value)
                                    <option value="{{ $value['province_id'] }}">
                                        {{ $value['province'] }}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="city">City</label>
                                <select class="form-control" name="city" id="city">
                                    <option selected="" hidden="" value="none">Select City</option>
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="province">District</label>
                                <select class="form-control" name="district" id="district">
                                    <option selected="" hidden="" value="none">Select District</option>
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="postal_code">Zip / Postal Code</label>
                                <input type="text" class="form-control" id="postal_code" value="" name="postal_code" placeholder="Input postal code here">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="address">Address</label>
                                <textarea class="form-control" rows="5" id="street" name="street"></textarea>
                            </div>
                        </div>

                        <div class="row">
                            <legend></legend>
                            <input type="hidden" name="serviceType" id="serviceType" value="">
                            <input type="hidden" name="serviceFee" id="serviceFee" value="">
                            <input type="hidden" id="customer_lat" name="lat">
                            <input type="hidden" id="customer_lng" name="lng">
                            <input type="hidden" id="distance_rate" name="rate" required="required">
                            <div class="shipping-method" id="hideShippingMethod">
                                @if ($shippingMethod->jne == 1)                        
                                <div class="form-group" id="optionJNE">
                                    <label class="disable" id="jneNotAvailable"><input type="radio" name="shippingRadio" disabled="">JNE - Not Available</label>
                                    <label id="jneAvailable"><input type="radio" name="shippingRadio" value="0">JNE - Available</label>
                                </div>
                                @endif
                                @if ($shippingMethod->pos == 1)
                                <div class="form-group" id="optionPOS">
                                    <label class="disable" id="posNotAvailable"><input type="radio" name="shippingRadio" disabled="">POS - Not Available</label>
                                    <label id="posAvailable"><input type="radio" name="shippingRadio" value="1">POS - Available</label>
                                </div>
                                @endif
                                @if ($shippingMethod->tiki == 1)
                                <div class="form-group" id="optionTIKI">
                                    <label class="disable" id="tikiNotAvailable"><input type="radio" name="shippingRadio" disabled="">TIKI - Not Available</label>
                                    <label id="tikiAvailable"><input type="radio" name="shippingRadio" value="2">TIKI - Available</label>
                                </div>
                                @endif
                                <!-- <button type="button" onclick="checkService('click')" class="btn btn-danger mg-bot-20" id="checkShipment">Check</button> -->
                            </div> 
                            <div id="cod_map" style="display: none;">
                                <div class="form-group">
                                    <input type="text" name="cod_address" class="form-control" id="searchmap" placeholder="Enter a address" autocomplete="off">
                                </div>
                                <div>
                                    <div id="map-canvas" style="height:280px;width:100%;margin-bottom:20px;"></div>
                                    <input type="hidden" class="form-control" id="searchmap">
                                    {!! Form::hidden('latitude', '1.0828276', ['class' => 'form-control', 'id' => 'lat']) !!}
                                    {!! Form::hidden('longitude', '104.03045350000002', ['class' => 'form-control', 'id' => 'lng']) !!}
                                </div>
                                <p id="amountCOD"></p>
                            </div>
                        </div>

                        <div class="row" id="paymentType">
                            <legend>Choose Payment Type</legend>
                            <div class="payment-method">
                                <div class="form-group">
                                  <label><input type="radio" name="paymentRadio" value="bank">Bank Transfer</label>
                                  <div class="bank bank-box">
                                   @if (count($bank) != 0)
                                   @foreach ($bank as $value)
                                   <div class="logo-bank">
                                    @if ($value->photo == null)
                                    <img src="{{ asset('assets/global/image/No-Picture-Available.jpg')}}" width="50" /> 
                                    @else
                                    <img src="{{asset($value->photo)}}" width="50" >
                                    @endif  
                                    <span>{{$value->bank}} / {{$value->account_name}} / {{$value->account_number}}</span>
                                </div>
                                @endforeach 
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                          <label><input type="radio" name="paymentRadio" value="paypal">Paypal</label>
                      </div>
                  </div>
              </div>
          </fieldset>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-sm btn-success" id="buttonSave"><span class="fa fa-save"></span> Save</button>
    </div>
</div>
</div>
</div>
</div>
{{ Form::close() }}
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCyB6K1CFUQ1RwVJ-nyXxd6W0rfiIBe12Q&libraries=places" type="text/javascript"></script>
<script type="text/javascript">
@include('includes.js.cod_admin');

    $("form").bind("keypress", function (e) {
        if (e.keyCode == 13) {
            $("#buttonSave").attr('value');
            //add more buttons here
            return false;
        }
    });
    $('#jneAvailable').hide();
    $('#posAvailable').hide();
    $('#tikiAvailable').hide();
    $('#address').hide();
    $('#hideAddress').hide();
    $('#existAddress').hide();
    $('#hideCOD').hide();
    $('#cod_map').hide();

    function resetShipmentMethod() {
        $('#selectJNE').remove();
        $('#selectPOS').remove();
        $('#selectTIKI').remove();
        $('#jneAvailable').hide();
        $('#posAvailable').hide();
        $('#tikiAvailable').hide();
        $('#jneNotAvailable').show();
        $('#posNotAvailable').show();
        $('#tikiNotAvailable').show();
        return "Success reset the shipment method";
    }

    function createAddress() {
        $('#address').show();
        $('#hideAddress').show();
        $('#createAddress').hide();
        $('#existAddress').hide();
    }

    function useCOD() {
        $('#hideShippingMethod').hide();
        $('#existAddress').hide();
        $('#useCOD').hide();
        $('#hideCOD').show();
        $('#cod_map').show();
        $('#paymentType').hide();
        initialize();
    }

    function hideCOD() {
        $('#hideShippingMethod').show();
        $('#existAddress').show();
        $('#useCOD').show();
        $('#hideCOD').hide();
        $('#cod_map').hide();
        $('#paymentType').show();
    }

    function hideAddress() {
        $('#address').hide();
        $('#hideAddress').hide();
        $('#createAddress').show();
        $('#existAddress').show();
    }

    function addProductWeight() {
        product_id  = document.getElementById('product').value;
        size_id = document.getElementById('size').value;
        color = document.getElementById('color').value;
        price = document.getElementById('price').value;
        weight = document.getElementById('weight').value;
        qty = document.getElementById('qty').value;

        $.ajax({
            headers: {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            url: "{{ route('admin.orders.add.cart') }}",
            method: "GET",
            data: { product_id : product_id,
                size_id : size_id,
                color : color,
                price : price,
                weight : weight,
                qty : qty},
                success: function (data) {
                    if (data.message == '') {
                        window.location.reload();
                    } else {
                        document.getElementById('message').innerHTML = data.message;
                        setTimeout(function() {
                            $('.alert').fadeOut('fast');
                        }, 5000)
                    }
                }
            }).error(function (error) {
                console.log("error");
            });
        }

        function checkService() {
            serviceJNE = JSON.parse(localStorage.getItem('serviceJNE'));
            shipmentFeeJNE = JSON.parse(localStorage.getItem('shipmentFeeJNE'));
            servicePOS = JSON.parse(localStorage.getItem('servicePOS'));
            shipmentFeePOS = JSON.parse(localStorage.getItem('shipmentFeePOS'));
            serviceTIKI = JSON.parse(localStorage.getItem('serviceTIKI'));
            shipmentFeeTIKI = JSON.parse(localStorage.getItem('shipmentFeeTIKI'));
            if ($('#selectJNE')[0] != undefined) {
                if ($('#selectJNE')[0].value != "Select a service") {
                    for (var i = 0; i < serviceJNE.length; i++) {
                        if (serviceJNE[i] == $('#selectJNE')[0].value) {
                            service = serviceJNE[i];
                            shipmentFee = shipmentFeeJNE[i];
                        }
                    }
                }
            }
            if ($('#selectPOS')[0] != undefined) {
               if ($('#selectPOS')[0].value != "Select a service") {
                for (var i = 0; i < servicePOS.length; i++) {
                    if (servicePOS[i] == $('#selectPOS')[0].value) {
                        service = servicePOS[i];
                        shipmentFee = shipmentFeePOS[i];
                    }
                }
            }
        }
        if ($('#selectTIKI')[0] != undefined) {
           if ($('#selectTIKI')[0].value != "Select a service") {
            for (var i = 0; i < serviceTIKI.length; i++) {
                if (serviceTIKI[i] == $('#selectTIKI')[0].value) {
                    service = serviceTIKI[i];
                    shipmentFee = shipmentFeeTIKI[i];
                }
            }
        }
    }
    valueJNE = null;
    valuePOS = null;
    valueTIKI = null;
    if ($('#selectJNE')[0] != undefined) {
        valueJNE = $('#selectJNE')[0].value;
    }
    if ($('#selectPOS')[0] != undefined) {
        valuePOS = $('#selectPOS')[0].value;
    }
    if ($('#selectTIKI')[0] != undefined) {
        valueTIKI = $('#selectTIKI')[0].value;
    }
    if (valueJNE == "Select a service" && valuePOS == "Select a service" && valueTIKI == "Select a service") {
        $('#message')[0].innerHTML = '<div class="alert alert-warning">We do not have shipment for this place</div>';
    }
    else {
        document.getElementById('serviceType').value = service;
        document.getElementById('serviceFee').value = shipmentFee;
    }
}

$(document).ready(function(){
    $('.select-product').change(function($event) {
        product_id = $($event.currentTarget)[0].value; 
        console.log(product_id);
        return getSize($event);
    });
    $('.select-size').change(function($event) {
        size_id = $($event.currentTarget)[0].value; 
        return getColor($event);
    });
    $('.select-color').change(function($event) {
        bg_color = $($event.currentTarget)[0].value; 
        return getDataProduct($event);
    });


    var getColor = function() {
        console.log(size_id, product_id);
        $.ajax({
            headers: {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            url: "{{ route('admin.orders.color') }}",
            method: "GET",
            data: { product_id : product_id,
                    size_id : size_id},
            success: function (data) {
                console.log(data);
                $('#color').empty();
                var select = document.getElementById('color');
                select.options.length = 0;
                options = data.variant_product;
                select.options.add(new Option("Select Color"));
                var dataOption = [];
                for (var i = 0; i < data.count; i++) {
                  option = options[i];
                    if (option.color != null) {
                        dataOption += '<option value="'+option.color+'" style="background-color:'+option.color+'">'+option.color+'</option>';
                    } else {
                        dataOption += '<option value="'+option.color+'" style="background-color:'+option.color+'">Free Color</option>';
                    }
              }
              $('#color').append('<option hidden>Select a color</option>' + dataOption);
          }
      }).error(function (error) {
        console.log("error");
    });
};

    var getSize = function() {
        $.ajax({
            headers: {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            url: "{{ route('admin.orders.size') }}",
            method: "GET",
            data: { product_id : product_id},
            success: function (data) {
                var select = document.getElementById('size');
                select.options.length = 0;
                options = data.variant_product;
                select.options.add(new Option("Select Size"));
                for (var i = 0; i < data.variant_product.length; i++) {
                  option = options[i];
                  select.options.add(new Option(data.variant_product[i], data.variant_product[i]));
              }
          }
      }).error(function (error) {
        console.log("error");
    });
};
    var getDataProduct = function() {
        console.log(bg_color);
        $('#color').css({
            'background-color': bg_color
        });
        $.ajax({
            headers: {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            url: "{{ route('admin.orders.product') }}",
            method: "GET",
            data: { product_id: product_id,
                    size_id : size_id},
            success: function (data) {
                console.log(data);
                document.getElementById('price').value = data.variant_product.price;
                document.getElementById('weight').value = data.variant_product.weight;
            }
        }).error(function (error) {
            console.log("error");
        });
    };

$('#customer').change(function($event) {
    user_id = $($event.currentTarget)[0].value; 
    return getDataAddress($event);
});

$('#province').change(function($event) {
    province_id = $($event.currentTarget)[0].value; 
    resetShipmentMethod();
    $('#postal_code').val("");
    return getDataCity($event);
});

$('#city').change(function($event) {
    city_id = $($event.currentTarget)[0].value; 
    resetShipmentMethod();
    $('#postal_code').val("");
    return getDataDistrict($event);
});

$('#district').change(function($event) {
    district_id = $($event.currentTarget)[0].value; 
    address_id = null;
    resetShipmentMethod();
    return getDataCost($event);
});

$('#customerAddress').change(function($event) {
    address_id = $($event.currentTarget)[0].value; 
    district_id = null;
    city_id = null;
    resetShipmentMethod();
    return getDataCost($event);
});

    // $('.select-product').change(function($event) {
    //     console.log($($event.currentTarget));
    //     console.log("EF");
    //     variant_id = $($event.currentTarget)[0].value;
    //     return getDataVariant();
    // });



$('.shipping-method input[type="radio"]').click(function(){
    var inputValue = $(this).attr("value");
    var targetBox = $("." + inputValue);
    console.log(targetBox);
    $(".col-box").not(targetBox).hide();
    $(targetBox).show();
    if ($('#selectJNE')[0] != undefined) {
        document.getElementById('selectJNE').value = 'Select a service';
    }
    if ($('#selectPOS')[0] != undefined) {
        document.getElementById('selectPOS').value = 'Select a service';
    }
    if ($('#selectTIKI')[0] != undefined) {
        document.getElementById('selectTIKI').value = 'Select a service';
    }
    document.getElementById('serviceType').value = "";
    document.getElementById('serviceFee').value = "";
});

$('.payment-method input[type="radio"]').click(function(){
    var inputValue = $(this).attr("value");
    var targetBox = $("." + inputValue);
    $(".bank-box").not(targetBox).hide();
    $(targetBox).show();
});

var getDataDistrict = function() {
    $.ajax({
        headers: {
            'X-CSRF-Token': $('input[name="_token"]').val()
        },
        url: "{{ route('admin.orders.get.district') }}",
        method: "GET",
        data: { city_id : city_id},
        success: function (data) {
            var select = document.getElementById('district');
            console.log(data);
            select.options.length = 0;
            options = data.district;
            select.options.add(new Option("Select District"));
            for (var i = 0; i < data.count; i++) {
              option = options[i];
              select.options.add(new Option(option.subdistrict_name, option.subdistrict_id));
          }
      }
  }).error(function (error) {
    console.log("error");
});
};

var getDataCity = function() {
    $.ajax({
        headers: {
            'X-CSRF-Token': $('input[name="_token"]').val()
        },
        url: "{{ route('admin.orders.get.city') }}",
        method: "GET",
        data: { province_id : province_id},
        success: function (data) {
            console.log(data);
            var select = document.getElementById('city');
            var selectDistrict = document.getElementById('district');
            select.options.length = 0;
            selectDistrict.options.length = 1;
            options = data.city;
            select.options.add(new Option("Select City"));
            for (var i = 0; i < data.count; i++) {
              option = options[i];
              select.options.add(new Option(option.city_name, option.city_id));
          }
      }
  }).error(function (error) {
    console.log("error");
});
};

var getDataAddress = function() {
    $.ajax({
        headers: {
            'X-CSRF-Token': $('input[name="_token"]').val()
        },
        url: "{{ route('admin.orders.get.address') }}",
        method: "GET",
        data: { user_id : user_id},
        success: function (data) {
            console.log(data);
            if (data.count != 0) {
                $('#existAddress').show();
                var select = document.getElementById('customerAddress');
                select.options.length = 0;
                options = data.address;
                select.options.add(new Option("Please select one...",'none'));
                for (var i = 0; i < data.count; i++) {
                  option = options[i];
                  select.options.add(new Option(option.address, option.id));
              }
          } else {
            message = '<div class="alert alert-warning">You still not have a address, Please create the address first</div>';
            document.getElementById('messageCost').innerHTML = message;
            setTimeout(function() {
                $('.alert').fadeOut('fast');
            }, 5000)
        }
    }
}).error(function (error) {
    console.log("error");
});
};

function getDataCost() {
    var weight = parseInt(document.getElementById('totalWeight').innerHTML);
    var total = parseInt(document.getElementById('totalPrice').innerHTML);
    if (weight == 0) {
        $('#messageCost')[0].innerHTML = '<div class="alert alert-warning">Please select the product first</div>';
        setTimeout(function() {
            $('.alert').fadeOut('fast');
        }, 5000)
    } else {
        $.ajax({
            headers: {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            url: "{{ route('admin.orders.get.cost') }}",
            method: "GET",
            data: { district_id : district_id,
                city_id : city_id,
                address_id : address_id,
                type : "normal",
                weight : weight,
                total : total},
                success: function (data) {
                    console.log(data);
                    if (data.countJNE == 0 && data.countPOS == 0 && data.countTIKI == 0) {
                        document.getElementById('#messageCost').innerHTML = '<div class="alert alert-warning">We do not have shipment for this place</div>';
                        $('#checkShipment').hide();
                    }
                    if (data.countJNE != 0) {
                        $('#jneNotAvailable').hide();
                        $('#jneAvailable').show();
                        var optionJNE = '';
                        if (data.costJNE[0].costs[0] == undefined) {
                            $('#tikiNotAvailable').show();
                            $('#tikiAvailable').hide();
                        }
                        else {
                            localStorage.removeItem('serviceJNE');
                            localStorage.removeItem('shipmentFeeJNE');
                            var dataServiceJNE = [];
                            var feeServiceJNE = [];
                            for (var i = 0; i < data.countJNE; i++) {
                                var service = data.costJNE[0].costs[i].service;
                                var etd = data.costJNE[0].costs[i].cost[0].etd;
                                var price = data.costJNE[0].costs[i].cost[0].value;
                                if (etd != "") {
                                    dataServiceJNE.push(service);
                                    feeServiceJNE.push(price);
                                    optionJNE += '<option value=' + service + '>' + service + ' (Estimate : ' + etd + ') (Price : ' + price.toLocaleString('en-US') + ')' + '</option>'; 
                                }
                            }
                            localStorage.setItem('serviceJNE',JSON.stringify(dataServiceJNE));
                            localStorage.setItem('shipmentFeeJNE',JSON.stringify(feeServiceJNE));
                            $('#optionJNE').append('<select class="0 col-box form-control" id="selectJNE" onchange="checkService()">' + '<option hidden>Select a service</option>' + optionJNE + '</select>');
                        }
                    }
                    if (data.countPOS != 0) {
                        $('#posNotAvailable').hide();
                        $('#posAvailable').show();
                        var optionPOS = '';
                        if (data.costPOS[0].costs[0] == undefined) {
                            $('#tikiNotAvailable').show();
                            $('#tikiAvailable').hide();
                        }
                        else {
                            localStorage.removeItem('servicePOS');
                            localStorage.removeItem('shipmentFeePOS');
                            var dataServicePOS = [];
                            var feeServicePOS = [];
                            for (var i = 0; i < data.countPOS; i++) {
                                var service = data.costPOS[0].costs[i].service;
                                var etd = data.costPOS[0].costs[i].cost[0].etd;
                                var price = data.costPOS[0].costs[i].cost[0].value;
                                if (etd != "") {
                                    if (service != "Express Sameday Dokumen" && service != "Surat Kilat Khusus" && service != "Express Next Day Dokumen") {
                                        dataServicePOS.push(service);
                                        feeServicePOS.push(price);
                                        optionPOS += '<option value=' + JSON.stringify(service) + '>' + service + ' (Estimate : ' + etd + ') (Price : ' + price.toLocaleString('en-US') + ')' + '</option>';
                                    }
                                }
                            }
                            localStorage.setItem('servicePOS',JSON.stringify(dataServicePOS));
                            localStorage.setItem('shipmentFeePOS',JSON.stringify(feeServicePOS));
                            $('#optionPOS').append('<select class="1 col-box form-control" id="selectPOS" onchange="checkService()">' + '<option hidden>Select a service</option>' + optionPOS + '</select>');
                        }
                    }
                    if (data.countTIKI != 0) {
                        $('#tikiNotAvailable').hide();
                        $('#tikiAvailable').show();
                        var optionTIKI = '';
                        if (data.costTIKI[0].costs[0] == undefined) {
                            $('#tikiNotAvailable').show();
                            $('#tikiAvailable').hide();
                        }
                        else {
                            localStorage.removeItem('serviceTIKI');
                            localStorage.removeItem('shipmentFeeTIKI');
                            var dataServiceTIKI = [];
                            var feeServiceTIKI = [];
                            for (var i = 0; i < data.countTIKI; i++) {
                                var service = data.costTIKI[0].costs[i].service;
                                var etd = data.costTIKI[0].costs[i].cost[0].etd;
                                var price = data.costTIKI[0].costs[i].cost[0].value;
                                if (etd != "") {
                                    dataServiceTIKI.push(service);
                                    feeServiceTIKI.push(price);
                                    optionTIKI += '<option value=' + service + '>' + service + ' (Estimate : ' + etd + ') (Price : ' + price.toLocaleString('en-US') + ')' + '</option>';
                                }
                            }
                            localStorage.setItem('serviceTIKI',JSON.stringify(dataServiceTIKI));
                            localStorage.setItem('shipmentFeeTIKI',JSON.stringify(feeServiceTIKI));
                            $('#optionTIKI').append('<select class="2 col-box form-control" id="selectTIKI" onchange="checkService()">' + '<option hidden>Select a service</option>' + optionTIKI + '</select>');
                        }
                    }
                }
            }).error(function (data) {
                console.log(data);
            });
        }
    };

    $("#addOrderDetailRow").on('click', function(e){
        e.preventDefault();
        e.stopPropagation();
        addRow('orderDetail');
    });

    $('#orderDetail').on('click', '.delRow', function(e){
        e.preventDefault();
        e.stopPropagation();
        $(this).closest('tr').remove();
        rearrange('orderDetail');
    }).on('click', '.delete', function(e){
        e.preventDefault();
        e.stopPropagation();
        var parentTr = $(this).closest('tr');
        deleteRow(parentTr, 'orderDetail', this);

    });
});
</script>
@endsection
@push('pageRelatedJs')

@endpush