@extends('layouts.dashboard')
@extends('layouts.dashboard_side_bar')
@include('layouts.dashboard_header_right')
@extends('shared.datatables')
@include('includes.modal_delete')
@section('content')
@include('includes.page_header_index')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <a href="{{ route('admin.admins.new') }}" class="btn btn-sm btn-primary pull-right"><span class="fa fa-plus"></span> Add</a>
                <h4>Admin List</h4>
            </div>
            <div class="panel-body">
                <table class="table table-bordered datatable" id="admin-list" width="100%">
                    <thead>
                        <tr>
                            <th>Photo</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Point</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    <tfoot>
                        <tr id="filter-row">
                            <th></th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Point</th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@push('pageRelatedJs')
<script type="text/javascript">
    $(document).ready(function () {
        var adminDataTable = $('#admin-list').DataTable(
        {
            dom: "lrtip",
            orderCellsTop: true,
            responsive: true,
            serverSide: true,
            ajax: {
                url:  "{{ route('admin.admins.list') }}",
                data: { '_token' : '{{csrf_token() }}'},
                type: 'POST',
            },
            columns: [
            { data: 'photo', name: 'photo', 'className': 'text-left', 
            'render': function (data, type, full, meta) {
                if (!data) return '';
                return '<img src='+data+' class="table-img index-data-image" style="width: 130px; "/>';
            }
        },
        { data: 'first_name', name: 'first_name',"className": 'text-center', },
        { data: 'last_name', name: 'last_name',"className": 'text-center', },
        { data: 'email', name: 'email',"className": 'text-center', },
        { data: 'phone', name: 'phone',"className": 'text-center', },
        { data: 'point', name: 'point',"className": 'text-center', },
        { data: 'action', name: 'action',"className": 'text-center', orderable: false, searchable: false  },
        ],
    });
        // $('#search').keypress(function (e) {
        //     if (e.which == 13) {
        //         var admin = $(this).val();
        //         adminDataTable.search(admin).draw();
        //     }
        // });
        $("#admin-list tfoot tr#filter-row th").each(function (index) { 
            var column = $(this).text();

            switch(column){ 
                case "First Name": 
                case "Last Name": 
                case "Email": 
                case "Phone": 
                case "Point": 
                var input = '<input type="text" class="form-control" width="100%" placeholder="Search By ' + column + '" />'; 

                $(this).html(input); 
                break; 
            } 
        });

        adminDataTable.columns().every(function() {
            var that = this;
            $('input', this.footer()).on('keyup change', function() {
                if (that.search() !== this.value) {
                    that
                    .search(this.value)
                    .draw();
                }
            });
        });

    });
</script>
@endpush
