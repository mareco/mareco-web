@extends('layouts.dashboard')
@extends('layouts.dashboard_side_bar')
@include('layouts.dashboard_header_right')
@section('content')
@include('includes.page_header_form')

@if (isset($user))
{!! Form::model($user, [
        'url' => route('admin.admins.update', ['user' => $user]),
        'method' => 'PUT',
        'data-parsley-validate' => 'true',
        'enctype'=>'multipart/form-data'
    ]) !!}
@else
{!! Form::open([
        'url' => route('admin.admins.save'),
        'method' => 'POST',
        'data-parsley-validate' => 'true',
        'enctype'=>'multipart/form-data'
    ]) !!}
@endif
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="{{ URL::previous() }}" class="btn btn-xs btn-primary pull-right"><span class="fa fa-reply"></span> Back</a>
                </div>
                <h4>Admin Registration Form</h4>
            </div>
            <div class="panel-body">
                <div class="modal-body">
                    <fieldset>
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="form-group col-md-1">
                                {{ Form::label('photo', 'Photo')}}
                            </div>
                            <div class="form-group col-md-5">
                                @if (isset($user))
                                <img src="{{ $user->photo }}" class="img-responsive img-center" id="previewImage" alt="Image" style="width: 150px;">
                                @else
                                <img src="{{ asset('assets/global/img/img-preview.png') }}" class="img-responsive img-center" id="previewImage" alt="Image" style="width: 150px;">
                                @endif
                                 <input type='file' name='photo' id="imageSelect" />
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="form-group col-md-1">
                                {{ Form::label('first_name', 'First Name')}}<span style="color: red">*</span>
                            </div>
                            <div class="form-group col-md-5">
                                {{ Form::text('first_name', null, array('class' => 'form-control', 'placeholder' => 'First Name','data-parsley-required' => 'true'))}}
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="form-group col-md-1">
                                {{ Form::label('last_name', 'Last Name')}}
                            </div>
                            <div class="form-group col-md-5">
                                {{ Form::text('last_name', null, array('class' => 'form-control', 'placeholder' => 'Last Name','data-parsley-required' => 'true'))}}
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="form-group col-md-1">
                                {{ Form::label('email', 'Email')}}<span style="color: red">*</span>
                            </div>
                            <div class="form-group col-md-5">
                                {{ Form::email('email', null, array('class' => 'form-control', 'placeholder' => 'Email','data-parsley-required' => 'true'))}}
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                        @if(!isset($user))
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="form-group col-md-1">
                                {{ Form::label('password', 'Password')}}<span style="color: red">*</span>
                            </div>
                            <div class="form-group col-md-5">
                                {{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'Password','data-parsley-required' => 'true'))}}
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                        @endif
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="form-group col-md-1">
                                {{ Form::label('phone', 'Phone No.')}}
                            </div>
                            <div class="form-group col-md-5">
                                {{ Form::number('phone', null, array('class' => 'form-control', 'placeholder' => 'Phone No.','data-parsley-required' => 'true'))}}
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                    </fieldset>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-success"><span class="fa fa-save"></span> Save</button>
                </div>
            </div>
        </div>
    </div>
</div>
{{ Form::close() }}
@endsection
@push('pageRelatedJs')
<script type="text/javascript">
    $(document).ready(function(){
        console.log('Hi');

        function readURL(input) {
            console.log('Read URL');
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    console.log(e);
                    $('#previewImage').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imageSelect").change(function(){
            console.log('Change!');
            readURL(this);
        });
    });
</script>
@endpush