@extends('layouts.dashboard')
@extends('layouts.dashboard_side_bar')
@include('layouts.dashboard_header_right')
@section('content')
@include('includes.page_header_form')

@if (isset($province))
{!! Form::model($province, [
        'url' => route('admin.provinces.update', ['province' => $province]),
        'method' => 'PUT',
        'data-parsley-validate' => 'true',
        'enctype'=>'multipart/form-data'
    ]) !!}
@else
{!! Form::open([
        'url' => route('admin.provinces.save'),
        'method' => 'POST',
        'data-parsley-validate' => 'true',
        'enctype'=>'multipart/form-data'
    ]) !!}
@endif

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="{{ URL::previous() }}" class="btn btn-xs btn-primary pull-right"><span class="fa fa-reply"></span> Back</a>
                </div>
                <h4>Province Registration Form</h4>
            </div>
            
            <div class="panel-body">
                <div class="modal-body">
                    <fieldset>
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="form-group col-md-6">
                                {{ Form::label('name', 'Province Name')}}
                                {{ Form::text('name', null, array('class' => 'form-control', 'placeholder' => 'Province Name','data-parsley-required' => 'true'))}}
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                    </fieldset>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-success"><span class="fa fa-save"></span> Save</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endsection
