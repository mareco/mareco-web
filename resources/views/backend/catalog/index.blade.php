@extends('layouts.dashboard')
@extends('layouts.dashboard_side_bar')
@include('layouts.dashboard_header_right')
@extends('shared.datatables')
@section('content')
@include('includes.page_header_index')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4>Product Catalog</h4>
            </div>
            <div class="panel-body">
                <table class="table table-bordered datatable" id="catalog-list" width="100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Thumbnail</th>
                            <th>Code</th>
                            <th>Name</th>
                            <th>Category</th>
                            <th>Price</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    <tfoot>
                        <tr id="filter-row">
                            <th></th>
                            <th></th>
                            <th>Code</th>
                            <th>Name</th>
                            <th>Category</th>
                            <th></th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalCatalog" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content red-style">
            <div class="modal-header">
                <h2 class="modal-title">Catalog Setting</h2>
                <a class="modal-close-round" data-dismiss="modal">
                    <svg class="" viewBox="0 0 24 24"><path d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"></path><path d="M0 0h24v24h-24z" fill="none"></path></svg>
                </a>
            </div>
            <div class="modal-body bd-wish-list">
                <div class="col-md-12" id="productStatus"> 
                    <select class="form-control mg-bot-15" id="statusSetting" name="status">
                    </select>
                </div>
                <div id="showSale">
                    <div class="col-md-6"> 
                        <select class="form-control" name="sale_type" id="saleType">
                        </select>
                    </div>
                    <div class="col-md-6" id=""> 
                        <input type="text" class="form-control" name="sale_amount" placeholder="Enter Amount" id="saleAmount">
                    </div>
                </div>
            </div>
            <p style="display:none;" id="idProduct"></p>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" onclick="updateSetting()">Update</button>
            </div>
        </div>  
    </div>
</div>
<script type="text/javascript">
    function settingCatalog($event) {
        $('#statusSetting').empty();
        $('#saleType').empty();
        var code = $($event.currentTarget).parent().parent()[0].children[2].innerHTML;
        $.ajax({
            headers: {
                'X-CSRF-Token': '{{csrf_token() }}'
            },
            url: "{{ route('admin.catalog.setting') }}",
            method: "GET",
            data: { code : code},
            success: function (data) {
                console.log(data);
                $('#modalCatalog').modal();
                document.getElementById('idProduct').innerHTML = data.product.id;
                var optionStatus = '';
                for (var i = 0; i < data.productStatus.length; i++) {
                    if (data.product.status == i) {
                        optionStatus += '<option value='+i+' selected="selected">'+ data.productStatus[i] +'</option>';
                    } else {
                        optionStatus += '<option value='+i+'>'+ data.productStatus[i] +'</option>';
                    }
                }
                var optionSaleType = '';
                for (var i = 0; i < data.saleType.length; i++) {
                    if (data.product.sale_type == i) {
                        optionSaleType += '<option value='+i+' selected="selected">'+ data.saleType[i] +'</option>';
                    } else {
                        optionSaleType += '<option value='+i+'>'+ data.saleType[i] +'</option>';
                    }
                }
                $('#statusSetting').append(optionStatus);
                $('#saleType').append(optionSaleType);

                if ($('#statusSetting').val() == "2") {
                    $('#showSale').show();
                    document.getElementById('saleAmount').value = data.product.sale_amount;
                } else {
                    $('#showSale').hide();
                }
            }
        }).error(function (error) {
            console.log("error");
        });
    }

    var statusSetting = $('#statusSetting');
    statusSetting.on('change', function(){
        console.log(this.value);
        if (this.value == "2") {
            $('#showSale').show();
        } else {
            $('#showSale').hide();
        }
    });

    if ($('#statusSetting').val() == "2") {
        $('#showSale').show();
    } else {
        $('#showSale').hide();
    }

    function updateSetting($event) {
        var id = document.getElementById('idProduct').innerHTML;
        var statusSetting = document.getElementById('statusSetting').value;
        var saleType = document.getElementById('saleType').value;
        var saleAmount = document.getElementById('saleAmount').value;
        console.log(saleAmount);
        $.ajax({
            headers: {
                'X-CSRF-Token': '{{csrf_token() }}'
            },
            url: "{{ route('admin.catalogs.update') }}",
            method: "PUT",
            data: { id : id,
                    statusSetting : statusSetting,
                    saleType : saleType,
                    saleAmount : saleAmount},
            success: function (data) {
                window.location.reload();
            }
        }).error(function (error) {
            console.log("error");
        });
    }
    
</script>
@endsection
@push('pageRelatedJs')
<script type="text/javascript">
    $(document).ready(function () {
        var catalogDataTable = $('#catalog-list').DataTable(
        {
            dom: "lrtip",
            orderCellsTop: true,
            responsive: true,
            processing: true,
            serverSide: true,
            ajax: {
                url:  "{{ route('admin.catalogs.list') }}",
                data: { '_token' : '{{csrf_token() }}'},
                type: 'POST',
            },
            columns: [
            {"data": "id",
            render: function (data, type, row, meta) {
                return meta.row + meta.settings._iDisplayStart + 1;
            }},

            { data: 'product_image', name: 'product_image', 'className': 'text-left', 
            'render': function (data, type, full, meta) {
                if (!data) return '';
                return '<img src='+data+' style="width: 73px; "/>';
            }},
            { data: 'code', name: 'code',"className": 'text-center', },
            { data: 'name', name: 'name',"className": 'text-center', },
            { data: 'category_name', name: 'category_id',"className": 'text-center', },
            { data: 'variant_price', name: 'variants.price',"className": 'text-center', },
            { data: 'status', name: 'status',"className": 'text-center', },
            { data: 'action', name: 'action',"className": 'text-center', orderable: false, searchable: false  },
            ],
        });

        $("#catalog-list tfoot tr#filter-row th").each(function (index) { 
            var column = $(this).text();

            switch(column){ 
                case "Code": 
                var input = '<input type="text" class="form-control" style="width: 100px" placeholder="Search By ' + column + '" />'; 

                $(this).html(input); 
                break; 

                case "Category": 
                var select = '{{ Form::select("category_id", $categoryList, null, array("id" => "category_id", "class" => "select-input form-control")) }}' 

                $(this).html(select); 
                break; 

                case "Name": 
                var inputName = '<input type="text" class="form-control" style="width: 200px" placeholder="Search By ' + column + '" />'; 

                $(this).html(inputName); 
                break; 

                case "Status": 
                var select = '{{ Form::select("status", $productStatus, null, array("id" => "status", "class" => "select-input form-control")) }}' 

                $(this).html(select); 
                break; 

            } 
        });

        catalogDataTable.columns().every(function() {
            var that = this;
            $('input', this.footer()).on('keyup change', function() {
                if (that.search() !== this.value) {
                    that
                    .search(this.value)
                    .draw();
                }
            });
            $('select', this.footer()).on('keyup change', function() {
                if (that.search() !== this.value) {
                    that
                    .search(this.value)
                    .draw();
                }
            });
        });
        $(".select-input").prepend('<option value="" selected="selected">All</option>');
    });
</script>
@endpush
