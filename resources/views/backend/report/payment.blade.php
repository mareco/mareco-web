<?php 
use App\Helpers\Enums\OrderStatus;
use App\Helpers\Enums\PaymentType;

?>

<html>
<h3>Data Payment</h3>
<table>
	</tr>
	<tr>
		<th>Order Number</th>
		<th>Customer</th>
		<th>Type Payment</th>
		<th>Date Payment</th>
		<th>Total</th>
		<th>Status</th>
	</tr>
	@foreach($order as $value)
	<tr>
		<td>{{$value->order_number}}</td>
		<td>{{$value->user->first_name.''.$value->user->last_name}}</td>
		<td>{{PaymentType::getString($value->payment_type)}}</td>
		<td>{{$value->date_payment}}</td>
		<td>{{$value->grand_total}}</td>
		<td>{{OrderStatus::getString($value->status)}}</td>
	</tr>
	@endforeach
</table>
</html>