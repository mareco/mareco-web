@extends('layouts.dashboard')
@extends('layouts.dashboard_side_bar')
@include('layouts.dashboard_header_right')
@extends('shared.datatables')
@include('includes.modal_delete')
@section('content')
@include('includes.page_header_index')
<?php

use App\Helpers\ReportType;

?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4>Select Report</h4>
            </div>
            <div class="panel-body">
                {!! Form::open([
                    'url' => route('report.generate'),
                    'method' => 'POST',
                    'data-parsley-validate' => 'true',
                    'id'  => 'formGenerate'
                    ]) !!}
                <fieldset>
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="form-group col-md-6">
                            {{ Form::label('report_type', 'Report Type')}}
                            {{ Form::select('report_type', ReportType::getArray(), null, array('class' => 'form-control select2me', 'placeholder' => 'Please select one...','data-parsley-required' => 'true'))}}
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                    <div class="row" id="filterDate">
                        <div class="col-md-3"></div>
                        <div class="form-group col-md-3">
                            {{ Form::label('start_date', 'Start Date')}}
                            {{ Form::text('start_date', null, array('class' => 'form-control dateonlypicker', 'placeholder' => 'Start Date'))}}
                        </div>
                        <div class="form-group col-md-3">
                            {{ Form::label('end_date', 'End Date')}}
                            {{ Form::text('end_date', null, array('class' => 'form-control dateonlypicker', 'placeholder' => 'End Date'))}}
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                    <div class="row" id="filterPayment">
                        <div class="col-md-3"></div>
                        <div class="form-group col-md-3">
                            <select class="form-control" name="typePayment">
                                <option value="cod">COD</option>
                                <option value="bank">Bank Transfer</option>
                                <option value="paypal">Paypal</option>
                            </select>                            
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="modal-footer">
                <a type="submit" id="btnGenerate" class="btn btn-sm btn-success"><span class="fa fa-save"></span> Generate Report</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
@push('pageRelatedJs')
<script type="text/javascript">
    $('#filterDate').hide();
    $('select[name="report_type"]').on('change', function(){
        if($(this).val() == 0 || $(this).val() == 3 || $(this).val() == 4) {
            $('#filterDate').hide();
        } else if($(this).val() == 1 || $(this).val() == 2) {
            $('#filterDate').show();
        }
        if($(this).val() != 4 ) {
            $('#filterPayment').hide();
        } else if($(this).val() == 4) {
            $('#filterPayment').show();
        }
    });
    if($('select[name="report_type"]').val() == 0 || $('select[name="report_type"]').val() == 3 ) {
        $('#filterDate').hide();
    }else if($('select[name="report_type"]').val() == 1 || $('select[name="report_type"]').val() == 2 ) {
        $('#filterDate').show();
    }

    $('select[name="report_type"]').trigger('change');
    $('#btnGenerate').on('click', function(e){
        $('#formGenerate').submit();
        setTimeout(function(){ window.location.reload(); }, 2000);
    })
</script>
@endpush