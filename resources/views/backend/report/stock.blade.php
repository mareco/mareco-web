<html>
<h3>Stock</h3>
<table>
	<tr>
		<th>Product</th>
		<th>Sku</th>
		<th>Size</th>
		<th>Weight</th>
		<th>Stock Qty</th>
	</tr>
	@foreach($products as $product)
	@foreach($product->productVariants as $value)
	<tr>
		<td>{{$product->name}}</td>
		<td>{{$value->sku}}</td>
		<td>{{$value->size}}</td>
		<td>{{$value->weight}}</td>
		<td>{{$value->qty}}</td>
	</tr>
	@endforeach
	@endforeach
</table>
</html>