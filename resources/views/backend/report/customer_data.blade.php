<html>
	<h3>Data Customer</h3>
	<table>
		<tr>
			<th>First Name</th>
			<th>Last Name</th>
			<th>Email</th>
			<th>Role</th>
			<th>Phone</th>
			<th>Total Order</th>
			<th>Grand Total</th>
		</tr>
		@foreach ($customer as $value)
			<tr>
				<td>{{$value->first_name}}</td>
				<td>{{$value->last_name}}</td>
				<td>{{$value->email}}</td>
				<td>{{$value->role}}</td>
				<td>{{$value->phone}}</td>
				<td>{{$value->count}}</td>
				<td>{{number_format($value->grand_total,0)}}</td>
			</tr>
		@endforeach
	</table>

</html>