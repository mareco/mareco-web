<?php 
use App\Helpers\Enums\OrderStatus;
use App\Helpers\Enums\PaymentType;

?>

<html>
<h3>Data Order</h3>
<table>
	<tr>
		<th>Start Date {{$start_date}}</th>
		<th>End Date {{$end_date}}</th>
	</tr>
	<tr>
		<th>Order Number</th>
		<th>Customer</th>
		<th>Type Payment</th>
		<th>Shipping Method</th>
		<th>Total</th>
		<th>Status</th>
	</tr>
	@foreach($order as $value)
	<tr>
		<td>{{$value->order_number}}</td>
		<td>{{$value->user->first_name.''.$value->user->last_name}}</td>
		<td>{{PaymentType::getString($value->payment_type)}}</td>
		<td>{{$value->shipment_type}}</td>
		<td>{{$value->grand_total}}</td>
		<td>{{OrderStatus::getString($value->status)}}</td>
	</tr>
	@endforeach
</table>
</html>