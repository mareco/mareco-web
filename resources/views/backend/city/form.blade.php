@extends('layouts.dashboard')
@extends('layouts.dashboard_side_bar')
@include('layouts.dashboard_header_right')
@section('content')
@include('includes.page_header_form')

@if (isset($city))
{!! Form::model($city, [
        'url' => route('admin.cities.update', ['city' => $city]),
        'method' => 'PUT',
        'data-parsley-validate' => 'true',
        'enctype'=>'multipart/form-data'
    ]) !!}
@else
{!! Form::open([
        'url' => route('admin.cities.save'),
        'method' => 'POST',
        'data-parsley-validate' => 'true',
        'enctype'=>'multipart/form-data'
    ]) !!}
@endif

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="{{ URL::previous() }}" class="btn btn-xs btn-primary pull-right"><span class="fa fa-reply"></span> Back</a>
                </div>
                <h4>City Registration Form</h4>
            </div>
            
            <div class="panel-body">
                <div class="modal-body">
                    <fieldset>
                        {{ csrf_field() }}
                        <div class="row">
                           <div class="row">
                                <div class="form-group col-md-6">
                                {{ Form::label('province_id', 'Province')}}
                                {{ Form::select('province_id', $listOfProvinces, null, array('class' => 'select2me form-control', 'placeholder' => 'Please select one...','data-parsley-required' => 'true'))}}
                            </div>
                            <div class="form-group col-md-6">
                                {{ Form::label('name', 'City')}}
                                {{ Form::text('name', null, array('class' => 'form-control', 'placeholder' => 'City','data-parsley-required' => 'true'))}}
                            </div>
                        </div>
                        </div>
                    </fieldset>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-success"><span class="fa fa-save"></span> Save</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endsection
