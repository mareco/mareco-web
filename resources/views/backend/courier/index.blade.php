@extends('layouts.dashboard')
@extends('layouts.dashboard_side_bar')
@include('layouts.dashboard_header_right')
@include('includes.modal_delete')
@section('content')
@include('includes.page_header_index')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <a href="{{route('admin.couriers.new')}}" class="btn btn-xs btn-primary pull-right"><span class="fa fa-plus"></span> Add</a>
                <h4>Courier</h4>
            </div>

            <div class="panel-body">
                <table class="table table-striped table-hover fullTable">
                    <thead>
                        <tr>
                            <th>Logo</th>
                            <th>Courier</th>
                            <th colspan="2">Action</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($couriers as $value)
                        <tr>
                            <td><img src="{{ ($value->logo) }}" style="width: 145px;"></td>
                            <td>{{$value->name}}</td>
                            <td>
                                <a href="{{ route('admin.couriers.edit', [$value->id]) }}" class="btn btn-info"><i class="fa fa-edit"></i> Edit</button>
                            </td>
                            <td>
                                <a data-url="{{ route('admin.couriers.destroy', [$value->id]) }}" class="btn btn-warning fa fa-trash" data-title="Confirmation" data-message="This will delete the selected couriers. Do you want to continue?" data-toggle="modal" data-target="#modalDelete"> Delete</button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
