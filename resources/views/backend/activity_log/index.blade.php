@extends('layouts.dashboard')
@extends('layouts.dashboard_side_bar')
@include('layouts.dashboard_header_right')
@extends('shared.datatables')
@include('includes.modal_delete')
@section('content')
@include('includes.page_header_index')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4>Activity Log</h4>
            </div>
            <div class="panel-body">
                <table class="table table-bordered datatable" id="log-list" width="100%">
                    <thead>
                        <tr>
                            <th>Type</th>
                            <th>Action</th>
                            <th>Description</th>
                            <th>User</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    <tfoot>
                        <tr id="filter-row">
                            <th>Type</th>
                            <th>Action</th>
                            <th>Description</th>
                            <th>User</th>
                            <th>Date</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@push('pageRelatedJs')
<script type="text/javascript">
    $(document).ready(function () {
        var logDataTable = $('#log-list').DataTable(
        {
            dom: "lrtip",
            orderCellsTop: true,
            responsive: true,
            processing: true,
            serverSide: true,
            ajax: {
                url:  "{{ route('admin.log.list') }}",
                data: { '_token' : '{{csrf_token() }}'},
                type: 'POST',
            },
            columns: [
        { data: 'type', name: 'type',"className": 'text-center', },
        { data: 'action', name: 'action',"className": 'text-center', },
        { data: 'description', name: 'description',"className": 'text-center', },
        { data: 'user_id', name: 'user_id',"className": 'text-center' },
        { data: 'created_at', name: 'created_at',"className": 'text-center' },
        ],
    });

        $("#log-list tfoot tr#filter-row th").each(function (index) { 
            var column = $(this).text();

            switch(column){ 
                case "Type": 
                case "Action": 
                case "Description":
                case "User": 
                case "Date":
                var input = '<input type="text" class="form-control" style="width:210px" placeholder="Search By ' + column + '" />'; 

                $(this).html(input); 
                break; 
            } 
        });

        logDataTable.columns().every(function() {
            var that = this;
            $('input', this.footer()).on('keyup change', function() {
                if (that.search() !== this.value) {
                    that
                    .search(this.value)
                    .draw();
                }
            });
        });

    });
</script>
@endpush