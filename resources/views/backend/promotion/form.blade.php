@extends('layouts.dashboard')
@extends('layouts.dashboard_side_bar')
@include('layouts.dashboard_header_right')
@section('content')
@include('includes.page_header_form')
@include('includes.modal_promotion')

<?php 

use App\Helpers\Enums\PromotionType;



?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="{{ URL::previous() }}" class="btn btn-xs btn-primary pull-right"><span class="fa fa-reply"></span> Back</a>
                </div>
                <legend>Promotion</legend>
            </div>
            {!! Form::open(['url' => route('admin.promotions.save'),'method' => 'POST','enctype'=>'multipart/form-data']) !!}
            <div class="panel-body">
                <div class="modal-body">
                    <fieldset>
                        {{ csrf_field() }}
                        <h4> <b>Promotion</b></h4>
                        <div class="row">
                            <div class="form-group col-md-6">
                                {{ Form::label('product_id', 'Product')}}
                                {{ Form::select('product_id', $promoProduct, null, array('class' => 'form-control', 'placeholder' => 'Please select one...'))}}
                            </div>
                            @if(count($promoProduct) == 0)
                            <p style="color: red">Only product with status stock will show</p>
                            @endif
                            <div class="form-group col-md-6">
                                {{ Form::label('title', 'Title')}}
                                {{ Form::text('title', null, array('class' => 'form-control', 'placeholder' => 'Title Promotion'))}}
                            </div>
                        </div>  
                        <div class="row">
                            <div class="form-group col-md-3">
                                {{ Form::label('start_date', 'Start Date')}}
                                {{ Form::text('start_date', null, array('class' => 'form-control dateonlypicker', 'placeholder' => 'Start Date Promotion'))}}
                            </div>
                            <div class="form-group col-md-3">
                                {{ Form::label('end_date', 'End Date')}}
                                {{ Form::text('end_date', null, array('class' => 'form-control dateonlypicker', 'placeholder' => 'End Date Promotion'))}}
                            </div>
                            <div class="form-group col-md-6">
                                {{ Form::label('content', 'Content')}}
                                {{ Form::text('content', null, array('class' => 'form-control', 'placeholder' => 'Content Promotion'))}}
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-3">
                                {{ Form::label('font_color', 'Font Color')}}
                                {{ Form::text('font_color', null, array('class' => 'form-control input-group colorpicker-component', 'data-format' => 'hex', 'placeholder' => 'Font Color','readonly'))}}
                                <span class="input-group-addon"> <i class="color-preview" style="width: 100%;"></i> </span>
                            </div>

                            <div class="form-group col-md-3">
                                {{ Form::label('bg_color', 'Background Color')}}
                                {{ Form::text('bg_color', null, array('class' => 'form-control input-group colorpicker-component', 'data-format' => 'hex', 'placeholder' => 'Background Color','readonly'))}}
                                <span class="input-group-addon"> <i class="color-preview" style="width: 100%;"></i> </span>
                            </div>
                            <div class="form-group col-md-3">
                                {{ Form::label('discount_type', 'Promotion Type')}}
                                {{ Form::select('discount_type', $promotionType, null, array('class' => 'form-control', 'placeholder' => 'Please select one...'))}}
                            </div>
                            <div class="form-group col-md-3">
                                {{ Form::label('discount_value', 'Promotion Value')}}
                                {{ Form::number('discount_value', null, array('class' => 'form-control', 'placeholder' => 'Promotion Value'))}}
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                {{ Form::label('photo', 'Photo')}}
                                <img src="{{ asset('assets/global/img/img-preview.png') }}" class="img-responsive img-center" id="previewImage" alt="Image" style="width: 160px;">
                                <input type='file' name='photo' id="imageSelect" />
                            </div>
                            <div class="form-group col-md-6">
                                {{ Form::label('remark', 'Remark for Notification')}}
                                {{ Form::text('remark', null, array('class' => 'form-control', 'placeholder' => 'Remark for Mobile Notification'))}}
                            </div>
                        </div>
                        <!-- <div class="row">
                            <div class="form-group col-md-6">
                                {{ Form::label('size', 'Photo Size')}}
                                <div class="bs-example">
                                    <div class="make-switch is-radio switch-small" data-text-label="Big" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Size of the image is HALF of the screen!" data-original-title="Information!">
                                        <input type="radio" name="size" value="0" checked />
                                    </div>
                                    <div class="make-switch is-radio switch-small" data-text-label="Medium" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Size of the image is 2/4 of the screen!" data-original-title="Information!">
                                        <input type="radio" name="size" value="2"/>
                                    </div>
                                    <div class="make-switch is-radio switch-small" data-text-label="Small" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Size of the image is 1/4 of the screen!" data-original-title="Information!">
                                        <input type="radio" name="size" value="1"/>
                                    </div>
                                </div>
                            </div>
                        </div>  -->
                        <button type="submit" class="pull-right btn btn-sm btn-success btn-config">
                            <span class="fa fa-save"></span> Save
                        </button>
                    </fieldset>
                </div>
            </div>   
            {{ Form::close() }}
            @if(isset($promotions) && count($promotions) != 0)
            <div class="panel-body">
                <div class="modal-body">
                    <fieldset>
                        {{ csrf_field() }}
                        <h4> <b>Currently Active Promotion</b></h4>
                        <table class="table" width="100%">
                            <thead>
                                <tr>
                                    <th>Product</th>
                                    <th>Title</th>
                                    <th>Content</th>
                                    <th>Photo</th>
                                    <th>Promotion Date</th>
                                    <th>Promotion</th>
                                    <th>Notification</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($promotions as $promotion)
                                <tr>
                                    <td>
                                        {{$promotion->product->name}}
                                    </td>
                                    <td>
                                        {{$promotion->title}}
                                    </td>
                                    <td>
                                        {{$promotion->content}}
                                    </td>
                                    <td>
                                        <img src="{{($promotion->photo) }}" style="width: 200px; height: 200px">
                                    </td>
                                    <td>
                                        {{$promotion->start_date}} - {{$promotion->end_date}}
                                    </td>
                                    <td>
                                        {{$promotion->discount_value}} - {{  PromotionType::getString($promotion->discount_type) }}
                                    </td>
                                    <td>
                                       {{ $promotion->remark }}
                                    </td>
                                    <td>
                                        <a data-url="{{ route('admin.promotions.destroy', [$promotion->id]) }}"
                                            data-toggle="modal" data-target=" #modalPromotion" data-title="Confirmation" data-message="Would you like to delete this promotion?" 
                                            class="btn btn-danger btn-outline btn-sm" >
                                            <span class="fa fa-trash-o"></span> Delete
                                        </a>
                                        <a href="{{ route('admin.promotions.push', [$promotion->id]) }}"
                                            class="btn btn-warning btn-outline btn-sm">
                                            <span class="fa fa-edit"></span> Notification
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </fieldset>
                </div>
            </div>   
            @endif
        </div>
    </div>
</div>
{{-- <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCSlAK_5cMtJS9jDISX0WIG3VDnWpQiUjk&callback=myMap"></script> --}}
@endsection
@push('pageRelatedJs')
<script type="text/javascript">


    $('#modalPromotion').on('shown.bs.modal', function(e) {
        e.stopPropagation();
        var id = $(e.relatedTarget).data('id');
        var parentTr = $(e.relatedTarget).closest('tr');
        var title = $(e.relatedTarget).data('title');
        var message = $(e.relatedTarget).data('message');
        var action = $(e.relatedTarget).data('url');
        var table_name = $(e.relatedTarget).data('table-name');
        var method = $(e.relatedTarget).data('method') == undefined ? 'DELETE' : $(e.relatedTarget).data('method');
        $('#modal_id_delete').val(id);
        $('.modal-header h4').text(title);
        $('.modal-body p').text(message);
        $('#modalPromotion form').attr('action',action).on("submit", function(e){
            e.preventDefault();
            e.stopPropagation();
            var that = this;
            $.ajax({
                url: action,
                type: method,
                data: $(that).serialize() ,
                success: function(result) {
                    $("#modalPromotion").modal('hide')
                    if (typeof table_name === "string") {
                        $(table_name).DataTable().ajax.reload();
                    }
                    $.gritter.add( 
                    {  
                        title: '<i class="entypo-check"></i> Promotion deleted!',  
                        sticky: false,  
                        time: ""
                    } 
                    ); 
                    location.reload();
                }
            });
        });
    }).on('hidden.bs.modal', function (e) {
        e.stopPropagation();
        $('#modal_id_delete').val('');
        $('.modal-header h4').text('');
        $('.modal-body p').text('');
        $('#modalPromotion form').attr('action','/').off('submit');
    });

    $(document).ready(function(){
        console.log('Hi');

        function readURL(input) {
            console.log('Read URL');
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    console.log(e);
                    $('#previewImage').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imageSelect").change(function(){
            console.log('Change!');
            readURL(this);
        });
    });
</script>
@endpush