@extends('layouts.dashboard')
@extends('layouts.dashboard_side_bar')
@include('layouts.dashboard_header_right')
@extends('shared.datatables')
@include('includes.modal_delete')
@section('content')
@include('includes.page_header_index')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <a href="{{route('admin.sub-categories.new')}}" class="btn btn-sm btn-primary pull-right"><span class="fa fa-plus"></span> Add</a>
                <h4>Sub Category</h4>
            </div>
            <div class="panel-body">
                <table class="table table-bordered datatable" id="subCategory-list" width="100%">
                    <thead>
                        <tr>
                            <th>Category</th>
                            <th>Sub Category</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    <tfoot>
                        <tr id="filter-row">
                            <th>Category</th>
                            <th>Sub Category</th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@push('pageRelatedJs')
<script type="text/javascript">
    $(document).ready(function () {
        var subCategoryDataTable = $('#subCategory-list').DataTable(
        {
            dom: "lrtip",
            orderCellsTop: true,
            responsive: true,
            serverSide: true,
            ajax: {
                url:  "{{ route('admin.sub-categories.list') }}",
                data: { '_token' : '{{csrf_token() }}'},
                type: 'POST',
            },
            columns: [
            { data: 'category_id', name: 'category_id',"className": 'text-center', },
            { data: 'name', name: 'name',"className": 'text-center', },
            { data: 'action', name: 'action',"className": 'text-center', orderable: false, searchable: false  },
            ],
        });

        $("#subCategory-list tfoot tr#filter-row th").each(function (index) { 
            var column = $(this).text();
            switch(column){ 
                case "Sub Category": 
                var input = '<input type="text" class="form-control" width="100%" placeholder="Search By ' + column + '" />'; 

                $(this).html(input); 
                break; 

                case "Category": 
                var select = '{{ Form::select("category_id", $categoryList, null, array("id" => "category_id", "class" => "select-input form-control", "style" => "width:165px")) }}' 

                $(this).html(select); 
                break; 
            } 
        });

        subCategoryDataTable.columns().every(function() {
            var that = this;
            $('input', this.footer()).on('keyup change', function() {
                if (that.search() !== this.value) {
                    that
                    .search(this.value)
                    .draw();
                }
            });
            $('select', this.footer()).on('keyup change', function() {
                if (that.search() !== this.value) {
                    that
                    .search(this.value)
                    .draw();
                }
            });
        });

        $(".select-input").prepend('<option value="" selected="selected">All</option>');

    });
</script>
@endpush