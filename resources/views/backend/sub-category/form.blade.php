@extends('layouts.dashboard')
@extends('layouts.dashboard_side_bar')
@include('layouts.dashboard_header_right')
@section('content')
@include('includes.page_header_form')

@if (isset($subCategory))
{!! Form::model($subCategory, ['url' => route('admin.sub-categories.update', ['subCategory' => $subCategory]),'method' => 'PUT','data-parsley-validate' => 'true','enctype'=>'multipart/form-data']) !!}
@else
{!! Form::open(['url' => route('admin.sub-categories.save'),'method' => 'POST','data-parsley-validate' => 'true','enctype'=>'multipart/form-data'
]) !!}
@endif

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="{{ URL::previous() }}" class="btn btn-xs btn-primary pull-right"><span class="fa fa-reply"></span> Back</a>
                </div>
                <h4>Sub Category Registration Form</h4>
            </div>

            <div class="panel-body">
                <div class="modal-body">
                    <fieldset>
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="form-group col-md-3">
                                {{-- @if(isset($subCategory->photo) && $subCategory->photo != '')
                                <img src="{{($subCategory->photo) }}" style="width: 145px;">
                                @else
                                {{ Form::label('photo', 'Photo')}}<br>
                                <img src="{{ asset('assets/images/nopic.png') }}" style="width: 145px; height: 145px">
                                @endif
                                {{ Form::file('photo', null, array('class' => 'form-control'))}} --}}
                            </div>
                            @if(isset($subCategory))
                            <div class="col-md-3">
                                <div style="margin-top: 122px;" class="make-switch popover-primary" data-on="success" data-off="info" data-toggle="popover" data-text-label="<i class='entypo-info'></i>" data-trigger="hover" data-placement="top" data-content="Toogle this to enable or disable this sub-category!" data-original-title="Information!">
                                    <input type="checkbox" name="is_active" {{$result}}> 
                                </div>
                            </div>
                            @endif
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="form-group col-md-6">
                                {{ Form::label('category_id', 'Category ')}}
                                {{ Form::select('category_id', $listOfCategories, null, array('class' => 'form-control', 'placeholder' => 'Category Name ','data-parsley-required' => 'true'))}}
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="form-group col-md-6">
                                {{ Form::label('name', 'Sub Category Name ')}}
                                {{ Form::text('name', null, array('class' => 'form-control', 'placeholder' => 'Sub Category Name ','data-parsley-required' => 'true'))}}
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                    </fieldset>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-success"><span class="fa fa-save"></span> Save</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endsection
