@extends('layouts.dashboard')
@extends('layouts.dashboard_side_bar')
@include('layouts.dashboard_header_right')
@section('content')
@include('includes.page_header_form')

{!! Form::open(['url' => route('admin.point.save',['user' => $user]),'method' => 'POST']) !!}

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="{{ URL::previous() }}" class="btn btn-xs btn-primary pull-right"><span class="fa fa-reply"></span> Back</a>
                </div>
                <h4>Point Adjustment</h4>
            </div>

            <div class="panel-body">
                <div class="modal-body">
                    <fieldset>
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="form-group col-md-3"></div>
                            <div class="form-group col-md-6">
                                {{ Form::label('point_type', 'Point Type')}}
                                {{ Form::select('point_type', array(''=>'Please Select one...','add' => '+ Addition','ded' => '- Deduction'), null,array('class' => 'form-control','id'=>'pointType'))}}
                            </div>
                            <div class="form-group col-md-3"></div>
                        </div>  
                        <div class="row" id="addition" style="display:none;">
                            <div class="form-group col-md-3"></div>
                            <div class="form-group col-md-6">
                                {{ Form::label('addition', 'Point Value')}}
                                {{ Form::number('addition', null, array('class' => 'form-control','placeholder'=>'Point Value'))}}
                            </div>
                            <div class="form-group col-md-3"></div>
                        </div> 
                        <div class="row" id="deduction" style="display:none;">
                            <div class="form-group col-md-3"></div>
                            <div class="form-group col-md-6">
                                {{ Form::label('deduction', 'Point Value')}}
                                {{ Form::number('deduction', null, array('class' => 'form-control','placeholder'=>'Point Value'))}}
                            </div>
                            <div class="form-group col-md-3"></div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-3"></div>
                            <div class="form-group col-md-6">
                                {{ Form::label('description', 'Point Remark')}}
                                {{ Form::text('description', null, array('class' => 'form-control','placeholder'=>'Point Remark'))}}
                            </div>
                            <div class="form-group col-md-3"></div>
                        </div>   
                    </fieldset>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-success"><span class="fa fa-save"></span> Save</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endsection
@push('pageRelatedJs')
<script type="text/javascript">
    var selectPoint = $('select[name="point_type"]');
    selectPoint.on('change', function(){
        if(selectPoint.val() == 'add') {
            $('#addition').show();
            $('#deduction').hide();
        } else if(selectPoint.val() == 'ded') {
            $('#addition').hide();
            $('#deduction').show();
        } else {
            $('#addition').hide();
            $('#deduction').hide();
        }
    });

</script>
@endpush
