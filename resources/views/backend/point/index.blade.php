@extends('layouts.dashboard')
@extends('layouts.dashboard_side_bar')
@include('layouts.dashboard_header_right')
@extends('shared.datatables')
@include('includes.modal_delete')
@section('content')
@include('includes.page_header_index')
<?php 

use App\Helpers\Enums\PaymentType;


?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h3>{{$user->first_name.' '.$user->last_name}} Detail</h3>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="panel-title">Point History</div>
                            <a href="{{ route('admin.users.point.form', $user) }}" class="btn-sm btn-primary pull-right"> Adjust Point</a>
                        </div>
                        <table class="table table-bordered table-responsive">
                            <thead>
                                <tr>
                                    <th>History</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($pointHistory))
                                @foreach($pointHistory as $point)
                                <tr>
                                    <td> <i>{{ $point->created_at->diffForHumans() }}</i> {{ $point->description }} {{$point->amount}} point</td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <b>Total Point : {{number_format($totalPoint,2,',','.')}}</b><br>
                    {{ $pointHistory->links() }}

                </div>
                <div class="col-sm-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="panel-title">Order History</div>
                        </div>
                        <table class="table table-bordered table-responsive">
                            <thead>
                                <tr>
                                    <th>Order No.</th>
                                    <th>Payment Type</th>
                                    <th>Order Amount</th>
                                    <th>Order Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($orderHistory))
                                @foreach($orderHistory as $order)
                                <tr>
                                    <td>{{ $order->order_number }}</td>
                                    <td>{{ PaymentType::getString($order->payment_type) }}</td>
                                    <td>Rp. {{ number_format($order->grand_total,2,',','.') }}</td>
                                    <td><i>{{ $order->created_at }} ({{$order->created_at->diffForHumans()}})</i></td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <b>Total Lifetime Ordered : Rp {{number_format($lifeTimeOrderByUser,2,',','.')}}</b> <br>
                    {{ $orderHistory->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('pageRelatedJs')
<script type="text/javascript">

</script>
@endpush