@extends('layouts.dashboard')
@extends('layouts.dashboard_side_bar')
@include('layouts.dashboard_header_right')
@extends('shared.datatables')
@include('includes.modal_delete')
@section('content')
@include('includes.page_header_index')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <a href="{{ route('admin.users.new') }}" class="btn btn-sm btn-primary pull-right"><span class="fa fa-plus"></span> Add</a>
                <h4>User List</h4>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-bordered datatable" id="user-list" width="100%">
                        <thead>
                            <tr>
                                <th>Photo</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Point</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                        <tfoot>
                            <tr id="filter-row">
                                <th></th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Point</th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalWishList" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content red-style">
            <div class="modal-header">
                <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                <h2 class="modal-title">Wish List</h2>
                <a class="modal-close-round" data-dismiss="modal">
                   <svg class="" viewBox="0 0 24 24"><path d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"></path><path d="M0 0h24v24h-24z" fill="none"></path></svg>
                </a>
            </div>
            <div class="modal-body bd-wish-list">
                <table class="table table-responsive tb-f14">
                    <thead>
                        <th colspan="2" class="text-center">Product</th>
                        <th class="text-center">Size</th>
                        <th class="text-center">Price</th>
                        <th class="text-center">Saved Date</th>
                    </thead>
                    <tbody class="tb-bd-body" id="appendData">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function getDataVariant($event) {
        $('#appendData').empty();
        var email = $($event.currentTarget).parent()[0].previousSibling.previousSibling.previousSibling.innerHTML;
        $.ajax({
            headers: {
                'X-CSRF-Token': '{{csrf_token() }}'
            },
            url: "{{ route('admin.wishlists.getData') }}",
            method: "GET",
            data: { email : email},
            success: function (variant) {
                if (variant.data.length > 0) {
                    $('#modalWishList').modal();
                    var trVariant = '';
                    for (var i = 0; i < variant.data.length; i++) {
                        var product = variant.product[i].name;
                        var size = variant.data[i].size;
                        var price = variant.data[i].price;
                        var saved_date = variant.product[i].created_at;
                        var image = variant.product[i].product_image;

                        trVariant += '<tr><td class="w-wish-list pd-left-0"><img src='+image+' class="img-responsive h-95 full-width"></td><td>'+product+'</td><td>'+size+'</td><td>Rp.'+price.toLocaleString('en-US')+'</td><td>'+saved_date+'</td></tr>'
                    }
                    $('#appendData').append(trVariant);
                } else {
                    $.gritter.add(
                    {
                        title: ' This user do not have a wish list yet!',
                        // text: '<i class="entypo-check"></i> Request successfully process',
                        sticky: false,
                        time: ""
                    });
                }
            }
        }).error(function (error) {
            console.log("error");
        });
    }
</script>
@endsection
@push('pageRelatedJs')
<script type="text/javascript">
    $(document).ready(function () {
        var userDataTable = $('#user-list').DataTable(
        {
            dom: "lrtip",
            orderCellsTop: true,
            responsive: true,
            serverSide: true,
            ajax: {
                url:  "{{ route('admin.users.list') }}",
                data: { '_token' : '{{csrf_token() }}'},
                type: 'POST',
            },
            columns: [
            { data: 'photo', name: 'photo', 'className': 'text-left',
            'render': function (data, type, full, meta) {
                if (!data) return '';
                return '<img src='+data+' class="table-img index-data-image" style="width: 130px; "/>';
            }
        },
        { data: 'first_name', name: 'first_name',"className": 'text-center', },
        { data: 'last_name', name: 'last_name',"className": 'text-center', },
        { data: 'email', name: 'email',"className": 'text-center', },
        { data: 'phone', name: 'phone',"className": 'text-center', },
        { data: 'point', name: 'point',"className": 'text-center', },
        { data: 'action', name: 'action',"className": 'text-center', orderable: false, searchable: false  },
        ],
    });

        $("#user-list tfoot tr#filter-row th").each(function (index) {
            var column = $(this).text();

            switch(column){
                case "First Name":
                case "Last Name":
                case "Email":
                case "Phone":
                case "Point":
                var input = '<input type="text" class="form-control" width="100%" placeholder="Search By ' + column + '" />';

                $(this).html(input);
                break;
            }
        });

        userDataTable.columns().every(function() {
            var that = this;
            $('input', this.footer()).on('keyup change', function() {
                if (that.search() !== this.value) {
                    that
                    .search(this.value)
                    .draw();
                }
            });
        });

    });
</script>
@endpush
