@extends('layouts.dashboard')
@extends('layouts.dashboard_side_bar')
@include('layouts.dashboard_header_right')
@extends('shared.datatables')
@section('content')
@include('includes.page_header_index')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4>User Feedback</h4>
            </div>
            <div class="panel-body">
                <table class="table table-bordered datatable" width="100%">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Message</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $cservice)
                        <tr>
                            <td>{{$cservice->name}}</td>
                            <td>{{$cservice->email}}</td>
                            <td>{{$cservice->message}}</td>
                            <td>
                                <a href="mailto:{{$cservice->email}}" class="btn btn-red"> Reply</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection