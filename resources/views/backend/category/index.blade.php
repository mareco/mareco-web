@extends('layouts.dashboard')
@extends('layouts.dashboard_side_bar')
@include('layouts.dashboard_header_right')
@extends('shared.datatables')
@include('includes.modal_delete')
@section('content')
@include('includes.page_header_index')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <a href="{{route('admin.categories.new')}}" class="btn btn-sm btn-primary pull-right"><span class="fa fa-plus"></span> Add</a>
                <h4>Category</h4>
            </div>
            <div class="panel-body">
                <table class="table table-bordered datatable" id="category-list" width="100%">
                    <thead>
                        <tr>
                            <th>Photo</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    <tfoot>
                        <tr id="filter-row">
                            <th></th>
                            <th>Name</th>
                            <th>Description</th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@push('pageRelatedJs')
<script type="text/javascript">
    $(document).ready(function () {
        var categoryDataTable = $('#category-list').DataTable(
        {
            dom: "lrtip",
            orderCellsTop: true,
            responsive: true,
            serverSide: true,
            ajax: {
                url:  "{{ route('admin.categories.list') }}",
                data: { '_token' : '{{csrf_token() }}'},
                type: 'POST',
            },
            columns: [
            { data: 'photo', name: 'photo', 'className': 'text-left', 
            'render': function (data, type, full, meta) {
                if (!data) return '';
                return '<img src='+data+' class="table-img index-data-image" style="width: 130px; "/>';
            }
        },
        { data: 'name', name: 'name',"className": 'text-center', },
        { data: 'description', name: 'description',"className": 'text-center', },
        { data: 'action', name: 'action',"className": 'text-center', orderable: false, searchable: false  },
        ],
    });

        $("#category-list tfoot tr#filter-row th").each(function (index) { 
            var column = $(this).text();

            switch(column){ 
                case "Name": 
                case "Description": 
                var input = '<input type="text" class="form-control" width="100%" placeholder="Search By ' + column + '" />'; 

                $(this).html(input); 
                break; 
            } 
        });

        categoryDataTable.columns().every(function() {
            var that = this;
            $('input', this.footer()).on('keyup change', function() {
                if (that.search() !== this.value) {
                    that
                    .search(this.value)
                    .draw();
                }
            });
        });

    });
</script>
@endpush