@extends('layouts.dashboard')
@extends('layouts.dashboard_side_bar')
@include('layouts.dashboard_header_right')
@section('content')
@include('includes.page_header_form')

@if (isset($category))
{!! Form::model($category, ['url' => route('admin.categories.update', ['category' => $category]),'method' => 'PUT','data-parsley-validate' => 'true','enctype'=>'multipart/form-data']) !!}
@else
{!! Form::open(['url' => route('admin.categories.save'),'method' => 'POST','enctype'=>'multipart/form-data']) !!}
@endif

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="{{ URL::previous() }}" class="btn btn-xs btn-primary pull-right"><span class="fa fa-reply"></span> Back</a>
                </div>
                <h4>Category Registration Form</h4>
            </div>

            <div class="panel-body">
                <div class="modal-body">
                    <fieldset>
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="form-group col-md-3">
                                @if(isset($category->photo) && $category->photo != '')
                                <img src="{{($category->photo) }}" style="width: 150px;" id="previewImage">
                                @else
                                {{ Form::label('photo', 'Photo')}}<br>
                                <img src="{{ asset('assets/images/nopic.png') }}" style="width: 150px; height: 150px" id="previewImage">
                                @endif
                                {{-- {{ Form::file('photo', null, array('class' => 'form-control'))}} --}}
                                 <input type='file' name='photo' id="imageSelect" />

                            </div>
                            @if(isset($category))
                            <div class="col-md-3">
                                <div style="margin-top: 122px;" class="make-switch popover-primary" data-on="success" data-off="info" data-toggle="popover" data-text-label="<i class='entypo-info'></i>" data-trigger="hover" data-placement="top" data-content="Toogle this to enable or disable this category!" data-original-title="Information!">
                                    <input type="checkbox" name="is_active" {{$result}}> 
                                </div><br><br>
                                <p>Enable/Disable Category</p>
                            </div>
                            @endif
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <h4>Default Language (English)</h4>
                                {{ Form::label('name', 'Category Name ')}}
                                {{ Form::text('name', null, array('class' => 'form-control', 'placeholder' => 'Category Name '))}}
                            </div>
                            @if(isset($category))
                                @if($category->categoryLang('id'))
                                <div class="form-group col-md-6">
                                    <h4>Alternate Language (Indonesia)</h4>
                                    {{ Form::label('category_name', 'Nama Kategori ')}}
                                    {{ Form::text('category_name', $category->categoryLang('id')->name, array('class' => 'form-control', 'placeholder' => 'Nama Kategori '))}}
                                </div>
                                @else
                                <div class="form-group col-md-6">
                                    <h4>Alternate Language (Indonesia)</h4>
                                    {{ Form::label('category_name', 'Nama Kategori ')}}
                                    {{ Form::text('category_name', null, array('class' => 'form-control', 'placeholder' => 'Nama Kategori '))}}
                                </div>
                                @endif
                            @else
                            <div class="form-group col-md-6">
                                <h4>Alternate Language (Indonesia)</h4>
                                {{ Form::label('category_name', 'Nama Kategori ')}}
                                {{ Form::text('category_name', null, array('class' => 'form-control', 'placeholder' => 'Nama Kategori '))}}
                            </div>
                            @endif
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                {{ Form::label('description', 'Description ')}}
                                {{ Form::textarea('description', null, array('class' => 'form-control', 'placeholder' => 'Description '))}}
                            </div>
                            @if(isset($category))
                                @if($category->categoryLang('id'))
                                <div class="form-group col-md-6">
                                    {{ Form::label('category_description', 'Deskripsi ')}}
                                    {{ Form::textarea('category_description', $category->categoryLang('id')->description, array('class' => 'form-control', 'placeholder' => 'Deskripsi '))}}
                                </div>
                                @else
                                    <div class="form-group col-md-6">
                                    {{ Form::label('category_description', 'Deskripsi ')}}
                                    {{ Form::textarea('category_description', null, array('class' => 'form-control', 'placeholder' => 'Deskripsi '))}}
                                    </div>
                                @endif
                            @else
                            <div class="form-group col-md-6">
                                {{ Form::label('category_description', 'Deskripsi ')}}
                                {{ Form::textarea('category_description', null, array('class' => 'form-control', 'placeholder' => 'Deskripsi '))}}
                            </div>
                            @endif
                        </div>

                    </fieldset>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-success"><span class="fa fa-save"></span> Save</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endsection
@push('pageRelatedJs')
<script type="text/javascript">
    $(document).ready(function(){
        console.log('Hi');

        function readURL(input) {
            console.log('Read URL');
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    console.log(e);
                    $('#previewImage').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imageSelect").change(function(){
            console.log('Change!');
            readURL(this);
        });
    });
</script>
@endpush
