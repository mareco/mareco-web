@extends('layouts.dashboard')
@extends('layouts.dashboard_side_bar')
@include('layouts.dashboard_header_right')
@section('content')
@include('includes.page_header_form')
@include('includes.modal_promotion')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="{{ URL::previous() }}" class="btn btn-xs btn-primary pull-right"><span class="fa fa-reply"></span> Back</a>
                </div>
                <legend>Configuration</legend>
            </div>
            @if($config != '')
            {!! Form::model($config, ['url' => route('admin.configs.update', ['config' => $config]),'method' => 'PUT','data-parsley-validate' => 'true','enctype'=>'multipart/form-data']) !!}
            @else
            {!! Form::open(['url' => route('admin.configs.save'),'method' => 'POST','data-parsley-validate' => 'true','enctype'=>'multipart/form-data']) !!}
            @endif
            <div class="panel-body">
                <div class="modal-body">
                    <fieldset>
                        {{ csrf_field() }}
                        <h4> <b>About Us</b></h4>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <h4>Default Language (English)</h4>
                                {{ Form::label('about_us_title', 'Title')}}
                                {{ Form::text('about_us_title', null, array('class' => 'form-control', 'placeholder' => 'Title About Us'))}}
                            </div>
                            @if(isset($config))
                                @if($config->aboutUsLang('id'))
                                <div class="form-group col-md-6">
                                <h4>Alternate Language (Indonesia)</h4>
                                    {{ Form::label('title_lang', 'Judul')}}
                                    {{ Form::text('title_lang', $config->aboutUsLang('id')->title, array('class' => 'form-control', 'placeholder' => 'Judul'))}}
                                </div>
                                @else
                                <div class="form-group col-md-6">
                                <h4>Alternate Language (Indonesia)</h4>
                                    {{ Form::label('title_lang', 'Judul')}}
                                    {{ Form::text('title_lang', null, array('class' => 'form-control', 'placeholder' => 'Judul'))}}
                                </div>
                                @endif
                            @else
                                <div class="form-group col-md-6">
                                <h4>Alternate Language (Indonesia)</h4>
                                    {{ Form::label('title_lang', 'Judul')}}
                                    {{ Form::text('title_lang', null, array('class' => 'form-control', 'placeholder' => 'Judul'))}}
                                </div>
                            @endif
                        </div>  
                        <div class="row">
                            <div class="form-group col-md-6">
                                {{ Form::label('about_us_content', 'Content')}}
                                {{ Form::textarea('about_us_content', null, array('class' => 'form-control', 'placeholder' => 'Content About Us'))}}
                            </div>
                            @if(isset($config))
                                @if($config->aboutUsLang('id'))
                                <div class="form-group col-md-6">
                                    {{ Form::label('about_us_lang', 'Konten')}}
                                    {{ Form::textarea('about_us_lang', $config->aboutUsLang('id')->about_us_content, array('class' => 'form-control', 'placeholder' => 'Konten'))}}
                                </div>
                                @else
                                <div class="form-group col-md-6">
                                    {{ Form::label('about_us_lang', 'Konten')}}
                                    {{ Form::textarea('about_us_lang', null, array('class' => 'form-control', 'placeholder' => 'Konten'))}}
                                </div>
                                @endif
                            @else
                            <div class="form-group col-md-6">
                                    {{ Form::label('about_us_lang', 'Konten')}}
                                    {{ Form::textarea('about_us_lang', null, array('class' => 'form-control', 'placeholder' => 'Konten'))}}
                                </div>
                            @endif
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                {{ Form::label('about_us_email', 'Email')}}
                                {{ Form::email('about_us_email', null, array('class' => 'form-control', 'placeholder' => 'Email'))}}
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                {{ Form::label('about_us_phone', 'Phone')}}
                                {{ Form::number('about_us_phone',null, array('class' => 'form-control', 'placeholder' => 'Phone'))}}
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                {{ Form::label('about_us_mobile', 'Mobile Phone')}}
                                {{ Form::number('about_us_mobile',null, array('class' => 'form-control', 'placeholder' => 'Mobile Phone'))}}
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-6" style="height: 320px; margin-bottom: 10px">
                                {{ Form::label('storage_address', 'Address')}}
                                {{ Form::text('storage_address', null, array('class' => 'form-control', 'placeholder' => 'Address', 'id'=>'storage_address'))}}<br>
                                {{-- <input type="text" name="storage_address" class="form-control" id="storage_address" placeholder="Enter a address" autocomplete="off"> --}}
                                <br>
                                {{ Form::label('storage_lat', 'Latitude')}}
                                {{ Form::text('storage_lat', null, array('class' => 'form-control', 'placeholder' => 'Latitude','id'=> 'storage_lat','readonly'))}}<br>
                                {{ Form::label('storage_lng', 'Longtitude')}}
                                {{ Form::text('storage_lng', null, array('class' => 'form-control', 'placeholder' => 'Longtitude','id' => 'storage_lng','readonly'))}}<br>
                                {{ Form::label('cod_amount', 'Rate Per Kilo')}}
                                {{ Form::number('cod_amount', null, array('class' => 'form-control', 'placeholder' => 'Rate per kilometer','id' => 'cod_amount'))}}
                            </div>
                            <div class="col-md-6" id="googleMapStorage" style="height: 300px; margin-bottom: 10px">
                            </div>
                            <input type="hidden" class="form-control" id="storage_address">
                            @if($config->storage_lat == null)
                            {!! Form::hidden('latitude', '1.0828276', ['class' => 'form-control', 'id' => 'lat']) !!}
                            {!! Form::hidden('longitude', '104.03045350000002', ['class' => 'form-control', 'id' => 'lng']) !!}
                            @else
                            {!! Form::hidden('latitude', $config->storage_lat, ['class' => 'form-control', 'id' => 'lat']) !!}
                            {!! Form::hidden('longitude', $config->storage_lng, ['class' => 'form-control', 'id' => 'lng']) !!}
                            @endif
                        </div>
                        <button type="submit" class="pull-right btn btn-sm btn-success btn-config">
                            <span class="fa fa-save"></span> Update
                        </button>   
                    </fieldset>
                    <fieldset>
                        {{ csrf_field() }}
                        <h4> <b>Social Media Login</b></h4>
                        <div class="row">
                            <div class="form-group col-md-1" align="center">
                                <img src="{{ asset('assets/images/facebook.png') }}" style="width: 30px;">
                            </div>
                            <div class="form-group col-md-5">
                                {{ Form::text('facebook_account', null, array('class' => 'form-control', 'placeholder' => 'Facebook'))}}
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-1" align="center">
                                <img src="{{ asset('assets/images/instagram.png') }}" style="width: 30px;">
                            </div>
                            <div class="form-group col-md-5">
                                {{ Form::text('instagram_account', null, array('class' => 'form-control', 'placeholder' => 'Instagram'))}}
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-1" align="center">
                                <img src="{{ asset('assets/images/twitter.png') }}" style="width: 30px;">
                            </div>
                            <div class="form-group col-md-5">
                                {{ Form::text('twitter_account', null, array('class' => 'form-control', 'placeholder' => 'Twitter'))}}
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                        <button type="submit" class="pull-right btn btn-sm btn-success btn-config">
                            <span class="fa fa-save"></span> Update
                        </button>   
                    </fieldset>
                </div>
            </div>
            {{ Form::close() }}
          {{--   {!! Form::open(['url' => route('admin.promotions.save'),'method' => 'POST','enctype'=>'multipart/form-data']) !!}
            <div class="panel-body">
                <div class="modal-body">
                    <fieldset>
                        {{ csrf_field() }}
                        <h4> <b>Promotion</b></h4>
                        <div class="row">
                            <div class="form-group col-md-6">
                                {{ Form::label('product_id', 'Product')}}
                                {{ Form::select('product_id', $promoProduct, null, array('class' => 'form-control', 'placeholder' => 'Please select one...'))}}
                            </div>
                            <div class="col-md-3"></div>
                        </div>  
                        @if(count($promoProduct) == 0)
                        <p style="color: red">Only product with status sale will show</p>
                        @endif
                        <div class="row">
                            <div class="form-group col-md-6">
                                {{ Form::label('title', 'Title')}}
                                {{ Form::text('title', null, array('class' => 'form-control', 'placeholder' => 'Title Promotion'))}}
                            </div>
                            <div class="col-md-3"></div>
                        </div>  
                        <div class="row">
                            <div class="form-group col-md-6">
                                {{ Form::label('content', 'Content')}}
                                {{ Form::text('content', null, array('class' => 'form-control', 'placeholder' => 'Content Promotion'))}}
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                {{ Form::label('photo', 'Photo')}}
                                {{ Form::file('photo', null, array('class' => 'form-control'))}}
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                {{ Form::label('size', 'Photo Size')}}
                                <div class="bs-example">
                                    <div class="make-switch is-radio switch-small" data-text-label="Big" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Size of the image is HALF of the screen!" data-original-title="Information!">
                                        <input type="radio" name="size" value="0" checked />
                                    </div>
                                    <!-- <div class="make-switch is-radio switch-small" data-text-label="Medium" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Size of the image is 2/4 of the screen!" data-original-title="Information!">
                                        <input type="radio" name="size" value="2"/>
                                    </div> -->
                                    <div class="make-switch is-radio switch-small" data-text-label="Small" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Size of the image is 1/4 of the screen!" data-original-title="Information!">
                                        <input type="radio" name="size" value="1"/>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <button type="submit" class="pull-right btn btn-sm btn-success btn-config">
                            <span class="fa fa-save"></span> Update
                        </button>
                    </fieldset>
                </div>
            </div>   
            {{ Form::close() }}
            @if(isset($promotions) && count($promotions) != 0)
            <div class="panel-body">
                <div class="modal-body">
                    <fieldset>
                        {{ csrf_field() }}
                        <h4> <b>Currently Active Promotion</b></h4>
                        <table class="table" width="100%">
                            <thead>
                                <tr>
                                    <th>Product</th>
                                    <th>Title</th>
                                    <th>Content</th>
                                    <th>Photo</th>
                                    <th>Size</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($promotions as $promotion)
                                <tr>
                                    <td>
                                        {{$promotion->product->name}}
                                    </td>
                                    <td>
                                        {{$promotion->title}}
                                    </td>
                                    <td>
                                        {{$promotion->content}}
                                    </td>
                                    <td>
                                        <img src="{{($promotion->photo) }}" style="width: 200px; height: 200px">
                                    </td>
                                    <td>
                                        @if($promotion->size == 0)
                                        <b>Big</b> (Half of the screen)
                                        @else
                                        <b>Small</b> (1/4 of the screen)
                                        @endif
                                    </td>
                                    <td>
                                        <a data-url="{{ route('admin.promotions.destroy', [$promotion->id]) }}"
                                            data-toggle="modal" data-target=" #modalPromotion" data-title="Confirmation" data-message="Would you like to delete this promotion?" 
                                            class="btn btn-danger btn-outline btn-sm" >
                                            <span class="fa fa-trash-o"></span> Delete
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </fieldset>
                </div>
            </div>   
            @endif --}}
            <div class="panel-body">
                {!! Form::open(['url' => route('admin.store.term'),'method' => 'POST','data-parsley-validate' => 'true']) !!}
                <div class="panel-heading">
                    <h4> <b>Terms & Conditions</b></h4>
                    <h6>Default Language (English)</h6>
                    @if(isset($enTerm))
                        {{ csrf_field() }}
                        {{Form::textarea('message', $enTerm->message)}}
                    @else
                        {{Form::textarea('message', null)}}
                    @endif
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-success"><span class="fa fa-save"></span> Save</button>
                </div>
                {{ Form::close() }}
            </div>
            <div class="panel-body">
                {!! Form::open(['url' => route('admin.store.term.id'),'method' => 'POST','data-parsley-validate' => 'true']) !!}
                <div class="panel-heading">
                    <h6>Alternate Language (Indonesia)</h6>
                    @if(isset($idTerm))
                        {{ csrf_field() }}
                        {{Form::textarea('alternate_message', $idTerm->message)}}
                    @else
                        {{Form::textarea('alternate_message', null)}}
                    @endif
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-success"><span class="fa fa-save"></span> Save</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script type="text/javascript">
    CKEDITOR.replace('message');
    CKEDITOR.replace('alternate_message');
    CKEDITOR.replace('about_us_content');
    CKEDITOR.replace('about_us_lang');
    
</script>
<script type="text/javascript">
    @include('includes.js.config_map');

    $(document).ready(function() {
        initialize();
    });
</script>
{{-- <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCSlAK_5cMtJS9jDISX0WIG3VDnWpQiUjk&callback=myMap"></script> --}}
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCyB6K1CFUQ1RwVJ-nyXxd6W0rfiIBe12Q&libraries=places" type="text/javascript"></script>
@endsection
@push('pageRelatedJs')
<script type="text/javascript">
    $('#modalPromotion').on('shown.bs.modal', function(e) {
        e.stopPropagation();
        var id = $(e.relatedTarget).data('id');
        var parentTr = $(e.relatedTarget).closest('tr');
        var title = $(e.relatedTarget).data('title');
        var message = $(e.relatedTarget).data('message');
        var action = $(e.relatedTarget).data('url');
        var table_name = $(e.relatedTarget).data('table-name');
        var method = $(e.relatedTarget).data('method') == undefined ? 'DELETE' : $(e.relatedTarget).data('method');
        $('#modal_id_delete').val(id);
        $('.modal-header h4').text(title);
        $('.modal-body p').text(message);
        $('#modalPromotion form').attr('action',action).on("submit", function(e){
            e.preventDefault();
            e.stopPropagation();
            var that = this;
            $.ajax({
                url: action,
                type: method,
                data: $(that).serialize() ,
                success: function(result) {
                    $("#modalPromotion").modal('hide')
                    if (typeof table_name === "string") {
                        $(table_name).DataTable().ajax.reload();
                    }
                    $.gritter.add( 
                    {  
                        title: '<i class="entypo-check"></i> Promotion deleted!',  
                        sticky: false,  
                        time: ""
                    } 
                    ); 
                    location.reload();
                }
            });
        });
    }).on('hidden.bs.modal', function (e) {
        e.stopPropagation();
        $('#modal_id_delete').val('');
        $('.modal-header h4').text('');
        $('.modal-body p').text('');
        $('#modalPromotion form').attr('action','/').off('submit');
    });

    $(document).ready(function(){
        $("#addTermRow").on('click', function(e){
            e.preventDefault();
            e.stopPropagation();
            addRow('productTerm');
        });

        $('#productTerm').on('click', '.delRow', function(e){
            e.preventDefault();
            e.stopPropagation();
            $(this).closest('tr').remove();
            rearrange('productTerm');
        }).on('click', '.delete', function(e){
            e.preventDefault();
            e.stopPropagation();
            var parentTr = $(this).closest('tr');
            deleteRow(parentTr, 'productTerm', this);

        });

        var addRow = function(tableName) {

            var totalTr = $('#'+tableName+' tbody tr').length;
            var tr = $('<tr>');

            var message = $('<input>')
            .addClass('form-control')
            .attr('name', tableName+'[new'+totalTr+'][message]')
            .attr('type','text')
            .attr('required', true)
            .attr('placeholder','Terms & Conditions');

            var del = $('<button>')
            .addClass('btn btn-warning btn-sm delRow')
            .text(' Delete')
            .prepend($('<span />')
                .addClass('fa fa-trash-o'));

            td_message = $('<td>')
            .append($('<div>')
                .addClass('form-group')
                .append(message));
            td_button = $('<td>')
            .addClass('form-group')
            .append(del);

            tr.append(td_message).append(td_button);

            tr.appendTo($('#'+tableName+' tbody'));
        }

    });
</script>
@endpush