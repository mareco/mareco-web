@extends('layouts.dashboard')
@extends('layouts.dashboard_side_bar')
@include('layouts.dashboard_header_right')
@extends('shared.datatables')
@section('content')
@include('includes.modal_delete_review')
@include('includes.page_header_index')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4>{{ $product->name }} review</h4>
            </div>
            <div class="panel-body">
                <div class="comments" style="font-size: 25px;">
                    @if(isset($reviews))
                    @foreach($reviews as $review)
                    <ul>
                        <div class="user-comment-thumb"> <img src="{{ $review->user->photo }}" alt="" class="img-circle" width="30" style="width: 70px;" /> </div>
                        <div class="user-comment-content"> 
                            <a href="#" class="user-comment-name">
                                {{$review->user->first_name.' '.$review->user->last_name}}
                            </a>
                            {{$review->remarks}}
                            <div class="user-comment-meta">{{ $review->created_at }}
                                @if($review->user->role == 'user')
                                -
                                @for ($i = 0; $i < $review->rate; $i++)
                                <span class="active"><img src="{{asset('assets/frontend-image/star-yellow.png')}}" alt="" /></span>
                                @endfor
                                @if($review->rate < 5)
                                @for ($j = $i; $j < 5; $j++)
                                <span class="unactive"><img src="{{asset('assets/frontend-image/star-white.png')}}" alt="" /></span>
                                @endfor
                                @endif
                                @endif
                            </div>
                            <div class="pull-right">
                                <a data-url="{{ route('admin.product.reviews.delete', $review->id) }}"  data-toggle="modal" data-target=" #modalDeleteReview" data-title="Confirmation" data-message="Would you like to delete this review?" class="btn btn-danger btn-outline btn-sm" style="width: 100%" class="btn btn-sm btn-primary">
                                    <span class="fa fa-trash"></span> Delete
                                </a>
                            </div>
                            <br>
                            <hr/>
                        </div>
                    </ul>
                    @endforeach
                    @endif
                    <ul class="comment-form">
                        <div class="user-comment-content">
                            {!! Form::open(['url' => route('admin.product.reviews.save', $product->id),'method' => 'POST','data-parsley-validate' => 'true','enctype'=>'multipart/form-data']) !!}
                            <textarea class="form-control autogrow" placeholder="Reply a review..." name="remarks" rows="5" style="font-size: 25px;"></textarea>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-sm btn-success"><span class="fa fa-save"></span> Save</button>
                            </div>
                            {{ Form::close() }}
                        </div>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('pageRelatedJs')
<script type="text/javascript">
    
 $('#modalDeleteReview').on('shown.bs.modal', function(e) {
    e.stopPropagation();
    var id = $(e.relatedTarget).data('id');
    var parentTr = $(e.relatedTarget).closest('tr');
    var title = $(e.relatedTarget).data('title');
    var message = $(e.relatedTarget).data('message');
    var action = $(e.relatedTarget).data('url');
    var table_name = $(e.relatedTarget).data('table-name');
    var method = $(e.relatedTarget).data('method') == undefined ? 'DELETE' : $(e.relatedTarget).data('method');
    $('#modal_id_delete').val(id);
    $('.modal-header h4').text(title);
    $('.modal-body p').text(message);
    $('#modalDeleteReview form').attr('action',action).on("submit", function(e){
        e.preventDefault();
        e.stopPropagation();
        var that = this;
        $.ajax({
            url: action,
            type: method,
            data: $(that).serialize() ,
            success: function(result) {
                $("#modalDeleteReview").modal('hide')
                if (typeof table_name === "string") {
                    $(table_name).DataTable().ajax.reload();
                }
                $.gritter.add( 
                {  
                    title: '<i class="entypo-check"></i> Process Success',  
                        // text: '<i class="entypo-check"></i> Request successfully process',  
                        sticky: false,  
                        time: ""
                    } 

                    ); 
                location.reload();
            }
        });
    });
}).on('hidden.bs.modal', function (e) {
    e.stopPropagation();
    $('#modal_id_delete').val('');
    $('.modal-header h4').text('');
    $('.modal-body p').text('');
    $('#modalDeleteReview form').attr('action','/').off('submit');
});
</script>
@endpush