<div class="tab-pane" id="tab3">
    <table class="table-hover table">
        <thead>
            <tr>
                <th>Image 1</th>
                <th>Image 2</th>
                <th>Image 3</th>
                <th>Image 4</th>
                <th>Image 5</th>
            </tr>
        </thead>
        <tbody>
            @if(isset($product))
            <tr>
                <td>     
                    <div style="width: 165px; height: 183px">
                    <span onclick="rotateRight('previewHaveVal',true)" class="cursor-onclick">Rotate</span>
                        @if(isset($product->product_image) && $product->product_image != '')
                        <span class="close" id="firstClose">&times;</span>
                        <img src="{{asset($product->product_image)}}" style="width: 165px; height: 165px" id="firstImageVal" class="previewHaveVal rotate{{$product->rotate}}">
                        <input type="hidden" id="previewHaveValRotate" value="{{$product->rotate}}" name="previewImageRotate">
                        @else
                        <input type="hidden" id="previewHaveValRotate" name="previewImageRotate">
                        <img src="{{ asset('assets/images/nopic.png') }}" style="width: 165px; height: 165px" class="previewHaveVal">
                        @endif
                        <img src="{{ asset('assets/images/nopic.png') }}" style="width: 165px; height: 165px; display: none;" id="firstImageNon" class="previewHaveVal">
                    </div>
                    {{-- {{ Form::file('product_image', null, $product->product_image, array('class' => 'form-control'))}} --}}
                    <input type='file' name='product_image' value="{{$product->product_image}}" id="imageSelect" />
                    {{ Form::hidden('product_image', $product->product_image,array('id'=>'firstImage'))}}

                </td>
                <td>            
                    <div style="width: 165px; height: 183px">
                    <span onclick="rotateRight('previewHaveVal2',true)" class="cursor-onclick">Rotate</span>
                        @if(isset($product->product_image_second) && $product->product_image_second != '')
                        <span class="close" id="secondClose">&times;</span>
                        <img src="{{asset($product->product_image_second)}}" style="width: 165px; height: 165px" id="secondImageVal"  class="previewHaveVal2 rotate{{$product->secondRotate}}">
                        <input type="hidden" id="previewHaveVal2Rotate" value="{{$product->secondRotate}}" name="previewImage2Rotate">
                        @else
                        <input type="hidden" id="previewHaveVal2Rotate" name="previewImage2Rotate">
                        <img src="{{ asset('assets/images/nopic.png') }}" style="width: 165px; height: 165px"  class="previewHaveVal2">
                        @endif
                        <img src="{{ asset('assets/images/nopic.png') }}" style="width: 165px; height: 165px; display: none;" id="secondImageNon"  class="previewHaveVal2">
                    </div>
                    {{-- {{ Form::file('product_image_second', null, , array('class' => 'form-control'))}} --}}
                    <input type='file' name='product_image_second' value="{{$product->product_image_second}}" id="imageSelect2" />
                    {{ Form::hidden('product_image_second', $product->product_image_second,array('id'=>'secondImage'))}}
                </td>
                <td>            
                    <div style="width: 165px; height: 183px">
                    <span onclick="rotateRight('previewHaveVal3',true)" class="cursor-onclick">Rotate</span>
                        @if(isset($product->product_image_third) && $product->product_image_third != '')
                        <span class="close" id="3rdClose">&times;</span>
                        <img src="{{asset($product->product_image_third)}}" style="width: 165px; height: 165px" id="thirdImageVal"  class="previewHaveVal3 rotate{{$product->thirdRotate}}">
                        <input type="hidden" id="previewHaveVal3Rotate" value="{{$product->thirdRotate}}" name="previewImage3Rotate">
                        @else
                        <input type="hidden" id="previewHaveVal3Rotate" name="previewImage3Rotate">
                        <img src="{{ asset('assets/images/nopic.png') }}" style="width: 165px; height: 165px"  class="previewHaveVal3">
                        @endif
                        <img src="{{ asset('assets/images/nopic.png') }}" style="width: 165px; height: 165px; display: none;" id="thirdImageNon"  class="previewHaveVal3">
                    </div>
                    <input type='file' name='product_image_third' value="{{$product->product_image_third}}" id="imageSelect3" />
                    {{ Form::hidden('product_image_third', $product->product_image_third,array('id'=>'thirdImage'))}}
                </td>
                <td>            
                    <div style="width: 165px; height: 183px">
                    <span onclick="rotateRight('previewHaveVal4',true)" class="cursor-onclick">Rotate</span>
                        @if(isset($product->product_image_fourth) && $product->product_image_fourth != '')
                        <span class="close" id="4thClose">&times;</span>
                        <img src="{{asset($product->product_image_fourth)}}" style="width: 165px; height: 165px" id="fourthImageVal"  class="previewHaveVal4 rotate{{$product->fourRotate}}">
                        <input type="hidden" id="previewHaveVal4Rotate" value="{{$product->fourRotate}}" name="previewImage4Rotate">
                        @else
                        <input type="hidden" id="previewHaveVal4Rotate" name="previewImage4Rotate">
                        <img src="{{ asset('assets/images/nopic.png') }}" style="width: 165px; height: 165px"  class="previewHaveVal4">
                        @endif
                        <img src="{{ asset('assets/images/nopic.png') }}" style="width: 165px; height: 165px; display: none;" id="fourthImageNon"  class="previewHaveVal4">
                    </div>
                    <input type='file' name='product_image_fourth' value="{{$product->product_image_fourth}}" id="imageSelect4" />
                    {{ Form::hidden('product_image_fourth', $product->product_image_fourth,array('id'=>'fourthImage'))}}
                </td>
                <td>            
                    <div style="width: 165px; height: 183px">
                    <span onclick="rotateRight('previewHaveVal5',true)" class="cursor-onclick">Rotate</span>
                        @if(isset($product->product_image_fifth) && $product->product_image_fifth != '')
                        <span class="close" id="5thClose">&times;</span>
                        <img src="{{asset($product->product_image_fifth)}}" style="width: 165px; height: 165px" id="fifthImageVal"  class="previewHaveVal5 rotate{{$product->fiveRotate}}">
                        <input type="hidden" id="previewHaveVal5Rotate" value="{{$product->fiveRotate}}" name="previewImage5Rotate">
                        @else
                        <input type="hidden" id="previewHaveVal5Rotate" name="previewImage5Rotate">
                        <img src="{{ asset('assets/images/nopic.png') }}" style="width: 165px; height: 165px"  class="previewHaveVal5">
                        @endif
                        <img src="{{ asset('assets/images/nopic.png') }}" style="width: 165px; height: 165px; display: none;" id="fifthImageNon"  class="previewHaveVal5">
                    </div>
                    <input type='file' name='product_image_fifth' value="{{$product->product_image_fifth}}" id="imageSelect5" />
                    {{ Form::hidden('product_image_fifth', $product->product_image_fifth,array('id'=>'fifthImage'))}}
                </td>
            </tr>
            @else
            <tr>
                <td>            
                    <div>
                        <span onclick="rotateRight('previewImage')" class="cursor-onclick">Rotate</span>
                        <input type="hidden" id="previewImageRotate" name="previewImageRotate">
                        <span class="close" id="closeImageSelect">&times;</span>
                        <img src="{{ asset('assets/images/nopic.png') }}" style="width: 165px; height: 165px" id="previewImage">
                        <img src="{{ asset('assets/images/nopic.png') }}" style="width: 165px; height: 165px; display:none;" id="previewImage_1">
                    </div>
                    <input type='file' name='product_image' id="imageSelect" />
                </td>
                <td>            
                    <div>
                        <span onclick="rotateRight('previewImage2')" class="cursor-onclick">Rotate</span>
                        <input type="hidden" id="previewImage2Rotate" name="previewImage2Rotate">
                        <span class="close" id="closeImageSelect2">&times;</span>
                        <img src="{{ asset('assets/images/nopic.png') }}" style="width: 165px; height: 165px" id="previewImage2">
                        <img src="{{ asset('assets/images/nopic.png') }}" style="width: 165px; height: 165px; display:none;" id="previewImage_2">
                    </div>
                    <input type='file' name='product_image_second' id="imageSelect2" />
                </td>
                <td>            
                    <div>
                        <span onclick="rotateRight('previewImage3')" class="cursor-onclick">Rotate</span>
                        <input type="hidden" id="previewImage3Rotate" name="previewImage3Rotate">
                        <span class="close" id="closeImageSelect3">&times;</span>
                        <img src="{{ asset('assets/images/nopic.png') }}" style="width: 165px; height: 165px" id="previewImage3">
                        <img src="{{ asset('assets/images/nopic.png') }}" style="width: 165px; height: 165px; display:none;" id="previewImage_3">
                    </div>
                    <input type='file' name='product_image_third' id="imageSelect3" />
                </td>
                <td>            
                    <div>
                        <span onclick="rotateRight('previewImage4')" class="cursor-onclick">Rotate</span>
                        <input type="hidden" id="previewImage4Rotate" name="previewImage4Rotate">
                        <span class="close" id="closeImageSelect4">&times;</span>
                        <img src="{{ asset('assets/images/nopic.png') }}" style="width: 165px; height: 165px" id="previewImage4">
                        <img src="{{ asset('assets/images/nopic.png') }}" style="width: 165px; height: 165px; display:none;" id="previewImage_4">
                    </div>
                    <input type='file' name='product_image_fourth' id="imageSelect4" />
                </td>
                <td>            
                    <div>
                        <span onclick="rotateRight('previewImage5')" class="cursor-onclick">Rotate</span>
                        <input type="hidden" id="previewImage5Rotate" name="previewImage5Rotate">
                        <span class="close" id="closeImageSelect5">&times;</span>
                        <img src="{{ asset('assets/images/nopic.png') }}" style="width: 165px; height: 165px" id="previewImage5">
                        <img src="{{ asset('assets/images/nopic.png') }}" style="width: 165px; height: 165px; display:none;" id="previewImage_5">
                    </div>
                    <input type='file' name='product_image_fifth' id="imageSelect5" />
                </td>
            </tr>
            @endif
        </tbody>
    </table>
</div>
@push('pageRelatedJs')
<script type="text/javascript">
function rotateRight(img, type) {
    var className = img;
    var name = img + 'Rotate';
    if (type == true) {
        img = document.getElementsByClassName(img);
        console.log($(img));
        var angle = document.getElementById(name).value;
    } else {
        img = document.getElementById(img);
        var angle = document.getElementById(name).value;
    }
    if (angle == '') {
        angle = 0;
    }
    angle = (parseFloat(angle) + 90) % 360;
    if (type == true) {
        $(img)[0].className = className + ' ' + 'rotate'+angle;
        document.getElementById(name).value = angle;
    } else {
        document.getElementsByClassName(name).value = angle;
    }
    img.className = "rotate" + angle;
}
    $(document).ready(function(){
        console.log('Hi');

        function readURL(input) {
            console.log('Read URL');
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    console.log(e);
                    $('#previewImage').attr('src', e.target.result);
                    $('.previewHaveVal').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        function readURL2(input) {
            console.log('Read URL');
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    console.log(e);
                    $('#previewImage2').attr('src', e.target.result);
                    $('.previewHaveVal2').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        function readURL3(input) {
            console.log('Read URL');
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    console.log(e);
                    $('#previewImage3').attr('src', e.target.result);
                    $('.previewHaveVal3').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        function readURL4(input) {
            console.log('Read URL');
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    console.log(e);
                    $('#previewImage4').attr('src', e.target.result);
                    $('.previewHaveVal4').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        function readURL5(input) {
            console.log('Read URL');
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    console.log(e);
                    $('#previewImage5').attr('src', e.target.result);
                    $('.previewHaveVal5').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imageSelect").change(function(){
            $("#previewImage").show();
            $("#previewImage_1").hide();
            console.log('Change!');
            readURL(this);
        });
        $("#imageSelect2").change(function(){
            $("#previewImage2").show();
          $("#previewImage_2").hide();
          console.log('Change!');
          readURL2(this);
      });
        $("#imageSelect3").change(function(){
          $("#previewImage3").show();
          $("#previewImage_3").hide();
          console.log('Change!');
          readURL3(this);
      });
        $("#imageSelect4").change(function(){
          $("#previewImage4").show();
          $("#previewImage_4").hide();
          console.log('Change!');
          readURL4(this);
      });
        $("#imageSelect5").change(function(){
          $("#previewImage5").show();
          $("#previewImage_5").hide();
          console.log('Change!');
          readURL5(this);
      });
    });
</script>
@endpush