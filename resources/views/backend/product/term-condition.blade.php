@extends('layouts.dashboard')
@extends('layouts.dashboard_side_bar')
@include('layouts.dashboard_header_right')
@section('content')
@include('includes.page_header_form')
@include('includes.modal_delete_term')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            @if (isset($term))
            {!! Form::model($term, [ 'url' => route('admin.update.term', ['term' => $term]), 'method' => 'PUT', 'data-parsley-validate' => 'true', 'enctype'=>'multipart/form-data' ]) !!}
            @else
            {!! Form::open(['url' => route('admin.store.term', ['product'=> $product]),'method' => 'POST','data-parsley-validate' => 'true']) !!}
            @endif
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="{{ URL::previous() }}" class="btn btn-sm btn-primary pull-right">
                        <span class="fa fa-reply"></span> Back
                    </a>
                </div>
                <h4>Terms & Conditions of <b>{{$product->name}}</b></h4>
                <table class="table table-striped table-hover" id="productTerm">
                    <div class="panel-heading-btn pull-right">
                        <a href="javascript::" class="btn btn-sm btn-primary addRow" id="addTermRow"><i class="fa fa-plus"></i> Add Row</a>
                    </div>
                    <thead>
                        <tr>
                            <th><b>Terms & Conditions</b></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($product->productTerms))
                        @foreach($product->productTerms as $productTerm)
                        {{ csrf_field() }}
                        <tr>
                            <td>
                                {{ Form::text('productTerm['.$productTerm->id.'][message]', $productTerm->message, array('class' => 'form-control', 'placeholder' => 'Terms & Conditions','required'))}}
                                {{ Form::hidden('productTerm['.$productTerm->id.'][id]', $productTerm['id'], array('class' => 'form-control'))}}
                            </td>
                            <td class="text-right">
                                <a data-url="{{ route('admin.term.destroy', [$productTerm->id]) }}"
                                    data-toggle="modal" data-target=" #modalDeleteTerm" data-title="Confirmation"  data-message="Would you like to delete this terms & conditions?" 
                                    class="btn btn-warning btn-sm" >
                                    <span class="fa fa-trash-o"></span> Delete
                                </a>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td>
                                {{ Form::text('productTerm[0][message]', null, array('class' => 'form-control', 'placeholder' => 'Terms & Conditions','data-validate' => 'required'))}}
                            </td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-sm btn-success"><span class="fa fa-save"></span> Save</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@endsection
@push('pageRelatedJs')
<script type="text/javascript">
    $('#modalDeleteTerm').on('shown.bs.modal', function(e) {
        e.stopPropagation();
        var id = $(e.relatedTarget).data('id');
        var parentTr = $(e.relatedTarget).closest('tr');
        var title = $(e.relatedTarget).data('title');
        var message = $(e.relatedTarget).data('message');
        var action = $(e.relatedTarget).data('url');
        var table_name = $(e.relatedTarget).data('table-name');
        var method = $(e.relatedTarget).data('method') == undefined ? 'DELETE' : $(e.relatedTarget).data('method');
        $('#modal_id_delete').val(id);
        $('.modal-header h4').text(title);
        $('.modal-body p').text(message);
        $('#modalDeleteTerm form').attr('action',action).on("submit", function(e){
            e.preventDefault();
            e.stopPropagation();
            var that = this;
            $.ajax({
                url: action,
                type: method,
                data: $(that).serialize() ,
                success: function(result) {
                    $("#modalDeleteTerm").modal('hide')
                    if (typeof table_name === "string") {
                        $(table_name).DataTable().ajax.reload();
                    }
                    $.gritter.add( 
                    {  
                        title: '<i class="entypo-check"></i> Process Success',  
                        // text: '<i class="entypo-check"></i> Request successfully process',  
                        sticky: false,  
                        time: ""
                    } 
                    );
                    location.reload(); 
                }
            });
        });
    }).on('hidden.bs.modal', function (e) {
        e.stopPropagation();
        $('#modal_id_delete').val('');
        $('.modal-header h4').text('');
        $('.modal-body p').text('');
        $('#modalDeleteTerm form').attr('action','/').off('submit');
    });

    $(document).ready(function(){
        $("#addTermRow").on('click', function(e){
            e.preventDefault();
            e.stopPropagation();
            addRow('productTerm');
        });

        $('#productTerm').on('click', '.delRow', function(e){
            e.preventDefault();
            e.stopPropagation();
            $(this).closest('tr').remove();
            rearrange('productTerm');
        }).on('click', '.delete', function(e){
            e.preventDefault();
            e.stopPropagation();
            var parentTr = $(this).closest('tr');
            deleteRow(parentTr, 'productTerm', this);

        });

        var addRow = function(tableName) {

            var totalTr = $('#'+tableName+' tbody tr').length;
            var tr = $('<tr>');

            var message = $('<input>')
            .addClass('form-control')
            .attr('name', tableName+'[new'+totalTr+'][message]')
            .attr('type','text')
            .attr('required', true)
            .attr('placeholder','Terms & Conditions');

            var del = $('<button>')
            .addClass('btn btn-warning btn-sm delRow')
            .text(' Delete')
            .prepend($('<span />')
                .addClass('fa fa-trash-o'));

            td_message = $('<td>')
            .append($('<div>')
                .addClass('form-group')
                .append(message));
            td_button = $('<td>')
            .addClass('form-group')
            .append(del);

            tr.append(td_message).append(td_button);

            tr.appendTo($('#'+tableName+' tbody'));
        }

    });
</script>
@endpush