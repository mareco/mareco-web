<div class="tab-pane" id="tab2">
    <table class="table table-striped table-hover" id="productVariant">
        <div class="panel-heading-btn pull-right">
            <a href="javascript::" class="btn btn-sm btn-primary addRow" id="addVariantRow"><i class="fa fa-plus"></i> Add Row</a>
        </div>
        <thead>
            <tr>
                <th><b>SKU</b></th>
                <th><b>Size</b></th>
                <th><b>Color</b></th>
                <th><b>Quantity</b></th>
                <th><b>Weight</b></th>
                <th><b>Unit</b></th>
                {{-- <th><b>Disc.Price</b></th> --}}
                <th><b>Price</b></th>
            </tr>
        </thead>
        <tbody>
            @if(isset($product->productVariants))
            @foreach($product->productVariants as $productVariant)
            {{ csrf_field() }}
            <tr>
                <td>
                    {{ Form::text('productVariant['.$productVariant->id.'][sku]', $productVariant->sku, array('class' => 'form-control', 'placeholder' => 'SKU','required'))}}
                    {{ Form::hidden('productVariant['.$productVariant->id.'][id]', $productVariant['id'], array('class' => 'form-control'))}}
                </td>
                <td>
                    {{ Form::text('productVariant['.$productVariant->id.'][size]', $productVariant['size'], array('class' => 'form-control', 'placeholder' => 'Size'))}}
                </td>
                <td>
                    {{ Form::hidden('productVariant['.$productVariant->id.'][color]', $productVariant['color'], array('class' => 'form-control', 'data-format' => 'hex', 'placeholder' => 'Color','readonly'))}}
                    <span class="input-group-addon inherit"> <i class="color-preview view-color-selector" style="background-color: {{$productVariant->color}}"></i> </span>

                    <!-- <input class="form-control" name="productVariant[new1][color]" type="hidden" data-format="hex" readonly="readonly" placeholder="Color"><span class="input-group-addon inherit"> <i class="color-preview view-color-selector"></i> </span> -->

                    <!-- <div class="form-group colorpicker-element">
                        {{ Form::text('productVariant['.$productVariant->id.'][color]', $productVariant['color'], array('class' => 'form-control input-group colorpicker-component', 'data-format' => 'hex', 'placeholder' => 'Color','readonly'))}}
                        <span class="input-group-addon cp"> <i class="color-preview" style="width: 100%"></i> </span>
                    </div> -->
                </td>
                <td>
                    {{ Form::number('productVariant['.$productVariant->id.'][qty]', $productVariant['qty'], array('class' => 'form-control', 'placeholder' => 'Qty'))}}
                </td>
                <td>
                    {{ Form::number('productVariant['.$productVariant->id.'][weight]', $productVariant['weight'], array('class' => 'form-control', 'placeholder' => 'Weight'))}}
                </td>
                <td style="width: 100px;">
                    {{ Form::select('productVariant['.$productVariant->id.'][unit_id]', $listOfUnits, $productVariant['unit_id'], array('class' => 'form-control'))}}
                </td>
                {{-- <td>
                    {{ Form::number('productVariant['.$productVariant->id.'][discount_price]', $productVariant['discount_price'], array('class' => 'form-control', 'placeholder' => 'Discount Price'))}}
                </td> --}}
                <td>
                    {{ Form::number('productVariant['.$productVariant->id.'][price]', $productVariant['price'], array('class' => 'form-control', 'placeholder' => 'Price', 'id'=>'normalPrice'))}}
                   {{--  <div class="checkbox checkbox-replace color-green" style="margin-left: 2px; margin-top: 6px;">
                        <input type="checkbox" name="is_discount" id="is_discount" {{$result}}>
                        <label style="font-size: 10px">Discount Price*</label>
                    </div>
                    {{ Form::number('productVariant['.$productVariant->id.'][discount_price]', $productVariant['discount_price'], array('class' => 'form-control', 'placeholder' => 'Discount Price', 'id'=>'discountDiv','style'=>'display:none;'))}} --}}

                </td>
                <td class="text-right">
                    <a class="btn btn-warning btn-sm delete delRow">
                        <span class="fa fa-trash-o"></span> Delete
                    </a>
                </td>
            </tr>
            @endforeach
            @endif
        </tbody>
    </table>
</div>
{{-- 
    <div class="checkbox checkbox-replace color-green" style="margin-left: 2px; margin-top: 6px;">
                         <input type="checkbox" name="is_discount" id="is_discount">
                         <label style="font-size: 10px">Discount Price*</label>
                     </div>
                     
                     {{ Form::number('productVariant[0][discount_price]', null, array('class' => 'form-control', 'placeholder' => 'Disc. Price', 'id'=>'discountDiv','style'=>'display:none;'))}}
 --}}