@extends('layouts.dashboard')
@extends('layouts.dashboard_side_bar')
@include('layouts.dashboard_header_right')
@extends('shared.datatables')
@include('includes.modal_delete')
@section('content')
@include('includes.page_header_index')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <a href="{{route('admin.products.new')}}" class="btn btn-xs btn-primary pull-right"><span class="fa fa-plus"></span> Add</a>
                <h4>Product</h4>
            </div>

            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-bordered datatable" id="product-list" width="100%">
                        <thead>
                            <tr>
                                <th>Thumbnail</th>
                                <th>Name</th>
                                <th>Category</th>
                                <th>Code</th>
                                <th>Description</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                        <tfoot>
                            <tr id="filter-row">
                                <th></th>
                                <th>Name</th>
                                <th>Category</th>
                                <th>Code</th>
                                <th>Description</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('pageRelatedJs')
<script type="text/javascript">
    $(document).ready(function () {
        var productDataTable = $('#product-list').DataTable(
        {
            dom: "lrtip",
            orderCellsTop: true,
            responsive: true,
            serverSide: true,
            ajax: {
                url:  "{{ route('admin.products.list') }}",
                data: { '_token' : '{{csrf_token() }}'},
                type: 'POST',
            },
            columns: [
            { data: 'product_image', name: 'product_image', 'className': 'text-left',
            'render': function (data, type, full, meta) {
                if (!data) return '';
                return '<img src='+data+' style="width: 70px;"/>';
            }
        },
        { data: 'name', name: 'name',"className": 'text-center', },
        { data: 'category_id', name: 'category_id',"className": 'text-center', },
        { data: 'code', name: 'code',"className": 'text-center', },
        { data: 'description', name: 'description',"className": 'text-center', },
        { data: 'status', name: 'status',"className": 'text-center', },
        { data: 'action', name: 'action',"className": 'text-center', orderable: false, searchable: false  },
        ],
    });

        $("#product-list tfoot tr#filter-row th").each(function (index) {
            var column = $(this).text();

            switch(column){
                case "Name":
                case "Code":
                case "Description":
                var input = '<input type="text" class="form-control" width="100%" placeholder="Search By ' + column + '" />';

                $(this).html(input);
                break;

                case "Category":
                var select = '{{ Form::select("category_id", $categoryList, null, array("id" => "category_id", "class" => "select-input form-control", "style" => "width:165px")) }}'

                $(this).html(select);
                break;

                case "Status":
                var select = '{{ Form::select("status", $productStatus, null, array("id" => "status", "class" => "select-input form-control")) }}'

                $(this).html(select);
                break;
            }
        });

        productDataTable.columns().every(function() {
            var that = this;
            $('input', this.footer()).on('keyup change', function() {
                if (that.search() !== this.value) {
                    that
                    .search(this.value)
                    .draw();
                }
            });
            $('select', this.footer()).on('keyup change', function() {
                if (that.search() !== this.value) {
                    that
                    .search(this.value)
                    .draw();
                }
            });
        });

        $(".select-input").prepend('<option value="" selected="selected">All</option>');


    });
</script>
@endpush
