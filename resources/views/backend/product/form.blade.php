@extends('layouts.dashboard')
@extends('layouts.dashboard_side_bar')
@include('layouts.dashboard_header_right')
@section('content')
@include('includes.page_header_form')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="{{ URL::previous() }}" class="btn btn-sm btn-primary pull-right">
                        <span class="fa fa-reply"></span> Back
                    </a>
                </div>
                <h4>Product Registration Form</h4>
            </div>
            @if(isset($product))
            <form id="rootwizard" method="POST" action="{{ route('admin.products.update', ['product' => $product]) }}"  enctype="multipart/form-data" class="form-wizard validate">
                @else
                <form id="rootwizard" method="POST" action="{{ route('admin.products.save') }}" enctype="multipart/form-data" class="form-wizard validate">
                    @endif
                    <div class="steps-progress">
                        <div class="progress-indicator"></div>
                    </div> 
                    <ul>
                        <li class="active"> <a href="#tab1" data-toggle="tab"><span>1</span>Product Info</a> </li> 
                        <li> <a href="#tab2" data-toggle="tab"><span>2</span>Product Variant</a> </li>
                        <li> <a href="#tab3" data-toggle="tab"><span>3</span>Product Picture</a> </li>
                    </ul>
                    {{ csrf_field() }}
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab1">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('category_id', 'Category ', array('class' => 'control-label'))}}
                                        @if(isset($product))
                                        {{ Form::select('category_id', $listOfCategories, $product->category_id, array('class' => 'form-control', 'placeholder' => 'Please select one... ','data-validate' => 'required'))}}
                                        @else
                                        {{ Form::select('category_id', $listOfCategories, null, array('class' => 'form-control', 'placeholder' => 'Please select one... ','data-validate' => 'required'))}}
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('code', 'Code ', array('class' => 'control-label'))}}
                                        @if(isset($product))
                                        {{ Form::text('code', $product->code, array('class' => 'form-control', 'placeholder' => 'Code','data-validate' => 'required'))}}
                                        @else
                                        {{ Form::text('code', null, array('class' => 'form-control', 'placeholder' => 'Code','data-validate' => 'required'))}}
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('point', 'Point ', array('class' => 'control-label'))}}
                                        @if(isset($product))
                                        {{ Form::number('point', $product->point, array('class' => 'form-control', 'placeholder' => 'Point','data-validate' => 'required','min'=>0))}}
                                        @else
                                        {{ Form::number('point', null, array('class' => 'form-control', 'placeholder' => 'Point','data-validate' => 'required','min'=>0))}}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <h4>Default Language (English)</h4>
                                    <div class="form-group">
                                        {{ Form::label('name', 'Product Name ', array('class' => 'control-label'))}}
                                        @if(isset($product))
                                        {{ Form::text('name', $product->name, array('class' => 'form-control', 'placeholder' => 'Product Name','data-validate' => 'required'))}}
                                        @else
                                        {{ Form::text('name', null, array('class' => 'form-control', 'placeholder' => 'Product Name','data-validate' => 'required'))}}
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <h4>Alternate Language (Indonesia)</h4>
                                    <div class="form-group">
                                        {{ Form::label('product_name', 'Nama Produk ', array('class' => 'control-label'))}}
                                        @if(isset($product))
                                        @if($product->productLang('id'))
                                        {{ Form::text('product_name', $product->productLang('id')->name, array('class' => 'form-control', 'placeholder' => 'Nama Produk'))}}
                                        @else
                                        {{ Form::text('product_name', null, array('class' => 'form-control', 'placeholder' => 'Nama Produk'))}}
                                        @endif
                                        @else
                                        {{ Form::text('product_name', null, array('class' => 'form-control', 'placeholder' => 'Nama Produk'))}}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {{ Form::label('description', 'Product Description ', array('class' => 'control-label'))}}
                                        @if(isset($product))
                                        {{ Form::textarea('description', $product->description, array('class' => 'form-control autogrow', 'placeholder' => 'Product Description','data-validate' => 'required','rows'=>5))}}
                                        @else
                                        {{ Form::textarea('description', null, array('class' => 'form-control autogrow', 'placeholder' => 'Product Description','data-validate' => 'required','rows'=>5))}}
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {{ Form::label('product_description', 'Deskripsi Produk ', array('class' => 'control-label'))}}
                                        @if(isset($product))
                                        @if($product->productLang('id'))
                                        {{ Form::textarea('product_description', $product->productLang('id')->description, array('class' => 'form-control autogrow', 'placeholder' => 'Deskripsi Produk','rows'=>5))}}
                                        @else
                                        {{ Form::textarea('product_description', null, array('class' => 'form-control autogrow', 'placeholder' => 'Deskripsi Produk','rows'=>5))}}
                                        @endif
                                        @else
                                        {{ Form::textarea('product_description', null, array('class' => 'form-control autogrow', 'placeholder' => 'Deskripsi Produk','rows'=>5))}}

                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    @if(isset($product))
                                    {{ Form::checkbox('is_featured', null, $product->is_featured) }}
                                    @else
                                    {{ Form::checkbox('is_featured', null) }}
                                    @endif
                                    {{ Form::label('is_featured', 'Featured in Homepage') }}
                                </div>
                                <div class="col-md-3">
                                    {{ Form::label('eta', 'Estimated Time of Arrival', array('class' => 'control-label'))}}
                                    @if(isset($product))
                                    {{ Form::text('eta', $product->eta, array('class' => 'form-control', 'placeholder' => 'Estimated Time of Arrival'))}}
                                    @else
                                    {{ Form::text('eta', null, array('class' => 'form-control', 'placeholder' => 'Estimated Time of Arrival'))}}
                                    @endif
                                </div>
                            </div>
                        </div>
                        @include('backend.product.tab_2')
                        @include('backend.product.tab_3')
                        <ul class="pager wizard">
                            <li class="previous"> <a href="#"><i class="entypo-left-open"></i> Back</a> </li>
                            <button type="submit" id="saveProduct" style="display: none;" class="btn btn-sm btn-success pull-right"><span class="fa fa-save"></span> Save</button>
                            <li id="nextStep" class="next"> <a href="#">Next <i class="entypo-right-open"></i></a> </li>
                        </ul> 
                    </div>
                </form> 
            </div>
        </div>
    </div>
    @endsection
    @push('pageRelatedJs')
    <script type="text/javascript">
        $(".form-wizard").each(function(i, el)
        {
            var $this = $(el),
            $progress = $this.find(".steps-progress div"),
            _index = $this.find('> ul > li.active').index();

            var checkFormWizardValidaion = function(tab, navigation, index)
            {
                if (index == 2) {
                    if ($('.colorpicker-element').length == 0) {
                        $.gritter.add(
                        {
                            title: '<i class="entypo-check"></i> You must have a variant',
                            sticky: false,
                            time: "3000"
                        });
                        return false;
                    } else {
                        $("#saveProduct").show(); 
                        $("#nextStep").hide();  
                    }
                } else {
                    $("#saveProduct").hide(); 
                    $("#nextStep").show();
                }

                if($this.hasClass('validate'))
                {
                    var $valid = $this.valid();

                    if( ! $valid)
                    {
                        $this.data('validator').focusInvalid();
                        return false;
                    }
                }

                return true;
            };


            $this.bootstrapWizard({
                tabClass: "",
                onTabShow: function($tab, $navigation, index)
                {
                    setCurrentProgressTab($this, $navigation, $tab, $progress, index);
                },

                onNext: checkFormWizardValidaion,
                onPrevious: function(tab, navigation, index) {
                    if (index === 2) {
                        $("#saveProduct").show();
                        $("#nextStep").hide();
                    } else {
                        $("#saveProduct").hide(); 
                        $("#nextStep").show();
                    }
                },
                onTabClick: checkFormWizardValidaion
            });

            $this.data('bootstrapWizard').show( _index );

        });
        $(document).ready(function(){

            $('#productVariant').on('click', '.addColor', function(e) {
                var totalTr = $('#productVariant'+' tbody tr').length;
                var totalColor = $('.color-list')[0].childElementCount;
                var tr = $('<tr>');

                var color = $('<input>')
                .addClass('form-control')
                .attr('name', 'productVariant'+'[new'+totalTr+'][color'+totalColor+']')
                .attr('type','hidden')
                .attr('data-format','hex')
                .attr('readonly', 'true')
                .attr('placeholder','Color');
                $('.color-list').append($('<div>')
                    .addClass('form-group')
                    .attr('data-format','hex')
                    .append(color)
                    .append('<span class="input-group-addon inherit"> <i class="color-preview view-color-selector"></i> </span>').colorpicker({
                        colorSelectors: {
                         '#000000': '#000000',
                         '#ffffff': '#ffffff',
                         '#FF0000': '#FF0000',
                         '#777777': '#777777',
                         '#337ab7': '#337ab7',
                         '#5cb85c': '#5cb85c',
                         '#5bc0de': '#5bc0de',
                         '#f0ad4e': '#f0ad4e',
                         '#d9534f': '#d9534f',
                         '#fc31b7': '#fc31b7'
                     }
                 })
                    )
            });

            $('#firstClose').on('click', function(e){
                console.log($('#firstImage').val());
                var firstImage = $('#firstImage').remove();
                $('#firstImageVal').hide(); 
                $('#firstImageNon').show();
            });

            $('#closeImageSelect').on('click', function(e){
                $('#imageSelect')[0].value = '';
                $('#previewImage').hide();
                $('#previewImage_1').show();
            });

            $('#closeImageSelect2').on('click', function(e){
                $('#imageSelect2')[0].value = '';
                $('#previewImage2').hide();
                $('#previewImage_2').show();
            });

            $('#closeImageSelect4').on('click', function(e){
                $('#imageSelect4')[0].value = '';
                $('#previewImage4').hide();
                $('#previewImage_4').show();
            });

            $('#closeImageSelect5').on('click', function(e){
                $('#imageSelect5')[0].value = '';
                $('#previewImage5').hide();
                $('#previewImage_5').show();
            });

            $('#closeImageSelect3').on('click', function(e){
                $('#imageSelect3')[0].value = '';
                $('#previewImage3').hide();
                $('#previewImage_3').show();
            });
            $('#secondClose').on('click', function(e){
                console.log($('#secondImage').val());
                var secondImage = $('#secondImage').remove();
                $('#secondImageVal').hide();
                $('#secondImageNon').show();
            });

            $('#3rdClose').on('click', function(e){
                var firstImage = $('#thirdImage').remove();
                $('#thirdImageVal').hide();
                $('#thirdImageNon').show();
                console.log(firstImage);
            });

            $('#4thClose').on('click', function(e){
                var firstImage = $('#fourthImage').remove();
                $('#fourthImageVal').hide();
                $('#fourthImageNon').show();
                console.log(firstImage);
            });

            $('#5thClose').on('click', function(e){
                var firstImage = $('#fifthImage').remove();
                $('#fifthImageVal').hide();
                $('#fifthImageNon').show();
                console.log(firstImage);
            });

            $("#addVariantRow").on('click', function(e){
                e.preventDefault();
                e.stopPropagation();
                addRow('productVariant');
            });

            $('#productVariant').on('click', '.delRow', function(e){
                e.preventDefault();
                e.stopPropagation();
                $(this).closest('tr').remove();
                rearrange('productVariant');
            }).on('click', '.delete', function(e){
                e.preventDefault();
                e.stopPropagation();
                var parentTr = $(this).closest('tr');
                deleteRow(parentTr, 'productVariant', this);

            });

            var addRow = function(tableName) {

                var totalTr = $('#'+tableName+' tbody tr').length;
                var tr = $('<tr>');

                var unit = {!!$listOfUnits!!};
                var sel_unit = 
                $('<select>').addClass('form-control select2me')
                .attr('name', tableName+'[new'+totalTr+'][unit_id]')
                .attr('required', 'required');
                $.each(unit, function(key, value) {
                    sel_unit.append($("<option>").attr('value',key).text(value));
                });
                td_unit_id = $('<td>').append($('<div>').addClass('form-group').append(sel_unit));

                var sku = $('<input>')
                .addClass('form-control')
                .attr('name', tableName+'[new'+totalTr+'][sku]')
                .attr('type','text')
                .attr('required', true)
                .attr('placeholder','SKU');

                var qty = $('<input>')
                .addClass('form-control')
                .attr('name', tableName+'[new'+totalTr+'][qty]')
                .attr('type','number')
                .attr('required', true)
                .attr('placeholder','Qty');

                var size = $('<input>')
                .addClass('form-control')
                .attr('name', tableName+'[new'+totalTr+'][size]')
                .attr('type','text')
                .attr('placeholder','Size');

                var weight = $('<input>')
                .addClass('form-control')
                .attr('name', tableName+'[new'+totalTr+'][weight]')
                .attr('type','number')
                .attr('required', true)
                .attr('placeholder','Weight');

                var color = $('<input>')
                .addClass('form-control')
                .attr('name', tableName+'[new'+totalTr+'][color]')
                .attr('type','hidden')
                .attr('data-format','hex')
                .attr('readonly', 'true')
                .attr('placeholder','Color');

                var price = $('<input>')
                .addClass('form-control')
                .attr('name', tableName+'[new'+totalTr+'][price]')
                .attr('type','number')
                .attr('required', true)
                .attr('placeholder','Price');

                // var discount_price = $('<input>')
                // .addClass('form-control')
                // .attr('name', tableName+'[new'+totalTr+'][discount_price]')
                // .attr('type','number')
                // .attr('placeholder','Price');

                var del = $('<button>')
                .addClass('btn btn-warning btn-sm delRow')
                .text(' Delete')
                .prepend($('<span />')
                    .addClass('fa fa-trash-o'));

                var colorBtn = $('<span>')
                .addClass('btn btn-primary btn-sm mg-bot-5 addColor')
                .text('Add Color');

                td_sku = $('<td>')
                .append($('<div>')
                    .addClass('form-group')
                    .append(sku));
                td_qty = $('<td>')
                .append($('<div>')
                    .addClass('form-group')
                    .append(qty));
                td_size = $('<td>')
                .append($('<div>')
                    .addClass('form-group')
                    .append(size));
                td_weight = $('<td>')
                .append($('<div>')
                    .addClass('form-group')
                    .append(weight));
                td_color = $('<td>')
                .addClass('color-list')
                .append($('<div>')
                    .addClass('form-group')
                    .attr('data-format','hex')
                    .append(color)
                    .append('<span class="input-group-addon inherit"> <i class="color-preview view-color-selector"></i> </span>').colorpicker({
                        colorSelectors: {
                           '#000000': '#000000',
                           '#ffffff': '#ffffff',
                           '#FF0000': '#FF0000',
                           '#777777': '#777777',
                           '#337ab7': '#337ab7',
                           '#5cb85c': '#5cb85c',
                           '#5bc0de': '#5bc0de',
                           '#f0ad4e': '#f0ad4e',
                           '#d9534f': '#d9534f',
                           '#fc31b7': '#fc31b7'
                       }
                   })
                    );
                td_price = $('<td>')
                .append($('<div>')
                    .addClass('form-group')
                    .append(price));

                td_button = $('<td>')
                .addClass('form-group')
                // .append(colorBtn)
                .append(del);

                tr.append(td_sku).append(td_size).append(td_color).append(td_qty).append(td_weight).append(td_unit_id).append(td_price).append(td_button);

                tr.appendTo($('#'+tableName+' tbody'));



            }


            // $('input:checkbox[name*=is_discount]').on('change', function(){
            //     useCheckbox();
            // });

            // var useCheckbox = function(){
            //     var checked = $('input:checkbox[name="is_discount"]').is(':checked');
            //     if(checked){
            //         $('#discountDiv').show();
            //         $('#normalPrice').attr('readonly',true);
            //     } else {
            //         $('#discountDiv').hide();
            //         $('#normalPrice').attr('readonly',false);

            //     }
            // }

            // useCheckbox();

        });
    </script>
    @endpush