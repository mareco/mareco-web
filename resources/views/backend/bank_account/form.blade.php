@extends('layouts.dashboard')
@extends('layouts.dashboard_side_bar')
@include('layouts.dashboard_header_right')
@section('content')
@include('includes.page_header_form')

@if (isset($bank))
{!! Form::model($bank, ['url' => route('admin.banks.update', ['bank' => $bank]),'method' => 'PUT','data-parsley-validate' => 'true','enctype'=>'multipart/form-data']) !!}
@else
{!! Form::open(['url' => route('admin.banks.save'),'method' => 'POST','data-parsley-validate' => 'true','enctype'=>'multipart/form-data']) !!}
@endif

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="{{ URL::previous() }}" class="btn btn-xs btn-primary pull-right"><span class="fa fa-reply"></span> Back</a>
                </div>
                <h4>Bank Account Registration Form</h4>
            </div>

            <div class="panel-body">
                <div class="modal-body">
                    <fieldset>
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="form-group col-md-3">
                                @if(isset($bank->logo) && $bank->logo != '')
                                <img src="{{($bank->logo) }}" style="width: 145px; height: 145px">
                                @else
                                {{ Form::label('logo', 'Bank Logo')}}<br>
                                <img src="{{ asset('assets/images/nopic.png') }}" style="width: 145px; height: 145px">
                                @endif
                                {{ Form::file('logo', null, array('class' => 'form-control', 'placeholder' => 'Bank Logo'))}}
                            </div>
                            <div class="col-md-3"></div>
                        </div>

                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="form-group col-md-6">
                                {{ Form::label('bank', 'Bank ')}}
                                {{ Form::text('bank', null, array('class' => 'form-control', 'placeholder' => 'Bank ','data-parsley-required' => 'true'))}}
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="form-group col-md-6">
                                {{ Form::label('account_name', 'Bank Account Name')}}
                                {{ Form::text('account_name', null, array('class' => 'form-control', 'placeholder' => 'Bank Account Name','data-parsley-required' => 'true'))}}
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="form-group col-md-6">
                                {{ Form::label('account_number', 'Bank Account No')}}
                                {{ Form::number('account_number', null, array('class' => 'form-control', 'placeholder' => 'Bank Account No','data-parsley-required' => 'true'))}}
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                        @if(isset($bank))
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="form-group col-md-6">
                                {{ Form::label('is_active', 'Enable Account')}}<br>
                                <div class="make-switch popover-primary" data-on="success" data-off="info" data-toggle="popover" data-text-label="<i class='entypo-info'></i>" data-trigger="hover" data-placement="top" data-content="Toogle this to enable or disable this account!" data-original-title="Information!">
                                    <input type="checkbox" name="is_active" {{$result}}> 
                                </div>
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                        @endif
                    </fieldset>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-success"><span class="fa fa-save"></span> Save</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endsection
