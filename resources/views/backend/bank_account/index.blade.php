@extends('layouts.dashboard')
@extends('layouts.dashboard_side_bar')
@include('layouts.dashboard_header_right')
@extends('shared.datatables')
@include('includes.modal_delete')
@section('content')
@include('includes.page_header_index')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <a href="{{route('admin.banks.new')}}" class="btn btn-sm btn-primary pull-right"><span class="fa fa-plus"></span> Add</a>
                <h4>Bank Account</h4>
            </div>
            <div class="panel-body">
                <table class="table table-bordered datatable" id="bank-list" width="100%">
                    <thead>
                        <tr>
                            <th>Logo</th>
                            <th>Bank</th>
                            <th>Bank Account Name</th>
                            <th>Bank Account No</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    <tfoot>
                        <tr id="filter-row">
                            <th></th>
                            <th>Bank</th>
                            <th>Bank Account Name</th>
                            <th>Bank Account No</th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@push('pageRelatedJs')
<script type="text/javascript">
    $(document).ready(function () {
        var bankDataTable = $('#bank-list').DataTable(
        {
            dom: "lrtip",
            orderCellsTop: true,
            responsive: true,
            processing: true,
            serverSide: true,
            ajax: {
                url:  "{{ route('admin.banks.list') }}",
                data: { '_token' : '{{csrf_token() }}'},
                type: 'POST',
            },
            columns: [
            { data: 'logo', name: 'logo', 'className': 'text-left', 
            'render': function (data, type, full, meta) {
                if (!data) return '';
                return '<img src='+data+' class="table-img index-data-image" style="width: 130px; "/>';
            }
        },
        { data: 'bank', name: 'bank',"className": 'text-center', },
        { data: 'account_name', name: 'account_name',"className": 'text-center', },
        { data: 'account_number', name: 'account_number',"className": 'text-center', },
        { data: 'action', name: 'action',"className": 'text-center', orderable: false, searchable: false  },
        ],
    });

        $("#bank-list tfoot tr#filter-row th").each(function (index) { 
            var column = $(this).text();

            switch(column){ 
                case "Bank": 
                case "Bank Account Name": 
                case "Bank Account No": 
                var input = '<input type="text" class="form-control" style="width:210px" placeholder="Search By ' + column + '" />'; 

                $(this).html(input); 
                break; 
            } 
        });

        bankDataTable.columns().every(function() {
            var that = this;
            $('input', this.footer()).on('keyup change', function() {
                if (that.search() !== this.value) {
                    that
                    .search(this.value)
                    .draw();
                }
            });
        });

    });
</script>
@endpush