@extends('layouts.dashboard')
@extends('layouts.dashboard_side_bar')
@include('layouts.dashboard_header_right')
@section('content')
@include('includes.page_header_form')

<div class="row">
    <div class="col-md-12">
        <div class="gallery-env">
            @if(count($pictures) != 0)
            <div class="row">
                <div class="col-sm-12">
                    <h3>Slider Image</h3>
                    <hr />
                </div>
                <div class="row">
                    <div class="col-md-12">
                        @foreach($pictures as $picture)
                        <div class="col-sm-2 col-xs-4" data-tag="1d">
                            <article class="image-thumb">
                               
                                <a href="#" class="image">
                                    <img src="{{$picture->feature_url}}" style="width: 240px; height: 240px" /><br>
                                </a>
                                <div class="image-options">
                                    <a href="admin/brands/delete" class="delete" id="{{$picture->id}}"><i class="entypo-cancel"></i></a>
                                </div>
                                
                            </article>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            @endif
            <hr />
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4>Upload More Images Here</h4>
                </div>
                <div class="panel-body">
                    <form action="" class="dropzone dz-min" id="dropzone">
                        <div class="fallback">
                            <input name="file" type="file" multiple /> 
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('pageRelatedJs')
<script type="text/javascript">
    // $(".select-input").prepend('<option value="none" selected="selected">No Category</option>');
</script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        Dropzone.autoDiscover = false;
        var csrf_token = $('input[name="_token"]').val();

        var myDropzone = new Dropzone("#dropzone", { 
            url: 'brands/save',
            autoProcessQueue:true,
            method :"post",
            maxFilesize: 256,
            paramName: "file",
            params: {
                _token: "{{csrf_token()}}"
            },
            createImageThumbnails: true,
            thumbnailWidth: 100,
            thumbnailHeight: 100,
            maxFiles: 10,
            clickable: true,
            ignoreHiddenFiles: true,
            acceptedFiles: null,
            acceptedMimeTypes: null,
            addRemoveLinks: true,
            dictDefaultMessage: "Drop files here to upload",
            dictRemoveFile: "Remove file",
            init: function() { 
                this.on("success", function(file, responseText) {
                    console.log(file);
                    location.reload();
                });
            }
        }); 
    });
</script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(".gallery-env").on("click", ".image-thumb .image-options a.delete", function(ev) {
            ev.preventDefault();
            var id = $(ev.currentTarget)[0].id;
            $.ajax({
                headers: {
                    'X-CSRF-Token': '{{csrf_token() }}'
                },
                url: "{{ route('admin.brands.delete') }}",
                method: "GET",
                data: { id : id},
                success: function (data) {
                    $.gritter.add( 
                    {  
                        title: '<i class="entypo-check"></i> Picture successfully delete!',  
                        sticky: false,  
                        time: ""
                    });
                }
            }).error(function (error) {
                console.log("error");
            });

            var $image = $(this).closest('[data-tag]');
            var t = new TimelineLite({
                onComplete: function() {
                    $image.slideUp(function() {
                        $image.remove();
                    });
                }
            });

            $image.addClass('no-animation');
            t.append(TweenMax.to($image, .2, {
                css: {
                    scale: 0.95
                }
            }));
            t.append(TweenMax.to($image, .5, {
                css: {
                    autoAlpha: 0,
                    transform: "translateX(100px) scale(.95)"
                }
            }));
        }).on("click", ".image-thumb .image-options a.edit", function(ev) {
            ev.preventDefault();
            var id = $(ev.currentTarget)[0].id;
            console.log(id);

            $.ajax({
                headers: {
                    'X-CSRF-Token': '{{csrf_token() }}'
                },
                url: "{{ route('admin.brands.update') }}",
                method: "get",
                data: { id : id},
                success: function (data) {
                    $.gritter.add( 
                    {  
                        title: '<i class="entypo-check"></i> First Picture Set!',  
                        sticky: false,  
                        time: ""
                    });
                    location.reload();

                }
            }).error(function (error) {
                console.log("error");
            });
        });

        var all_items = $("div[data-tag]"),
        categories_links = $(".image-categories a");
        categories_links.click(function(ev) {
            ev.preventDefault();
            var $this = $(this),
            filter = $this.data('filter');
            categories_links.removeClass('active');
            $this.addClass('active');
            all_items.addClass('not-in-filter').filter('[data-tag="' + filter + '"]').removeClass('not-in-filter');
            if (filter == 'all' || filter == '*') {
                all_items.removeClass('not-in-filter');
                return;
            }
        });
    });
</script>
@endpush
