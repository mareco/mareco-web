<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Mareco </title>
	<link rel="stylesheet" type="text/css" href="{{asset('css/frontend/style.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/js/parsley/src/parsley.css')}}">
	<link href="{{ asset('js/bootstrap-eonasdan-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('js/bootstrap-datepicker.min.css') }}" />
	<script src="{{ asset('css/frontend/js/jquery-3.1.1.min.js') }}"></script>
	<script src="{{ asset('css/frontend/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('css/frontend/js/jquery.flexslider-min.js') }}"></script>
	<script src="{{ asset('assets/js/parsley/dist/parsley.js') }}"></script>
	<script src="{{ asset('css/frontend/js/owl.carousel.js') }}"></script>
	<script src="{{ asset('css/frontend/js/owl.carousel2.thumbs.min.js') }}"></script>
	<script src="{{ asset('css/frontend/js/bootstrap-magnify.min.js') }}"></script>
	<script src="{{ asset('js/bootstrap-daterangepicker/moment.js') }}"></script>
	<script src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
	<script src="{{ asset('js/bootstrap-eonasdan-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
	<link rel="stylesheet" href="{{ asset('assets/css/bootstrap.css') }}" id="style-resource-4">
	<link href="{{ asset('assets/global/plugins/gritter/css/jquery.gritter.css') }}" rel="stylesheet" />
	<link rel="stylesheet" href="{{ asset('assets/css/neon-core.css') }}" id="style-resource-5">
	<link rel="stylesheet" href="{{ asset('assets/css/neon-theme.css') }}" id="style-resource-6">
	<link rel="stylesheet" href="{{ asset('assets/css/neon-forms.css') }}" id="style-resource-7">
	<link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}" id="style-resource-8">
	<link rel="stylesheet" href="{{ asset('css/backend/style.css') }}" id="style-resource-9">
	<script src="{{ asset('assets/js/jquery-1.11.3.min.js') }}"></script>
	<link rel="stylesheet" href="{{ asset('assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css') }}" id="style-resource-1">
	<link rel="stylesheet" href="{{ asset('assets/css/font-icons/entypo/css/entypo.css') }}" id="style-resource-2">
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic" id="style-resource-3">
</head>
<body>
	<section class="top-header">
		<div class="container">
			<div class="top-header-wrap">
				<div class="row">
					<div class="col-md-12">
						<section class="nav-header">
							<nav class="navbar navbar-default">
								<div class="container">
									<div class="navbar-header">
										<div class="store-name">
											<a href="/"><img src="{{asset('assets/frontend-image/logo-red.png')}}" alt="" class="img-responsive logo" /></a>
										</div>
									</div>
								</div>
							</nav>
						</section>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container">
			<div class="col-md-12" style="font-size: 25px; text-align: center;">
				<div class="page-error-404">
					<div class="error-symbol"> <i class="entypo-attention"></i> </div>
					<div class="error-text">
						<h1>404</h1>
						<br><br>
						<p>We couldn't find it...</p>
						<p>
							The page you're looking for doesn't exist. <br />
						</p>
						<a href="{{ URL::previous() }}" class="btn btn-xs btn-primary"><span class="fa fa-reply"></span> Back</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	@include('layouts.frontend-footer')
</body>
</html>
