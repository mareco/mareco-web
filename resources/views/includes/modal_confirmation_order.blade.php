<div class="modal fade" id="modalConfirmOrder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title" id="myModalLabel"><b>Confirmation Payment</b></h4>
			</div>
			{!! Form::open(['url' => '/customer/payment/confirm/save', 'method' => 'POST','data-parsley-validate' => 'true','enctype'=>'multipart/form-data']) !!}
			<div class="modal-body confirm-payment">
				{!! csrf_field() !!}
	                <div class="row">
	                    <div class="col-sm-12 col-md-12">
	                        <div class="form-group">
	                            {{ Form::label('account_name', 'Card Holder')}}
	                            {{ Form::text('account_name', null, array('class' => 'form-control payment','placeholder' => 'BCA , BRI, BNI , MANDIRI , etc','data-parsley-required' => 'true'))}}
	                        </div>
	                    </div>
	                </div>
	                <div class="row">
	                    <div class="col-sm-12 col-md-12">
	                        <div class="form-group">
	                            {{ Form::label('account_number', 'Account Number')}}
	                            {{ Form::text('account_number', null, array('class' => 'form-control payment','placeholder' => '8070369029','data-parsley-required' => 'true'))}}
	                        </div>
	                    </div>
	                </div>
	                <div class="row">
	                    <div class="col-sm-12 col-md-12">
	                        <div class="form-group">
	                            {{ Form::label('bank_id', 'Bank')}}
	                            {{ Form::select('bank_id', $getBankList ,null, array('class' => 'form-control payment', 'placeholder' => 'Please select one...','data-parsley-required' => 'true'))}}
	                        </div>
	                    </div>
	                </div>
	                <div class="row">
	                    <div class="col-sm-12 col-md-12">
	                        <div class="form-group">
	                            {{ Form::label('amount', 'Amount')}}
	                            {{ Form::number('amount', null, array('class' => 'form-control payment','placeholder' => '280000','data-parsley-required' => 'true'))}}
	                        </div>
	                    </div>
	                </div>
	                <div class="row">
	                    <div class="col-sm-12 col-md-12">
	                        <div class="form-group">
	                             {{ Form::label('date_payment', 'Date Payment')}}
	                            {{ Form::text('date_payment', null, array('class' => 'dateonlypicker form-control payment', 'placeholder' => ' 2017-04-18','data-parsley-required' => 'true'))}}
	                        </div>
	                    </div>
	                </div>
	                <div class="row">
	                    <div class="col-sm-12 col-md-12">
	                        <div class="form-group payment">
	                            {{ Form::label('receipt_url', 'Proof Photo')}}
	                            {{ Form::file('receipt_url', null, array('class' => 'form-control payment', 'placeholder' => 'Photo','data-parsley-required' => 'true'))}}
	                        </div>
	                    </div>
	                </div>
				<p></p>
			</div>
			<input type="hidden" name="id" value="{{$order->id}}">
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="submit" class="btn btn-primary">Save</button>
			</div>
            {!! Form::close() !!}
		</div>
	</div>
</div>
<script type="text/javascript">
	
</script>