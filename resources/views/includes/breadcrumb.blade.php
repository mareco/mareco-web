<div class="container">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Home</a></li>
      @if (!isset($secondBreadcrumb))
      	<li class="breadcrumb-item active">{{$breadcrumb}}</li>
      @else
      	<li class="breadcrumb-item">{{$breadcrumb}}</li>
      	<li class="breadcrumb-item active">{{$secondBreadcrumb}}</li>
      @endif
    </ol>
</div>