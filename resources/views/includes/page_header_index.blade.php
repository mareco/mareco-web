<div class="breadcrumbs">
    <h2 class="page-header"><b>{{$title}}</b>  <small>manage your {{$small_title}}(s) here...</small></h2>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="/">Home</a>
        </li>
        <li>
            <a href="#"><span class="active">{{$title}}</span></a>
        </li>
    </ul>
</div>
<div id="session-message" style="display: none;">
    @include('flash::message')
</div>
@if (count($errors))
<span class="help-block alert alert-danger">
    <ul>
        @foreach($errors->all() as $error)
        <strong><li>{{ $error }}</li></strong>
        @endforeach
    </ul>
</span>
@endif