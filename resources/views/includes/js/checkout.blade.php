function initialize() {
  var latValue = Number($('#lat').val());
  var lngValue = Number($('#lng').val());

  var map = new google.maps.Map(document.getElementById('map-canvas'),{
    center:{
      lat: latValue,
      lng: lngValue
    },
    zoom:12
  });

  var marker = new google.maps.Marker({
    position: {
      lat: latValue,
      lng: lngValue
    },
    map: map,
    draggable: true
  });

  var searchBox = new google.maps.places.SearchBox(document.getElementById('searchmap'));
  google.maps.event.addListener(searchBox,'places_changed',function(){
    var places = searchBox.getPlaces();
    var bounds = new google.maps.LatLngBounds();
    var i, place;
    for(i=0; place=places[i];i++){
      bounds.extend(place.geometry.location);
        marker.setPosition(place.geometry.location); //set marker position new...
      }
      map.fitBounds(bounds);
      map.setZoom(15);
    });

  google.maps.event.addListener(marker,'position_changed',function(){
    var lat = marker.getPosition().lat();
    var lng = marker.getPosition().lng();

    $.ajax({
        headers: {
            'X-CSRF-Token': $('input[name="_token"]').val()
        },
        url: "{{ route('customer.address.distance', $locale) }}",
        method: "GET",
        data: { lat : lat,
                lng : lng},
        success: function (data) {
          var total = document.getElementById('total').value;
          var grandTotal = parseInt(total) + parseInt(data.rate);
          document.getElementById('grandTotal').value = grandTotal; 
          document.getElementById('shipmentFee').innerHTML = 'IDR ' + data.rate.toLocaleString('en-US');
          document.getElementById('orderTotal').innerHTML = 'IDR ' + grandTotal.toLocaleString('en-US');
          document.getElementById('distance_rate').value = data.rate;
          document.getElementById('customer_lat').value = lat;
          document.getElementById('customer_lng').value = lng;
          document.getElementById('shipmentHTML').innerHTML = 'Amount Distance';
        }
    });

    $('#lat').val(lat);
    $('#lng').val(lng);
  });
}