function initialize() {
  var latValue = Number($('#lat').val());
  var lngValue = Number($('#lng').val());
  
  var map = new google.maps.Map(document.getElementById('googleMapStorage'),{
    center:{
      lat: latValue,
      lng: lngValue
    },
    zoom:12
  });

  var marker = new google.maps.Marker({
    position: {
      lat: latValue,
      lng: lngValue
    },
    map: map,
    draggable: true
  });

  var searchBox = new google.maps.places.SearchBox(document.getElementById('storage_address'));
  google.maps.event.addListener(searchBox,'places_changed',function(){
    var places = searchBox.getPlaces();
    var bounds = new google.maps.LatLngBounds();
    var i, place;
    for(i=0; place=places[i];i++){
      bounds.extend(place.geometry.location);
        marker.setPosition(place.geometry.location); //set marker position new...
      }
      map.fitBounds(bounds);
      map.setZoom(15);
    });

  google.maps.event.addListener(marker,'position_changed',function(){
    var lat = marker.getPosition().lat();
    var lng = marker.getPosition().lng();

    $('#lat').val(lat);
    $('#lng').val(lng);
    $('#storage_lat').val(lat);
    $('#storage_lng').val(lng);
  });

  function geoCoder(latlng) {
    var geocoder = new google.maps.Geocoder;
    geocoder.geocode({'location': latlng}, function(results, status) {
      if (status === 'OK') {
        if (results[1]) {
          var address = results[0].formatted_address;
          setStoreLocation(address, latlng);
        }
      } else {
        window.alert('No results found');
      }
    }); 
  }
  function setStoreLocation(address, latlng) {
    $('#storage_address').val(address);
    $('#storage_lat').val(latValue.lat);
    $('#storage_lng').val(lngValue.lng);
  }

  function getStoreLocation() {
    return { lat: $('#storage_lat').val(), lng: $('#storage_lng').val(), address: $('#storage_address').val() };
  }
}